<?php

namespace Modules\AboutUs\Entities;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table 		= "about_us";
    protected $primaryKey 	= "about_us_serial_id";
    protected $guarded = array('about_us_serial_id');
    public $timestamps = false;
}
