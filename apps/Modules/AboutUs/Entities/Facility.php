<?php

namespace Modules\AboutUs\Entities;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table 		= "fasilitas";
    protected $primaryKey 	= "fasilitas_serial_id";
    protected $guarded = array('fasilitas_serial_id');
    public $timestamps = false;
}
