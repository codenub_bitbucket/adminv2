<?php

namespace Modules\AboutUs\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Redirect;
use Session;
use Intervention\Image\Facades\Image;

use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;

class AboutUsController extends Controller
{

	function __construct(Request $request)
	{
		session()->forget('redirect');
		session()->put('redirect', $request->path());
		
	}
		
	public function index()
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$auh = new aboutus_helper();
		$info = $auh->getAllInfo();
		$info = $auh->convertInfoArray($info);

		$view_data = array(
				'data' => $info,
		);

		$view = 'aboutus::index';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function detail($name)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$name = base64_decode($name, true);

		$auh = new aboutus_helper();
		$info = $auh->getInfoAbout($name);

		$view_data = array(
				'data' => $info,
				'function' => 'edit'
		);

		$view = 'aboutus::detail';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function update(Request $request,$name)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$sys = new sys();

		$name = base64_decode($name, true);
		$criteria = $request->all();

		if ($request->file('about_us_description')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['about_us_description']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['about_us_description'] = $upload['file']->image;
				}
		}

		$auh = new aboutus_helper();

		$update = $auh->update($name, $criteria);

		if ($update)
		{
				return redirect('smkbisa/landing')->with(['success' => 'Setting Berhasil Di Update']);
		}
		else
		{
				return redirect('smkbisa/landing')->with('failed', 'Setting Gagal Di Update');
		}
	}

	public function list_fasilitas(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$criteria = $request->all();

		$auh = new aboutus_helper();
		$sys = new sys();

		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		
		$count_data = $auh->getFacility('count');
		$list_data = $auh->getFacility('get', $criteria);

		$view_data = array(
			'table_module'   => 'fasilitas',
			'list_data'      => $list_data,
			'count_data'     => $count_data,
			'data_per_page'  => $data_per_page,
		);

		$view = 'aboutus::list_facility';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function detail_fasilitas($category)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$category = strtolower(str_replace('_', ' ', base64_decode($category, true)));

		$auh = new aboutus_helper();
		$data = $auh->getFacilityByCategory(str_replace(' ', '_', $category));

		$view_data = array(
				'data' => $data,
				'category' => $category
		);

		$view = 'aboutus::detail_facility';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function profile()
	{
		$auh = new aboutus_helper();
		$info = $auh->getAllInfo();
		$info = $auh->convertInfoArray($info);

		$view_data = array(
				'data' => $info,
		);
		
		$view = 'aboutus::profile';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function facility()
	{
		$auh = new aboutus_helper();
		$info = $auh->getAllInfo();
		$info = $auh->convertInfoArray($info);
		$facility = $auh->getFacility();
		$facility_cat = $auh->getFacility();

		$view_data = array(
				'data' => $info,
				'facility' => $facility,
				'facility_cat' => $facility_cat,
		);

		$view = 'aboutus::facility';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function getInfo($name)
	{
			$auh = new aboutus_helper();

			$result = $auh->getInfoAbout($name);

			return $result;
	}

	public function category_update(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$criteria = $request->all();

		$auh = new aboutus_helper();

		$update = $auh->updateCategory($criteria);

		if ($update)
		{
				return redirect('smkbisa/fasilitas/detail/'.base64_encode($criteria['fasilitas_kategori']))->with(['success' => 'Nama Kategori Berhasil Disimpan!']);
		}
		else
		{
				return redirect('smkbisa/fasilitas/detail/'.base64_encode($criteria['fasilitas_kategori']))->with('failed', 'Nama Kategori Gagal Disimpan!');
		}
	}

	public function delete(Request $request, $uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$criteria = $request->all();

		$auh = new aboutus_helper();

		$del = $auh->delete($uuid);

		if ($del)
		{
				return redirect()->back()->with(['success' => 'Fasilitas Berhasil Dihapus!']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Fasilitas Gagal Dihapus!');
		}
	}

	public function delete_category($name)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$name = base64_decode($name, true);

		$auh = new aboutus_helper();

		$del = $auh->delete_category($name);

		if ($del)
		{
				return redirect()->back()->with(['success' => 'Fasilitas Berhasil Dihapus!']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Fasilitas Gagal Dihapus!');
		}
	}

	public function add(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$sys = new sys();
		$criteria = $request->all();

		if ($request->file('fasilitas_photo')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['fasilitas_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['fasilitas_photo'] = $upload['file']->image;
				}
		}

		$auh = new aboutus_helper();

		$add = $auh->add($criteria);

		if ($add)
		{
				return redirect()->back()->with(['success' => 'Fasilitas Berhasil Ditambahkan!']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Fasilitas Gagal Ditambahkan!');
		}
	}

	public function add_category()
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$view_data = array(
		);

		$view = 'aboutus::add_category';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}
}
