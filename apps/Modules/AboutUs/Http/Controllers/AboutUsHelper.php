<?php
namespace Modules\AboutUs\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

class AboutUsHelper
{
  var $table_module = 'about_us';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_aboutus = 'Modules\AboutUs\Entities\AboutUs';
  var $model_facility = 'Modules\AboutUs\Entities\Facility';
  
  function getAllInfo()
  {
    $result = array();

    $query = $this->model_aboutus::where('deleted', 0)->get();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function getInfoAbout($name)
  {
    $result = array();

    $query = $this->model_aboutus::where('about_us_name', $name)->where('deleted', 0)->first();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function convertInfoArray($data)
  {
    $result = $data;
    if (@count($data) > 0) 
    {
      $newArray = array();
      foreach ($data as $key => $value) 
      {
        foreach ($value as $ko => $ob) 
        {
          if ($ko == 'about_us_name') 
          {
            $newArray[$ob] = array(
              'name' => $value['about_us_name'],
              'label' => $value['about_us_label'],
              'description' => $value['about_us_description'],
              'created_at' => $value['created_at'],
              'updated_at' => $value['updated_at'],
            );
          }
        }
      }
      $result = $newArray;
    }

    return $result;
  }

  function getFacility($action='get', $criteria= array())
  {
    $result = array();

    $query = $this->model_facility::where('deleted', 0);

    
    if ($action == 'count') 
    {
      $query = $query->select("*")->groupBy('fasilitas_kategori');
      $result = $query->get();
      $result = @count($result);
    }
    else
    {
      $query = $query->select('*','fasilitas_kategori', DB::raw('count(*) as total'))->groupBy('fasilitas_kategori');
      if(isset($criteria['show']))
      {
        $result = $query->paginate($criteria['show']);
        $result->appends($criteria)->unique();
      }
      else
      {
        $result = $query->get();
      }
    }

    return $result;
  }

  function getFacilityByCategory($category)
  {
    $result = array();

    $query = $this->model_facility::where('fasilitas_kategori', $category)->where('deleted', 0)->get();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function update($name, $criteria)
  {
    unset($criteria['_token']);
    unset($criteria['files']);
    $result = false;

    $criteria['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_aboutus::where('about_us_name', $name)
                            ->update($criteria);

    if ($query)
    {
      $result = true;
    }

    return $result;
  }

  function updateCategory($criteria)
  {
    unset($criteria['_token']);
    $result = false;

    $criteria['fasilitas_kategori'] = strtolower(str_replace(' ', '_', $criteria['fasilitas_kategori']));

    if (@count($criteria['list_facility']) > 0)  
    {
      $update['updated_at'] = date('Y-m-d H:i:s');
      $update['fasilitas_kategori'] = $criteria['fasilitas_kategori'];
      $query = $this->model_facility::whereIn('fasilitas_uuid', $criteria['list_facility'])
                                    ->update($update);
      if ($query)
      {
        $result = true;
      }
    }

    return $result;
  }
  
  function delete($uuid)
  {
    $result = false;

    $del['updated_at'] = date('Y-m-d H:i:s');
    $del['deleted'] = 1;
    $query = $this->model_facility::where('fasilitas_uuid', $uuid)
                            ->update($del);

    if ($query)
    {
      $result = true;
    }

    return $result;
  }

  function delete_category($name)
  {
    $result = false;

    $del['updated_at'] = date('Y-m-d H:i:s');
    $del['deleted'] = 1;
    $query = $this->model_facility::where('fasilitas_kategori', $name)
                            ->update($del);

    if ($query)
    {
      $result = true;
    }

    return $result;
  }

  function add($criteria)
  {
    unset($criteria['_token']);
    $criteria['fasilitas_kategori'] = strtolower(str_replace(' ', '_', $criteria['fasilitas_kategori']));
    $criteria['fasilitas_uuid'] = Uuid::uuid4()->toString();

    $query = $this->model_facility::insertGetId($criteria);

    return $query;
  }
}