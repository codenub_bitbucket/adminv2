@extends('app')
@section('title', 'Landing')
@section('content')

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama Metro</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item ">Fasilitas</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3 align-items-center d-flex">
                <div class="col-6">
                  <h4>Tambah Fasilitas</h4>
                  {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                </div>
                <div class="col-6 text-right">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/fasilitas')}}">
                    Batal
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Fasilitas Kategori</label>
                    <div class="input-group mb-3">
                      <input class="form-control" name="fasilitas_kategori" type="text" value="" placeholder="Type here..." id="fasilitas_kategori" style="text-transform: uppercase">
                      <a class="btn btn-info" type="submit" disabled="true" href="#" id="btn_next">Lanjutkan</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

<script>
  $('#fasilitas_kategori').keyup(function(){
    let cat = $('#fasilitas_kategori').val().replace(' ', '_').toLowerCase();
    console.log(cat);
    let href = `{{ URL('smkbisa/fasilitas/detail/${btoa(cat)}') }}`
    console.log(href);
    $('#btn_next').attr('href', href)
  })
</script>
@endsection