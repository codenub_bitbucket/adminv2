@extends('app')
@section('title', 'Landing')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Detail Landing Page Setting</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item ">Landing</li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <form action="{{ URL('smkbisa/landing/update/'.base64_encode($data['about_us_name'])) }}" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Nama Setting</label>
                      <input class="form-control" name="about_us_label" type="text" value="{{ $data['about_us_label'] }}" placeholder="Type here...">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Detail Setting</label>
                      @include('input.'.$data['about_us_type'], array(
                          'table_module'            => 'about_us',
                          'fields_name'             => 'about_us_description', 
                          'fields_label'             => 'Detail Setting', 
                          'dropdown'                => isset($data['dropdown']) ? $data['dropdown'] : array(),
                          'required'                => 0,
                      ))
                    </div>
                  </div>
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  <a class="btn btn-light px-3 mr-2" href="{{ url('smkbisa/landing') }}">
                    Batalkan
                  </a>
                  <button class="btn btn-primary px-4">
                    Simpan Perubahan
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
@endsection