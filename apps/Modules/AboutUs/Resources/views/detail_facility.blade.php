@extends('app')
@section('title', 'Landing')
@section('content')

<style>
  .form-control:disabled {
    background-color: #eee
  }
  .preview_tag{
    padding: 1px 10px;
    font-size: 12px;
    border-radius: 3px;
    position: absolute;
    top: 10px;
    right: 10px;
    background: red;
  }
</style>
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama Metro</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item ">Fasilitas</li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3 align-items-center d-flex">
                <div class="col-6">
                  <h4>Detail Fasilitas</h4>
                  {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                </div>
                <div class="col-6 text-right">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/fasilitas')}}">
                    Kembali
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  @if (\Session::has('success'))
                    <div class="alert alert-success" id="success-alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>Berhasil!.</strong>
                            {!! \Session::get('success') !!}
                    </div>
                    <script>
                        $(document).ready(function() {
                            setTimeout(() => {
                                $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                                    $("#success-alert").slideUp(500);
                                });
                            }, 30000);
                        });
                    </script>
                  @endif
                  @if (\Session::has('failed'))
                    <div class="alert alert-danger" id="failed-alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>Gagal!.</strong>
                            {!! \Session::get('failed') !!}
                    </div>
                    <script>
                        $(document).ready(function() {
                            setTimeout(() => {
                                $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                                    $("#failed-alert").slideUp(500);
                                });
                            }, 30000);
                        });
                    </script>
                  @endif
                  <div class="form-group">
                    <label>Fasilitas Kategori</label>
                    <form action="{{ URL('smkbisa/fasilitas/update/category') }}" method="post">
                      @csrf
                      @if (@count($data) > 0)
                        @foreach ($data as $item)
                          <input type="hidden" value="{{$item['fasilitas_uuid']}}" name="list_facility[]">
                        @endforeach
                      @endif
                      <div class="input-group mb-3">
                        <input class="form-control" name="fasilitas_kategori" type="text" value="{{ $category }}" placeholder="Type here..." id="facility_cat_input" disabled="true" style="text-transform: uppercase">
                        <div class="input-group-append">
                          @if (@count($data) > 0)
                            <button class="btn btn-success" type="button" id="facility_cat_button_edit">Edit</button>
                          @else
                            <button class="btn btn-success" type="button" id="facility_cat_button_edit" disabled="true">Edit</button>
                          @endif
                          <button class="btn btn-info" type="submit" id="facility_cat_button_save" disabled="true"><span id="saveText">Simpan</span>
                            {{-- <div id="savingText" style="display:none">
                              Menyimpan...
                              <div class="spinner-border text-secondary" role="status" style="height: 15px;width: 15px;top: -2px;position: relative;">
                              </div>
                            </div> --}}
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>List Fasilitas</label>
                    <div class="row">
                      @if (@count($data) > 0)
                        @foreach ($data as $item)
                          <div class="col-md-3">
                            <div class="card">
                              <img class="card-img-top" src="{{ url('apps/public/'.$item['fasilitas_photo']) }}" style="height: 200px;">
                              <div class="card-body">
                                <h5 class="card-title">{{ $item['fasilitas_name'] }}</h5>
                                <a href="{{ URL('smkbisa/fasilitas/delete/'.$item['fasilitas_uuid']) }}" class="btn btn-primary btn-block">Hapus</a>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                      <div class="col-md-3">
                        <div class="card" id="add_card" style="position: relative;top: 50%;transform: translateY(-50%);border: 1px solid green;box-shadow: 3px 5px;">
                          <img class="card-img-top add_preview" style="display:none;">
                          <span class="post-tag bg-grad text-white mb-3 clearfix preview_tag" style="display:none">Preview</span>
                          <div class="card-body">
                            <button id="add_btn" class="btn btn-primary btn-block">Tambah</button>
                            <form action="{{ URL('smkbisa/fasilitas/add') }}" method="POST" enctype="multipart/form-data" id="upload_form" style="display:none">
                              @csrf
                              <input type="file" id="add_fields" name="fasilitas_photo" style="display:none">
                              <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="fasilitas_name" class="form-control">
                              </div>
                              <input type="hidden" name="fasilitas_kategori" value="{{ $category }}">
                              <button class="btn btn-info btn-block">Upload</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

<script>
  $('#facility_cat_button_edit').click(function() {
    $('#facility_cat_input').attr('disabled', false);
    $('#facility_cat_button_save').attr('disabled', false);
    $('#facility_cat_button_edit').attr('disabled', true);
  })

  $('#add_btn').click(function() {
    $('#add_fields').click();
  })

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#add_card').removeAttr('style');
              $('.add_preview').show();
              $('#add_btn').hide();
              $('#upload_form').show();
              $('.preview_tag').show();
              $('.add_preview').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $("#add_fields").change(function(){
      readURL(this);
  });

  // $('#facility_cat_button_save').click(function() {
  //   $('#saveText').hide();
  //   $('#savingText').show();

  //   $('#facility_cat_input').attr('disabled', true);
  //   $('#facility_cat_button_save').attr('disabled', true);
  //   $('#facility_cat_button_edit').attr('disabled', false);
    


  // })
</script>
@endsection