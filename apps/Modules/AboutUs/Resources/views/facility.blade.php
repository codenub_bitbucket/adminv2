@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Tentang</li>
        <li class="breadcrumb-item">Fasilitas</li>
      </ol>
    </nav>
  </div>

   <!-- =======================
	Portfolio -->
	<section class="portfolio portfolio-style-2 pb-0">
		<div class="container">
      <div class="row">
        <div class="col-12 col-lg-12 mx-auto ">
          <div class="title text-center">
            <h2>Fasilitas Kami</h2>
            <p>{{ (isset($data['facility_desc']) ? $data['facility_desc']['label'] : '') }}</p>
          </div>
        </div>
      </div>
			<div class="row">
				<div class="col-md-12 p-0">
					<div class="nav justify-content-center">
						<ul class="nav-tabs nav-tabs-style-3 text-center px-2 p-md-0 m-0 mb-4">
							<li class="nav-filter active" data-filter="*">All Facility</li>
							@if (@count($facility_cat) > 0)
								@foreach ($facility_cat as $item)
									<li class="nav-filter" data-filter=".{{strtolower($item['fasilitas_kategori'])}}">{{strtoupper(str_replace('_', ' ', $item['fasilitas_kategori']))}}</li>
								@endforeach
							@endif
						</ul>
					</div>
					<div class="portfolio-wrap grid items-3 items-padding">
						<!-- portfolio-card -->
						@if (@count($facility) > 0)
							@foreach($facility as $item)
								<div class="portfolio-card isotope-item {{strtolower($item['fasilitas_kategori'])}}">
									<div class="portfolio-card-body">
										<div class="portfolio-card-header">
											<img src="{{ URL('apps/public/'.$item['fasilitas_photo']) }}" alt="">
										</div>
										<div class="portfolio-card-footer">
											<a class="full-screen" href="{{ URL('apps/public/'.$item['fasilitas_photo']) }}" data-fancybox="portfolio" data-caption="{{strtoupper($item['fasilitas_kategori'])}}"><i class="ti-fullscreen"></i></a>
											<h6 class="info-title"><a href="#" title="">{{strtoupper($item['fasilitas_name'])}}</a></h6>
											<p>{{strtoupper($item['fasilitas_name'])}}</p>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						
					</div>
					<!-- portfolio wrap -->
				</div>
			</div>
		</div>
	</section>
	<!-- =======================
	Portfolio -->>
</section>

<br><br><br>


@endsection
