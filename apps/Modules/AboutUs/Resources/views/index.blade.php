@extends('app')
@section('title', 'Landing')
@section('content')
<div class="main-content">

    <div class="page-content">
      <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
          <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
              <h4 class="mb-0">SMK Kartikatama</h4>
              @if (\Session::has('success'))
                <div class="alert alert-success" id="success-alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>Berhasil!.</strong>
                        {!! \Session::get('success') !!}
                </div>
                <script>
                    $(document).ready(function() {
                        setTimeout(() => {
                            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                                $("#success-alert").slideUp(500);
                            });
                        }, 30000);
                    });
                </script>
              @endif
              @if (\Session::has('failed'))
                <div class="alert alert-danger" id="failed-alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>Gagal!.</strong>
                        {!! \Session::get('failed') !!}
                </div>
                <script>
                    $(document).ready(function() {
                        setTimeout(() => {
                            $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                                $("#failed-alert").slideUp(500);
                            });
                        }, 30000);
                    });
                </script>
              @endif
              <div class="page-title-right">
                <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                  <li class="breadcrumb-item active">Landing</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- end page title -->

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="row mb-3 align-items-center d-flex">
                  <div class="col-6">
                    <h4> Setting Landing Page SMK Kartikatama Metro</h4>
                    {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                  </div>
                  {{-- <div class="col-6 text-right">
                    <button class="btn btn-primary px-4">
                      + Admin
                    </button>
                  </div> --}}
                </div>
                <div class="table-responsive">
                  <table class="table mb-0">
                    <thead>
                      <tr>
                        <th style="white-space:nowrap">Setting Name</th>
                        <th style="white-space:nowrap">Created At</th>
                        <th style="white-space:nowrap">Updated At</th>
                        <th style="white-space:nowrap">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (@count($data) > 0)
                            @foreach ($data as $key => $item)
                                <tr>
                                    <td style="white-space:nowrap">{{ $item['label'] }}</td>
                                    <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
                                    <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
                                    <td style="white-space:nowrap">
                                        {{-- <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div> --}}
                                        <a class="btn btn-success" href="{{ url('smkbisa/landing/detail/' . base64_encode($item['name']))}}">Lihat</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
  </div>
@endsection
