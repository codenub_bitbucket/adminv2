@extends('app')
@section('title', 'Landing')
@section('content')
<div class="main-content">

    <div class="page-content">
      <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
          <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
              <h4 class="mb-0">SMK Kartikatama | Fasilitas</h4>
              @if (\Session::has('success'))
                <div class="alert alert-success" id="success-alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>Berhasil!.</strong>
                        {!! \Session::get('success') !!}
                </div>
                <script>
                    $(document).ready(function() {
                        setTimeout(() => {
                            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                                $("#success-alert").slideUp(500);
                            });
                        }, 30000);
                    });
                </script>
                @endif
              <div class="page-title-right">
                <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                  <li class="breadcrumb-item active">Fasilitas</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- end page title -->

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="d-lg-flex align-items-center justify-content-between mb-4">
                  <div class="align-items-center d-lg-flex d-none w-50 unshow-select">
                    @include('component.filter_show')
                  </div>
                  <div class="d-flex align-items-center unshow-select">
                    <a class="btn btn-primary px-4" href="{{ url('smkbisa/fasilitas/category/add') }}">
                      + Fasilitas
                    </a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table mb-0">
                    <thead>
                      <tr>
                        <th style="white-space:nowrap">Action</th>
                        <th style="white-space:nowrap">Fasilitas Kategori</th>
                        <th style="white-space:nowrap">Total Fasilitas</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (@count($list_data) > 0)
                            @foreach ($list_data as $key => $item)
                                <tr>
                                  <td style="white-space:nowrap">
                                      <a class="btn btn-success" href="{{ url('smkbisa/fasilitas/detail/' . base64_encode($item['fasilitas_kategori']))}}">Lihat</a>
                                      <a class="btn btn-danger" href="{{ url('smkbisa/fasilitas/category/delete/' . base64_encode($item['fasilitas_kategori']))}}">Hapus</a>
                                  </td>
                                  <td style="white-space:nowrap">{{ strtoupper(str_replace('_', ' ', $item['fasilitas_kategori'])) }}</td>
                                  <td style="white-space:nowrap">{{ $item['total'] }}</td>
                                </tr>
                            @endforeach
                        @else
                          <tr>
                            <td colspan="4">
                              <h5 class="text-center">Belum Ada Fasilitas</h5>
                            </td>
                          </tr>
                        @endif
                    </tbody>
                  </table>
                  @include('component.pagination')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
  </div>
@endsection
