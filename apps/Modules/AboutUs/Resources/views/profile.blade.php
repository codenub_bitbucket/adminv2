@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Tentang</li>
        <li class="breadcrumb-item">Profil</li>
      </ol>
    </nav>
  </div>

  <!-- =======================
	About us -->
	<section class="p-0 ">
		<div class="container h-100">
			<div class="row mt-7">
				<!-- process item-->
				<div class="col-md-12 mb-12">
					<div class="process">
						<h3 class="process-title">{{ (isset($data['about_school']) ? $data['about_school']['label'] : 'Tentang Sekolah') }}</h3>
						<p class="process-content" >{!! (isset($data['about_school']) ? $data['about_school']['description'] : 'Tidak Ada Deskripsi') !!}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- =======================
	About us -->
</section>

<br><br><br>


@endsection
