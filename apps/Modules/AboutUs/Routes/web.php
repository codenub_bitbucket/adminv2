<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('about')->group(function() {
    Route::get('/profile', 'AboutUsController@profile');
    Route::get('/facility', 'AboutUsController@facility');
});

Route::prefix('smkbisa')->group(function() {
    Route::get('/landing', 'AboutUsController@index');
    Route::get('/landing/detail/{name}', 'AboutUsController@detail');
    Route::post('/landing/update/{name}', 'AboutUsController@update');
    Route::get('/fasilitas', 'AboutUsController@list_fasilitas');
    Route::get('/fasilitas/detail/{category}', 'AboutUsController@detail_fasilitas');
    Route::post('/fasilitas/update/category', 'AboutUsController@category_update');
    Route::get('/fasilitas/delete/{uuid}', 'AboutUsController@delete');
    Route::get('/fasilitas/category/delete/{name}', 'AboutUsController@delete_category');
    Route::post('/fasilitas/add', 'AboutUsController@add');
    Route::get('/fasilitas/category/add', 'AboutUsController@add_category');
});

