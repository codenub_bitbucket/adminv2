<?php

namespace Modules\Academic\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;
use Modules\Extracurricular\Http\Controllers\ExtracurricularHelper as extracurricular_helper;

class AcademicController extends Controller
{
    public function major()
    {
        $auh = new aboutus_helper();
        $jh = new jurusan_helper();
        
        $info = $auh->getAllInfo();
        $info = $auh->convertInfoArray($info);

        $con = array(
            'show' => 15
        );
        
        $jurusan = $jh->list_data('get', $con);

		$view_data = array(
				'data' => $info,
				'jurusan' => $jurusan,
		);
		
        $view = 'academic::major';
        
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
    }

    public function extracurricular()
    {
        $auh = new aboutus_helper();
        $eh = new extracurricular_helper();
        
        $info = $auh->getAllInfo();
        $info = $auh->convertInfoArray($info);

        $con = array(
            'show' => 15
        );
        
        $extracurricular = $eh->list_data('get', $con);

		$view_data = array(
				'data' => $info,
				'extracurricular' => $extracurricular,
		);
		
        $view = 'academic::extracurricular';
        
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
    }
}
