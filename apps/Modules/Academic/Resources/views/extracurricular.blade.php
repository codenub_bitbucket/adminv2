@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Akademis</li>
        <li class="breadcrumb-item">Ekstrakurikuler</li>
      </ol>
    </nav>
  </div>

	<section class="portfolio portfolio-style-2 pb-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 mx-auto ">
          <div class="title text-center">
            <h2>Ekstrakurikuler SMK Kartikatama Metro</h2>
            {{-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p> --}}
          </div>
        </div>
      </div>

      <div class="row mb-4">
				@if (@count($extracurricular) > 0)
					@foreach ($extracurricular as $row)
						<div class="col-sm-6 col-md-4 mb-3">
							<div class="feature-box h-100 icon-grad border">
								<img src="{{ URL('apps/public/'.$row['extracurricular_photo']) }}" style="height: 300px; width: 100%">
								<div class="px-3">
									<h3 class="feature-box-title mt-3">{{ $row['extracurricular_name'] }}</h3>
									<p class="feature-box-desc">
										{{ $row['extracurricular_desc'] }}
									</p>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<h3>Belum Ada Ekstrakurikuler</h3>
				@endif
      </div>

    </div>
		</div>
	</section>
</section>

<br><br><br>


@endsection
