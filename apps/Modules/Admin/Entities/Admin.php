<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table 		= "admin";
    protected $primaryKey 	= "admin_serial_id";
    protected $guarded = array('admin_serial_id');
    public $timestamps = false;
}
