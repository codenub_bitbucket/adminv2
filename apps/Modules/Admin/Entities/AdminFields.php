<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminFields extends Model
{
    protected $table 		= "admin_fields";
    protected $primaryKey 	= "admin_fields_serial_id";
    protected $guarded = array('admin_fields_serial_id');
    public $timestamps = false;
}
