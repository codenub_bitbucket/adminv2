<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Modules\Admin\Http\Controllers\AdminHelper as admin_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportCallBack;
use Illuminate\Support\Collection;


class AdminController extends Controller
{
    var $module       = 'Admin';
    var $table_module = 'admin';

    function __construct(Request $request)
    {
			$this->middleware('auth');
			session()->forget('redirect');
			session()->put('redirect', $request->path());
    }

    public function index(Request $request)
    {
			$criteria = $request->all();
			$sys = new sys();
			$helper = new admin_helper();
			
			$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
			$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
			$list = $helper->list_data('get', $criteria);
			$count = $helper->list_data('count');

			$fields = $sys->getFields($this->table_module);

			$view = 'admin::index';

			$view_data = array(
				'table_module'   => $this->table_module,
				'list_data'      => $list,
				'count_data'     => $count,
				'data_per_page'  => $data_per_page,
				'fields'         => $fields
			);
        
			if(view()->exists($view))
			{
				return view($view, $view_data)->render();
			}
    }

    public function create()
    {
			$sys = new sys();

			$fields = $sys->getFields($this->table_module);

			// foreach ($fields as $key => $value) 
			// {
			// 	if ($value['admin_fields_input_type'] == 'singleoption' && $value['admin_fields_options'] !== null) 
			// 	{
			// 		if ($value['admin_fields_options'] == 'kelas_id') 
			// 		{
			// 			$list_kelas = $k_helper->list_data('get_dropdown');
			// 			$fields[$key]['dropdown'] = $list_kelas;
			// 		}
			// 		else
			// 		{
			// 			$fields[$key]['dropdown'] = $sys->getDropdown($value['admin_fields_options']);
			// 		}
			// 	}
			// }

			$view = 'admin::create';

			$view_data = array(
				'table_module'  => $this->table_module,
				'fields'        => $fields,
				'function' => 'save'
			);
			
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}
		
		public function save(Request $request)
    {
			$helper = new admin_helper();
			$sys = new sys();

			$users = Auth::user();

			$data = $request->all();
			
			if ($request->hasFile('admin_photo')) 
			{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $data['admin_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$data['admin_photo'] = $upload['file']->image;
				}
			}

			$save = $helper->save($data, $users);

			if ($save)
			{
				return redirect('smkbisa/staff')->with(['success' => 'Admin Berhasil Disimpan']);
			}
			else
			{
				return redirect('smkbisa/staff')->with('failed', 'Admin Gagal Disimpan');
			}
		}

		public function edit(Request $request, $uuid)
		{
			$sys = new sys();
			$k_helper = new kelas_helper();
			$helper = new admin_helper();
			
			$admin = $helper->get_detail($uuid);
			$fields = $sys->getFields($this->table_module);
			
			foreach ($fields as $key => $value) 
			{
				if ($value['admin_fields_input_type'] == 'singleoption' && $value['admin_fields_options'] !== null) 
				{
					if ($value['admin_fields_options'] == 'kelas_id') 
					{
						$list_kelas = $k_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_kelas;
					}
					else
					{
						$fields[$key]['dropdown'] = $sys->getDropdown($value['admin_fields_options']);
					}
				}
			}
			
			$view = 'admin::create';
			
			$view_data = array(
				'table_module'   => $this->table_module,
				'fields'         => $fields,
				'data' 					 => $admin,
				'function'   				 => 'edit'	
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function update(Request $request, $uuid)
		{
			$users = Auth::user();

			$sys = new sys();
			$criteria = $request->all();

			if ($request->file('admin_photo')) 
			{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['admin_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['admin_photo'] = $upload['file']->image;
				}
			}

			$helper = new admin_helper();

			$update = $helper->update($criteria, $users, $uuid);

			if ($update)
			{
				return redirect('smkbisa/staff')->with(['success' => 'Admin Berhasil Diperbaharui']);
			}
			else
			{
				return redirect('smkbisa/staff')->with('failed', 'Admin Gagal Diperbaharui');
			}
		}

		public function delete($uuid)
		{
			$sys = new sys();

			$helper = new admin_helper();

			$delete = $helper->delete($uuid);

			if ($delete)
			{
					return redirect('smkbisa/staff')->with(['success' => 'Admin Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/staff')->with('failed', 'Admin Gagal Dihapus');
			}
		}

		public function mass_delete(Request $request)
		{
			$data = $request->all();
			$helper = new admin_helper();

			$data = json_decode($data['data_selected'], true);
			$delete = $helper->mass_delete($data);

			if ($delete)
			{
					return redirect('smkbisa/staff')->with(['success' => 'Admin Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/staff')->with('failed', 'Admin Gagal Dihapus');
			}
		}

		public function import()
    {
			$sys = new sys();

			$view = 'admin::import';

			$view_data = array(
					
			);
        
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}
		

		public function importPreview(Request $request)
		{
			session()->forget('redirect');

			$request->validate([
					'file' => 'required|mimes:csv,xls,xlsx'
			]);
			
			$array = Excel::toArray(new ImportCallBack, $request->file('file'));

			$fieldsHeader = $array[0][0];
			$dataReal = array();
			unset($array[0][0]);

			foreach ($fieldsHeader as $key => $value) 
			{
				if($value == 'Nama')
				{
					$fieldsHeader[$key] = 'admin_name';
				}
				else if($value == 'Password')
				{
					$fieldsHeader[$key] = 'password';
				}
			}

			foreach ($array[0] as $key => $value) 
			{
				foreach ($value as $k => $v) 
				{
					$newValue[$fieldsHeader[$k]] = $v;
					if(!empty($newValue['admin_name']))
					{
						$dataReal[$key] = $newValue;
					}
				}
			}

			$dataReal = array_values($dataReal);
			
			$view_data = array(
				'data' => $dataReal,
			);

			$view = 'admin::import-preview';
        
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function importSave(Request $request)
		{
			$sys = new sys();
			$helper = new admin_helper();
			$users = Auth::user();

			$data = $request->all();

			$data_admin = json_decode($data['data_admin'], true);
			unset($data['_token']);
			unset($data['data_admin']);


			$success = 0;
			$failed = 0;

			foreach ($data_admin as $key => $value)
			{
				foreach ($data as $k => $v)
				{
					$value[$k] = $v;
				}

				$save = $helper->save($value, $users);
				if ($save) 
				{
					$success++;
				}
				else
				{
					$failed++;
				}
			}

			if ($success >= 0 || $failed >= 0)
			{
					return redirect('smkbisa/staff')->with(['success' => 'Admin Berhasil Diimport : '.$success.' Berhasil, '.$failed.' Gagal']);
			}
			else
			{
					return redirect('smkbisa/staff')->with('failed', 'Admin Gagal Diimport');
			}
		}
}
