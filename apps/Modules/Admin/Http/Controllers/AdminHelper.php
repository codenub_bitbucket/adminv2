<?php
namespace Modules\Admin\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class AdminHelper
{
  var $table_module = 'admin';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_admin = 'Modules\Admin\Entities\Admin';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_admin::select("admin.*", "users.email")->where('admin.deleted', 0)
                              ->leftjoin('sys_rel', function($join)
                                {
                                  $join->on('sys_rel.rel_to_id', '=', 'admin.admin_serial_id')
                                    ->where('sys_rel.rel_from_module', '=', 'users')
                                    ->where('sys_rel.rel_to_module', '=', 'admin');
                                })
                                ->leftJoin('users', 'users.id', '=', 'sys_rel.rel_from_id');
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('admin.admin_name',"LIKE", "%".$criteria['search']."%");
                                }

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("admin.admin_name as label", "admin.admin_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;

    $sys = new sys();

    $email = $sys->createEmailAdmin($data);
    $dataUser = array(
      'name'      => $data['admin_name'],
      'email'     => $email,
      'password'  => bcrypt(isset($data['password']) ? $data['password'] : "Admin".rand(1,10)."!"),
      'photo'     => isset($data['admin_photo']) ? $data['admin_photo'] : null,
      'role'      => 1, //1 => admin, 2 => guru, 3 => siswa, => 4 => wali
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
    );

    $pass = $data['password'];
    
    unset($data['_token']);
    unset($data['admin_photo']);
    unset($data['password']);

    $data['admin_uuid'] = Uuid::uuid4()->toString();
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $users->id;
    $data['admin_password'] = $pass;

    $query = $this->model_admin::insertGetId($data);

    if ($query)
    {
      $query2 = DB::table('users')->insertGetId($dataUser);
      if($query2)
      {
        $rel = array(
          'rel_from_module' => 'users',
          'rel_from_id' => $query2,
          'rel_to_module' => 'admin',
          'rel_to_id' => $query,
        );
        $query3 = DB::table('sys_rel')->insert($rel);
      }
      $result = true;
    }


    return $result;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['admin_dob'] = isset($data['admin_dob']) ? date("Y-m-d", strtotime($data['admin_dob'])) : '1111-11-11';
    $query = $this->model_admin::where('admin_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $sys = new sys();

    $result = false;
    
    $update['deleted'] = 1;
    $query = $this->model_admin::where('admin_uuid', $uuid)->first();
    $query->update($update);

    $serial_id = $query->admin_serial_id;
    if ($query)
    {
      $getRel = DB::table('sys_rel')->where('rel_to_module', 'admin')->where('rel_from_module', 'users')->where('rel_to_id', $serial_id)->first();
      
      if(@count($getRel) > 0)
      {
        $users = DB::table('users')->where('id', $getRel->rel_to_id)->first();
        if(@count($users) > 0)
        {
          $email = $users->email;
          $uname = explode('@', $email);
          $sys->deleteEmailAdmin($uname[0]);
        }
        DB::table('users')->where('id', $getRel->rel_to_id)->delete();
        $delRel = DB::table('sys_rel')->where('rel_to_module', 'admin')->where('rel_from_module', 'users')->where('rel_to_id', $serial_id)->delete();
      }
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->model_admin::whereIn('admin_uuid', $data)->update($update);
    $get = $this->model_admin::whereIn('admin_uuid', $data)->get();

    if ($query)
    {
      if(@count($get) > 0)
      {
        $serial_ids = array();
        foreach($get as $key => $value)
        {
          $serial_ids[] = $value->admin_serial_id;
        }

        $getRel = DB::table('sys_rel')->where('rel_to_module', 'admin')->where('rel_from_module', 'users')->whereIn('rel_to_id', $serial_ids)->get();
        
        if(@count($getRel) > 0)
        {
          $rel_ids = array();
          foreach($getRel as $key => $value)
          {
            $rel_ids[] = $value->rel_to_id;
          }
          $users = DB::table('users')->whereIn('id', $rel_ids)->get();
          if(@count($users) > 0)
          {
            foreach ($users as $key => $value) 
            {
              $email = $value->email;
              $uname = explode('@', $email);
              $sys->deleteEmailAdmin($uname[0]);
            }
          }
          DB::table('users')->whereIn('id', $rel_ids)->delete();
          $delRel = DB::table('sys_rel')->where('rel_to_module', 'admin')->where('rel_from_module', 'users')->whereIn('rel_to_id', $serial_ids)->delete();
        }
      }
      $result = true;
    }
    return $query;
  }
  
}