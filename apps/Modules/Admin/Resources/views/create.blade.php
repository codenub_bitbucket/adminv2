@extends('app')
@section('title', 'Tambah Admin')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Tambah Admin</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Admin</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <form action="{{ ($function == 'edit') ? URL('smkbisa/staff/update/'.$data[$table_module.'_uuid']) : URL('smkbisa/staff/save') }}" method="post" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
              @csrf
                <div class="row">
                  <div class="col-md-12">
                    @foreach ($fields as $key => $value)
                    <div class="form-group">
                      <label>{{ $value[$table_module.'_fields_label'] }}</label>
                      @include('input.'.$value[$table_module.'_fields_input_type'], array(
                          'table_module'            => $table_module,
                          'fields_name'             => $value[$table_module.'_fields_name'], 
                          'fields_label'             => $value[$table_module.'_fields_label'], 
                          'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                          'required'                => $value[$table_module.'_fields_validation'],
                      ))
                    </div>
                    @endforeach   
                    <div class="form-group">
                      <label>Password (for login in web admin)</label>
                      <input class="form-control" type="text" name="password" onkeyup="checkPassStrength(event)">
                      <i class="pass_strength"></i>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  <a class="btn btn-light px-3 mr-2" href="{{ url('smkbisa/staff') }}">
                    Cancel
                  </a>
                  <button class="btn btn-primary px-4 btn_submit" type="submit" disabled="true">
                    {{ ($function == 'save') ? 'Save' : 'Update'}}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
<script>
  let strength = 0;
  function scorePassword(pass) {
      var score = 0;
      if (!pass)
          return score;

      // award every unique letter until 5 repetitions
      var letters = new Object();
      for (var i=0; i<pass.length; i++) {
          letters[pass[i]] = (letters[pass[i]] || 0) + 1;
          score += 5.0 / letters[pass[i]];
      }

      // bonus points for mixing it up
      var variations = {
          digits: /\d/.test(pass),
          lower: /[a-z]/.test(pass),
          upper: /[A-Z]/.test(pass),
          nonWords: /\W/.test(pass),
      }

      var variationCount = 0;
      for (var check in variations) {
          variationCount += (variations[check] == true) ? 1 : 0;
      }
      score += (variationCount - 1) * 10;

      return parseInt(score);
  }
  function checkPassStrength(e) {
    var score = scorePassword(e.target.value);
    if(score >= 70)
    {
      $('.btn_submit').attr('disabled',false);
    }
    console.log(score);
    if (score > 80)
    {
      strength = score;
      $('.pass_strength').html("Password strength is <b style='color:green'>strong.</b>")
      return "strong";
    }
    else if (score >= 70)
    {
      strength = score;
      $('.pass_strength').html("Password strength is <b style='color:orange'>good.</b>")
      return "good";
    }
    else if (score <= 30)
    {
      strength = score;
      $('.pass_strength').html("Password strength is <b style='color:red'>week.</b>")
      return "weak";
    }

    return "";
  }
</script>
@endsection