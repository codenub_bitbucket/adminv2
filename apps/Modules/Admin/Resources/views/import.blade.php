@extends('app')
@section('title', 'Import Admin')
@section('content')
<div class="main-content">
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Import Admin</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Admin</li>
                <li class="breadcrumb-item active">Import</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
  
      <div class="card shadow mb-4">
        <div class="card-body">
          <h6>Template import admin</h6>
          <a href="{{ URL('/assets/import/template_admin.xlsx') }}"><i class="ri-file-excel-2-line"></i> Download Template</a>
          <hr>
          <h6>Silahkan pilih excel yang akan di import</h6>
          <form method="post" action="{{ URL('smkbisa/staff/import_preview') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label>Pilih file excel</label>
              <input type="file" name="file" required="required">
            </div>
            <a href="{{ URL('smkbisa/staff')}}"type="button" class="btn btn-secondary">Batal</a>
            <button type="submit" class="btn btn-primary">Import</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection