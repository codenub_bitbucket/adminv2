<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/staff')->group(function() {
    Route::get('/', 'AdminController@index');
    Route::get('/tambah', 'AdminController@create');
    Route::post('/save', 'AdminController@save');
    Route::get('/delete/{uuid}', 'AdminController@delete');
    Route::get('/edit/{uuid}', 'AdminController@edit');
    Route::post('/update/{uuid}', 'AdminController@update');
    
    Route::get('/import', 'AdminController@import');
    Route::post('/import_preview', 'AdminController@importPreview');
    Route::post('/import_save', 'AdminController@importSave');

    Route::post('/mass_delete', 'AdminController@mass_delete');
});
