<?php

namespace Modules\Api\Entities;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table 		= "absensi";
    protected $primaryKey 	= "absensi_serial_id";
    protected $guarded = array('absensi_serial_id');
    public $timestamps = false;
}
