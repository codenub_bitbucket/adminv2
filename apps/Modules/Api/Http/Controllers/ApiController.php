<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Auth;
use DB;
use Intervention\Image\Facades\Image;
use Exceptions;

use App\User as User;
use Carbon\Carbon;
use Modules\Api\Entities\Absensi;
use Modules\Siswa\Entities\Siswa as Siswa;
use Modules\Sys\Entities\Rel as Rel;
use Modules\Sys\Http\Controllers\SysHelper as sys;

use Modules\Ujian\Http\Controllers\UjianHelper as ujian;
use Modules\Learning\Http\Controllers\LearningHelper as elearning;
use Modules\Wali\Http\Controllers\WaliHelper as wali;
use Modules\Pengumuman\Http\Controllers\PengumumanHelper as pengumuman;


class ApiController extends Controller
{

	public function __construct()
	{
			// $this->allow_CORS();
	}

	public function allow_CORS()
	{
			if (isset($_SERVER['HTTP_ORIGIN']))
			{
					header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
					header('Access-Control-Allow-Credentials: true');
					header('Access-Control-Max-Age: 86400');    // cache for 1 day
			}
			// Access-Control headers are received during OPTIONS requests
			if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
			{
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			{
					header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
			}

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			{
					header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			}

			exit(0);
			}
	}

	function auth(Request $request)
	{
		$data = $request->all();
		$akun = $request->only(['email', 'password']);

		// check email is valid or username

		$checkEmail = strpos($akun['email'], "@");

		$response = array(
			'status'   => 500,
			'msg' => "ERROR",
			'data' => array()
		);

		if($checkEmail !== false)
		{
			// login by email (users/siswa)
			if (!Auth::attempt($akun))
			{
				$response['msg'] = "Akun Anda Salah, Coba lagi!";
				return response()->json($response);
			}
			else
			{
				$users = Auth::user();
				$token = Str::random(60);

				if ($users->role == 1)
				{
					$response['msg'] = "Akun Anda Salah, Coba lagi!";
					return response()->json($response);
				}


				$model = new User();
				$rel = new Rel();
				$siswa = new Siswa();

				$model->where('id', '=', $users->id)->update(['api_token' => $token]);
				$rel = $rel->where('rel_to_module', '=', 'siswa')->where('rel_from_module', '=', 'users')->where('rel_from_id', '=', $users->id)->first();

				if(@count($rel) == 0)
				{
					$response['msg'] = "Akun anda telah terhapus. Silahkan hubungi admin!";
					return response()->json($response);
				}

				$siswa = $siswa->select('siswa.*',
						DB::raw('kelas.kelas_name as siswa_kelas_name'),
						DB::raw('subjurusan.subjurusan_name as siswa_sub_jurusan_name'),
						DB::raw('jurusan.jurusan_name as siswa_jurusan_name'),
						DB::raw('gen.dropdown_options_label as siswa_gender_name'),
						DB::raw('agama.dropdown_options_label as siswa_agama_name'))
				->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
				->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
				->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
				->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'siswa.siswa_gender')
				->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'siswa.siswa_agama')
				->where('siswa_serial_id', '=', $rel->rel_to_id)->first();

				if($siswa->deleted == 1)
				{
					$model->where('id', '=', $users->id)->update(['api_token' => null]);
					$response['msg'] = "Akun Anda Salah, Coba lagi!";
					return response()->json($response);
				}

				$users = $model->where('id', '=', $users->id)->first()->toArray();

				$users['photo'] = URL('/apps/public')."/".$users['photo'];


				$response['msg'] = "Berhasil Login!";
				$response['status'] = 200;
				$response['data'] = array(
					'users' => $users,
					'siswa' => $siswa
				);
				return response()->json($response);
			}
		}
		else
		{
			$data_wali = array();
			$data_siswa = array();
			$get = DB::table('wali')->where('wali_username', $akun['email'])->where('wali_password', $akun['password'])->first();

			$response['msg'] = "Akun tidak ditemukan!";
			$response['status'] = 500;
			$response['data'] = array();

			if(@count($get) > 0)
			{
				$data_wali = json_decode(json_encode($get), true);
				$data_wali['isParent'] = true;

				$model = new User();
				$rel = new Rel();
				$siswa = new Siswa();

				$siswa = $siswa->select('siswa.*',
						DB::raw('kelas.kelas_name as siswa_kelas_name'),
						DB::raw('subjurusan.subjurusan_name as siswa_sub_jurusan_name'),
						DB::raw('jurusan.jurusan_name as siswa_jurusan_name'),
						DB::raw('gen.dropdown_options_label as siswa_gender_name'),
						DB::raw('agama.dropdown_options_label as siswa_agama_name'))
				->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
				->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
				->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
				->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'siswa.siswa_gender')
				->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'siswa.siswa_agama')
				->where('siswa.siswa_serial_id', '=', $data_wali['wali_siswa_id'])->first();

				if(@count($siswa) > 0)
				{
					$data_siswa = json_decode(json_encode($siswa), true);
					$rel = $rel->where('rel_to_module', '=', 'siswa')->where('rel_from_module', '=', 'users')->where('rel_to_id', '=', $data_siswa['siswa_serial_id'])->first();
					$users = $model->where('id', '=', $rel->rel_from_id)->first()->toArray();
					$data_siswa['photo'] = URL('/apps/public')."/".$users['photo'];
				}
				$response['msg'] = "Berhasil Login!";
				$response['status'] = 200;
				$response['data'] = array(
					'data_wali' => $data_wali,
					'data_siswa' => $data_siswa,
				);
			}

			return response()->json($response);
		}
	}

    function logout(Request $request)
    {
		$data = $request->all();
        $model = new User();
        $logout = $model->where('id', '=', $data['users_id'])->update(['api_token' => null]);

        $response = array(
			'status'   => 500,
			'msg' => "ERROR",
			'data' => array()
		);

        if($logout)
        {
            $response['msg'] = "Berhasil keluar";
            $response['status'] = 200;
            $response['data'] = $logout;
        }

		return response()->json($response);
    }

	public function listData(Request $request, $table_module)
	{
		$ujian = new ujian();
		$elearning = new elearning();
		$pengumuman = new pengumuman();
		$data = $request->all();
		unset($data['token']);

		$response = array(
			'status'   => 500,
			'msg' => "ERROR",
			'data' => array()
		);

		if(isset($data['isParent']) && $data['isParent'] == true)
		{
			$data['who_get'] = 'wali';
		}
		else
		{
			$data['who_get'] = 'users';
		}

		try {
            if($table_module == 'ujian')
            {
                $data['show'] = 100;
            }
			$data = ${$table_module}->list_data('get', $data);

			$response['msg'] = "Success";
			$response['status'] = 200;
			$response['data'] = $data;
        } catch (\Exeptions $th) {
			$response['msg'] = "Gagal Mendapatkan Data!";
		}

		return response()->json($response);
	}

	public function getDetail(Request $request, $table_module)
	{
		$ujian = new ujian();
		$elearning = new elearning();

		$data = $request->all();

		$uuid = $data['uuid'];

		unset($data['token']);

		$response = array(
			'status'   => 500,
			'msg' => "ERROR",
			'data' => array()
		);

		$data['who_get'] = 'users';

		try {
			$data = ${$table_module}->get_detail($uuid);

			if($table_module == 'ujian')
			{
				$data['who_get'] = 'users';

				$data['soal'] = ${$table_module}->getSoal($data);
			}

			$response['msg'] = "Success";
			$response['status'] = 200;
			$response['data'] = $data;
		} catch (\Throwable $th) {
			$response['msg'] = "Gagal Mendapatkan Data!";
		}

		return response()->json($response);
	}

    public function saveData(Request $request, $table_module)
	{
		$ujian = new ujian();
		$data = $request->all();

		unset($data['token']);

		$response = array(
			'status'   => 500,
			'msg' => "ERROR",
			'data' => array()
		);

        if($table_module == 'ujian')
        {
            try {
                $data = ${$table_module}->saveUjianSiswa($data);

                $response['msg'] = "Success";
                $response['status'] = 200;
                $response['data'] = $data;
            } catch (\Throwable $th) {
                $response['msg'] = "Gagal Menyimpan Data!";
            }
        }

		return response()->json($response);
	}

    public function getAbsensiStatus(Request $request)
    {
        try {
            $data = $request->all();

            $ujian = DB::table('ujian')->where("ujian_uuid", $data['ujian_uuid'])->first();

            $ujian = json_decode(json_encode($ujian),true);

            $query = Absensi::where("absensi_siswa_id", $data['siswa_id'])->where("absensi_ujian_id", $ujian['ujian_serial_id']);

            $response = [
                'is_absen'  => $query->exists(),
                'data'      => $query->first() == null ? [] : $query->first()
            ];
        } catch (\Throwable $th) {
            $response = [
                'is_absen'  => false,
                'data'      => []
            ];
        }
        return response()->json($response);
    }

    public function saveAbsen(Request $request)
    {
        try {
            $data = $request->all();

            $insert = [
                "absensi_uuid" => Str::uuid()->toString(),
                "absensi_siswa_id" => $data['users']['siswa_id'],
                "absensi_ujian_id"  => $data['ujian']['ujian_serial_id'],
                "created_at"  => date("Y-m-d H:i:s"),
            ];
            $sys = new sys();

            $png_url = "absensi-".$data['users']['siswa_id'].".png";
            $path = public_path().'\uploads\\' . $png_url;
            $filePath = "/uploads/$png_url";

            $upload = Image::make(file_get_contents($data['absen']))->save($path);

            $insert['absensi_document_url'] = URL('apps/public/'.$filePath);

            if(Absensi::where("absensi_siswa_id", $data['users']['siswa_id'])->where("absensi_ujian_id", $data['ujian']['ujian_serial_id'])->exists())
            {
                $update = [
                    "updated_at"  => date("Y-m-d H:i:s"),
                    "created_at"  => date("Y-m-d H:i:s"),
                    "is_approve"  => 0,
                    "is_decline"  => 0,
                    "absensi_document_url"  => $insert['absensi_document_url'],
                ];
                $query = Absensi::where("absensi_siswa_id", $data['users']['siswa_id'])
                ->where("absensi_ujian_id", $data['ujian']['ujian_serial_id'])->update($update);
            }
            else
            {
                $query = Absensi::insert($insert);
            }


            $response = array(
                'status'   => 200,
                'msg' => "OK",
            );
        } catch (\Throwable $th) {
            echo '<pre>';
                print_r($th->getMessage());
            exit;
            $response = array(
                'status'   => 500,
                'msg' => "ERROR",
            );
        }
        return response()->json($response);
    }

    public function serverTime()
    {
        $response = array(
            'status'   => 200,
            'msg' => "OK",
            'data' => date("H:i:s"),
        );
        return response()->json($response);
    }
}
