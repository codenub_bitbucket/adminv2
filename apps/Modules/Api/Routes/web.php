<?php
use App\Http\Middleware\EnsureTokenIsValid;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function() {
    Route::post('/authorize', 'ApiController@auth');
    Route::post('/absensi', 'ApiController@getAbsensiStatus');
    Route::post('/do-absen', 'ApiController@saveAbsen');

    Route::middleware([EnsureTokenIsValid::class])->group(function () {
        Route::post('/logout', 'ApiController@logout');

        Route::post('/{table_module}/list', 'ApiController@listData');
        Route::post('/{table_module}/detail', 'ApiController@getDetail');
        Route::post('/{table_module}/save', 'ApiController@saveData');
        Route::post('/server-time', 'ApiController@serverTime');
    });


});
