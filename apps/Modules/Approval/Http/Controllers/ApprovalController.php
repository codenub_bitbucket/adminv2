<?php

namespace Modules\Approval\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Api\Entities\Absensi;

class ApprovalController extends Controller
{
    public function index(Request $request)
    {
        $criteria = $request->all();
        $criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

        $query = Absensi::leftjoin("siswa as s", "s.siswa_serial_id", "=", "absensi.absensi_siswa_id")
        ->leftjoin("ujian as u", "u.ujian_serial_id", "=", "absensi.absensi_ujian_id")
        ->leftjoin("mata_pelajaran as mt", "mt.mata_pelajaran_serial_id", "=", "u.ujian_mata_pelajaran_id")
        ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 's.siswa_kelas_id')
        ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 's.siswa_jurusan_id')
        ->whereNotNull("absensi.absensi_document_url")
        ->where("is_approve", "0")
        ->where("is_decline", "0");

        $count = $query->count();

        if (isset($criteria['search']) && $criteria['search'] != '')
        {
            $query->where('s.siswa_name',"LIKE", "%".$criteria['search']."%");
        }

        $list = $query->paginate($criteria['show']);
        $list->appends($criteria);

		$view = 'approval::index';

		$view_data = array(
			'table_module'   => "approval",
			'list_data'      => $list,
			'count_data'     => $count,
			'data_per_page'  => $data_per_page,
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}

    }

    public function detail(Request $request, $uuid)
    {
        $query = Absensi::leftjoin("siswa as s", "s.siswa_serial_id", "=", "absensi.absensi_siswa_id")
        ->leftjoin("ujian as u", "u.ujian_serial_id", "=", "absensi.absensi_ujian_id")
        ->leftjoin("mata_pelajaran as mt", "mt.mata_pelajaran_serial_id", "=", "u.ujian_mata_pelajaran_id")
        ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 's.siswa_kelas_id')
        ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 's.siswa_jurusan_id')
        ->where("absensi_uuid", $uuid)->first();

        if($query !== null)
        {
            $query = $query->toArray();

            $view = 'approval::detail';
            $view_data = array(
			    'table_module'   => "approval",
                'content'        => $query,
            );

            if(view()->exists($view))
            {
                return view($view, $view_data)->render();
            }
        }
        else
        {
            return redirect()->back();
        }
    }

    public function action($action, $uuid)
	{
        $query = false;

        if($action == 'decline')
        {
            $query = Absensi::where("absensi_uuid", $uuid)->update(["is_decline" => 1]);
        }
        else if("approve")
        {
            $query = Absensi::where("absensi_uuid", $uuid)->update(["is_approve" => 1]);
        }

		if ($query)
		{
				return redirect('smkbisa/approval-absensi')->with(['success' => 'Absensi Berhasil Di'.ucfirst($action)]);
		}
		else
		{
				return redirect('smkbisa/approval-absensi')->with('failed', 'Absensi Gagal Di'.ucfirst($action));
		}
	}

}
