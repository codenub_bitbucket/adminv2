@extends('app')
@section('title', 'Approval Absen Detail')
@section('content')
<div class="main-content">
	<div class="page-content">
        <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Approval Absen Detail</h4>
                    <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Approval Absen</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12">
                <div class="bg-primary div-profile-user px-4 py-4 mb-3">
                    <div class="media">
                        <div class="media-body">
                                <h4 class="mt-0">{{ $content['siswa_name'] }}</h4>
                                <p class="mb-0 text-white">
                                    {{ $content['kelas_name'] }} - {{ $content['jurusan_name']}}
                                </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ $content['absensi_document_url'] }}" width="100%">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Ujian Name</p>
                                <h6>{{ $content['ujian_name'] }}</h6>
                            </li>

                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Mata Pelajaran</p>
                                <h6>{{ $content['mata_pelajaran_name'] }}</h6>
                            </li>

                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Tanggal Absen</p>
                                <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y H:i:s", strtotime($content['created_at']))) }}</h6>
                            </li>

                        </ul>
                        <a class="btn btn-block btn-primary text-white" href="{{ url('smkbisa/approval-absensi/action/approve/'. $content['absensi_uuid']) }}">
                            Approve
                        </a>
                        <a class="btn btn-block btn-danger text-white" href="{{ url('smkbisa/approval-absensi/action/decline/'. $content['absensi_uuid']) }}">
                            Decline
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div> <!-- container-fluid -->
	</div>
</div>
@endsection
