@extends('app')
@section('title', 'Siswa')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row pt-3">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">SMK Kartikatama | Approval Absensi</h4>
                            @if (\Session::has('success'))
                                <div class="alert alert-success" id="success-alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>Berhasil!.</strong>
                                    {!! \Session::get('success') !!}
                                </div>
                                <script>
                                    $(document).ready(function() {
                                        setTimeout(() => {
                                            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                                                $("#success-alert").slideUp(500);
                                            });
                                        }, 30000);
                                    });
                                </script>
                            @endif
                            @if (\Session::has('failed'))
                                <div class="alert alert-danger" id="failed-alert">
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                    <strong>Opps!.</strong>
                                    {!! \Session::get('failed') !!}
                                </div>
                                <script>
                                    $(document).ready(function() {
                                        setTimeout(() => {
                                            $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                                                $("#failed-alert").slideUp(500);
                                            });
                                        }, 30000);
                                    });
                                </script>
                            @endif
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Approval Absensi</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th style="white-space:nowrap">Action</th>
                                                <th style="white-space:nowrap">Siswa</th>
                                                <th style="white-space:nowrap">Kelas | Jurusan</th>
                                                <th style="white-space:nowrap">Nama Ujian</th>
                                                <th style="white-space:nowrap">Mata Pelajaran</th>
                                                <th style="white-space:nowrap">Tanggal Absen</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (@count($list_data) > 0)
                                            @foreach ($list_data as $key => $item)
                                            <tr>
                                                <td style="white-space:nowrap">
                                                    <a title="Lihat" class="btn btn-success" href="{{ URL('smkbisa/approval-absensi/detail/'.$item['absensi_uuid']) }}"><i
                                                            class="ri-eye-line"></i></a>
                                                </td>
                                                <td style="white-space:nowrap">{{ $item['siswa_name'] }}</td>
                                                <td style="white-space:nowrap">{{ $item['kelas_name'] }} | {{ $item['jurusan_name'] }}</td>
                                                <td style="white-space:nowrap">{{ $item['ujian_name'] }}</td>
                                                <td style="white-space:nowrap">{{ $item['mata_pelajaran_name'] }}</td>
                                                <td style="white-space:nowrap">{{ $item['created_at'] }}</td>
                                            </tr>

                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="11">
                                                        <h5 class="text-center">Belum Ada Absen</h5>
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @include('component.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
@endsection
