<?php

namespace Modules\Beranda\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Siswa\Http\Controllers\SiswaHelper as siswa_helper;
use Modules\Pendaftar\Http\Controllers\PendaftarHelper as pendaftar_helper;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;
use Modules\Blog\Http\Controllers\BlogHelper as blog_helper;
use Modules\Extracurricular\Http\Controllers\ExtracurricularHelper as extracurricular_helper;

class BerandaController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $s_helper = new siswa_helper();
        $p_helper = new pendaftar_helper();
        $au_helper = new aboutus_helper();
        $b_helper = new blog_helper();
        $e_helper = new extracurricular_helper();
        $sys = new sys();


        $criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 5;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 5;
        
        $count_siswa = $s_helper->list_data('count');

        $count_pendaftar = $p_helper->list_data('count');
        $pendaftar = $p_helper->list_data('get', $criteria);
        $fields_pendaftar = $sys->getFields('pendaftar');
        
        $count_fasilitas = $au_helper->getFacility('count');

		$con['publish'] = [0, 1];
        $count_berita = $b_helper->list_data('count', $con);
        
		$count_extracurricular = $e_helper->list_data('count');


        $view = 'beranda::index';

		$view_data = array(
			'count_pendaftar'      => $count_pendaftar,
			'count_siswa'      => $count_siswa,
			'count_fasilitas'      => $count_fasilitas,
            'count_berita'      => $count_berita,
            'count_extracurricular' => $count_extracurricular,
            'pendaftar' => $pendaftar,
            'fields_pendaftar' => $fields_pendaftar,
		);
        
        // dd($view_data);

		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }
}
