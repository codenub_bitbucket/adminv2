@extends('app')
@section('title', 'Dashboard')
@section('content')
  <div class="main-content">

    <div class="page-content">
      <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
          <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
              <h4 class="mb-0 mt-4">SMK Kartikatama</h4>
              <div class="page-title-right">
                <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item active">Dashboard</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- end page title -->

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <img src="{{ URL('resources_admin/images/header-1.png') }}">
            </div>
          </div>
          {{-- <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <div class="dropdown float-right">
                  <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                      <i class="mdi mdi-dots-vertical"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                      <a href="javascript:void(0);" class="dropdown-item">Action</a>
                  </div>
                </div>
                <h4 class="card-title mb-4">Recent Activity</h4>
                <ul class="list-unstyled activity-wid">
                  <li class="activity-list">
                      <div class="activity-icon avatar-xs">
                          <span class="avatar-title bg-soft-primary text-primary rounded-circle">
                              <i class="ri-edit-2-fill"></i>
                          </span>
                      </div>
                      <div>
                          <div>
                              <h5 class="font-size-13">28 Apr, 2020 <small class="text-muted">12:07 am</small></h5>
                          </div>
                          <div>
                              <p class="text-muted mb-0">Sumarni add new exam "This is exams name"</p>
                          </div>
                      </div>
                  </li>
                  <li class="activity-list">
                    <div class="activity-icon avatar-xs">
                        <span class="avatar-title bg-soft-primary text-primary rounded-circle">
                            <i class="ri-edit-2-fill"></i>
                        </span>
                    </div>
                    <div>
                        <div>
                            <h5 class="font-size-13">28 Apr, 2020 <small class="text-muted">12:07 am</small></h5>
                        </div>
                        <div>
                            <p class="text-muted mb-0">Sumarni add new exam "This is exams name"</p>
                        </div>
                    </div>
                  </li>
                  <li class="activity-list">
                    <div class="activity-icon avatar-xs">
                        <span class="avatar-title bg-soft-primary text-primary rounded-circle">
                            <i class="ri-edit-2-fill"></i>
                        </span>
                    </div>
                    <div>
                        <div>
                            <h5 class="font-size-13">28 Apr, 2020 <small class="text-muted">12:07 am</small></h5>
                        </div>
                        <div>
                            <p class="text-muted mb-0">Sumarni add new exam "This is exams name"</p>
                        </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div> --}}
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3">
                <div class="card">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body overflow-hidden">
                          <p class="text-truncate font-size-14 mb-2">Jumlah Pendaftar</p>
                          <h4 class="mb-0">{{ $count_pendaftar }}</h4>
                      </div>
                      <div class="text-primary">
                          <i class="  ri-user-2-line font-size-24"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body overflow-hidden">
                          <p class="text-truncate font-size-14 mb-2">Jumlah Fasilitas</p>
                          <h4 class="mb-0">{{ $count_fasilitas }}</h4>
                      </div>
                      <div class="text-primary">
                          <i class=" ri-building-3-line font-size-24"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body overflow-hidden">
                          <p class="text-truncate font-size-14 mb-2">Jumlah Berita</p>
                          <h4 class="mb-0">{{ $count_berita }}</h4>
                      </div>
                      <div class="text-primary">
                          <i class="  ri-newspaper-line font-size-24"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body overflow-hidden">
                          <p class="text-truncate font-size-14 mb-2">Jumlah Ekstrakurikuler</p>
                          <h4 class="mb-0">{{ $count_extracurricular }}</h4>
                      </div>
                      <div class="text-primary">
                          <i class="  ri-basketball-line font-size-24"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row mb-3 align-items-center d-flex">
                      <div class="col-6">
                        <h4> Daftar Pendaftar Terbaru</h4>
                        {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                      </div>
                      {{-- <div class="col-6 text-right">
                        <a class="btn btn-primary px-4" href="{{ url('smkbisa/fasilitas/category/add') }}">
                          + Fasilitas
                        </a>
                      </div> --}}
                    </div>
                    <div class="table-responsive">
                      <table class="table mb-0">
                        <thead>
                          <tr>
                            <th style="white-space:nowrap">#</th>
                            @foreach ($fields_pendaftar as $fields)
                              @if ($fields['pendaftar_fields_name'] !== 'pendaftar_photo' && $fields['pendaftar_fields_name'] !== 'pendaftar_nisn' && $fields['pendaftar_fields_name'] !== 'pendaftar_phone' && $fields['pendaftar_fields_name'] !== 'pendaftar_address' && $fields['pendaftar_fields_name'] !== 'pendaftar_dob')
                                <th style="white-space:nowrap">{{ $fields['pendaftar_fields_label'] }}</th>
                              @endif
                            @endforeach
                            <th style="white-space:nowrap">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @if (@count($pendaftar) > 0)
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($pendaftar as $key => $item)
                                  <tr>
                                    <td style="white-space:nowrap">{{$i++}}</td>
                                    @foreach ($fields_pendaftar as $fields)
                                      @if ($fields['pendaftar_fields_name'] !== 'pendaftar_photo' && $fields['pendaftar_fields_name'] !== 'pendaftar_nisn' && $fields['pendaftar_fields_name'] !== 'pendaftar_phone' && $fields['pendaftar_fields_name'] !== 'pendaftar_address' && $fields['pendaftar_fields_name'] !== 'pendaftar_dob')
                                        @if ($fields['pendaftar_fields_input_type'] == 'date')
                                          <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item[$fields['pendaftar_fields_name']]))) }}</td>
                                        @else
                                          @if ($fields['pendaftar_fields_name'] == 'pendaftar_jenis_id')
                                            <td style="white-space:nowrap">{{ $item['pendaftar_jenis_name'] }}</td>
                                          @elseif ($fields['pendaftar_fields_name'] == 'pendaftar_jurusan_id')
                                            <td style="white-space:nowrap">{{ $item['pendaftar_jurusan_name'] }}</td>
                                          @elseif ($fields['pendaftar_fields_name'] == 'pendaftar_gender')
                                            <td style="white-space:nowrap">{{ $item['pendaftar_gender_name'] }}</td>
                                          @elseif ($fields['pendaftar_fields_name'] == 'pendaftar_agama')
                                            <td style="white-space:nowrap">{{ $item['pendaftar_agama_name'] }}</td>
                                          @else
                                            <td style="white-space:nowrap">{{ $item[$fields['pendaftar_fields_name']] }}</td>
                                          @endif
                                        @endif
                                      @endif
                                    @endforeach
                                    <td style="white-space:nowrap">
                                      <a title="Lihat" class="btn btn-success" href="{{ url('smkbisa/pendaftar/detail/' . $item['pendaftar_uuid'])}}"><i class="ri-eye-line"></i></a>
                                    </td>
                                  </tr>
                                @endforeach
                            @else
                              <tr>
                                <td colspan="4">
                                  <h5 class="text-center">Belum Ada Fasilitas</h5>
                                </td>
                              </tr>
                            @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
  </div>
@endsection