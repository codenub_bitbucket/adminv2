<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table 		= "berita_sekolah";
    protected $primaryKey 	= "berita_sekolah_serial_id";
    protected $guarded = array('berita_sekolah_serial_id');
    public $timestamps = false;
}
