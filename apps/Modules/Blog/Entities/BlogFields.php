<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class BlogFields extends Model
{
    protected $table 		= "berita_sekolah_fields";
    protected $primaryKey 	= "berita_sekolah_fields_serial_id";
    protected $guarded = array('berita_sekolah_fields_serial_id');
    public $timestamps = false;
}
