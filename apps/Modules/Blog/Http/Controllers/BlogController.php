<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Redirect;
use Session;
use Intervention\Image\Facades\Image;

use Modules\Blog\Http\Controllers\BlogHelper as blog_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class BlogController extends Controller
{
	var $module       = 'Blog';
	var $table_module = 'berita_sekolah';

	function __construct(Request $request)
	{
		session()->forget('redirect');
		session()->put('redirect', $request->path());
		
	}
	
	public function index(Request $request)
	{
		$criteria = $request->all();
		$b_helper = new blog_helper();
		$sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

		$list_blog = $b_helper->list_data('get', $criteria);

		$rec_cre = array(
				'show' => 5
		);
		$list_blog_rec = $b_helper->list_data('get', $rec_cre, 'recent');
		// $count_blog = $b_helper->list_data('count');

		$fields = $sys->getFields($this->table_module);


		$view = 'blog::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_blog,
			'list_data_rec'  => $list_blog_rec,
			'count_data'     => @count($list_blog),
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function detail($uuid)
	{
		$b_helper = new blog_helper();
		
		$rec_cre = array(
				'show' => 5
		);
		$list_blog_rec = $b_helper->list_data('get', $rec_cre, 'recent');
		
		$detail = $b_helper->get_detail($uuid);

		if (@count($detail) == 0) 
		{
				return redirect('blog/news');
		}

		$view = 'blog::detail';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
			'list_data_rec'  => $list_blog_rec,
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function list_berita(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
        
		$criteria = $request->all();
		$b_helper = new blog_helper();
		$sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		$count_blog = $b_helper->list_data('count');

		$criteria['publish'] = [0, 1];

		$list_blog = $b_helper->list_data('get', $criteria);
		$fields = $sys->getFields($this->table_module);

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_blog,
			'count_data'     => $count_blog,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);

		$view = 'blog::list_berita';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function create(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
        
		$criteria = $request->all();
		$sys = new sys();
		
		$fields = $sys->getFields($this->table_module);

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' => array(), 
			'function' => 'save',
		);

		$view = 'blog::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function save_publish(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($criteria['berita_sekolah_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}

		$criteria['berita_sekolah_description'] = $dom->saveHTML();
		if ($request->file('berita_sekolah_thumbnail')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['berita_sekolah_thumbnail']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['berita_sekolah_thumbnail'] = $upload['file']->image;
				}
		}

		$bh = new blog_helper();

		$update = $bh->save('publish', $criteria, $users);

		if ($update)
		{
				return redirect('smkbisa/berita-sekolah')->with(['success' => 'Berita Sekolah Berhasil Disimpan dan Dipublish']);
		}
		else
		{
				return redirect('smkbisa/berita-sekolah')->with('failed', 'Berita Sekolah Gagal Disimpan dan Dipublish');
		}
	}

	public function save_draft(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$users = Auth::user();
		$sys = new sys();
		$criteria = $request->all();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($criteria['berita_sekolah_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}
		$criteria['berita_sekolah_description'] = $dom->saveHTML();

		if ($request->file('berita_sekolah_thumbnail')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['berita_sekolah_thumbnail']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['berita_sekolah_thumbnail'] = $upload['file']->image;
				}
		}

		$bh = new blog_helper();

		$update = $bh->save('draft', $criteria, $users);

		if ($update)
		{
				return redirect('smkbisa/berita-sekolah')->with(['success' => 'Berita Sekolah Berhasil Disimpan Sebagai Draft']);
		}
		else
		{
				return redirect('smkbisa/berita-sekolah')->with('failed', 'Berita Sekolah Gagal Disimpan Sebagai Draft');
		}
	}

	public function publish($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$users = Auth::user();

		$sys = new sys();

		$bh = new blog_helper();

		$pub = $bh->publish($uuid);
		$detail = $bh->get_detail($uuid, 'admin');

		$text = ($detail['publish'] == '0') ? 'Di Unpublish' : 'Di Publish';

		if ($pub)
		{
				return redirect()->back()->with('success', 'Berita Sekolah Berhasil '.$text);
		}
		else
		{
				return redirect()->back()->with('failed', 'Berita Sekolah Gagal '.$text);
		}
	}

	public function delete($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$users = Auth::user();

		$sys = new sys();

		$bh = new blog_helper();

		$publish = $bh->delete($uuid);

		if ($publish)
		{
				return redirect()->back()->with(['success' => 'Berita Sekolah Berhasil Dihapus']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Berita Sekolah Gagal Dihapus');
		}
	}

	public function detail_berita($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$b_helper = new blog_helper();
		
		$detail = $b_helper->get_detail($uuid, 'admin');

		if (@count($detail) == 0) 
		{
				return redirect('smkbisa/berita_sekolah')->with('failed', "Berita Sekolah Tidak Valid!");
		}

		$view = 'blog::detail_berita';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function edit($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$b_helper = new blog_helper();
		$sys = new sys();
		$detail = $b_helper->get_detail($uuid, 'admin');
		
		$fields = $sys->getFields($this->table_module);

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		$view = 'blog::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function update(Request $request, $uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$users = Auth::user();
		$sys = new sys();
		$criteria = $request->all();
		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($criteria['berita_sekolah_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}
		$criteria['berita_sekolah_description'] = $dom->saveHTML();

		if ($request->file('berita_sekolah_thumbnail')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['berita_sekolah_thumbnail']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['berita_sekolah_thumbnail'] = $upload['file']->image;
				}
		}

		$bh = new blog_helper();

		$update = $bh->update($criteria, $users, $uuid);

		if ($update)
		{
				return redirect('smkbisa/berita-sekolah/detail/'.$uuid)->with(['success' => 'Berita Sekolah Berhasil Diubah']);
		}
		else
		{
				return redirect('smkbisa/berita-sekolah/detail/'.$uuid)->with('failed', 'Berita Sekolah Gagal Diubah');
		}
	}
}
