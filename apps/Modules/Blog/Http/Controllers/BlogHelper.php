<?php
namespace Modules\Blog\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

class BlogHelper
{
  var $table_module = 'berita_sekolah';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_blog = 'Modules\Blog\Entities\Blog';
  
  public function list_data($type='get', $criteria = array(), $by='')
  {
    $result = array();

    $select = array(
      'berita_sekolah.berita_sekolah_uuid',
      'berita_sekolah.berita_sekolah_thumbnail',
      'berita_sekolah.berita_sekolah_title',
      'berita_sekolah.berita_sekolah_description',
      'berita_sekolah.berita_sekolah_total_seen',
      'berita_sekolah.created_by',
      'berita_sekolah.created_at',
      'berita_sekolah.publish',
    );
    
    $query = $this->model_blog::where('berita_sekolah.deleted', 0);
                              if (isset($criteria['publish'])) 
                              {
                                $query->whereIn('berita_sekolah.publish', $criteria['publish']);
                              }
                              else 
                              {
                                $query->where('berita_sekolah.publish', 1);
                              }

    if ($type == 'get') 
    {
      $query->select($select,"users.name as created_by_name")
            ->leftjoin('users', 'users.id', "=", 'berita_sekolah.created_by');
      if ($by !== '') 
      {
        $query->orderBy('berita_sekolah_serial_id', 'DESC');
      }

      $result = $query->paginate($criteria['show']);
      $result->appends($criteria)->unique();

      $temp = $result;

      
      foreach ($result as $key => $item)
      {
        $item->berita_sekolah_description = $this->limit_text($item->berita_sekolah_description);
      }

      $result = $temp;
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($type ,$data, $users)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['publish'] = 1;
    $data['created_by'] = $users->id;
    if ($type == 'draft') 
    {
      $data['publish'] = 0;
    }

    $data['berita_sekolah_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_blog::insert($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function get_detail($uuid, $from = null)
  {
    $result = array();

    $query = $this->model_blog::select("berita_sekolah.*","users.name as created_by_name")
                              ->where('berita_sekolah_uuid', $uuid);
                              if ($from == 'admin')
                              {
                                $query->whereIn('berita_sekolah.publish', [1,0]);
                              }
                              else 
                              {
                                $query->where('berita_sekolah.publish', 1);
                                $query->where('berita_sekolah.deleted', 0);
                              }
                              $query->leftjoin('users', 'users.id', "=", 'berita_sekolah.created_by');
                              $query = $query->first();
    
    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query),true);

      if ($from !== 'admin')
      {
        $seen = $this->model_blog::where('berita_sekolah_uuid', $uuid)->update(['berita_sekolah_total_seen' => ($result['berita_sekolah_total_seen']+1)]);
      }
    }

    return $result;
  }
  
  function limit_text($text, $limit=20) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
  }

  function publish($uuid)
  {
    $result = false;

    $get = $this->model_blog::where('berita_sekolah_uuid', $uuid)->first();

    if (@count($get) > 0)
    {
      $get = json_decode(json_encode($get), true);

      $published = $get['publish'];

      if ($published == '0') 
      {
        $update['publish'] = '1';
      }
      else 
      {
        $update['publish'] = '0';
      }
      $update['updated_at'] = date('Y-m-d H:i:s');

      $query = $this->model_blog::where('berita_sekolah_uuid', $uuid)->update($update);

      if ($query)
      {
        $result = true;
      }
    }
    return $result;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_blog::where('berita_sekolah_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_blog::where('berita_sekolah_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

}