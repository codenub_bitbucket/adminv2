@extends('app')
@section('title', 'Landing')
@section('content')

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama Metro</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item ">Berita Sekolah</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-9">
          <div class="card">
            <div class="card-body">
              <form action="{{ (isset($function) && $function == 'edit') ? URL('smkbisa/berita-sekolah/update/'.$data['berita_sekolah_uuid']) : URL('smkbisa/berita-sekolah/save_publish')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-12">
                    @foreach ($fields as $key => $value)
                      <div class="form-group">
                        <label>{{ $value[$table_module.'_fields_label'] }}</label>
                        @include('input.'.$value[$table_module.'_fields_input_type'], array(
                            'table_module'            => $table_module,
                            'fields_name'             => $value[$table_module.'_fields_name'], 
                            'fields_label'             => $value[$table_module.'_fields_label'], 
                            'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                            'required'                => $value[$table_module.'_fields_validation'],
                        ))
                      </div>
                    @endforeach   
                  </div>
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  @if (isset($function) && $function == 'edit')
                    <a class="btn btn-light px-3 mr-2" href="{{ URL('smkbisa/berita-sekolah') }}">
                      Cancel
                    </a>
                    <button class="btn btn-primary px-4" type="submit">
                      Update
                    </button>
                  @else
                    <a class="btn btn-light px-3 mr-2" href="{{ URL('smkbisa/berita-sekolah') }}">
                      Cancel
                    </a>
                    <button class="btn btn-primary px-4 mr-2" type="submit" formaction="{{ URL('smkbisa/berita-sekolah/save_draft') }}" formenctype="multipart/form-data">
                      Save Only
                    </button>
                    <button class="btn btn-primary px-4" type="submit">
                      Save & Publish
                    </button>
                  @endif
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card">
            <div class="card-header">
              <b>Preview Thumbnail</b>
            </div>
            <div class="card-body">
              <img src="{{ URL('resources/images/preview.png') }}" alt="Preview Image" class="img-responsive w-100" id="img-preview" style="max-height: 250px">

              <div class="mt-4">
                <h5 id="title-preview">Judul Kabar Berita</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

<script>
  $('#berita_sekolah_title').keyup(function(){
    $('#title-preview').text($('#berita_sekolah_title').val())
  })
</script>
@if ($function == 'edit')
    <script>
      let image = "{{ URL('/apps/public') }}" + "/" +"{{ $data['berita_sekolah_thumbnail'] }}";

      $('#img-preview').attr('src', image)
      $('#title-preview').text($('#berita_sekolah_title').val())
    </script>
@endif
@endsection