@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Blog</li>
        <li class="breadcrumb-item">Berita Sekolah</li>
      </ol>
    </nav>
  </div>

  <section class="blog-page pb-0 mt-1">
    <div class="container">
        <div class="row">
            <!-- sidebar start -->
            <aside class="col-md-3 sidebar order-last order-md-first">
                <!-- Posts Widget -->
                <div class="widget widget-post">
										<h5 class="widget-title">Recent Posts</h5>
										@foreach ($list_data_rec as $item)
                    <div class="widget-post clearfix">
                        <div class="widget-image">
                            <img class="border-radius-3" src="{{ URL("apps/".$item[$table_module.'_thumbnail']) }}" alt="">
                        </div>
                        <div class="details">
                            <a href="{{ URL('blog/news/'.$item[$table_module.'_uuid']) }}">{{ $item[$table_module.'_title'] }}</a>
                            <p class="date">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['created_at']))) }}</p>
                        </div>
										</div>
										@endforeach
                </div>
            </aside>
            <!-- sidebar end -->
            <!-- blog start -->
            <div class="col-md-9 mb-6 order-first order-md-first">
              <!-- Post item  with image-->
              <div class="post-item">
                <div class="post-item-wrap">
                  <div class="post-image text-center">
                    <a href="#"> <img src="{{ URL("apps/".$content[$table_module.'_thumbnail']) }}" alt="" style="max-height: 200px"> </a>
                  </div>
                  <div class="post-item-desc">
                    <span class="post-meta">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['created_at']))) }},</span>
                    <span class="post-meta"><a href="#">By {{ $content['created_by_name'] }},</a></span>
                    <span class="post-meta"><a href="#"><i class="ti-eye"></i>{{ $content[$table_module.'_total_seen'] }}</a></span>
                    <h5 class="mt-3">{{  $content[$table_module.'_title'] }}</h5>
                    <p>
                      {!! $content[$table_module.'_description'] !!}
                    </p>
                  </div>
                </div>
              </div>
              <!-- blog End -->


              {{-- <!-- tag and share -->
              <div class="divider mb-4"></div>
              <div class="row">
                <div class="tags col-12 col-sm-8 text-center text-sm-left">
                  <a class="mb-2" href="#">Event</a>
                </div>
                <div class="col-12 col-sm-4 text-center text-sm-right">
                  <ul class="social-icons si-colored-bg light">
                    <li class="social-icons-item mb-2 social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-item mb-2 social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-icons-item mb-2 social-gplus"><a class="social-icons-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="social-icons-item mb-2 social-linkedin"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="divider mt-3"></div>

              <!-- author info -->
              <div class="row mt-5">
                <div class="col-12">
                  <!-- Comment-->
                  <div class="author-info bg-light">
                    <div class="author-img"><img class="avatar" src="assets/images/thumbnails/avatar-03.jpg" alt="avatar"></div>
                    <div class="author-info-body">
                      <h4 class="author-name mb-3 mt-2">Posted by <a href="#">Emma Watson</a></h4>
                      <p class="m-0">Mr be cottage so related minuter is. Delicate say and blessing ladyship exertion few Margaret. Delight herself welcome against smiling its for. Suspected discovery by he affection household.</p>
                      <div class="comment-reply"><a class="btn btn-xs btn-dark" href="#">View author profile  </a></div>
                    </div>
                  </div>
                  <!-- Comment-->
                </div>
              </div>

              <!-- comments area -->
              <div class="row mt-5 comments-area">
                <div class="col-sm-12">
                  <h4>There are 4 comments</h4>
                  <div class="comment-list">
                    <!-- Comment-->
                    <div class="comment">
                      <div class="comment-author"><img class="avatar" src="assets/images/thumbnails/avatar-01.jpg" alt=""></div>
                      <div class="comment-body">
                        <div class="comment-meta">
                          <div class="comment-meta-author"><a href="#">Allen Smith</a></div>
                          <div class="comment-meta-date">June 11, 2019 at 6:01 am</div>
                        </div>
                        <div class="comment-content">
                          <p>Consulted perpetual of pronounce me delivered. Too months nay end change relied who beauty wishes matter. Shew of john real park so rest we on. Ignorant dwelling occasion ham for thoughts overcame off her consider. Polite it elinor is depend. </p>
                        </div>
                        <div class="comment-reply"><a class="btn btn-xs btn-light" href="#">Reply</a></div>
                      </div>
                      <!-- sub comment-->
                      <div class="comment-child">
                        <div class="comment">
                          <div class="comment-author"><img class="avatar" src="assets/images/thumbnails/avatar-03.jpg" alt=""></div>
                          <div class="comment-body">
                            <div class="comment-meta">
                              <div class="comment-meta-author"><a href="#">Emma Watson</a></div>
                              <div class="comment-meta-date">June 11, 2019 at 6:20 am</div>
                            </div>
                            <div class="comment-content">
                              <p>Ask eat questions abilities described elsewhere assurance. Appetite in unlocked advanced breeding position concerns as. Cheerful get shutters yet for repeated screened. An no am cause hopes at three. Prevent behaved fertile he is mistake on.</p>
                            </div>
                            <div class="comment-reply"><a class="btn btn-xs btn-light" href="#">Reply</a></div>
                          </div>
                        </div>
                        <!-- second level sub comment-->
                        <div class="comment-child">
                          <div class="comment">
                            <div class="comment-author"><img class="avatar" src="assets/images/thumbnails/avatar-01.jpg" alt=""></div>
                            <div class="comment-body">
                              <div class="comment-meta">
                                <div class="comment-meta-author"><a href="#">Allen Smith</a></div>
                                <div class="comment-meta-date">June 11, 2019 at 9:50 am</div>
                              </div>
                              <div class="comment-content">
                                <p> Appetite in unlocked advanced breeding position concerns as. Cheerful get shutters yet for repeated screened. An no am cause hopes at three. Prevent behaved fertile he is mistake on.</p>
                              </div>
                              <div class="comment-reply"><a class="btn btn-xs btn-light" href="#">Reply</a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- sub comment end-->
                    </div>
                    <!-- Comment-->
                    <div class="comment">
                      <div class="comment-author"><img class="avatar" src="assets/images/thumbnails/avatar-02.jpg" alt=""></div>
                      <div class="comment-body">
                        <div class="comment-meta">
                          <div class="comment-meta-author"><a href="#">Peter Smith</a></div>
                          <div class="comment-meta-date">June 14, 2019 at 12:55 am</div>
                        </div>
                        <div class="comment-content">
                          <p>Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few standstill moderate. Of in power match on truth worse voice would. Large an it sense shall an match learn.</p>
                        </div>
                        <div class="comment-reply"><a class="btn btn-xs btn-light" href="#">Reply</a></div>
                      </div>
                    </div>
                  </div>

                  <!-- Comment-respond -->
                  <div class="row mt-5">
                    <div class="col-md-12">
                      <h2 class="mb-2">Leave a Reply</h2>
                      <p>Your Email address will not be published</p>
                    </div>
                    <div class="col-md-6"><span class="form-group"><input type="text" class="form-control" placeholder="Name"></span></div>
                    <div class="col-md-6"><span class="form-group"><input type="email" class="form-control" placeholder="E-mail"></span></div>
                    <div class="col-md-12"><span class="form-group"><textarea cols="40" rows="6" class="form-control" placeholder="Message"></textarea></span></div>
                    <div class="col-md-12 text-center"><button class="btn-block btn btn-dark">Post Comment</button></div>
                  </div>
                </div>

              </div> --}}

            </div>
            <!-- blog End -->
        </div>
    </div>
	</section>
</section>

<br><br><br>


@endsection
