@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Blog</li>
        <li class="breadcrumb-item">Berita Sekolah</li>
      </ol>
    </nav>
  </div>

  <section class="blog-page pb-0 mt-1">
    <div class="container">
        <div class="row">
            <!-- sidebar start -->
            <aside class="col-md-3 sidebar order-last order-md-first">
                <!-- Posts Widget -->
                <div class="widget widget-post">
										<h5 class="widget-title">Recent Posts</h5>
										@foreach ($list_data_rec as $item)
                    <div class="widget-post clearfix">
                        <div class="widget-image">
                            <img class="border-radius-3" src="{{ URL("apps/".$item[$table_module.'_thumbnail']) }}" alt="">
                        </div>
                        <div class="details">
                            <a href="#">{{ $item[$table_module.'_title'] }}</a>
                            <p class="date">{{ $item['created_at'] }}</p>
                        </div>
										</div>
										@endforeach
                </div>
            </aside>
            <!-- sidebar end -->
            <div class="col-md-9 order-2 order-md-2 blog-grid blog-grid-2 portfolio-wrap" data-isotope='{ "itemSelector": ".post-item", "layoutMode": "fitRows" }'>
                <!-- Post item  with image-->
                @foreach ($list_data as $item)
								<div class="post-item">
									<div class="post-item-wrap">
											<div class="post-image">
													<a href="{{ URL('blog/news/'.$item[$table_module.'_uuid']) }}"> <img src="{{ URL("apps/".$item[$table_module.'_thumbnail']) }}" alt="" style="height: 250px;"> </a>
													{{-- <span class="post-meta-category bg-grad"><a href="#">Blog Image</a></span> --}}
											</div>
											<div class="post-item-desc">
													<span class="post-meta">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</span>
													<span class="post-meta"><a href="#">{{ $item['created_by_name'] }}</a></span>
													<h4><a href="{{ URL('blog/news/'.$item[$table_module.'_uuid']) }}">{{ $item[$table_module.'_title'] }}</a></h4>
													<a href="{{ URL('blog/news/'.$item[$table_module.'_uuid']) }}" class="item-link">Lanjutkan Membaca<i class="ti-minus"></i></a>
											</div>
									</div>
								</div>
								@endforeach
            </div>
            <!-- Blog End -->
            <!-- pagination -->
						@include('component.pagination')
            <!-- pagination -->
        </div>
    </div>
	</section>
</section>

<br><br><br>


@endsection
