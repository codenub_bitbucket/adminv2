@extends('app')
@section('title', 'Landing')
@section('content')

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Fasilitas</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3 align-items-center d-flex">
                <div class="col-6">
                  <h4> Daftar Berita Sekolah</h4>
                  {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                </div>
                <div class="col-6 text-right">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/berita-sekolah/tambah') }}">
                    + Berita Sekolah
                  </a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table mb-0">
                  <thead>
                    <tr>
                      <th style="white-space:nowrap">Action</th>
                      <th style="white-space:nowrap">Judul</th>
                      <th style="white-space:nowrap">Total Dilihat</th>
                      <th style="white-space:nowrap">Created By</th>
                      <th style="white-space:nowrap">Created</th>
                      <th style="white-space:nowrap">Updated</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (@count($list_data) > 0)
                      @foreach ($list_data as $row)
                        <tr>
                          <td style="white-space:nowrap">
                            <a title="Lihat Data" class="btn btn-success m-1" href="{{ url('smkbisa/berita-sekolah/detail/'. $row['berita_sekolah_uuid'])}}"><i class="ri-eye-line"></i></a>
                            <a title="Hapus Data" class="btn btn-danger m-1" href="{{ url('smkbisa/berita-sekolah/delete/'. $row['berita_sekolah_uuid'])}}"><i class="ri-delete-bin-line"></i></a>
                            <a class="btn btn-{{ ($row['publish'] == '0') ? 'info' : 'light' }} m-1" href="{{ url('smkbisa/berita-sekolah/publish/'. $row['berita_sekolah_uuid'])}}">{{ ($row['publish'] == '0') ? 'Publish' : 'Unpublish' }}</a>
                          </td>
                          <td>{{ $row['berita_sekolah_title'] }}</td>
                          <td style="white-space:nowrap">{{ $row['berita_sekolah_total_seen'] }}</td>
                          <td style="white-space:nowrap">{{ $row['created_by_name'] }}</td>
                          <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($row['created_at']))) }}</td>
                          <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($row['updated_at']))) }}</td>
                        </tr>
                      @endforeach  
                    @else
                        <tr>
                          <td colspan="7">
                            <h5 class="text-center">Belum Ada Berita Sekolah</h5>
                          </td>
                        </tr>
                      @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

@endsection