<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('blog')->group(function() {
    Route::get('/news', 'BlogController@index');
    Route::get('/news/{uuid}', 'BlogController@detail');
});

Route::prefix('smkbisa/berita-sekolah')->group(function() {
    Route::get('/', 'BlogController@list_berita');
    Route::get('/tambah', 'BlogController@create');
    Route::post('/save_publish', 'BlogController@save_publish');
    Route::post('/update/{uuid}', 'BlogController@update');
    Route::post('/save_draft', 'BlogController@save_draft');
    Route::get('/detail/{uuid}', 'BlogController@detail_berita');
    Route::get('/publish/{uuid}', 'BlogController@publish');
    Route::get('/delete/{uuid}', 'BlogController@delete');
    Route::get('/edit/{uuid}', 'BlogController@edit');
});