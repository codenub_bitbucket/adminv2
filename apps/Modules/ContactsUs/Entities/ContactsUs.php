<?php

namespace Modules\ContactsUs\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactsUs extends Model
{
    protected $table 		= "mailbox";
    protected $primaryKey 	= "mailbox_serial_id";
    protected $guarded = array('mailbox_serial_id');
    public $timestamps = false;
}
