<?php

namespace Modules\ContactsUs\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactsUsFields extends Model
{
    protected $table 		= "mailbox_fields";
    protected $primaryKey 	= "mailbox_fields_serial_id";
    protected $guarded = array('mailbox_fields_serial_id');
    public $timestamps = false;
}
