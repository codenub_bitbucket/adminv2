<?php

namespace Modules\ContactsUs\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Mail;

use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;
use Modules\ContactsUs\Http\Controllers\ContactsUsHelper as contactsus_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class ContactsUsController extends Controller
{
    var $module       = 'Mailbox';
	var $table_module = 'mailbox';
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $auh = new aboutus_helper();

        $info = $auh->getAllInfo();
        $info = $auh->convertInfoArray($info);

        $view_data = array(
            'data' => $info,
        );
        
        $view = 'contactsus::index';
		// $template = 'email.email';

        // $send_mail = Mail::send($template, $view_data, function($mail)
        // {
        //     $mail->from('smkkartikatama1@gmail.com', 'SMK KARTIKATAMA');
        //     $mail->to('rendifebrian42@gmail.com');
        //     $mail->subject('Test');
        // });

		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }

    }

    public function save(Request $request)
    {
        $data = $request->all();

        $ch = new contactsus_helper();
        
        $save = $ch->save($data);

        if ($save)
        {
            return redirect()->back()->with('success', 'Berhasil Mengirim Pesan');
        }
        else
        {
            return redirect()->back()->with('failed', 'Gagal Mengirim Pesan');
        }
    }

    public function mailbox()
    {
        $ch = new contactsus_helper();
        $sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
        
        $data = $ch->mailbox('get', $criteria);
        $count = $ch->mailbox('count');
		$fields = $sys->getFields($this->table_module);

        $view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $data,
			'count_data'     => $count,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);
        
        $view = 'contactsus::mailbox';

		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

    public function detail($uuid)
    {
        $ch = new contactsus_helper();
        $detail = $ch->get_detail($uuid);

        if (@count($detail) == 0) 
		{
            return redirect()->back();
        }
        
        $view_data = array(
            'table_module'   => $this->table_module,
            'content' => $detail,
		);
        
        $view = 'contactsus::mailbox_detail';

		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }

    }
}
