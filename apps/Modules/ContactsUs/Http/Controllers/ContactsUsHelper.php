<?php
namespace Modules\ContactsUs\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

class ContactsUsHelper
{
  var $table_module = 'mailbox';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_contactsus = 'Modules\ContactsUs\Entities\ContactsUs';

  function save($data)
  {
    unset($data['_token']);

    $data['mailbox_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_contactsus::insert($data);

    return $query;
  }

  public function mailbox($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_contactsus::select("*")
                              ->where('deleted', 0)
                              ->orderBy('mailbox_serial_id', 'DESC');

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_contactsus::where('mailbox_uuid', $uuid)->first();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }
}