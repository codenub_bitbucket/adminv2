@extends('layout.home.app')

@section('content')

<section class="py-2">
  <div class="container">
    <nav aria-label="breadcrumb" class="float-right mt-2">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ URL('home') }}"><i class="ti-home"></i> Halaman Utama</a></li>
        <li class="breadcrumb-item">Hubungi</li>
      </ol>
    </nav>
  </div>

	<!-- =======================
	contact -->
	<section class="contact-page">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 mx-auto">
					<div class="title text-center">
						<h2>Contact Us</h2>
						{{-- <p>
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p> --}}
						@if (\Session::has('success'))
						<div class="alert alert-success" id="success-alert">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<strong>Berhasil</strong>
								{!! \Session::get('success') !!}
						</div>
						<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
						<script>
							$(document).ready(function() {
								setTimeout(() => {
									$("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
										$("#success-alert").slideUp(500);
									});
								}, 1000);
							});
						</script>
						@endif
						@if (\Session::has('failed'))
						<div class="alert alert-danger" id="failed-alert">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<strong>Berhasil</strong>
								{!! \Session::get('failed') !!}
						</div>
						<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
						<script>
							$(document).ready(function() {
								setTimeout(() => {
									$("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
										$("#failed-alert").slideUp(500);
									});
								}, 1000);
							});
						</script>
						@endif
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-6 mb-5">
					<div class="contact-box h-100 bg-dark px-3 py-4" >
						<!-- Phone -->
						<div class="contact-info all-text-white">
							<div class="contact-box-icon"><i class="ti-map-alt"></i></div>
							<h5 class="mb-2">{{ isset($data['location']) ? $data['location']['label'] : 'Alamat'}}</h5>
							<p>{{ isset($data['location']) && $data['location']['description'] !== '' ? $data['location']['description'] : 'Belum Ada '.$data['location']['label']}}.</p>
						</div>
						<!-- Email -->
						<div class="contact-info all-text-white">
							<div class="contact-box-icon"><i class="ti-email"></i></div>
							<h5 class="mb-2">{{ isset($data['email']) ? $data['email']['label'] : 'e-Mail'}}</h5>
							<p>{{ isset($data['email']) && $data['email']['description'] !== '' ? $data['email']['description'] : 'Belum Ada '.$data['email']['label']}}.</p>
						</div>
						<!-- Phone -->
						<div class="contact-info all-text-white">
							<div class="contact-box-icon"><i class="ti-panel"></i></div>
							<h5 class="mb-2">{{ isset($data['phone']) ? $data['phone']['label'] : 'Telepon'}}</h5>
							<p>{{ isset($data['phone']) && $data['phone']['description'] !== '' ? $data['phone']['description'] : 'Belum Ada '.$data['phone']['label']}}.</p>
						</div>

						<div class="contact-info all-text-white">
							<div class="contact-box-icon"><i class="ti-instagram"></i></div>
							<h5 class="mb-2">{{ isset($data['instagram']) ? $data['instagram']['label'] : 'Instagram'}}</h5>
							<p>{{ isset($data['instagram']) && $data['instagram']['description'] !== '' ? $data['instagram']['description'] : 'Belum Ada '.$data['instagram']['label']}}.</p>
						</div>

						<div class="contact-info all-text-white">
							<div class="contact-box-icon"><i class="ti-facebook"></i></div>
							<h5 class="mb-2">{{ isset($data['facebook']) ? $data['facebook']['label'] : 'Facebook'}}</h5>
							<p>{{ isset($data['facebook']) && $data['facebook']['description'] !== '' ? $data['facebook']['description'] : 'Belum Ada '.$data['facebook']['label']}}.</p>
						</div>
					</div>
				</div>
				<!-- contact form -->
				<div class="col-md-6">
					<div class="h-100">
						{{-- <h3>Connect with us</h3>
						<p>Get in touch with us to see how we can help you with your educations</p> --}}
						<div class="contact-form" >
							<!-- Start main form -->
							<form action="{{ URL('contacts-us/submit') }}" method="POST" >
								@csrf
								<div class="row">
									<div class="col-md-6">
										<!-- name -->
										<span class="form-group">
											<label > Nama</label>
											<span class="text-danger">*</span>
											<input id="con-name" name="mailbox_from_name" type="text" class="form-control" placeholder="Name" required>
										</span>
									</div>
									<div class="col-md-6">
										<!-- email -->
										<span class="form-group">
											<label > Email</label>
											<span class="text-danger">*</span>
											<input id="con-email" name="mailbox_from_email" type="email" class="form-control" placeholder="E-mail" required>
										</span>
									</div>
									<div class="col-md-12">
										<!-- Subject -->
										<span class="form-group">
											<label > Subjek</label>
											<span class="text-danger">*</span>
											<input id="con-subject" name="mailbox_subject" type="text" class="form-control" placeholder="Subject" required>
										</span>
									</div>
									<div class="col-md-12">
										<!-- Message -->
										<span class="form-group">
											<label > Pesan</label>
											<span class="text-danger">*</span>
											<textarea id="con-message" name="mailbox_message" cols="40" rows="6" class="form-control" placeholder="Message" required></textarea>
										</span>
									</div>
									<!-- submit button -->
									<div class="col-md-12 text-center"><button class="btn btn-dark btn-block" type="submit">Send Message</button></div>
								</div>
							</form>
							<!-- End main form -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- =======================
  contact -->
  
  <!-- =======================
	Address-->
	<section class="py-0">
		<div class="container-fluid">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15895.047505973429!2d105.2994077!3d-5.1419933!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3097d4f84dbe215c!2sSMK%20Kartikatama%201%20Metro!5e0!3m2!1sid!2sid!4v1611845012968!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>
	</section>
	<!-- =======================
	Address-->
</section>

<br><br><br>


@endsection
