@extends('app')
@section('title', 'Jurusan')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Jurusan</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3 align-items-center d-flex">
                <div class="col-6">
                  <h4> Daftar Pesan Masuk</h4>
                  {{-- <input class="form-control" type="text" value="" placeholder="Search here..."> --}}
                </div>
                <div class="col-6 text-right">
                    @include('component.filter_show')
                  {{-- <a class="btn btn-primary px-4" href="{{ url('smkbisa/pendaftar/tambah') }}">
                    + Siswa Baru
                  </a> --}}
                </div>
              </div>
              <div class="table-responsive">
                <table class="table mb-0">
                    <thead>
                      <tr>
                        <th>#</th>
                        @foreach ($fields as $f)
                          @if ($f[$table_module.'_fields_name'] !== $table_module . '_message')
                            <th style="white-space:nowrap">{{ $f[$table_module.'_fields_label'] }}</th>
                          @endif
                        @endforeach
                        <th style="white-space:nowrap">Created</th>
                        <th style="white-space:nowrap">Updated</th>
                        <th style="white-space:nowrap">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (@count($list_data) > 0)
                        @php
                            $i = ($list_data->currentPage() * $list_data->perPage()) - $list_data->perPage() + 1;
                        @endphp
                            @foreach ($list_data as $key => $item)
                              <tr>
                                <td>{{$i++}}</td>
                                @foreach ($fields as $f)
                                  @if ($f[$table_module.'_fields_name'] !== $table_module . '_message')
                                    <td style="white-space:nowrap">{{ $item[$f[$table_module.'_fields_name']] }}</td>
                                  @endif
                                @endforeach
                                <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
                                <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
                                <td style="white-space:nowrap">
                                    <a title="Lihat" class="btn btn-success" href="{{ url('smkbisa/mailbox/detail/' . $item[$table_module.'_uuid'])}}"><i class="ri-eye-line"></i></a>
                                </td>
                              </tr>
                            @endforeach
                        @else
                          <tr>
                            <td colspan="11">
                              <h5 class="text-center">Belum Ada Pesan</h5>
                            </td>
                          </tr>
                        @endif
                    </tbody>
                  </table>
                @include('component.pagination')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
@endsection