<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('contacts-us')->group(function() {
    Route::get('/', 'ContactsUsController@index');
    Route::post('/submit', 'ContactsUsController@save');
});

Route::prefix('smkbisa/mailbox')->group(function() {
    Route::get('/', 'ContactsUsController@mailbox');
    Route::get('/detail/{uuid}', 'ContactsUsController@detail');
});
