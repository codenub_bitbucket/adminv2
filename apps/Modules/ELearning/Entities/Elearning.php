<?php

namespace Modules\Elearning\Entities;

use Illuminate\Database\Eloquent\Model;

class Elearning extends Model
{
    protected $table 		= "elearning";
		protected $primaryKey 	= "elearning_serial_id";
		protected $guarded = array('elearning_serial_id');
		public $timestamps = false;
}
