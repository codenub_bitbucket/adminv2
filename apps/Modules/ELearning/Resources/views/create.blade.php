@extends('app')
@section('title', ($function == 'edit') ? 'Edit Siswa' : 'Tambah Siswa')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Tambah E-Learning</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">E-Learning</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <div class="card">
            <div class="card-body">
              <form action="{{ ($function == 'edit') ? URL('smkbisa/elearning/update/'.$data[$table_module.'_uuid']) : URL('smkbisa/elearning/save') }}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>E-Learning Name</label>
                      @include('input.text', array(
                          'table_module'            => $table_module,
                          'fields_name'             => 'elearning_name', 
                          'fields_label'             => "E-Learning Name", 
                          'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                          'required'                => "required",
                      ))                      
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Kelas</label>
                      @include('input.singleoption', array(
                          'table_module'            => $table_module,
                          'fields_name'             => 'elearning_kelas_id', 
                          'fields_label'             => "Kelas", 
                          'dropdown'                => isset($kelas) ? $kelas : array(),
                          'required'                => "required",
                      ))                      
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Jurusan</label>
                      @include('input.singleoption', array(
                          'table_module'            => $table_module,
                          'fields_name'             => 'elearning_jurusan_id', 
                          'fields_label'             => "Jurusan", 
                          'dropdown'                => isset($jurusan) ? $jurusan : array(),
                          'required'                => "required",
                      ))                      
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>E-Learning File</label>
                      @include('input.s_file', array(
                          'table_module'            => $table_module,
                          'fields_name'             => 'elearning_file', 
                          'fields_label'             => 'E-Learning File', 
                          'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                          'required'                => "required",
                      ))                      
                    </div>
                  </div>
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  <a class="btn btn-light px-3 mr-2" href="{{ url('smkbisa/elearning') }}">
                    Cancel
                  </a>
                  <button class="btn btn-primary px-4" type="submit">
                    {{ ($function == 'save') ? 'Save' : 'Update'}}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
@endsection