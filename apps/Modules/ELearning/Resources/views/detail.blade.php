@extends('app')
@section('title', 'E-Learning Detail')
@section('content')
<div class="main-content">
	<div class="page-content">
        <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">E-Learning Detail</h4>
                    <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Elearning</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12">
                <div class="bg-primary div-profile-user px-4 py-4 mb-3">
                    <div class="media">
                        <div class="media-body">
                                <h4 class="mt-0">{{ $content['elearning_name'] }}</h4>
                                <p class="mb-0 text-white">
                                    {{ $content['elearning_kelas_name'] }} - {{ $content['elearning_jurusan_name']}}
                                </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <embed class="embed-pdf" src="{{ $content['elearning_file'] }}" width="600" height="500" alt="pdf" />
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Date Created</p>
                                <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['created_at']))) }}</h6>
                            </li>
                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Date Modified</p>
                                <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['updated_at']))) }}</h6>
                            </li>
                            <li class="list-item border-bottom pb-2 mb-1">
                                <p class="mb-1">Created By</p>
                                <h6>{{ $content['created_by_name'] }}</h6>
                            </li>
                        </ul>
                        <a class="btn btn-block btn-danger text-white" href="{{ url('smkbisa/elearning/delete/'. $content[$table_module.'_uuid']) }}">
                            Delete
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div> <!-- container-fluid -->
	</div>
</div>
@endsection