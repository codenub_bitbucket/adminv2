@extends('app')
@section('title', 'E-Learning')
@section('content')
<div class="main-content">

	<div class="page-content">
		<div class="container-fluid">
			<!-- start page title -->
			<div class="row">
				<div class="col-12">
					<div class="page-title-box d-flex align-items-center justify-content-between">
						<h4 class="mb-0">SMK Kartikatama | E-Learning</h4>
						@if (\Session::has('success'))
							<div class="alert alert-success" id="success-alert">
											<button type="button" class="close" data-dismiss="alert">x</button>
											<strong>Berhasil!.</strong>
											{!! \Session::get('success') !!}
							</div>
							<script>
									$(document).ready(function() {
											setTimeout(() => {
													$("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
															$("#success-alert").slideUp(500);
													});
											}, 30000);
									});
							</script>
						@endif
						@if (\Session::has('failed'))
							<div class="alert alert-danger" id="failed-alert">
											<button type="button" class="close" data-dismiss="alert">x</button>
											<strong>Gagal!.</strong>
											{!! \Session::get('failed') !!}
							</div>
							<script>
									$(document).ready(function() {
											setTimeout(() => {
													$("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
															$("#failed-alert").slideUp(500);
													});
											}, 30000);
									});
							</script>
						@endif
						<div class="page-title-right">
							<ol class="breadcrumb m-0">
								<li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
								<li class="breadcrumb-item active">E-Learning</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<!-- end page title -->

			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="d-lg-flex align-items-center justify-content-between mb-4">
                <section class="d-flex align-items-center w-100" id="select-area" style="display:none!important">
                  <div class="w-50">
                    <span style="font-weight: bold">Total Selected : <span id="count_select"></span></span>
                  </div>
                  <div class="w-50">
                    <form name="form_mass_delete" action="{{ URL('smkbisa/elearning/mass_delete') }}" method="post">
                      @csrf
                      <input type="hidden" name="data_selected" value="">
                    </form>
                    <button type="button" class="btn btn-danger float-right" onclick="confirmDelete()">Delete</button>
                    <button type="button" class="btn btn-secondary float-right mr-2 cancel_mass_delete">Cancel</button>
                  </div>
                </section>
                <div class="align-items-center d-lg-flex d-none w-50 unshow-select">
                  @include('component.filter_show')
                  @include('component.search')
                </div>
                <div class="d-flex align-items-center unshow-select">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/elearning/tambah') }}">
                    + E-Learning
                  </a>
                </div>
              </div>
							<div class="table-responsive">
								<table class="table mb-0">
									<thead>
										<tr>
											<th style="white-space:nowrap">Action</th>
											<th style="white-space:nowrap">E-learning Name</th>
											<th style="white-space:nowrap">E-Learning Class</th>
											<th style="white-space:nowrap">E-Learning Major</th>
											<th style="white-space:nowrap">Created By</th>
											<th style="white-space:nowrap">Created At</th>
											<th style="white-space:nowrap">Updated At</th>
										</tr>
									</thead>
									<tbody>
											@if (@count($list_data) > 0)
													@foreach ($list_data as $key => $item)
															<tr>
                                  <td style="white-space:nowrap">
                                      <a title="Lihat" class="btn btn-success" href="{{ url('smkbisa/elearning/detail/' . $item[$table_module.'_uuid'])}}"><i class="ri-eye-line"></i></a>
                                      <a title="Hapus" class="btn btn-danger" href="{{ url('smkbisa/elearning/delete/' . $item[$table_module.'_uuid'])}}"><i class="ri-delete-bin-line"></i></a>
                                  </td>
																	<td style="white-space:nowrap">{{ $item['elearning_name'] }}</td>
																	<td style="white-space:nowrap">{{ $item['elearning_kelas_name'] }}</td>
																	<td style="white-space:nowrap">{{ $item['elearning_jurusan_name'] }}</td>
																	<td style="white-space:nowrap">{{ ($item['created_by_name'] == null) ? '-' : $item['created_by_name'] }}</td>
																	<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
																	<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
															</tr>
													@endforeach
											@else
												<tr>
													<td colspan="11">
														<h5 class="text-center">Belum Ada E-Learning</h5>
													</td>
												</tr>
											@endif
									</tbody>
								</table>
                @include('component.pagination')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container-fluid -->
	</div>
	<!-- End Page-content -->
</div>
<script>
  function doSelect()
  {
    $('#count_select').text($('.checkbox_data:checked').length);
    if($('.checkbox_data:checked').length > 0)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });

      $('input[name=data_selected]').val(JSON.stringify(val, true));
      
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

    let checked_data = $('.checkbox_data:checked').length;
    let check_all = $('.check_all').is(":checked");
    let count_all = '{{ @count($list_data) }}';

    console.log(check_all,checked_data,parseInt(count_all));
    if(check_all && checked_data < parseInt(count_all))
    {
      console.log('masuk');
      $('.check_all').prop('checked', false); 
    }
  }

  $(".check_all").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);

    let checked = $('.check_all').is(":checked");
    if(checked)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });
      $('input[name=data_selected]').val(JSON.stringify(val, true));

      $('#count_select').text($('.checkbox_data:checked').length);
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

  });

  $('.cancel_mass_delete').click(function () {
      $('input:checkbox').prop('checked', false);
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
  })
  
  function confirmDelete()
  {
    swal({
      title: "Are you sure?",
      text: "You will deleting selected data?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        if($('.checkbox_data:checked').length == 0)
        {
          swal("No data selected!");
        }
        else
        {
          $('form[name=form_mass_delete]').submit();
        }
      }
    });
  }
</script>
@endsection
