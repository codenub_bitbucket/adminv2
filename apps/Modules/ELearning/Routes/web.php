<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('smkbisa/elearning')->group(function() {
//     Route::get('/', 'ELearningController@index');
//     Route::get('/tambah', 'ELearningController@create');
//     Route::post('/save', 'ELearningController@save');
//     Route::get('/detail/{uuid}', 'ELearningController@detail');
//     Route::get('/delete/{uuid}', 'ELearningController@delete');
// });
