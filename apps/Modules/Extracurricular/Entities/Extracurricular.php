<?php

namespace Modules\Extracurricular\Entities;

use Illuminate\Database\Eloquent\Model;

class Extracurricular extends Model
{
    protected $table 		= "extracurricular";
    protected $primaryKey 	= "extracurricular_serial_id";
    protected $guarded = array('extracurricular_serial_id');
    public $timestamps = false;
}
