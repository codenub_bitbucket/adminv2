<?php

namespace Modules\Extracurricular\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtracurricularFields extends Model
{
    protected $table 		= "extracurricular_fields";
    protected $primaryKey 	= "extracurricular_fields_serial_id";
    protected $guarded = array('extracurricular_fields_serial_id');
    public $timestamps = false;
}
