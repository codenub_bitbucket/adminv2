<?php

namespace Modules\Extracurricular\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Extracurricular\Http\Controllers\ExtracurricularHelper as extracurriculer_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class ExtracurricularController extends Controller
{
	var $module       = 'Ekstrakulikuler';
	var $table_module = 'extracurricular';
	function __construct(Request $request)
	{
		$this->middleware('auth');
		session()->forget('redirect');
		session()->put('redirect', $request->path());
		
	}
	
	/**
	 * Display a listing of the resource.
	 * @return Renderable
	 */
	public function index(Request $request)
	{
		$criteria = $request->all();

		$sys = new sys();
		$e_helper = new extracurriculer_helper();

		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

		$count_data = $e_helper->list_data('count');

		$list_data = $e_helper->list_data('get', $criteria);
		$fields = $sys->getFields($this->table_module);

		$view = 'extracurricular::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_data,
			'count_data'     => $count_data,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Renderable
	 */
	public function create()
	{
		$sys = new sys();
		$e_helper = new extracurriculer_helper();

		$fields = $sys->getFields($this->table_module);

		$view = 'extracurricular::create';

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'function'			=> 'save'
		);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function add(Request $request)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		if ($request->file('extracurricular_photo')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['extracurricular_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['extracurricular_photo'] = $upload['file']->image;
				}
		}

		$e_helper = new extracurriculer_helper();

		$save = $e_helper->save($criteria, $users);

		if ($save)
		{
				return redirect('smkbisa/extracurricular')->with(['success' => 'Ekstrakulikuler Berhasil Disimpan']);
		}
		else
		{
				return redirect('smkbisa/extracurricular')->with('failed', 'Ekstrakulikuler Gagal Disimpan');
		}
	}

	public function delete($uuid)
	{
		$sys = new sys();

		$e_helper = new extracurriculer_helper();

		$delete = $e_helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/extracurricular')->with(['success' => 'Ekstrakulikuler Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/extracurricular')->with('failed', 'Ekstrakulikuler Gagal Dihapus');
		}
	}

	public function edit($uuid)
	{
		$sys = new sys();
		$e_helper = new extracurriculer_helper();

		$detail = $e_helper->get_detail($uuid);

		$fields = $sys->getFields($this->table_module);

		$view = 'extracurricular::create';

		$view_data = array(
			'table_module'   => $this->table_module,
			'data'         => $detail,
			'fields'         => $fields,
			'function'			=> 'edit'
		);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function update(Request $request, $uuid)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		if ($request->file('extracurricular_photo')) 
		{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['extracurricular_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['extracurricular_photo'] = $upload['file']->image;
				}
		}

		$e_helper = new extracurriculer_helper();

		$save = $e_helper->update($criteria, $users, $uuid);

		if ($save)
		{
				return redirect('smkbisa/extracurricular')->with(['success' => 'Ekstrakulikuler Berhasil Diperbaharui']);
		}
		else
		{
				return redirect('smkbisa/extracurricular')->with('failed', 'Ekstrakulikuler Gagal Diperbaharui');
		}
	}

	public function detail($uuid)
	{
		$sys = new sys();
		$e_helper = new extracurriculer_helper();

		$detail = $e_helper->get_detail($uuid);

		$view = 'extracurricular::detail';

		$view_data = array(
			'table_module'   => $this->table_module,
			'data' => $detail
		);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}
}
