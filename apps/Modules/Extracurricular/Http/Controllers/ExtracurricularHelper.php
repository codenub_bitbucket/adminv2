<?php
namespace Modules\Extracurricular\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

class ExtracurricularHelper
{
  var $table_module = 'extracurricular';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_extracurricular = 'Modules\Extracurricular\Entities\Extracurricular';

  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_extracurricular::orderby('extracurricular.extracurricular_name', 'ASC')
                                  ->where('extracurricular.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_extracurricular::select("extracurricular.*","users.name as created_by_name")
                                         ->where('extracurricular_uuid', $uuid)
                                         ->where('extracurricular.deleted', 0)
                                         ->leftjoin('users', 'users.id', "=", 'extracurricular.created_by')
                                         ->first();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;

  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_extracurricular::where('extracurricular_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['created_by'] = $users->id;
    $data['extracurricular_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_extracurricular::insert($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_extracurricular::where('extracurricular_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

}