@extends('app')
@section('title', 'Landing')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Tambah Ekstrakulikuler</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Ekstrakulikuler</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <form action="{{ ($function == 'edit') ? URL('smkbisa/extracurricular/update/'.$data[$table_module.'_uuid']) : URL('smkbisa/extracurricular/add') }}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="row">
                  <div class="col-md-12">
                    @foreach ($fields as $key => $value)
                    <div class="form-group">
                      <label>{{ $value[$table_module.'_fields_label'] }}</label>
                      @include('input.'.$value[$table_module.'_fields_input_type'], array(
                          'table_module'            => $table_module,
                          'fields_name'             => $value[$table_module.'_fields_name'], 
                          'fields_label'             => $value[$table_module.'_fields_label'], 
                          'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                          'required'                => $value[$table_module.'_fields_validation'],
                      ))
                    </div>
                    @endforeach   
                  </div>
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  <a class="btn btn-light px-3 mr-2" href="{{ url('smkbisa/extracurricular') }}">
                    Cancel
                  </a>
                  <button class="btn btn-primary px-4" type="submit">
                    {{ ($function == 'save') ? 'Save' : 'Update'}}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
@endsection