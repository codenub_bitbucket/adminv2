@extends('app')
@section('title', 'Landing')
@section('content')

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama | Ekstrakulikuler</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Ekstrakulikuler</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="d-lg-flex align-items-center justify-content-between mb-4">
                <div class="align-items-center d-lg-flex d-none w-50 unshow-select">
                  @include('component.filter_show')
                </div>
                <div class="d-flex align-items-center unshow-select">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/extracurricular/tambah') }}">
                    + Ekstrakulikuler
                  </a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table mb-0">
                  <thead>
                    <th style="white-space:nowrap">Action</th>
                    <tr>
                      @foreach ($fields as $f)
                        @if ($f[$table_module.'_fields_name'] !== $table_module . '_photo')
                            <th style="white-space:nowrap">{{ $f['extracurricular_fields_label'] }}</th>
                        @endif
                      @endforeach
                      <th style="white-space:nowrap">Created</th>
                      <th style="white-space:nowrap">Updated</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if (@count($list_data) > 0)
                          @foreach ($list_data as $key => $item)
                            <tr>
                              <td style="white-space:nowrap">
                                  <a title="Lihat" class="btn btn-success" href="{{ url('smkbisa/extracurricular/detail/' . $item[$table_module.'_uuid'])}}"><i class="ri-eye-line"></i></a>
                                  <a title="Edit" class="btn btn-info" href="{{ url('smkbisa/extracurricular/edit/' . $item[$table_module.'_uuid'])}}"><i class="ri-pencil-line"></i></a>
                                  <a title="Hapus" class="btn btn-danger" href="{{ url('smkbisa/extracurricular/delete/' . $item[$table_module.'_uuid'])}}"><i class="ri-delete-bin-line"></i></a>
                              </td>
                              @foreach ($fields as $f)
                                @if ($f[$table_module.'_fields_name'] !== $table_module . '_photo')
                                  @if ($f[$table_module.'_fields_input_type'] == 'date')
                                    <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item[$f[$table_module.'_fields_name']]))) }}</td>
                                  @elseif ($f[$table_module.'_fields_input_type'] == 'longtext' || $f[$table_module.'_fields_input_type'] == 'textarea')
                                    <td>{{ $item[$f[$table_module.'_fields_name']] }}</td>
                                  @else
                                    <td style="white-space:nowrap">{{ $item[$f[$table_module.'_fields_name']] }}</td>
                                  @endif
                                @endif
                              @endforeach
                              <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
                              <td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
                            </tr>
                          @endforeach
                      @else
                        <tr>
                          <td colspan="4">
                            <h5 class="text-center">Belum Ada Ekstrakurikuler</h5>
                          </td>
                        </tr>
                      @endif
                  </tbody>
                </table>
                @include('component.pagination')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

@endsection