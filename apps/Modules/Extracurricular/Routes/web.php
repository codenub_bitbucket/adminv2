<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/extracurricular')->group(function() {
    Route::get('/', 'ExtracurricularController@index');
    Route::get('/tambah', 'ExtracurricularController@create');
    Route::post('/add', 'ExtracurricularController@add');
    Route::get('/edit/{uuid}', 'ExtracurricularController@edit');
    Route::post('/update/{uuid}', 'ExtracurricularController@update');
    Route::get('/delete/{uuid}', 'ExtracurricularController@delete');
    Route::get('/detail/{uuid}', 'ExtracurricularController@detail');
});
