<?php

namespace Modules\Guru\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Kelas\Http\Controllers\KelasHelper as kelas_helper;
use Modules\Guru\Http\Controllers\GuruHelper as guru_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class GuruController extends Controller
{
    var $module       = 'Guru';
    var $table_module = 'guru';

    function __construct(Request $request)
    {
        $this->middleware('auth');
		session()->forget('redirect');
        session()->put('redirect', $request->path());
        
    }

    public function index(Request $request)
    {
        $criteria = $request->all();
        $sys = new sys();
        $guru_helper = new guru_helper();
        
        $criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
        $list_guru = $guru_helper->list_data('get', $criteria);
        $count_guru = $guru_helper->list_data('count');

        $fields = $sys->getFields($this->table_module);

        $view = 'guru::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_guru,
            'count_data'     => $count_guru,
            'data_per_page'  => $data_per_page,
            'fields'         => $fields
        );
        
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

     /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $sys = new sys();
        $k_helper = new kelas_helper();

        $fields = $sys->getFields($this->table_module);

				foreach ($fields as $key => $value) 
				{
					if ($value['guru_fields_input_type'] == 'singleoption' && $value['guru_fields_options'] !== null) 
					{
						if ($value['guru_fields_options'] == 'kelas_id') 
						{
							$list_kelas = $k_helper->list_data('get_dropdown');
							$fields[$key]['dropdown'] = $list_kelas;
						}
						else
						{
							$fields[$key]['dropdown'] = $sys->getDropdown($value['guru_fields_options']);
						}
					}
				}

        $view = 'guru::create';

        $view_data = array(
					'table_module'  => $this->table_module,
					'fields'        => $fields,
					'function' => 'save'
				);
        
				if(view()->exists($view))
				{
            return view($view, $view_data)->render();
        }
		}
		
		public function save(Request $request)
    {
			$helper = new guru_helper();
			$sys = new sys();

			$users = Auth::user();

			$data = $request->all();
			
			if ($request->hasFile('guru_photo')) 
			{
					$temp = new \Illuminate\Http\Request();
					$temp->replace(['file' => $data['guru_photo']]);
					
					$upload = $sys->uploadFile($temp);

					if ($upload['status'] == 200) 
					{
							$data['guru_photo'] = $upload['file']->image;
					}
			}

			$save = $helper->save($data, $users);

			if ($save)
			{
					return redirect('smkbisa/guru')->with(['success' => 'Guru Berhasil Disimpan']);
			}
			else
			{
					return redirect('smkbisa/guru')->with('failed', 'Guru Gagal Disimpan');
			}
		}

		public function edit(Request $request, $uuid)
		{
			$sys = new sys();
			$k_helper = new kelas_helper();
			$helper = new guru_helper();
			
			$guru = $helper->get_detail($uuid);
			$fields = $sys->getFields($this->table_module);
			
			foreach ($fields as $key => $value) 
			{
				if ($value['guru_fields_input_type'] == 'singleoption' && $value['guru_fields_options'] !== null) 
				{
					if ($value['guru_fields_options'] == 'kelas_id') 
					{
						$list_kelas = $k_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_kelas;
					}
					else
					{
						$fields[$key]['dropdown'] = $sys->getDropdown($value['guru_fields_options']);
					}
				}
			}
			
			$view = 'guru::create';
			
			$view_data = array(
				'table_module'   => $this->table_module,
				'fields'         => $fields,
				'data' 					 => $guru,
				'function'   				 => 'edit'	
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function update(Request $request, $uuid)
		{
			$users = Auth::user();

			$sys = new sys();
			$criteria = $request->all();

			if ($request->file('guru_photo')) 
			{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['guru_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['guru_photo'] = $upload['file']->image;
				}
			}

			$helper = new guru_helper();

			$update = $helper->update($criteria, $users, $uuid);

			if ($update)
			{
				return redirect('smkbisa/guru')->with(['success' => 'Guru Berhasil Diperbaharui']);
			}
			else
			{
				return redirect('smkbisa/guru')->with('failed', 'Guru Gagal Diperbaharui');
			}
		}

		public function delete($uuid)
		{
			$sys = new sys();

			$helper = new guru_helper();

			$delete = $helper->delete($uuid);

			if ($delete)
			{
					return redirect('smkbisa/guru')->with(['success' => 'Guru Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/guru')->with('failed', 'Guru Gagal Dihapus');
			}
		}

		public function mass_delete(Request $request)
		{
			$data = $request->all();
			$helper = new guru_helper();

			$data = json_decode($data['data_selected'], true);
			$delete = $helper->mass_delete($data);

			if ($delete)
			{
					return redirect('smkbisa/guru')->with(['success' => 'Guru Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/guru')->with('failed', 'Guru Gagal Dihapus');
			}
		}
}
