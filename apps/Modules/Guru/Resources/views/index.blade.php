@extends('app')
@section('title', 'Guru')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama | Guru</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Guru</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="d-lg-flex align-items-center justify-content-between mb-4">
                <section class="d-flex align-items-center w-100" id="select-area" style="display:none!important">
                  <div class="w-50">
                    <span style="font-weight: bold">Total Selected : <span id="count_select"></span></span>
                  </div>
                  <div class="w-50">
                    <form name="form_mass_delete" action="{{ URL('smkbisa/guru/mass_delete') }}" method="post">
                      @csrf
                      <input type="hidden" name="data_selected" value="">
                    </form>
                    <button type="button" class="btn btn-danger float-right" onclick="confirmDelete()">Delete</button>
                    <button type="button" class="btn btn-secondary float-right mr-2 cancel_mass_delete">Cancel</button>
                  </div>
                </section>
                <div class="align-items-center d-lg-flex d-none w-50 unshow-select">
                  @include('component.filter_show')
                  @include('component.search')
                </div>
                <div class="d-flex align-items-center unshow-select">
                  <a class="btn btn-primary px-4" href="{{ url('smkbisa/guru/tambah') }}">
                    + Guru
                  </a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table mb-0">
                  <thead>
                    <tr>
                      <th style="white-space:nowrap">Action</th>
                      <th>
                        <input type="checkbox" class="check_all">
                      </th>
                      @foreach ($fields as $f)
                        @if ($f[$table_module.'_fields_name'] !== $table_module . '_photo' && $f[$table_module.'_fields_name'] !== $table_module . '_desc')
                            <th style="white-space:nowrap">{{ $f['guru_fields_label'] }}</th>
                        @endif
                      @endforeach
                      <th style="white-space:nowrap">Created</th>
                      <th style="white-space:nowrap">Updated</th>
                    </tr>
                  </thead>
                  <tbody>
										@if (@count($list_data) > 0)
												@foreach ($list_data as $key => $item)
													<tr>
                            <td style="white-space:nowrap">
                                <a title="Edit" class="btn btn-info" href="{{ url('smkbisa/guru/edit/' . $item[$table_module.'_uuid'])}}"><i class="ri-pencil-line"></i></a>
                                <a title="Hapus" class="btn btn-danger" href="{{ url('smkbisa/guru/delete/' . $item[$table_module.'_uuid'])}}"><i class="ri-delete-bin-line"></i></a>
                            </td>
														<td>
															<input type="checkbox" class="checkbox_data" multiple="true" onclick="doSelect()" value="{{$item[$table_module.'_uuid']}}" autocomplete="off">
														</td>
														@foreach ($fields as $f)
															@if (
																$f[$table_module.'_fields_name'] !== 'guru_photo'
															)
																@if ($f[$table_module.'_fields_name'] == 'guru_kelas_id')
																	<td style="white-space:nowrap">{{ $item['guru_kelas_name'] }}</td>
																@elseif($f[$table_module.'_fields_name'] == 'guru_mata_pelajaran_id')
																	<td style="white-space:nowrap">{{ $item['mata_pelajaran_name'] }}</td>
																@else
																	@if ($f[$table_module.'_fields_input_type'] == 'date')
																		<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item[$f[$table_module.'_fields_name']]))) }}</td>
																	@else
																		<td style="white-space:nowrap">{{ $item[$f[$table_module.'_fields_name']] }}</td>
																	@endif
																@endif
															@endif
														@endforeach
														<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
														<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
													</tr>
												@endforeach
										@else
											<tr>
												<td colspan="10">
													<h5 class="text-center">Belum Ada Guru</h5>
												</td>
											</tr>
										@endif
                  </tbody>
                </table>
                @include('component.pagination')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
<script>
  function doSelect()
  {
    $('#count_select').text($('.checkbox_data:checked').length);
    if($('.checkbox_data:checked').length > 0)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });

      $('input[name=data_selected]').val(JSON.stringify(val, true));
      
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

    let checked_data = $('.checkbox_data:checked').length;
    let check_all = $('.check_all').is(":checked");
    let count_all = '{{ @count($list_data) }}';

    console.log(check_all,checked_data,parseInt(count_all));
    if(check_all && checked_data < parseInt(count_all))
    {
      console.log('masuk');
      $('.check_all').prop('checked', false); 
    }
  }

  $(".check_all").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);

    let checked = $('.check_all').is(":checked");
    if(checked)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });
      $('input[name=data_selected]').val(JSON.stringify(val, true));

      $('#count_select').text($('.checkbox_data:checked').length);
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

  });

  $('.cancel_mass_delete').click(function () {
      $('input:checkbox').prop('checked', false);
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
  })
  
  function confirmDelete()
  {
    swal({
      title: "Are you sure?",
      text: "You will deleting selected data?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        if($('.checkbox_data:checked').length == 0)
        {
          swal("No data selected!");
        }
        else
        {
          $('form[name=form_mass_delete]').submit();
        }
      }
    });
  }
</script>
@endsection