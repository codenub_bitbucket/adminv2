<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/guru')->group(function() {
    Route::get('/', 'GuruController@index');
    Route::get('/tambah', 'GuruController@create');
    Route::post('/save', 'GuruController@save');
    Route::get('/delete/{uuid}', 'GuruController@delete');
    Route::get('/edit/{uuid}', 'GuruController@edit');
    Route::post('/update/{uuid}', 'GuruController@update');

    Route::post('/mass_delete', 'GuruController@mass_delete');
});
