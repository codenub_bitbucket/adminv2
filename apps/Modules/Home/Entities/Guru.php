<?php

namespace Modules\Guru\Entities;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table 		= "guru";
    protected $primaryKey 	= "guru_serial_id";
    protected $guarded = array('guru_serial_id');
    public $timestamps = false;
}
