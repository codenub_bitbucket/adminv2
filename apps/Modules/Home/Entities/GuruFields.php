<?php

namespace Modules\Guru\Entities;

use Illuminate\Database\Eloquent\Model;

class GuruFields extends Model
{
    protected $table 		= "guru_fields";
    protected $primaryKey 	= "guru_fields_serial_id";
    protected $guarded = array('guru_fields_serial_id');
    public $timestamps = false;
}
