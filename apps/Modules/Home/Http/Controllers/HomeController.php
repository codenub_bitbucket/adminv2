<?php

namespace Modules\Home\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;
use Modules\Blog\Http\Controllers\BlogHelper as blog_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
			$auh = new aboutus_helper();
			$bh = new blog_helper();

			$criteria_blog = array(
					'show' => 10
			);
			$blog = $bh->list_data('get', $criteria_blog);

			$info = $auh->getAllInfo();
			$info = $auh->convertInfoArray($info);

			$view_data = array(
					'data' => $info,
					'blog' => $blog,
					'data_per_page'  => 10,

			);
			
			$view = 'home::index';


			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}
		
		
		
		

    
}
