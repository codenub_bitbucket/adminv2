@extends('layout.home.app')

@section('content')
<!-- =======================
	Main Banner -->
	<section class="p-0 container">
		<div class="swiper-container height-600-responsive">
			<div class="swiper-wrapper">
				<!-- slide 1-->
				<div class="swiper-slide ">
					<div class="container h-100">
						<div class="row d-flex h-100">
							<div class="col-lg-6 col-xl-6 mr-auto slider-content justify-content-center align-self-center align-items-start text-left">
								<h2 class="text-dark" style="font-size: 40px; font-weight: bold;">
									{{ (isset($data['banner_text']) ? $data['banner_text']['description'] : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit') }}
								</h2>
								<p class="" style="font-size: 20px; padding-right: 10%; margin-top: 40px;">
									{!! (isset($data['banner_desc']) ? nl2br($data['banner_desc']['description']) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit') !!}
								</p>
							</div>
							<div class="col-lg-6 col-xl-6 mr-auto slider-content justify-content-center align-self-center align-items-start text-left">
								<img src="{{ (isset($data['banner_img']) ? URL('apps/public/'.$data['banner_img']['description']) : 'resources/images/banner/hero-1.png') }}" class="w-100">
							</div>
						</div>
					</div>
                </div>
			</div>
			<!-- Slider buttons -->
			{{-- <div class="swiper-button-next"><i class="ti-angle-right"></i></div>
			<div class="swiper-button-prev"><i class="ti-angle-left"></i></div>
			<div class="swiper-pagination"></div> --}}
		</div>
	</section>
	<!-- =======================
  Main banner -->
  
<!-- =======================
	service -->
	{{-- <section class="service bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						<img src="assets/images/icon-report.png">
						<h3 class="feature-box-title font-weight-bold">Report</h3>
						<p class="feature-box-desc">
							Lorem Ipsum has been the industry's standard dummy text ever since.
						</p>
						<a class="mt-3" href="#">Open</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						<img src="assets/images/icon-graduations.png">
						<h3 class="feature-box-title font-weight-bold">Graduations Info</h3>
						<p class="feature-box-desc">
							Lorem Ipsum has been the industry's standard dummy text ever since.
						</p>
						<a class="mt-3" href="#">Open</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 h-100 bg-primary">
						<h3 class="feature-box-title text-white text-download-apps">SMK Kartikatama E-learning</h3>
						<p class="feature-box-desc text-white">
							Download SMK Kartikatama Metro Apps for e-learning
						</p>
						<div class="d-flex align-items-center mt-4">
							<a href="#" class="btn btn-dark text-left w-50 mr-2 my-0">
								<div class="media align-items-center">
									<span class="fa fa-android fa-2x mr-3"></span>
									<span class="d-block">
									<span class="d-block small">Get it on</span>
									<strong class="d-block">Google Play</strong>
									</span>
								</div>
							</a>
							<a href="#" class="btn btn-dark text-left w-50 my-0">
								<div class="media align-items-center">
									<span class="fa fa-apple fa-2x mr-3"></span>
									<span class="d-block">
									<span class="d-block small">Download from</span>
									<strong class="d-block">App Store</strong>
									</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!-- =======================
	service -->

	<section class="why-us p-0">
		<div class="container-fluid">
			<div class="row">
				<!--why us left-->
				<div class="col-lg-6 d-none d-lg-block bg-light p-0" style="background:url({{ (isset($data['vm_img']) ? URL('apps/public/'.$data['vm_img']['description']) : 'resources/images/bg-visimisi.png') }}) no-repeat; background-size:cover; height: 480px; background-position: center;">
					{{-- <img src="{{ (isset($data['vm_img']) ? $data['vm_img']['description'] : 'assets/images/bg-visimisi.png') }}"> --}}
				</div>
				<!--why us right-->
				<div class="col-lg-6 bg-primary px-4 py-5 p-lg-5 text-white align-items-center d-flex">
					<div class="h-100 m-auto align-items-center d-flex">
						<div>
							<div class="title text-left p-0">
								<h2 class="text-white">{{ (isset($data['our_vision']) ? $data['our_vision']['label'] : 'Our Vision') }}</h2>
								<p>
									{!! (isset($data['our_vision']) ? nl2br($data['our_vision']['description']) : 'Our Vision') !!}
								</p>
							</div>
							<div class="title text-left p-0 mt-5">
								<h2 class="text-white">{{ (isset($data['our_mision']) ? $data['our_mision']['label'] : 'Our Mision') }}</h2>
								<p>
									{!! (isset($data['our_mision']) ? nl2br($data['our_mision']['description']) : 'Our Mision') !!}
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- =======================
	blog -->
	<section class="blog pb-0">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-12 mx-auto align-items-center d-flex justify-content-between">
					<div class="title">
						<span class="pre-title">Lihat Kabar Berita Terbaru.</span>
						<h2>Berita Terbaru</h2>
					</div>
					<a class="btn btn-outline-primary mb-0" href="{{URL("blog")}}">Lihat Lebih</a>
				</div>
			</div>
			<div class="row">
                @if (@count($blog) > 0)
                <div class="col-md-{{(@count($blog) <= 3) ? '4' : '12'}}">
                    <div class="owl-carousel arrow-dark arrow-hover" data-dots="false" data-items-xl="{{ (@count($blog) <= 3) ? '1' : '3'}}" data-items-lg="{{ (@count($blog) <= 3) ? '1' : '3'}}" data-items-md="{{ (@count($blog) <= 3) ? '1' : '2'}}" data-items-sm="{{ (@count($blog) <= 3) ? '1' : '2'}}" data-items-xs="{{ (@count($blog) <= 3) ? '1' : '1'}}" data-autoplay="4000" data-pause="false">
                        <!-- post -->
                        @foreach ($blog as $i)
												<div class="item">
														<div class="post">
																<img src="{{ URL("apps/public/".$i['berita_sekolah_thumbnail']) }}" style="height: 250px;">
																<div class="post-info">
																		<div class="post-author"><a href="{{ URL('blog/news/'.$i['berita_sekolah_uuid']) }}"> {{ $i['created_by_name'] }}</a></div>,
																		<div class="post-time"><a href="{{ URL('blog/news/'.$i['berita_sekolah_uuid']) }}">{{ date('d-m-Y', strtotime($i['created_at'])) }}</a></div>
																		<a class="post-title" href="{{ URL('blog/news/'.$i['berita_sekolah_uuid']) }}">{{ $i['berita_sekolah_title'] }}</a>
																</div>
														</div>
												</div>
												@endforeach
                    </div>
                </div>
                @else
                <h5 class="">Belum ada kabar berita terbaru.</h5>
                @endif
			</div>
		</div>
	</section>
	<!-- =======================
	blog -->

	<!-- =======================
	Address-->
	<section class="p5-4">
		<div class="container-fluid">
			<div class="card card-address">
				<div class="card-body">
					<ul class="list-unstyled">
						<li class="media mb-3"><i class="mr-3 display-8 ti-map-alt text-primary"></i>{{ (isset($data['location']) && $data['location']['description'] !== '' ? $data['location']['description'] : 'Belum Ada '.$data['location']['label']) }}</li>
						<li class="media mb-3"><i class="mr-3 display-8 ti-headphone-alt text-primary"></i> {{ (isset($data['phone']) && $data['phone']['description'] !== '' ? $data['phone']['description'] : 'Belum Ada '.$data['phone']['label']) }} </li>
						<li class="media mb-0"><i class="mr-3 display-8 ti-email text-primary"></i> {{ (isset($data['email']) && $data['email']['description'] !== '' ? $data['email']['description'] : 'Belum Ada '.$data['email']['label']) }}</li>
					</ul>
				</div>
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15895.047505973429!2d105.2994077!3d-5.1419933!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3097d4f84dbe215c!2sSMK%20Kartikatama%201%20Metro!5e0!3m2!1sid!2sid!4v1611845012968!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>
	</section>
	<!-- =======================
	Address-->
@endsection
