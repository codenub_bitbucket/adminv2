<?php

namespace Modules\Jurusan\Entities;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table 		= "jurusan";
    protected $primaryKey 	= "jurusan_serial_id";
    protected $guarded = array('jurusan_serial_id');
    public $timestamps = false;
}
