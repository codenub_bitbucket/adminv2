<?php

namespace Modules\Jurusan\Entities;

use Illuminate\Database\Eloquent\Model;

class JurusanFields extends Model
{
    protected $table 		= "jurusan_fields";
    protected $primaryKey 	= "jurusan_fields_serial_id";
    protected $guarded = array('jurusan_fields_serial_id');
    public $timestamps = false;
}
