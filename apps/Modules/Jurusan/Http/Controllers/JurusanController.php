<?php

namespace Modules\Jurusan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class JurusanController extends Controller
{
    var $module       = 'Jurusan';
    var $table_module = 'jurusan';

    function __construct(Request $request)
    {
        $this->middleware('auth');
		session()->forget('redirect');
        session()->put('redirect', $request->path());
        
    }

    public function index(Request $request)
    {
        $criteria = $request->all();
        $j_helper = new jurusan_helper();
        $sys = new sys();
        
        $criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

        $list_jurusan = $j_helper->list_data('get', $criteria);
        $count_jurusan = $j_helper->list_data('count');

        $fields = $sys->getFields($this->table_module);


        $view = 'jurusan::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_jurusan,
            'count_data'     => $count_jurusan,
            'data_per_page'  => $data_per_page,
            'fields'         => $fields
        );
        
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

    public function create()
    {
        $sys = new sys();

        $fields = $sys->getFields($this->table_module);
        foreach ($fields as $key => $value) 
        {
            if ($value['jurusan_fields_input_type'] == 'singleoption' && $value['jurusan_fields_options'] !== null) 
            {
                $fields[$key]['dropdown'] = $sys->getDropdown($value['jurusan_fields_options']);
            }
        }

        $view = 'jurusan::create';

        $view_data = array(
            'table_module'  => $this->table_module,
            'fields'        => $fields,
            'function' => 'save'
        );
        
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

    public function add(Request $request)
    {
        $users = Auth::user();

		$criteria = $request->all();
        $j_helper = new jurusan_helper();
        $sys = new sys();

        $status       = 500;
        $msg          = "Gagal Menyimpan Data Jurusan";

        $data = $request->all();
        
        if ($request->file('jurusan_photo')) 
		{
            $temp = new \Illuminate\Http\Request();
            $temp->replace(['file' => $data['jurusan_photo']]);
            
            $upload = $sys->uploadFile($temp);

            if ($upload['status'] == 200) 
            {
                    $data['jurusan_photo'] = $upload['file']->image;
            }
		}
        $save = $j_helper->save($data, $users);

        if ($save)
		{
            return redirect('smkbisa/jurusan')->with(['success' => 'Jurusan Berhasil Ditambah']);
		}
		else
		{
            return redirect('smkbisa/jurusan')->with('failed', 'Jurusan Gagal Ditambah');
		}
    }

    public function update(Request $request, $uuid)
		{
			$users = Auth::user();

			$sys = new sys();
			$criteria = $request->all();

			if ($request->file('jurusan_photo')) 
			{
					$temp = new \Illuminate\Http\Request();
					$temp->replace(['file' => $criteria['jurusan_photo']]);
					
					$upload = $sys->uploadFile($temp);

					if ($upload['status'] == 200) 
					{
							$criteria['jurusan_photo'] = $upload['file']->image;
					}
			}

			$j_helper = new jurusan_helper();

			$update = $j_helper->update($criteria, $users, $uuid);
        
			$back = 'smkbisa/'.$this->table_module;
			$segment = session()->get('back');

			if (!empty($segment)) 
			{
				$back = $segment;
			}
			if ($update)
			{
				return redirect($back)->with(['success' => 'Jurusan Berhasil Diperbaharui']);
			}
			else
			{
				return redirect($back)->with('failed', 'Jurusan Gagal Diperbaharui');
			}
    }
    
    public function delete($uuid)
	{
		$sys = new sys();

		$j_helper = new jurusan_helper();

		$delete = $j_helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/jurusan')->with(['success' => 'Jurusan Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/jurusan')->with('failed', 'Jurusan Gagal Dihapus');
		}
    }
    
    public function detail($uuid)
	{
		$sys = new sys();
		$j_helper = new jurusan_helper();
		
		$detail = $j_helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);

		if (@count($detail) == 0) 
		{
				return redirect('smkbisa/jurusan')->with('failed', "Jurusan Tidak Valid!");
		}

		$view = 'jurusan::detail';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
			'fields'				=> $fields
		);
		session()->forget('back');
		session()->put('back', 'smkbisa/jurusan/detail/'.$uuid);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
    }
    
    public function edit($uuid)
	{
		$j_helper = new jurusan_helper();
		$sys = new sys();
		$detail = $j_helper->get_detail($uuid);

		
		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
        {
            if ($value['jurusan_fields_input_type'] == 'singleoption' && $value['jurusan_fields_options'] !== null) 
            {
                $fields[$key]['dropdown'] = $sys->getDropdown($value['jurusan_fields_options']);
            }
        }

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		$view = 'jurusan::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new jurusan_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
			return redirect('smkbisa/jurusan')->with(['success' => 'Jurusan Berhasil Dihapus']);
		}
		else
		{
			return redirect('smkbisa/jurusan')->with('failed', 'Jurusan Gagal Dihapus');
		}
	}

}
