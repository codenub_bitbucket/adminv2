<?php
namespace Modules\Jurusan\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class JurusanHelper
{
  var $table_module = 'jurusan';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_jurusan = 'Modules\Jurusan\Entities\Jurusan';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_jurusan::orderby('jurusan.jurusan_name', 'ASC');
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('jurusan.jurusan_name',"LIKE", "%".$criteria['search']."%");
                                }
                                $query->where('jurusan.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("jurusan.jurusan_name as label", "jurusan.jurusan_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_jurusan::select("jurusan.*", "users.name as created_by_name")
                                  ->leftjoin('users', 'users.id',"=", "jurusan.created_by")
                                  ->where('jurusan.jurusan_uuid', $uuid)
                                  ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['created_by'] = $users->id;
    $data['jurusan_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_jurusan::insert($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_jurusan::where('jurusan_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_jurusan::where('jurusan_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
}