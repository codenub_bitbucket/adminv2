<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/jurusan')->group(function() {
    Route::get('/', 'JurusanController@index');
    Route::get('/tambah', 'JurusanController@create');
    Route::post('/add', 'JurusanController@add');
    Route::get('/delete/{uuid}', 'JurusanController@delete');
    Route::get('/edit/{uuid}', 'JurusanController@edit');
    Route::post('/update/{uuid}', 'JurusanController@update');
    Route::get('/detail/{uuid}', 'JurusanController@detail');

    Route::post('/mass_delete', 'JurusanController@mass_delete');
});
