<?php

namespace Modules\Kelas\Entities;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table 		= "kelas";
    protected $primaryKey 	= "kelas_serial_id";
    protected $guarded = array('kelas_serial_id');
    public $timestamps = false;
}
