<?php

namespace Modules\Kelas\Entities;

use Illuminate\Database\Eloquent\Model;

class KelasFields extends Model
{
    protected $table 		= "kelas_fields";
    protected $primaryKey 	= "kelas_fields_serial_id";
    protected $guarded = array('kelas_fields_serial_id');
    public $timestamps = false;
}
