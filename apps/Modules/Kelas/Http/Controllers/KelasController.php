<?php

namespace Modules\Kelas\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Kelas\Http\Controllers\KelasHelper as kelas_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class KelasController extends Controller
{
    var $module       = 'Kelas';
    var $table_module = 'kelas';

    function __construct(Request $request)
    {
        $this->middleware('auth');
		session()->forget('redirect');
        session()->put('redirect', $request->path());
        
    }

    public function index(Request $request)
    {
        $criteria = $request->all();
        $k_helper = new kelas_helper();
        $sys = new sys();
        
        $criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
        $data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

        $list_kelas = $k_helper->list_data('get', $criteria);
        $count_kelas = $k_helper->list_data('count');

        $fields = $sys->getFields($this->table_module);

        $view = 'kelas::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_kelas,
            'count_data'     => $count_kelas,
            'data_per_page'  => $data_per_page,
            'fields'         => $fields
        );
        
        // echo "<pre>";
        //     print_r($view_data);
        // exit();
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

    public function create()
    {
        $sys = new sys();

        $fields = $sys->getFields($this->table_module);
        foreach ($fields as $key => $value) 
        {
            if ($value['kelas_fields_input_type'] == 'singleoption' && $value['kelas_fields_options'] !== null) 
            {
                $fields[$key]['dropdown'] = $sys->getDropdown($value['kelas_fields_options']);
            }
        }

        $view = 'kelas::create';

        $view_data = array(
            'table_module'  => $this->table_module,
            'fields'        => $fields,
        );
        
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
        }
    }

    public function save(Request $request)
    {
        $k_helper = new kelas_helper();
        $sys = new sys();

        $status       = 500;
        $msg          = "Gagal Menyimpan Data Kelas";

        $data = $request->all();
        
        $save = $k_helper->save($data);

        if ($save) 
        {
            $status     = 200;
            $msg        = 'Berhasil Menyimpan Data Kelas';
        }

        $response = array(
            'status'  => $status, 
            'msg'     => $msg, 
        );
      
        return response()->json($response);
    }

}
