<?php
namespace Modules\Kelas\Http\Controllers;

use DB;

class KelasHelper
{
  var $table_module = 'kelas';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_kelas = 'Modules\Kelas\Entities\Kelas';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_kelas::select('kelas.kelas_name', DB::raw('count(siswa.siswa_serial_id) as count_siswa'))
                               ->leftjoin('siswa', 'siswa.siswa_kelas_id', '=', 'kelas.kelas_serial_id')
                               ->groupby('kelas.kelas_name')
                               ->orderby('kelas.kelas_sorting', 'ASC')
                               ->where('kelas.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("kelas.kelas_name as label", "kelas.kelas_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data)
  {
    unset($data['_token']);
    $query = $this->model_kelas::insert($data);

    return $query;
  }
  
}