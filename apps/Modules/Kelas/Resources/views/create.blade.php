@extends('app')
@section('title', 'Tambah Kelas')
@section('content')
<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Kelas</h1>
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				Tambah Kelas
			</button>
    </div>

    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Kelas</h6>
      </div>
      <div class="card-body">
        <form action="{{ URL('kelas/save') }}" method="POST" enctype="multipart/form-data" id="create">
            {{ csrf_field() }}
            @foreach ($fields as $key => $value)
              <div class="form-group">
                <label>{{ $value[$table_module.'_fields_label'] }}</label>
                @include('input.'.$value[$table_module.'_fields_input_type'], array(
                    'table_module'            => $table_module,
                    'fields_name'             => $value[$table_module.'_fields_name'], 
                    'fields_label'             => $value[$table_module.'_fields_label'], 
                    'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                    'required'                => $value[$table_module.'_fields_validation'],
                ))
              </div>
              @endforeach                           
            <a href="{{ URL($table_module) }}" class="btn btn-secondary float-left">Batal</a>
            <button type="button" class="btn btn-primary float-right btn_save">Simpan</button>                      
        </form>  
      </div>
    </div>

</div>

<script src="{{ URL('assets/vendor/jquery/jquery.min.js') }}"></script>
<script>
  $(".btn_save").click(function(event)
  {
    event.preventDefault();
    let data = $('#create').serializeArray();
    Swal.queue([{
      title: 'Konfirmasi',
      confirmButtonText: 'Simpan',
      text:
        'Simpan data kelas?',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        var fd = new FormData();
        data.forEach((item) => {
          fd.append(item['name'], item['value'])
        });

        return $.ajax({
                  url: "{{ URL($table_module.'/save') }}",
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function (data) { 
                    Swal.fire({
                      icon: 'success',
                      title: data.msg,
                      confirmButtonText: `OK`,
                    }).then((result) => {
                      window.location.href = "{{ URL($table_module) }}";
                    })
                  }
              }); 
      }
    }])
  });
</script>
@endsection