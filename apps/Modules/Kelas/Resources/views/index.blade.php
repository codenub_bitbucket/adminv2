@extends('app')
@section('title', 'Kelas')
@section('content')
<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Kelas</h1>
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				Tambah Kelas
			</button>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="{{ URL('kelas/create') }}">Tambah</a>
			</div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary">Data Kelas</h6>
        </div>
        <div class="card-body">
					@include('component.filter_show')
          <div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
								@if (@count($fields) > 0)
									<tr>
										@foreach ($fields as $f)
										<th style="white-space:nowrap">{{ $f[$table_module.'_fields_label'] }}</th>
										@endforeach
										<th style="white-space:nowrap">Total Siswa</th>
									</tr>
								@endif
								</thead>
              <tbody>
								@if (@count($list_data) > 0)
									@foreach ($list_data as $row)
										<tr>
											@if (@count($fields) > 0)
												@foreach ($fields as $f)
													<td style="white-space:nowrap">{{ $row[$f[$table_module.'_fields_name']] }}</td>
												@endforeach
											@endif
											<td style="white-space:nowrap">{{ isset($row->count_siswa) ? $row->count_siswa : ' - ' }}</td>
										</tr>
									@endforeach
								@endif
              </tbody>
						</table>
						@include('component.pagination')
          </div>
        </div>
      </div>

</div>
@endsection