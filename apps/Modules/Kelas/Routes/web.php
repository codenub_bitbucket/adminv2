<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('kelas')->group(function() {
    Route::get('/', 'KelasController@index');
    Route::get('/create', 'KelasController@create');
    Route::post('/save', 'KelasController@save');
});
