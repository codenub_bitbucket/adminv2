<?php

namespace Modules\Learning\Entities;

use Illuminate\Database\Eloquent\Model;

class Learning extends Model
{
    protected $table 		= "elearning";
		protected $primaryKey 	= "elearning_serial_id";
		protected $guarded = array('elearning_serial_id');
		public $timestamps = false;
}
