<?php

namespace Modules\Learning\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Learning\Http\Controllers\LearningHelper as elearning_helper;
use Modules\Kelas\Http\Controllers\KelasHelper as kelas_helper;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;

class LearningController extends Controller
{
    var $module       = 'Elearning';
	var $table_module = 'elearning';

	function __construct(Request $request)
	{
		$this->middleware('auth');
		session()->forget('redirect');
		session()->put('redirect', $request->path());
	}

    public function index(Request $request)
	{
		$criteria = $request->all();
		$helper = new elearning_helper();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		$list = $helper->list_data('get', $criteria);
		$count = $helper->list_data('count');


		$view = 'elearning::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list,
			'count_data'     => $count,
			'data_per_page'  => $data_per_page,
		);
        
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function create()
	{
        $k_helper = new kelas_helper();
        $j_helper = new jurusan_helper();
        $list_kelas = $k_helper->list_data('get_dropdown');
        $list_jurusan = $j_helper->list_data('get_dropdown');
        
        $view = 'elearning::create';

        $view_data = array(
            'function' => 'save',
            'table_module'  => $this->table_module,
            'kelas' => $list_kelas,
            'jurusan' => $list_jurusan,
        );
        
        if(view()->exists($view))
        {
                return view($view, $view_data)->render();
        }
	}

    public function save(Request $request)
	{
        
        $helper = new elearning_helper();
        
        $users = Auth::user();
        
        $data = $request->all();
        $request->validate([
                'elearning_file'=>'mimes:pdf'
        ]);

        $uniqueFileName = uniqid() . '.' . $request->file('elearning_file')->getClientOriginalExtension();

        $filePath = "/uploads/";

        $request->file('elearning_file')->move(public_path($filePath), $uniqueFileName);
        $data['elearning_file'] = URL('apps/public/uploads/'.$uniqueFileName);

        $save = $helper->save($data, $users);

        if ($save)
        {
                return redirect('smkbisa/elearning')->with(['success' => 'E-Learning Berhasil Disimpan']);
        }
        else
        {
                return redirect('smkbisa/elearning')->with('failed', 'E-Learning Gagal Disimpan');
        }
	}

    public function detail($uuid)
	{
        $helper = new elearning_helper();
        
        $detail = $helper->get_detail($uuid);

        $view = 'elearning::detail';
        $view_data = array(
            'table_module'   => $this->table_module,
            'content'        => $detail,
        );

        if(view()->exists($view))
        {
            return view($view, $view_data)->render();
        }
	}

    public function delete($uuid)
	{
		$helper = new elearning_helper();

		$delete = $helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/elearning')->with(['success' => 'E-Learning Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/elearning')->with('failed', 'E-Learning Gagal Dihapus');
		}
	}

    public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new elearning_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
				return redirect('smkbisa/elearning')->with(['success' => 'E-Learning Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/elearning')->with('failed', 'E-Learning Gagal Dihapus');
		}
	}
    
}
