<?php
namespace Modules\Learning\Http\Controllers;

use Session;
use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class LearningHelper
{
  var $table_module = 'elearning';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_elearning = 'Modules\Learning\Entities\Learning';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();

    $selectOther = "kelas.kelas_name as elearning_kelas_name,
                    users.name as created_by_name,
                    jurusan.jurusan_name as elearning_jurusan_name";
   
    $query = $this->model_elearning::select('elearning.*', DB::raw($selectOther))
                                ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'elearning.elearning_kelas_id')
                                ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'elearning.elearning_jurusan_id')
                                ->leftjoin('users', 'users.id',"=", "elearning.created_by");
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('elearning.elearning_name',"LIKE", "%".$criteria['search']."%");
                                }

                                if(isset($criteria['who_get']) && $criteria['who_get'] == 'users')
                                {
                                  $get_class = DB::table('siswa')->where('siswa_serial_id', $criteria['siswa_id'])->select("siswa_kelas_id", "siswa_jurusan_id")->first();

                                  if(@count($get_class) > 0)
                                  {
                                    $get_class = json_decode(json_encode($get_class), true);

                                    $query->where('elearning.elearning_kelas_id', $get_class['siswa_kelas_id'])
                                          ->where('elearning.elearning_jurusan_id', $get_class['siswa_jurusan_id']);
                                  }
                                  else
                                  {
                                    return array();
                                  }
                                }
                                $query->where('elearning.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);

    $data['created_by'] = $users->id;
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['elearning_uuid'] = Uuid::uuid4()->toString();

    $query = $this->model_elearning::insert($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $dataUser = array(
      'photo'     => isset($data['siswa_photo']) ? $data['siswa_photo'] : null,
      'updated_at' => date('Y-m-d H:i:s'),
    );

    unset($data['siswa_photo']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['siswa_dob'] = isset($data['siswa_dob']) ? date("Y-m-d", strtotime($data['siswa_dob'])) : '1111-11-11';
    $query = $this->model_siswa::where('siswa_uuid', $uuid)->first();
    $query->update($data);

    $serial_id = $query->siswa_serial_id;

    if ($query)
    {
      $getRel = DB::table('sys_rel')->where('rel_to_id', $serial_id)
                                    ->where('rel_from_module', 'users')
                                    ->where('rel_to_module', 'siswa')
                                    ->first();

      if (@count($getRel) > 0) 
      {
        $getRel = json_decode(json_encode($getRel),true);

        $query2 = DB::table('users')->where('id', $getRel['rel_to_id'])->update($dataUser);
      }

      $result = true;
    }
    return $query;
  }

  function get_detail($uuid)
  {
    $result = array();
    
    $query = $this->model_elearning::select('elearning.*', 
                                        DB::raw('kelas.kelas_name as elearning_kelas_name'), 
                                        DB::raw('jurusan.jurusan_name as elearning_jurusan_name'),
                                        "users.name as created_by_name")
                                ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'elearning.elearning_kelas_id')
                                ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'elearning.elearning_jurusan_id')
                                ->leftjoin('users', 'users.id',"=", "elearning.created_by")
                                ->where('elearning.elearning_uuid', $uuid)
                                ->first();

    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;

  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_elearning::where('elearning_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->model_elearning::whereIn('elearning_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

}