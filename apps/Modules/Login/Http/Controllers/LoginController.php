<?php

namespace Modules\Login\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Redirect;
use Session;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;

class LoginController extends Controller
{

    public function index()
    {
        if (Auth::user()) 
        {
            return Redirect::to('beranda');
        }
        else 
        {
            $auh = new aboutus_helper();
            $info = $auh->getAllInfo();
            $info = $auh->convertInfoArray($info);

            $view_data = array(
                    'data' => $info,
            );

            $view = 'login::index';

            if(view()->exists($view))
            {
                return view($view, $view_data)->render();
            }
            
        }
    }

    public function login(Request $request)
    {
        $akun = $request->only(['email', 'password']);

        if (!Auth::attempt($akun)) 
        {
            return redirect()->back()->with('failed', 'Akun Anda Salah, Coba lagi!');
        }

        $users = Auth::user();
        

        if (intval($users->role) !== 1)
        {
            $this->logout(401);
        }

        $request->session()->put('users_detail', Auth::user());

        $redirect = 'smkbisa/beranda';

        if (!empty(session()->get('redirect')) && explode('/', session()->get('redirect'))[0] == 'smkbisa') 
        {
            $redirect = session()->get('redirect');
        }
        return Redirect::to($redirect);
    }

    public function logout($status=200)
    {
        Auth::logout();
        Session::flush();

        if($status == 401)
        {
            return redirect()->back()->with('failed', 'Akun Anda Salah!!!');
        }
        else
        {
            return Redirect::to('login');
        }
    }
}
