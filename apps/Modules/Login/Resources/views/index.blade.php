
<!doctype html>
<html lang="en">

<head>
	<title>Admin | SMK Kartikatama Metro</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="">
	<meta name="description" content="SMK Kartikatama Metro">
	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ URL('resources/images/favicon.png') }}" />

	<!-- Google Font -->
	<link href="https://mayindri.online/cloudme.fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CPlayfair+Display:400,400i,700,700i%7CRoboto:400,400i,500,700" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/font-awesome/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/themify-icons/css/themify-icons.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/animate/animate.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/css/three-dots.css') }}" />

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/css/style.css') }}" />

</head>

<body>
	<!-- <div class="preloader">
		<img src="{{ URL('resources/images/preloader.svg') }}" alt="Pre-loader">
	</div> -->

	<div class="preloader">
		<div class="d-flex align-items-center w-100">
			<img src="{{ URL('assets/img/logo.png') }}" >
			<div class="snippet" data-title=".dot-typing">
				<div class="stage">
					<div class="dot-typing"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- =======================
	Sign in -->
	<section class="p-0 d-flex align-items-center">
		<div class="container-fluid">
			<div class="row">
				<!-- left -->
				<div class="col-12 col-md-5 col-lg-5 d-md-flex align-items-center bg-light h-sm-100-vh">
					<div class="w-100 p-3 p-lg-5 all-text-white">
						<img src="{{ (isset($data['school_logo']) ? url('apps/public/'.$data['school_logo']['description']) : url('resources/images/logo-kartikatama.png')) }}" class="w-100">
					</div>
				</div>
				<!-- Right -->
				<div class="col-12 col-md-7 col-xl-7 mx-auto my-5">
					@if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
					<div class="row h-100">
						<div class="col-12 col-md-10 col-lg-8 text-left mx-auto d-flex align-items-center">
							<div class="w-100">
								<h2 class="">Sign into your account!</h2>
								<h5 class="font-weight-light">Nice to see you! Please log in with your account.</h5>
								<form action="{{ URL('login') }}" method="POST">
									{{ csrf_field() }}
									<div class="form mt-4 ">
										<div>
											<p class="text-left mb-2">Email address</p>
											<span class="form-group"><input type="email" class="form-control" placeholder="E-mail" name="email"></span>
										</div>
										<div>
											<div class="d-flex justify-content-between align-items-center">
												<p class="text-left mb-2">Password</p>
												{{-- <a class="text-muted small mb-2" href="">Lost password? Click Here.</a> --}}
											</div>
											<span class="form-group"><input type="password" name="password" class="form-control" placeholder="*********"></span>
										</div>
										<div class="row align-items-center no-gutters m-0">
											<div class="col-6 col-md-8">

											</div>
											<div class="col-6 col-md-4 text-right"><button class="btn btn-dark " type="submit">Login</button></div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- =======================
	Sign in -->

	<!--Global JS-->
	<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/popper.js/umd/popper.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

	<!--Template Functions-->
	<script src="{{ URL('resources/js/functions.js') }}"></script>

</body>

</html>
