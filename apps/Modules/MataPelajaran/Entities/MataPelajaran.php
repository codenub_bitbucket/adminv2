<?php

namespace Modules\MataPelajaran\Entities;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table 		= "mata_pelajaran";
    protected $primaryKey 	= "mata_pelajaran_serial_id";
    protected $guarded = array('mata_pelajaran_serial_id');
    public $timestamps = false;
}
