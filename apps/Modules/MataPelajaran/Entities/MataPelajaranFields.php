<?php

namespace Modules\MataPelajaran\Entities;

use Illuminate\Database\Eloquent\Model;

class MataPelajaranFields extends Model
{
    protected $table 		= "mata_pelajaran_fields";
    protected $primaryKey 	= "mata_pelajaran_fields_serial_id";
    protected $guarded = array('mata_pelajaran_fields_serial_id');
    public $timestamps = false;
}
