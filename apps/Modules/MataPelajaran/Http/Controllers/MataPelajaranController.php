<?php

namespace Modules\MataPelajaran\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\MataPelajaran\Http\Controllers\MataPelajaranHelper as mata_pelajaran_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class MataPelajaranController extends Controller
{
	var $module       = 'Mata Pelajaran';
	var $table_module = 'mata_pelajaran';

	public function index(Request $request)
	{
		$criteria = $request->all();
		$helper = new mata_pelajaran_helper();
		$sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

		$list_mata_pelajaran = $helper->list_data('get', $criteria);
		$count_mata_pelajaran = $helper->list_data('count');

		$fields = $sys->getFields($this->table_module);


		$view = 'matapelajaran::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_mata_pelajaran,
			'count_data'     => $count_mata_pelajaran,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function create()
	{
		$sys = new sys();

		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
		{
			if ($value['mata_pelajaran_fields_input_type'] == 'singleoption' && $value['mata_pelajaran_fields_options'] !== null) 
			{
					$fields[$key]['dropdown'] = $sys->getDropdown($value['mata_pelajaran_fields_options']);
			}
		}

		$view = 'matapelajaran::create';

		$view_data = array(
				'table_module'  => $this->table_module,
				'fields'        => $fields,
				'function' => 'save'
		);
        
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function add(Request $request)
	{
		$users = Auth::user();

		$criteria = $request->all();
		$helper = new mata_pelajaran_helper();
		$sys = new sys();

		$data = $request->all();
		
		$save = $helper->save($data, $users);

		if ($save)
		{
			return redirect('smkbisa/mata-pelajaran')->with(['success' => 'Mata Pelajaran Berhasil Ditambah']);
		}
		else
		{
			return redirect('smkbisa/mata-pelajaran')->with('failed', 'Mata Pelajaran Gagal Ditambah');
		}
	}

	public function update(Request $request, $uuid)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$helper = new mata_pelajaran_helper();

		$update = $helper->update($criteria, $users, $uuid);
			
		if ($update)
		{
			return redirect('smkbisa/mata-pelajaran')->with(['success' => 'Mata Pelajaran Berhasil Diperbaharui']);
		}
		else
		{
			return redirect('smkbisa/mata-pelajaran')->with('failed', 'Mata Pelajaran Gagal Diperbaharui');
		}
	}
    
	public function delete($uuid)
	{
		$sys = new sys();

		$helper = new mata_pelajaran_helper();

		$delete = $helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/mata-pelajaran')->with(['success' => 'Mata Pelajaran Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/mata-pelajaran')->with('failed', 'Mata Pelajaran Gagal Dihapus');
		}
	}
    
	public function edit($uuid)
	{
		$helper = new mata_pelajaran_helper();
		$sys = new sys();
		$detail = $helper->get_detail($uuid);

		
		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
		{
				if ($value['mata_pelajaran_fields_input_type'] == 'singleoption' && $value['mata_pelajaran_fields_options'] !== null) 
				{
						$fields[$key]['dropdown'] = $sys->getDropdown($value['mata_pelajaran_fields_options']);
				}
		}

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		$view = 'matapelajaran::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new mata_pelajaran_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
				return redirect('smkbisa/mata-pelajaran')->with(['success' => 'Mata Pelajaran Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/mata-pelajaran')->with('failed', 'Mata Pelajaran Gagal Dihapus');
		}
	}
}
