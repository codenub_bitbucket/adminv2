<?php
namespace Modules\MataPelajaran\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class MataPelajaranHelper
{
  var $table_module = 'mata_pelajaran';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_mata_pelajaran = 'Modules\MataPelajaran\Entities\MataPelajaran';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_mata_pelajaran::select("mata_pelajaran.*", "users.name as created_by_name")
                                        ->leftjoin('users', 'users.id',"=", "created_by")
                                        ->orderby('mata_pelajaran.mata_pelajaran_name', 'ASC');
                                        if (isset($criteria['search']) && $criteria['search'] != '')
                                        {
                                          $query->where('mata_pelajaran.mata_pelajaran_name',"LIKE", "%".$criteria['search']."%");
                                        }
                                        $query->where('mata_pelajaran.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select($this->table_module.".".$this->table_module."_name as label", $this->table_module.".".$this->table_module."_serial_id as value")->take(15)->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_mata_pelajaran::select("mata_pelajaran.*", "users.name as created_by_name")
                                  ->leftjoin('users', 'users.id',"=", "mata_pelajaran.created_by")
                                  ->where('mata_pelajaran.mata_pelajaran_uuid', $uuid)
                                  ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);

    $data['created_by'] = $users->id;
    $data['mata_pelajaran_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_mata_pelajaran::insert($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_mata_pelajaran::where('mata_pelajaran_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_mata_pelajaran::where('mata_pelajaran_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
}