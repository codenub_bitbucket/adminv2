<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/mata-pelajaran')->group(function() {
    Route::get('/', 'MataPelajaranController@index');
    Route::get('/tambah', 'MataPelajaranController@create');
    Route::post('/add', 'MataPelajaranController@add');
    Route::get('/delete/{uuid}', 'MataPelajaranController@delete');
    Route::get('/edit/{uuid}', 'MataPelajaranController@edit');
    Route::post('/update/{uuid}', 'MataPelajaranController@update');
    Route::post('/mass_delete', 'MataPelajaranController@mass_delete');
});
