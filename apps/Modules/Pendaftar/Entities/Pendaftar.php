<?php

namespace Modules\Pendaftar\Entities;

use Illuminate\Database\Eloquent\Model;

class Pendaftar extends Model
{
    protected $table 		= "pendaftar";
    protected $primaryKey 	= "pendaftar_serial_id";
    protected $guarded = array('pendaftar_serial_id');
    public $timestamps = false;
}
