<?php

namespace Modules\Pendaftar\Entities;

use Illuminate\Database\Eloquent\Model;

class PendaftarFields extends Model
{
    protected $table 		= "pendaftar_fields";
    protected $primaryKey 	= "pendaftar_fields_serial_id";
    protected $guarded = array('pendaftar_fields_serial_id');
    public $timestamps = false;
}
