<?php

namespace Modules\Pendaftar\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Redirect;
use Auth;

use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;
use Modules\Pendaftar\Http\Controllers\PendaftarHelper as pendaftar_helper;

class PendaftarController extends Controller
{

	var $module       = 'Pendaftar';
	var $table_module = 'pendaftar';

	function __construct(Request $request)
	{
		session()->forget('redirect');
		session()->put('redirect', $request->path());
		
	}
	
	public function index(Request $request)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$criteria = $request->all();
		$p_helper = new pendaftar_helper();
		$sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		$count_data = $p_helper->list_data('count');

		$data = $p_helper->list_data('get', $criteria);
		$fields = $sys->getFields($this->table_module);

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $data,
			'count_data'     => $count_data,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);

		$view = 'pendaftar::index';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function register()
	{
		$auh = new aboutus_helper();
		$j_helper = new jurusan_helper();
		$sys = new sys();

		$fields = $sys->getFields('pendaftar');
		foreach ($fields as $key => $value) 
		{
			if ($value['pendaftar_fields_input_type'] == 'singleoption' && $value['pendaftar_fields_options'] !== null) 
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['pendaftar_fields_options']);
			}
			elseif ($value['pendaftar_fields_input_type'] == 'singleoption') 
			{
				if ($value['pendaftar_fields_name'] == 'pendaftar_jurusan_id') 
				{
					$list_jurusan = $j_helper->list_data('get_dropdown');
					$fields[$key]['dropdown'] = $list_jurusan;
				}
			}
		}

		$info = $auh->getAllInfo();
		$info = $auh->convertInfoArray($info);

		$view_data = array(
				'function' => 'save',
				'info' => $info,
				'fields' => $fields,
				'table_module'   => 'pendaftar'
		);
		
		$view = 'pendaftar::registration';


		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function saveRegistrant(Request $request)
	{
			$sys = new sys();
			$auh = new aboutus_helper();
			$info = $auh->getAllInfo();
			$info = $auh->convertInfoArray($info);

			$data = $request->all();
			
			if ($request->file('pendaftar_photo')) 
			{
					$temp = new \Illuminate\Http\Request();
					$temp->replace(['file' => $data['pendaftar_photo']]);
					
					$upload = $sys->uploadFile($temp);

					if ($upload['status'] == 200) 
					{
							$data['pendaftar_photo'] = $upload['file']->image;
					}
			}
			
			$ph = new pendaftar_helper();
			
			$save = $ph->save($data);
			
			$data = $ph->getDataById($save);

			if ($save)
			{
					return redirect()->back()->with(['success' => 'Pendaftaran Anda Berhasil. Silahkan Simpan Tanda Pendaftaran Anda', 'data' => $data, 'info' => $info]);
			}
			else
			{
					return redirect()->back()->with('failed', 'Pendaftaran Anda Gagal');
			}
	}

	public function create()
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$auh = new aboutus_helper();
		$j_helper = new jurusan_helper();
		$sys = new sys();

		$fields = $sys->getFields('pendaftar');
		foreach ($fields as $key => $value) 
		{
			if ($value['pendaftar_fields_input_type'] == 'singleoption' && $value['pendaftar_fields_options'] !== null) 
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['pendaftar_fields_options']);
			}
			elseif ($value['pendaftar_fields_input_type'] == 'singleoption') 
			{
				if ($value['pendaftar_fields_name'] == 'pendaftar_jurusan_id') 
				{
					$list_jurusan = $j_helper->list_data('get_dropdown');
					$fields[$key]['dropdown'] = $list_jurusan;
				}
			}
		}

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'function' => 'save',
		);

		$view = 'pendaftar::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function add(Request $request)
	{
		$users = Auth::user();

		$criteria = $request->all();
		$p_helper = new pendaftar_helper();
		$sys = new sys();

		$data = $request->all();
        
		if ($request->file('pendaftar_photo')) 
		{
			$temp = new \Illuminate\Http\Request();
			$temp->replace(['file' => $data['pendaftar_photo']]);
			
			$upload = $sys->uploadFile($temp);

			if ($upload['status'] == 200) 
			{
				$data['pendaftar_photo'] = $upload['file']->image;
			}
		}
		$save = $p_helper->saveAdmin($data, $users);

		if ($save)
		{
			return redirect('smkbisa/pendaftar')->with(['success' => 'Siswa Baru Berhasil Ditambah']);
		}
		else
		{
			return redirect('smkbisa/pendaftar')->with('failed', 'Siswa Baru Gagal Ditambah');
		}
	}

	public function edit($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}
		$p_helper = new pendaftar_helper();
		$j_helper = new jurusan_helper();
		$sys = new sys();
		$detail = $p_helper->get_detail($uuid);

		
		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
		{
			if ($value['pendaftar_fields_input_type'] == 'singleoption' && $value['pendaftar_fields_options'] !== null) 
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['pendaftar_fields_options']);
			}
			elseif ($value['pendaftar_fields_input_type'] == 'singleoption') 
			{
				if ($value['pendaftar_fields_name'] == 'pendaftar_jurusan_id') 
				{
					$list_jurusan = $j_helper->list_data('get_dropdown');
					$fields[$key]['dropdown'] = $list_jurusan;
				}
			}
		}

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		dd($view_data);

		$view = 'pendaftar::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function update(Request $request, $uuid)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		if ($request->file('pendaftar_photo')) 
		{
			$temp = new \Illuminate\Http\Request();
			$temp->replace(['file' => $criteria['pendaftar_photo']]);
			
			$upload = $sys->uploadFile($temp);

			if ($upload['status'] == 200) 
			{
					$criteria['pendaftar_photo'] = $upload['file']->image;
			}
		}

		$p_helper = new pendaftar_helper();

		$update = $p_helper->update($criteria, $users, $uuid);

		$back = 'smkbisa/'.$this->table_module;
		$segment = session()->get('back');

		if (!empty($segment)) 
		{
			$back = $segment;
		}
		if ($update)
		{
			return redirect($back)->with(['success' => 'Siswa Baru Berhasil Diperbaharui']);
		}
		else
		{
			return redirect($back)->with('failed', 'Siswa Baru Gagal Diperbaharui');
		}
	}
    
	public function delete($uuid)
	{
		$sys = new sys();

		$p_helper = new pendaftar_helper();

		$delete = $p_helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/pendaftar')->with(['success' => 'Siswa Baru Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/pendaftar')->with('failed', 'Siswa Baru Gagal Dihapus');
		}
	}

	public function detail($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$sys = new sys();
		$p_helper = new pendaftar_helper();
		
		$detail = $p_helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);

		if (@count($detail) == 0) 
		{
				return redirect('smkbisa/pendaftar')->with('failed', "Siswa Baru Tidak Valid!");
		}

		$view = 'pendaftar::detail';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
			'fields'				=> $fields
		);
		session()->forget('back');
		session()->put('back', 'smkbisa/pendaftar/detail/'.$uuid);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function print($uuid)
	{
		if (!Auth::user()) 
		{
				return Redirect::to('login');
		}

		$sys = new sys();
		$p_helper = new pendaftar_helper();
		$auh = new aboutus_helper();

		$info = $auh->getAllInfo();
		$info = $auh->convertInfoArray($info);
		$detail = $p_helper->get_detail($uuid);

		if (@count($detail) == 0) 
		{
				return redirect('smkbisa/pendaftar')->with('failed', "Siswa Baru Tidak Valid!");
		}

		$view = 'pendaftar::print';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
			'info' => $info
		);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}
}
