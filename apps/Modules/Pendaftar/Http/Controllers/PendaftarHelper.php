<?php
namespace Modules\Pendaftar\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

class PendaftarHelper
{
  var $table_module = 'pendaftar';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_pendaftar = 'Modules\Pendaftar\Entities\Pendaftar';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_pendaftar::select("pendaftar.*", "users.name as created_by_name", DB::raw('jurusan.jurusan_name as pendaftar_jurusan_name'), DB::raw('gen.dropdown_options_label as pendaftar_gender_name'), DB::raw('jenis.dropdown_options_label as pendaftar_jenis_name'), DB::raw('agama.dropdown_options_label as pendaftar_agama_name'))
                              ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'pendaftar.pendaftar_jurusan_id')
                              ->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_gender')
                              ->leftjoin('dropdown_options as jenis', 'jenis.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_jenis_id')
                              ->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_agama')
                              ->leftjoin('users', 'users.id',"=", "pendaftar.created_by")
                              ->where('pendaftar.deleted', 0)
                              ->orderBy('pendaftar.pendaftar_serial_id', 'DESC');

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data)
  {
    unset($data['_token']);

    $data['pendaftar_dob'] = isset($data['pendaftar_dob']) ? date("Y-m-d", strtotime($data['pendaftar_dob'])) : '1111-11-11';
    $data['pendaftar_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_pendaftar::insertGetId($data);
    
    if (@count($query) > 0) 
    {
      $code['pendaftar_code'] = 'KARTIKA-N'.$query;
      $update = $this->model_pendaftar::where('pendaftar_serial_id', $query)
                                      ->update($code);
    }

    return $query;
  }

  function saveAdmin($data, $users)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['created_by'] = $users->id;
    $data['pendaftar_dob'] = isset($data['pendaftar_dob']) ? date("Y-m-d", strtotime($data['pendaftar_dob'])) : '1111-11-11';
    $data['pendaftar_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_pendaftar::insertGetId($data);
    if (@count($query) > 0)
    {
      $code['pendaftar_code'] = 'KARTIKA-N'.$query;
      $update = $this->model_pendaftar::where('pendaftar_serial_id', $query)
                                      ->update($code);
      $result = true;
    }
    return $query;
  }

  function getDataById($id)
  {
    $result = array();

    $query = $this->model_pendaftar::where('pendaftar.pendaftar_serial_id', $id)
                                   ->select('pendaftar.*', 'jurusan.jurusan_name as pendaftar_jurusan_name')
                                   ->leftJoin('jurusan', 'jurusan.jurusan_serial_id', 'pendaftar.pendaftar_jurusan_id')
                                   ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_pendaftar::select("pendaftar.*", "users.name as created_by_name", DB::raw('jurusan.jurusan_name as pendaftar_jurusan_name'), DB::raw('gen.dropdown_options_label as pendaftar_gender_name'), DB::raw('jenis.dropdown_options_label as pendaftar_jenis_name'), DB::raw('agama.dropdown_options_label as pendaftar_agama_name'))
                                  ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'pendaftar.pendaftar_jurusan_id')
                                  ->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_gender')
                                  ->leftjoin('dropdown_options as jenis', 'jenis.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_jenis_id')
                                  ->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'pendaftar.pendaftar_agama')
                                  ->leftjoin('users', 'users.id',"=", "pendaftar.created_by")
                                  ->where('pendaftar.pendaftar_uuid', $uuid)
                                  ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }
  
  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['pendaftar_dob'] = isset($data['pendaftar_dob']) ? date("Y-m-d", strtotime($data['pendaftar_dob'])) : '1111-11-11';
    $query = $this->model_pendaftar::where('pendaftar_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_pendaftar::where('pendaftar_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }
}