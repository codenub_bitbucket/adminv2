@extends('app')
@section('title', 'Tambah Jurusan')
@section('content')
{{-- <div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Jurusan</h1>
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				Tambah Jurusan
			</button>
    </div>

    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Jurusan</h6>
      </div>
      <div class="card-body">
        <form action="{{ URL('jurusan/save') }}" method="POST" enctype="multipart/form-data" id="create">
            {{ csrf_field() }}
            @foreach ($fields as $key => $value)
              <div class="form-group">
                <label>{{ $value[$table_module.'_fields_label'] }}</label>
                @include('input.'.$value[$table_module.'_fields_input_type'], array(
                    'table_module'            => $table_module,
                    'fields_name'             => $value[$table_module.'_fields_name'], 
                    'fields_label'             => $value[$table_module.'_fields_label'], 
                    'dropdown'                => isset($value['dropdown']) ? $value['dropdown'] : array(),
                    'required'                => $value[$table_module.'_fields_validation'],
                ))
              </div>
              @endforeach                           
            <a href="{{ URL($table_module) }}" class="btn btn-secondary float-left">Batal</a>
            <button type="button" class="btn btn-primary float-right btn_save">Simpan</button>                      
        </form>  
      </div>
    </div>

</div>

<script src="{{ URL('assets/vendor/jquery/jquery.min.js') }}"></script>
<script>
  $(".btn_save").click(function(event)
  {
    event.preventDefault();
    let data = $('#create').serializeArray();
    Swal.queue([{
      title: 'Konfirmasi',
      confirmButtonText: 'Simpan',
      text:
        'Simpan data jurusan?',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        var fd = new FormData();
        data.forEach((item) => {
          fd.append(item['name'], item['value'])
        });

        return $.ajax({
                  url: "{{ URL($table_module.'/save') }}",
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function (data) { 
                    Swal.fire({
                      icon: 'success',
                      title: data.msg,
                      confirmButtonText: `OK`,
                    }).then((result) => {
                      window.location.href = "{{ URL($table_module) }}";
                    })
                  }
              }); 
      }
    }])
  });
</script> --}}

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Tambah Jurusan</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Jurusan</li>
                <li class="breadcrumb-item active">Tambah</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <form action="{{ ($function == 'edit') ? URL('smkbisa/pendaftar/update/'.$data[$table_module.'_uuid']) : URL('smkbisa/pendaftar/add') }}" method="post" enctype="multipart/form-data">
               @csrf
                <div class="row">
                  @for ($i = 0; $i < @count($fields); $i++)
											@if ($i % 2 == 0)
											<div class="col-md-6">
												<div class="form-group">
													<label>{{ $fields[$i][$table_module.'_fields_label'] }}</label>
													@include('input.'.$fields[$i][$table_module.'_fields_input_type'], array(
															'table_module'            => $table_module,
															'fields_name'             => $fields[$i][$table_module.'_fields_name'], 
															'fields_label'             => $fields[$i][$table_module.'_fields_label'], 
															'dropdown'                => isset($fields[$i]['dropdown']) ? $fields[$i]['dropdown'] : array(),
															'required'                => $fields[$i][$table_module.'_fields_validation'],
													))
												</div>
											</div>
											@else
											<div class="col-md-6">
												<div class="form-group">
													<label>{{ $fields[$i][$table_module.'_fields_label'] }}</label>
													@include('input.'.$fields[$i][$table_module.'_fields_input_type'], array(
															'table_module'            => $table_module,
															'fields_name'             => $fields[$i][$table_module.'_fields_name'], 
															'fields_label'             => $fields[$i][$table_module.'_fields_label'], 
															'dropdown'                => isset($fields[$i]['dropdown']) ? $fields[$i]['dropdown'] : array(),
															'required'                => $fields[$i][$table_module.'_fields_validation'],
													))
												</div>
											</div>
											@endif
									@endfor
                </div>
                <hr>
                <div class="d-flex w-100 justify-content-end">
                  <a class="btn btn-light px-3 mr-2" href="{{ url('smkbisa/jurusan') }}">
                    Cancel
                  </a>
                  <button class="btn btn-primary px-4" type="submit">
                    {{ ($function == 'save') ? 'Save' : 'Update'}}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
@endsection