
<!DOCTYPE html>
<html>

<head>
    <title>KARTU TANDA PENERIMAAN PESERTA DIDIK BARU {{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+1 year')) ) }}| SMK KARTIKARAMA METRO</title>
    <link rel="shortcut icon" href="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" />
    <link href="{{ URL('resources_admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ URL('resources_admin/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL('resources_admin/html2canvas.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <style>
    .kartu-peserta-seleksi{padding:16px;margin:auto;width:250mm;border:1px solid #000}.kartu-peserta-seleksi p{font-size:20px}.kartu-peserta-seleksi td,.kartu-peserta-seleksi .footer-wrapper p{line-height:30px;font-size:15px}.kartu-peserta-seleksi .head-wrapper{display:flex;padding:8pt;flex-direction:row;margin:-16px -16px 0;align-items:center;justify-content:center;border-bottom:2px solid #000}.kartu-peserta-seleksi .head-wrapper .sec{width:142px;text-align:center}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(2) {flex:1}.kartu-peserta-seleksi .head-wrapper img{height:117px;margin-left:54px}.kartu-peserta-seleksi .head-wrapper .sec:last-child{font-weight:900}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(-1n+3) p {margin-bottom:0}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(2) p:nth-child(-n+3) {font-weight:bold}.kartu-peserta-seleksi .content-wrapper{padding:30px}.kartu-peserta-seleksi .content-wrapper tr:nth-last-child(-n+2) td:last-child {color:#00f}.kartu-peserta-seleksi .content-wrapper tr td:nth-child(2) {width:15px;text-align:center}.kartu-peserta-seleksi .footer-wrapper{text-align:right;padding-right:24px;margin-top:30px}.kartu-peserta-seleksi .footer-wrapper p{margin-bottom:0}.kartu-peserta-seleksi .pasfoto{height:179px;float:right}
    @media print {
      body * {
        visibility: hidden;
      }
      #section-to-print, #section-to-print * {
        visibility: visible;
      }
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
    }
    </style>
</head>

<body>
  <div class="text-center">
    <img src="{{ URL('resources/images/preloader.svg') }}" alt="Pre-loader" id="img-preloader">
  </div>
  <div id="vcard">
    <div class="kartu-peserta-seleksi-wrapper mt-5" id="section-to-print">
      <div class="kartu-peserta-seleksi canvas_div_pdf" id="canvas_div_pdf" style="background:white">
          <div class="head-wrapper">
              <div class="sec"><img class="img-thumbnail" src="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" alt="SMK KARTIKATAMA METRO" style="background:white"></div>
              <div class="sec">
                <p>KARTU TANDA</p>
                <p>PENERIMAAN PESERTA DIDIK BARU</p>
                <p>SMK KARTIKATAMA METRO</p>
                <p>TAHUN PELAJARAN {{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+1 year')) ) }}/{{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+2 year')) ) }}</p>
              </div>
              <div class="sec">
                  <p>PESERTA</p>
              </div>
          </div>
          <div class="content-wrapper">
            <img class="pasfoto" src="{{ URL('apps/public/'.$content['pendaftar_photo']) }}">
            <table>
                <tbody>
                    <tr>
                        <td style="white-space:nowrap">No. Pendaftaran</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_code'] }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">Nama</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_name'] }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">NISN</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_nisn'] }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">Alamat</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_address'] }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">Tanggal Lahir</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ date('d-m-Y', strtotime($content['pendaftar_dob'])) }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">Asal Sekolah</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_asal_sekolah'] }}</strong></td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">Jurusan Yg Dipilih</td>
                        <td style="white-space:nowrap">:</td>
                        <td style="text-transform: uppercase; color:black"><strong>{{ $content['pendaftar_jurusan_name'] }}</strong></td>
                    </tr>
                </tbody>
            </table>
            <strong>Catatan: *</strong>
            <ul>
              <li>Kartu Harap Dibawa Saat Datang Ke Sekolahan.</li>
            </ul>
        </div>
      </div>
    </div>
  </div>
    <div class="text-center mt-5" id="vcard-action">
      <button class="btn btn-primary" onclick="getPDF()">Download PDF</button>
      <button class="btn btn-primary print-btn">Print</button>
    </div>
    
			<script>
        $('#label-done').hide();
				$('#vcard').hide();
        $('#vcard-action').hide();
        setTimeout(() => {
					$('#label-process').hide();
					$('#img-preloader').hide();
					$('#label-done').show();
					$('#vcard').show();
					$('#vcard-action').show();
        }, 5000);
        
				$('.print-btn').click(function() {
					window.print();
				})

				function getPDF(){

				var HTML_Width = $(".canvas_div_pdf").width();
				var HTML_Height = $(".canvas_div_pdf").height();
				var top_left_margin = 15;
				var PDF_Width = HTML_Width+(top_left_margin*2);
				var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
				var canvas_image_width = HTML_Width;
				var canvas_image_height = HTML_Height;

				var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;


				html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
					canvas.getContext('2d');
					
					console.log(canvas.height+"  "+canvas.width);
					
					
					var imgData = canvas.toDataURL("image/jpeg", 1.0);
					var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
						pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
					
					
					for (var i = 1; i <= totalPDFPages; i++) { 
						pdf.addPage(PDF_Width, PDF_Height);
						pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
					}
					
						pdf.save("kartu-pendaftaran.pdf");
						});
				};

				function downloadImage() 
				{
					html2canvas(document.getElementById("canvas_div_pdf")).then(function (canvas) {
						download(canvas.toDataURL("image/png"), "kartu-pendaftaran.png", 'image/png')
					});
				}

			</script>
</body>

</html>