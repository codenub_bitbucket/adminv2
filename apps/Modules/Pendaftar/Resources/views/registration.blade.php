@extends('layout.home.app')

@section('content')

<section class="p-5 d-flex align-items-center">
	<div class="container-fluid">
		@if (\Session::has('success'))
		<div class="alert alert-success" id="success-alert">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<strong>Berhasil!.</strong>
				{!! \Session::get('success') !!}
		</div>
		<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				setTimeout(() => {
					$("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
						$("#success-alert").slideUp(500);
					});
				}, 30000);
			});
		</script>
		@endif
		@if (\Session::has('failed'))
		<div class="alert alert-danger" id="failed-alert">
				<button type="button" class="close" data-dismiss="alert">x</button>
				<strong>Gagal!.</strong>
				{!! \Session::get('failed') !!}
		</div>
		<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				setTimeout(() => {
					$("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
						$("#failed-alert").slideUp(500);
					});
				}, 3000);
			});
		</script>
		@endif
		@if (!\Session::has('success'))
		<div class="row">
			{{-- <!-- left -->
			<div class="col-12 col-md-4 col-lg-4 d-md-flex align-items-center bg-light h-sm-100-vh">
				<div class="w-100 p-3 p-lg-5 all-text-white">
					<img src="{{ (isset($data['school_logo']) ? URL($data['school_logo']['description']) : URL('resources/images/logo-kartikatama.png')) }}" class="w-100">
				</div>
			</div>
			<!-- Right --> --}}
			<form action="{{ URL('registration/new-student/submit') }}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="col-12 col-md-12 col-xl-12 mx-auto my-5">
					<div class="row h-100" style="overflow: auto; max-height: 100%;">
						<div class="col-12 col-md-10 col-lg-10 text-left mx-auto d-flex align-items-center">
							<div class="w-100">
								<h2 class="">Formulir Penerimaan Siswa Baru {{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+1 year')) ) }}</h2>
								<h5 class="font-weight-light">Please fill the form.</h5>
								<div class="row form mt-4">
									@for ($i = 0; $i < @count($fields); $i++)
											@if ($i % 2 == 0)
											<div class="col-md-6">
												<div>
													<label>{{ $fields[$i][$table_module.'_fields_label'] }}</label>
													@include('input.'.$fields[$i][$table_module.'_fields_input_type'], array(
															'table_module'            => $table_module,
															'fields_name'             => $fields[$i][$table_module.'_fields_name'], 
															'fields_label'             => $fields[$i][$table_module.'_fields_label'], 
															'dropdown'                => isset($fields[$i]['dropdown']) ? $fields[$i]['dropdown'] : array(),
															'required'                => $fields[$i][$table_module.'_fields_validation'],
													))
												</div>
											</div>
											@else
											<div class="col-md-6">
												<div>
													<label>{{ $fields[$i][$table_module.'_fields_label'] }}</label>
													@include('input.'.$fields[$i][$table_module.'_fields_input_type'], array(
															'table_module'            => $table_module,
															'fields_name'             => $fields[$i][$table_module.'_fields_name'], 
															'fields_label'             => $fields[$i][$table_module.'_fields_label'], 
															'dropdown'                => isset($fields[$i]['dropdown']) ? $fields[$i]['dropdown'] : array(),
															'required'                => $fields[$i][$table_module.'_fields_validation'],
													))
												</div>
											</div>
											@endif
									@endfor
									
								</div>
								<button class="btn btn-dark btn-block" type="submit">Kirim Formulir Pendaftaran </button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		@else
		
		@php

		$data = session()->get('data');
		$info = session()->get('info');
				
		@endphp
		<div class="text-center">
			<h4 id='label-process'>Sedang memproses kartu tanda pendaftaran. Harap jangan tutup atau refresh halaman ini sebelum kartu terproses</h4>
			<h4 id='label-done'>Silahkan download atau print kartu dibawah ini.</h4>
			<img src="{{ URL('resources/images/preloader.svg') }}" alt="Pre-loader" id="img-preloader">

			<link href="{{ URL('resources_admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
			<script src="{{ URL('resources_admin/libs/jquery/jquery.min.js') }}"></script>
			<script src="{{ URL('resources_admin/html2canvas.js') }}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
			<style>
				.kartu-peserta-seleksi{padding:16px;margin:auto;width:250mm;border:1px solid #000}.kartu-peserta-seleksi p{font-size:20px}.kartu-peserta-seleksi td,.kartu-peserta-seleksi .footer-wrapper p{line-height:30px;font-size:15px}.kartu-peserta-seleksi .head-wrapper{display:flex;padding:8pt;flex-direction:row;margin:-16px -16px 0;align-items:center;justify-content:center;border-bottom:2px solid #000}.kartu-peserta-seleksi .head-wrapper .sec{width:142px;text-align:center}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(2) {flex:1}.kartu-peserta-seleksi .head-wrapper img{height:117px;margin-left:54px}.kartu-peserta-seleksi .head-wrapper .sec:last-child{font-weight:900}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(-1n+3) p {margin-bottom:0}.kartu-peserta-seleksi .head-wrapper .sec:nth-child(2) p:nth-child(-n+3) {font-weight:bold}.kartu-peserta-seleksi .content-wrapper{padding:30px}.kartu-peserta-seleksi .content-wrapper tr:nth-last-child(-n+2) td:last-child {color:#00f}.kartu-peserta-seleksi .content-wrapper tr td:nth-child(2) {width:15px;text-align:center}.kartu-peserta-seleksi .footer-wrapper{text-align:right;padding-right:24px;margin-top:30px}.kartu-peserta-seleksi .footer-wrapper p{margin-bottom:0}.kartu-peserta-seleksi .pasfoto{height:179px;float:right}
				@media print {
					body * {
						visibility: hidden;
					}
					#section-to-print, #section-to-print * {
						visibility: visible;
					}
					#section-to-print {
						position: absolute;
						left: 0;
						top: 0;
					}
				}
			</style>
			<div id="vcard">
				<div class="text-center" id="vcard-action">
					<button class="btn btn-primary" onclick="getPDF()">Download PDF</button>
					<button class="btn btn-primary" onclick="downloadImage()">Download Image</button>
					<button class="btn btn-primary print-btn">Print</button>
				</div>
				<div class="kartu-peserta-seleksi-wrapper" style="overflow: scroll" id="section-to-print">
					<div class="kartu-peserta-seleksi canvas_div_pdf" id="canvas_div_pdf" style="background:white">
							<div class="head-wrapper">
									<div class="sec"><img class="img-thumbnail" src="{{ (isset($data['school_logo']) ? $data['school_logo']['description'] : url('resources/images/logo-kartikatama.png')) }}" alt="SMK KARTIKATAMA METRO" style="background: white"></div>
									<div class="sec">
											<p>KARTU TANDA</p>
											<p>PENERIMAAN PESERTA DIDIK BARU</p>
											<p>SMK KARTIKATAMA METRO</p>
											<p>TAHUN PELAJARAN {{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+1 year')) ) }}/{{ (isset($info['new_registration_year']) && $info['new_registration_year']['description'] !== '' ? $info['new_registration_year']['description'] : date('Y', strtotime('+2 year')) ) }}</p>
									</div>
									<div class="sec">
											<p>PESERTA</p>
									</div>
							</div>
							<div class="content-wrapper text-left">
									<img class="pasfoto" src="{{ URL('apps/public/'.$data['pendaftar_photo']) }}">
									<table>
											<tbody>
													<tr>
															<td style="white-space:nowrap">No. Pendaftaran</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_code'] }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Tgl. Pendaftaran</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($data['created_at']))) }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Nama</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_name'] }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">NISN</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_nisn'] }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Alamat</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_address'] }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Tanggal Lahir</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ date('d-m-Y', strtotime($data['pendaftar_dob'])) }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Asal Sekolah</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_asal_sekolah'] }}</strong></td>
													</tr>
													<tr>
															<td style="white-space:nowrap">Jurusan Yg Dipilih</td>
															<td style="white-space:nowrap">:</td>
															<td style="text-transform: uppercase; color:black"><strong>{{ $data['pendaftar_jurusan_name'] }}</strong></td>
													</tr>
											</tbody>
									</table>
									<strong>Catatan: *</strong>
									<ul>
										<li>Kartu Harap Dibawa Saat Datang Ke Sekolahan.</li>
									</ul>
							</div>
					</div>
				</div>
			</div>
			<script>
				$('#label-done').hide();
				$('#vcard').hide();
				$('#vcard-action').hide();
				window.onbeforeunload = confirmExit;
				function confirmExit() {
						return "Anda yakin meninggalkan halaman? \n";
				}
				setTimeout(() => {
					$('#label-process').hide();
					$('#img-preloader').hide();
					$('#label-done').show();
					$('#vcard').show();
					$('#vcard-action').show();
				}, 5000);

				$('.print-btn').click(function() {
					window.print();
				})

				function getPDF(){

				var HTML_Width = $(".canvas_div_pdf").width();
				var HTML_Height = $(".canvas_div_pdf").height();
				var top_left_margin = 15;
				var PDF_Width = HTML_Width+(top_left_margin*2);
				var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
				var canvas_image_width = HTML_Width;
				var canvas_image_height = HTML_Height;

				var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;


				html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
					canvas.getContext('2d');
					
					console.log(canvas.height+"  "+canvas.width);
					
					
					var imgData = canvas.toDataURL("image/jpeg", 1.0);
					var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
						pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
					
					
					for (var i = 1; i <= totalPDFPages; i++) { 
						pdf.addPage(PDF_Width, PDF_Height);
						pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
					}
					
						pdf.save("kartu-pendaftaran.pdf");
						});
				};

				function downloadImage() 
				{
					html2canvas(document.getElementById("canvas_div_pdf")).then(function (canvas) {
						download(canvas.toDataURL("image/png"), "kartu-pendaftaran.png", 'image/png')
					});
				}

			</script>
		</div>

		@endif
	</div>
</section>

@endsection
