<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('registration')->group(function() {
    Route::get('/new-student', 'PendaftarController@register');
    Route::post('/new-student/submit', 'PendaftarController@saveRegistrant');
});

Route::prefix('smkbisa/pendaftar')->group(function() {
    Route::get('/', 'PendaftarController@index');
    Route::get('/tambah', 'PendaftarController@create');
    Route::post('/add', 'PendaftarController@add');
    Route::get('/delete/{uuid}', 'PendaftarController@delete');
    Route::get('/edit/{uuid}', 'PendaftarController@edit');
    Route::post('/update/{uuid}', 'PendaftarController@update');
    Route::get('/detail/{uuid}', 'PendaftarController@detail');
    Route::get('/print/{uuid}', 'PendaftarController@print');
});
