<?php

namespace Modules\Pengumuman\Entities;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table 		= "pengumuman";
    protected $primaryKey 	= "pengumuman_serial_id";
    protected $guarded = array('pengumuman_serial_id');
    public $timestamps = false;
}
