<?php

namespace Modules\Pengumuman\Entities;

use Illuminate\Database\Eloquent\Model;

class PengumumanFields extends Model
{
    protected $table 		= "pengumuman_fields";
    protected $primaryKey 	= "pengumuman_fields_serial_id";
    protected $guarded = array('pengumuman_fields_serial_id');
    public $timestamps = false;
}
