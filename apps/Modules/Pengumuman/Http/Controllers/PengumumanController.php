<?php

namespace Modules\Pengumuman\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Redirect;
use Session;

use Intervention\Image\Facades\Image;

use Modules\Pengumuman\Http\Controllers\PengumumanHelper as pengumuman_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class PengumumanController extends Controller
{
    var $module       = 'Pengumuman';
    var $table_module = 'pengumuman';
    var $input = 'Illuminate\Support\Facades\Input';
    var $excel = 'Maatwebsite\Excel\Facades\Excel';
		
    function __construct(Request $request)
    {
			$this->middleware('auth');
			session()->forget('redirect');
			session()->put('redirect', $request->path());
    }

    public function index(Request $request)
    {
			$criteria = $request->all();
			$helper = new pengumuman_helper();
			$sys = new sys();
			
			$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
			$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

			$list_data = $helper->list_data('get', $criteria);
			$count_data = $helper->list_data('count');
			$fields = $sys->getFields($this->table_module);
			
			$view = 'pengumuman::index';

			$view_data = array(
				'table_module'   => $this->table_module,
				'list_data'      => $list_data,
				'count_data'     => $count_data,
				'data_per_page'  => $data_per_page,
				'fields'         => $fields,
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
    }

	public function create()
	{
			$sys = new sys();
			$helper = new pengumuman_helper();

			$fields = $sys->getFields($this->table_module);

			foreach ($fields as $key => $value) 
			{
				if ($value['pengumuman_fields_input_type'] == 'singleoption' && $value['pengumuman_fields_options'] !== null) 
				{
					$fields[$key]['dropdown'] = $sys->getDropdown($value['pengumuman_fields_options']);
				}
				elseif ($value['pengumuman_fields_input_type'] == 'singleoption') 
				{
					
				}
			}

			$view = 'pengumuman::create';

			$view_data = array(
					'function' => 'save',
					'table_module'  => $this->table_module,
					'fields'        => $fields,
			);
			
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
	}

	public function save(Request $request)
	{
		$helper = new pengumuman_helper();
		$sys = new sys();

		$users = Auth::user();

		$data = $request->all();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($data['pengumuman_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}

		$data['pengumuman_description'] = $dom->saveHTML();
			
		$save = $helper->save($data, $users);

		if ($save)
		{
				return redirect('smkbisa/pengumuman')->with(['success' => 'Pengumuman Berhasil Disimpan']);
		}
		else
		{
				return redirect('smkbisa/pengumuman')->with('failed', 'Pengumuman Gagal Disimpan');
		}
	}

	public function update(Request $request, $uuid)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$helper = new pengumuman_helper();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($criteria['pengumuman_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}
		$criteria['pengumuman_description'] = $dom->saveHTML();

		$update = $helper->update($criteria, $users, $uuid);

		if ($update)
		{
			return redirect('smkbisa/pengumuman/detail/'.$uuid)->with(['success' => 'Pengumuman Berhasil Diperbaharui']);
		}
		else
		{
			return redirect('smkbisa/pengumuman/detail/'.$uuid)->with('failed', 'Pengumuman Gagal Diperbaharui');
		}
	}

	public function edit(Request $request, $uuid)
	{
		$sys = new sys();
		$helper = new pengumuman_helper();

		$data = $helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);
		
		foreach ($fields as $key => $value) 
		{
			if ($value['pengumuman_fields_input_type'] == 'singleoption' && $value['pengumuman_fields_options'] !== null) 
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['pengumuman_fields_options']);
			}
			elseif ($value['pengumuman_fields_input_type'] == 'singleoption') 
			{
			}
		}

		$view = 'pengumuman::create';
		
		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $data,
			'function'   				 => 'edit'	
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function delete($uuid)
	{
		$sys = new sys();

		$helper = new pengumuman_helper();

		$delete = $helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/pengumuman')->with(['success' => 'Pengumuman Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/pengumuman')->with('failed', 'Pengumuman Gagal Dihapus');
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new pengumuman_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
				return redirect('smkbisa/pengumuman')->with(['success' => 'Pengumuman Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/pengumuman')->with('failed', 'Pengumuman Gagal Dihapus');
		}
	}

	public function detail(Request $request, $uuid)
	{
		$sys = new sys();
		$helper = new pengumuman_helper();

		$data = $helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);

		$view = 'pengumuman::detail';

		$view_data = array(
				'content' => $data,
				'fields' => $fields,
				'table_module' => $this->table_module
		);

		session()->forget('back');
		session()->put('back', 'smkbisa/pengumuman/detail/'.$uuid);
			
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}
}
