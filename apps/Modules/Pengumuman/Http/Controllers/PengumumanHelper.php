<?php
namespace Modules\Pengumuman\Http\Controllers;

use Session;
use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class PengumumanHelper
{
  var $table_module = 'pengumuman';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_pengumuman = 'Modules\Pengumuman\Entities\Pengumuman';

  public function list_data($type='get', $criteria = array())
  {
    $result = array();

    $select = array(
      'pengumuman.*',
      'users.name as created_by_name',
    );
    
    $query = $this->model_pengumuman::select($select)->where('pengumuman.deleted', 0)
                                    ->leftjoin('users', 'users.id',"=", "pengumuman.created_by")
                                    ->orderBy('pengumuman_serial_id', 'DESC');

                                    if (isset($criteria['who_get']) && $criteria['who_get'] == 'wali')
                                    {
                                      $query->where(function($query) {
                                        $query->where('pengumuman_start_date', '>=', date('Y-m-d'))
                                                ->where('pengumuman_end_date', '<=', date('Y-m-d'))
                                                ->orWhere('pengumuman_end_date', '>', date('Y-m-d'));
                                      });
                                    }

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria)->unique();
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);

    $data['created_by'] = $users->id;
    $data['pengumuman_start_date'] = isset($data['pengumuman_start_date']) ? date("Y-m-d", strtotime($data['pengumuman_start_date'])) : date('Y-m-d');
    $data['pengumuman_end_date'] = isset($data['pengumuman_end_date']) ? date("Y-m-d", strtotime($data['pengumuman_end_date'])) : date('Y-m-d');
    $data['pengumuman_uuid'] = Uuid::uuid4()->toString();

    $query = $this->model_pengumuman::insert($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_pengumuman::select("pengumuman.*","users.name as created_by_name")
                              ->where('pengumuman_uuid', $uuid);
                              $query->leftjoin('users', 'users.id', "=", 'pengumuman.created_by');
                              $query = $query->first();
    
    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query),true);
    }

    return $result;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['pengumuman_start_date'] = isset($data['pengumuman_start_date']) ? date("Y-m-d", strtotime($data['pengumuman_start_date'])) : date('Y-m-d');
    $data['pengumuman_end_date'] = isset($data['pengumuman_end_date']) ? date("Y-m-d", strtotime($data['pengumuman_end_date'])) : date('Y-m-d');
    $query = $this->model_pengumuman::where('pengumuman_uuid', $uuid)->update($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $update['deleted'] = 1;
    $query = $this->model_pengumuman::where('pengumuman_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function limit_text($text, $limit=20) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
  }

}
