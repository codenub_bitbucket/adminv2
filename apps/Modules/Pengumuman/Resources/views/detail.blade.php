@extends('app')
@section('title', 'Pengumuman Detail')
@section('content')

<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Pengumuman Detail</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Pengumuman</li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="bg-primary div-profile-user px-4 py-4 mb-3">
            <div class="media">
              <div class="media-body">
                  <h4 class="mt-0">{{ $content['pengumuman_title'] }}</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="card">
            <div class="card-body">
              <p>{!! $content['pengumuman_description'] !!}</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <ul class="list-unstyled">
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Date Start</p>
                  <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['pengumuman_start_date']))) }}</h6>
                </li>
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Date End</p>
                  <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['pengumuman_end_date']))) }}</h6>
                </li>
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Date Created</p>
                  <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['created_at']))) }}</h6>
                </li>
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Date Modified</p>
                  <h6>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($content['updated_at']))) }}</h6>
                </li>
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Created By</p>
                  <h6>{{ $content['created_by_name'] }}</h6>
                </li>
              </ul>
              <a class="btn btn-block btn-info text-white" href="{{ url('smkbisa/pengumuman/edit/'. $content['pengumuman_uuid'])}}">
                Edit
              </a>
              <a class="btn btn-block btn-danger text-white" href="{{ url('smkbisa/pengumuman/delete/'. $content['pengumuman_uuid'])}}">
                Delete
              </a>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
<!-- end main content-->

@endsection