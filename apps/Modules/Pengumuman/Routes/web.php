<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/pengumuman')->group(function() {
    Route::get('/', 'PengumumanController@index');
    Route::get('/tambah', 'PengumumanController@create');
    Route::post('/save', 'PengumumanController@save');
    Route::get('/delete/{uuid}', 'PengumumanController@delete');
    Route::get('/edit/{uuid}', 'PengumumanController@edit');
    Route::post('/update/{uuid}', 'PengumumanController@update');
    Route::get('/detail/{uuid}', 'PengumumanController@detail');
    Route::post('/mass_delete', 'PengumumanController@mass_delete');
});
