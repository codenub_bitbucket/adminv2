<?php

namespace Modules\Siswa\Entities;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table 		= "siswa";
		protected $primaryKey 	= "siswa_serial_id";
		protected $guarded = array('siswa_serial_id');
		public $timestamps = false;
}
