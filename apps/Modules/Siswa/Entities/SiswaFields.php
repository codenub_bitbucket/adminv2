<?php

namespace Modules\Siswa\Entities;

use Illuminate\Database\Eloquent\Model;

class SiswaFields extends Model
{
    protected $table 		= "siswa_fields";
    protected $primaryKey 	= "siswa_fields_serial_id";
    protected $guarded = array('siswa_fields_serial_id');
    public $timestamps = false;
}
