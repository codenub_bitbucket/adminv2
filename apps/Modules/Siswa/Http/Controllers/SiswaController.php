<?php

namespace Modules\Siswa\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Siswa\Http\Controllers\SiswaHelper as siswa_helper;
use Modules\Kelas\Http\Controllers\KelasHelper as kelas_helper;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;
use Modules\SubJurusan\Http\Controllers\SubJurusanHelper as subjurusan_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;
use Auth;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportCallBack;
use Illuminate\Support\Collection;

class SiswaController extends Controller
{
    var $module       = 'Siswa';
    var $table_module = 'siswa';
		var $input = 'Illuminate\Support\Facades\Input';
		var $excel = 'Maatwebsite\Excel\Facades\Excel';
		
    function __construct(Request $request)
    {
			$this->middleware('auth');
			session()->forget('redirect');
			session()->put('redirect', $request->path());
			
    }
    
    public function index(Request $request)
    {
			$criteria = $request->all();
			$s_helper = new siswa_helper();
			$k_helper = new kelas_helper();
			$sys = new sys();
			
			$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
			$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
			$criteria['class'] = isset($criteria['class']) ? base64_decode($criteria['class']) : 'All';

			$list_siswa = $s_helper->list_data('get', $criteria);
			$count_siswa = $s_helper->list_data('count');
			$fields = $sys->getFields($this->table_module);
			$list_kelas = $k_helper->list_data('get_dropdown');
			
			$view = 'siswa::index';

			$view_data = array(
				'table_module'   => $this->table_module,
				'list_data'      => $list_siswa,
				'count_data'     => $count_siswa,
				'data_per_page'  => $data_per_page,
				'fields'         => $fields,
				'list_kelas'         => $list_kelas,
				'class'         => $criteria['class'],
			);
        
			// echo "<pre>";
			//     print_r($view_data['list_data']);
			// exit();
	
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $sys = new sys();
        $k_helper = new kelas_helper();
        $j_helper = new jurusan_helper();
        $sj_helper = new subjurusan_helper();

        $fields = $sys->getFields($this->table_module);

        foreach ($fields as $key => $value) 
        {
					if ($value['siswa_fields_input_type'] == 'singleoption' && $value['siswa_fields_options'] !== null) 
					{
						$fields[$key]['dropdown'] = $sys->getDropdown($value['siswa_fields_options']);
					}
					elseif ($value['siswa_fields_input_type'] == 'singleoption') 
					{
						if ($value['siswa_fields_name'] == 'siswa_kelas_id') 
						{
							$list_kelas = $k_helper->list_data('get_dropdown');
							$fields[$key]['dropdown'] = $list_kelas;
						}
						else if ($value['siswa_fields_name'] == 'siswa_jurusan_id') 
						{
							$list_jurusan = $j_helper->list_data('get_dropdown');
							$fields[$key]['dropdown'] = $list_jurusan;
						}
						else if ($value['siswa_fields_name'] == 'siswa_subjurusan_id') 
						{
							$list_sjurusan = $sj_helper->list_data('get_dropdown');
							$fields[$key]['dropdown'] = $list_sjurusan;
						}
					}
        }

        $view = 'siswa::create';

        $view_data = array(
						'function' => 'save',
            'table_module'  => $this->table_module,
            'fields'        => $fields,
        );
        
				if(view()->exists($view))
				{
            return view($view, $view_data)->render();
        }
    }

    public function save(Request $request)
    {
        $s_helper = new siswa_helper();
        $sys = new sys();

				$users = Auth::user();

        $data = $request->all();
        
        if ($request->hasFile('siswa_photo')) 
        {
            $temp = new \Illuminate\Http\Request();
            $temp->replace(['file' => $data['siswa_photo']]);
            
            $upload = $sys->uploadFile($temp);

            if ($upload['status'] == 200) 
            {
                $data['siswa_photo'] = $upload['file']->image;
            }
        }

        $save = $s_helper->save($data, $users);

        if ($save)
				{
						return redirect('smkbisa/siswa')->with(['success' => 'Siswa Berhasil Disimpan']);
				}
				else
				{
						return redirect('smkbisa/siswa')->with('failed', 'Siswa Gagal Disimpan');
				}
		}
		
		public function detail(Request $request, $uuid)
		{
			$sys = new sys();
			$s_helper = new siswa_helper();

			$siswa = $s_helper->detail($uuid);
			$fields = $sys->getFields($this->table_module);
			$list_ujian = $s_helper->getHasilUjian($siswa);

			$view = 'siswa::detail';

			$view_data = array(
					'data' => $siswa,
					'fields' => $fields,
					'list_ujian' => $list_ujian,
					'table_module' => $this->table_module,
			);
			
			session()->forget('back');
			session()->put('back', 'smkbisa/siswa/detail/'.$uuid);
				
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function edit(Request $request, $uuid)
		{
			$sys = new sys();
			$k_helper = new kelas_helper();
			$j_helper = new jurusan_helper();
			$sj_helper = new subjurusan_helper();
			$s_helper = new siswa_helper();

			$siswa = $s_helper->detail($uuid);
			$fields = $sys->getFields($this->table_module);
			
			foreach ($fields as $key => $value) 
			{
				if ($value['siswa_fields_input_type'] == 'singleoption' && $value['siswa_fields_options'] !== null) 
				{
					$fields[$key]['dropdown'] = $sys->getDropdown($value['siswa_fields_options']);
				}
				elseif ($value['siswa_fields_input_type'] == 'singleoption') 
				{
					if ($value['siswa_fields_name'] == 'siswa_kelas_id') 
					{
						$list_kelas = $k_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_kelas;
					}
					else if ($value['siswa_fields_name'] == 'siswa_jurusan_id') 
					{
						$list_jurusan = $j_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_jurusan;
					}
					else if ($value['siswa_fields_name'] == 'siswa_subjurusan_id') 
					{
						$list_sjurusan = $sj_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_sjurusan;
					}
				}
			}
			
			$view = 'siswa::create';
			
			$view_data = array(
				'table_module'   => $this->table_module,
				'fields'         => $fields,
				'data' 					 => $siswa,
				'function'   				 => 'edit'	
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function update(Request $request, $uuid)
		{
			$users = Auth::user();

			$sys = new sys();
			$criteria = $request->all();

			if ($request->file('siswa_photo')) 
			{
				$temp = new \Illuminate\Http\Request();
				$temp->replace(['file' => $criteria['siswa_photo']]);
				
				$upload = $sys->uploadFile($temp);

				if ($upload['status'] == 200) 
				{
						$criteria['siswa_photo'] = $upload['file']->image;
				}
			}

			$s_helper = new siswa_helper();

			$update = $s_helper->update($criteria, $users, $uuid);

			$back = 'smkbisa/'.$this->table_module;
			$segment = session()->get('back');

			if (!empty($segment)) 
			{
				$back = $segment;
			}

			if ($update)
			{
				return redirect($back)->with(['success' => 'Siswa Berhasil Diperbaharui']);
			}
			else
			{
				return redirect($back)->with('failed', 'Siswa Gagal Diperbaharui');
			}
		}

		public function delete($uuid)
		{
			$sys = new sys();

			$s_helper = new siswa_helper();

			$delete = $s_helper->delete($uuid);

			if ($delete)
			{
					return redirect('smkbisa/siswa')->with(['success' => 'Siswa Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/siswa')->with('failed', 'Siswa Gagal Dihapus');
			}
		}

		public function absence($uuid)
		{
			$sys = new sys();

			$s_helper = new siswa_helper();

			$update = $s_helper->updateAbsence($uuid);

			$back = 'smkbisa/'.$this->table_module;
			$segment = session()->get('back');

			if (!empty($segment)) 
			{
				$back = $segment;
			}

			if ($update)
			{
				return redirect($back)->with(['success' => 'Absensi Siswa Berhasil Diperbaharui']);
			}
			else
			{
				return redirect($back)->with('failed', 'Absensi Siswa Gagal Diperbaharui');
			}
		}

		public function generate_token($type)
		{
			$sys = new sys();

			$s_helper = new siswa_helper();

			$gen = $s_helper->generateTokenSiswa($type);

			if ($gen)
			{
				return redirect()->back()->with(['success' => 'Berhasil mengisi token siswa.']);
			}
			else
			{
				if($type == 'empty')
				{
					return redirect()->back()->with('failed', 'Semua token siswa sudah terisi!');
				}
				else
				{
					return redirect()->back()->with('failed', 'Gagal mengisi token siswa!');
				}
			}
		}

		public function reset_single_token($uuid)
		{
			$sys = new sys();

			$s_helper = new siswa_helper();

			$reset = $s_helper->resetSingleToken($uuid);($type);

			if ($reset)
			{
				return redirect()->back()->with(['success' => 'Berhasil mereset token siswa.']);
			}
			else
			{
				return redirect()->back()->with('failed', 'Gagal mereset token siswa!');
			}
		}

		public function import()
    {
			$sys = new sys();

			$view = 'siswa::import';

			$view_data = array(
					
			);
        
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}
		

		public function importPreview(Request $request)
		{
			session()->forget('redirect');

			$request->validate([
					'file' => 'required|mimes:csv,xls,xlsx'
			]);
			
			$array = Excel::toArray(new ImportCallBack, $request->file('file'));

			$fieldsHeader = $array[0][0];
			$dataReal = array();
			unset($array[0][0]);

			foreach ($fieldsHeader as $key => $value) 
			{
				if($value == 'Nama')
				{
					$fieldsHeader[$key] = 'siswa_name';
				}
				else if($value == 'NIS')
				{
					$fieldsHeader[$key] = 'siswa_nis';
				}
				else if($value == 'NISN')
				{
					$fieldsHeader[$key] = 'siswa_nisn';
				}
				else if($value == 'Tempat Lahir')
				{
					$fieldsHeader[$key] = 'siswa_dob_place';
				}
				else if($value == 'Tanggal Lahir')
				{
					$fieldsHeader[$key] = 'siswa_dob';
				}
				else if($value == 'Jenis Kelamin')
				{
					$fieldsHeader[$key] = 'siswa_gender';
				}
				else if($value == 'Agama')
				{
					$fieldsHeader[$key] = 'siswa_agama';
				}
				else if($value == 'Nomor Telepon')
				{
					$fieldsHeader[$key] = 'siswa_phone';
				}
				else if($value == 'Alamat')
				{
					$fieldsHeader[$key] = 'siswa_address';
				}
			}

			foreach ($array[0] as $key => $value) 
			{
				foreach ($value as $k => $v) 
				{
					$newValue[$fieldsHeader[$k]] = $v;
					if(!empty($newValue['siswa_name']))
					{
						$dataReal[$key] = $newValue;
					}
				}
			}

			$k_helper = new kelas_helper();
			$j_helper = new jurusan_helper();
			$sj_helper = new subjurusan_helper();

			$kelas = $k_helper->list_data('get_dropdown');
			$jurusan = $j_helper->list_data('get_dropdown');
			$subjurusan = $sj_helper->list_data('get_dropdown');

			foreach ($dataReal as $key => $value) 
			{
				foreach ($value as $k => $v)
				{
					if($k == '' || empty($k))
					{
						unset($dataReal[$key][$k]);
					}
				}
			}

			// echo "<pre>"; 
			//   print_r($dataReal);
			// exit();

			$dataReal = array_values($dataReal);
			
			$view_data = array(
				'data' => $dataReal,
				'kelas' => $kelas,
				'jurusan' => $jurusan,
				'subjurusan' => $subjurusan,
			);

			$view = 'siswa::import-preview';
        
			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
		}

		public function importSave(Request $request)
		{
			$sys = new sys();
			$s_helper = new siswa_helper();
			$users = Auth::user();

			$data = $request->all();

			$data_siswa = json_decode($data['data_siswa'], true);
			unset($data['_token']);
			unset($data['data_siswa']);


			$success = 0;
			$failed = 0;


			foreach ($data_siswa as $key => $value)
			{
				foreach ($data as $k => $v)
				{
					$value[$k] = $v;
				}

				foreach ($value as $dk => $dv) 
				{
					if($dk == 'siswa_dob')
					{
						$value[$dk] = !empty($dv) ? date("Y-m-d", strtotime($dv)) : '1111-11-11';
					}
					else if($dk == 'siswa_gender')
					{
						$value[$dk] = $sys->getDropdownValueByLabel($dv, 'gender');
					}
					else if($dk == 'siswa_agama')
					{
						$value[$dk] = $sys->getDropdownValueByLabel($dv, 'religion');
					}
				}
				$save = $s_helper->save($value, $users);
				if ($save) 
				{
					$success++;
				}
				else
				{
					$failed++;
				}
			}

			if ($success >= 0 || $failed >= 0)
			{
					return redirect('smkbisa/siswa')->with(['success' => 'Siswa Berhasil Diimport : '.$success.' Berhasil, '.$failed.' Gagal']);
			}
			else
			{
					return redirect('smkbisa/siswa')->with('failed', 'Siswa Gagal Diimport');
			}
		}

		public function mass_delete(Request $request)
		{
			$data = $request->all();
			$s_helper = new siswa_helper();

			$data = json_decode($data['data_selected'], true);
			$delete = $s_helper->mass_delete($data);

			if ($delete)
			{
					return redirect('smkbisa/siswa')->with(['success' => 'Siswa Berhasil Dihapus']);
			}
			else
			{
					return redirect('smkbisa/siswa')->with('failed', 'Siswa Gagal Dihapus');
			}
		}
}
