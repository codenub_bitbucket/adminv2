<?php
namespace Modules\Siswa\Http\Controllers;

use Session;
use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Ujian\Http\Controllers\UjianHelper as ujian;

class SiswaHelper
{
  var $table_module = 'siswa';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_siswa = 'Modules\Siswa\Entities\Siswa';

  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    $selectOther = "kelas.kelas_name as siswa_kelas_name,
                    users.name as created_by_name,
                    jurusan.jurusan_name as siswa_jurusan_name,
                    subjurusan.subjurusan_name as siswa_sub_jurusan_name,
                    gen.dropdown_options_label as siswa_gender,
                    agama.dropdown_options_label as siswa_agama,
                    u2.email
                    ";
    $query = $this->model_siswa::select('siswa.*', DB::raw($selectOther))
                                ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
                                ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
                                ->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
                                ->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'siswa.siswa_gender')
                                ->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'siswa.siswa_agama')
                                ->leftjoin('users', 'users.id',"=", "siswa.created_by")
                                ->leftjoin('sys_rel', function($join)
                                {
                                  $join->on('sys_rel.rel_to_id', '=', 'siswa.siswa_serial_id')
                                    ->where('sys_rel.rel_from_module', '=', 'users')
                                    ->where('sys_rel.rel_to_module', '=', 'siswa');
                                })
                                ->leftJoin('users as u2', 'u2.id', '=', 'sys_rel.rel_from_id');
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('siswa.siswa_name',"LIKE", "%".$criteria['search']."%");
                                }
                                $query->where('siswa.deleted', 0);
                                if (isset($criteria['class']) && $criteria['class'] !== 'All') 
                                {
                                  $query->where('siswa.siswa_kelas_id', $criteria['class']);
                                }

    if ($type == 'get')
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown')
    {
      $query = $query->select($this->table_module.".".$this->table_module."_name as label", $this->table_module.".".$this->table_module."_serial_id as value", DB::raw($selectOther))->take(15)->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;

    $sys = new sys();

    $email = $sys->createEmailSiswa($data);
    $pass = $email['password'];
    $dataUser = array(
      'name'      => $data['siswa_name'],
      'email'     => (isset($email['email']) && !empty($email['email'])) ? $email['email'] : $sys->generateRandomString().'@smkkartikatama.sch.id',
      'password'  => bcrypt($pass),
      'photo'     => isset($data['siswa_photo']) ? $data['siswa_photo'] : null,
      'role'      => 3, //1 => admin, 2 => guru, 3 => siswa, => 4 => wali
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
    );

    unset($data['_token']);
    unset($data['siswa_photo']);
    
    $data['siswa_dob'] = isset($data['siswa_dob']) ? date("Y-m-d", strtotime($data['siswa_dob'])) : '1111-11-11';
    $data['siswa_uuid'] = Uuid::uuid4()->toString();
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $users->id;
    $data['siswa_password'] = $pass;

    $query = $this->model_siswa::insertGetId($data);

    if ($query)
    {
      $query2 = DB::table('users')->insertGetId($dataUser);
      if($query2)
      {
        $rel = array(
          'rel_from_module' => 'users',
          'rel_from_id' => $query2,
          'rel_to_module' => 'siswa',
          'rel_to_id' => $query,
        );
        $query3 = DB::table('sys_rel')->insert($rel);
      }
      $result = true;
    }


    return $result;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $dataUser = array(
      'photo'     => isset($data['siswa_photo']) ? $data['siswa_photo'] : null,
      'updated_at' => date('Y-m-d H:i:s'),
    );

    unset($data['siswa_photo']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['siswa_dob'] = isset($data['siswa_dob']) ? date("Y-m-d", strtotime($data['siswa_dob'])) : '1111-11-11';
    $query = $this->model_siswa::where('siswa_uuid', $uuid)->first();
    $query->update($data);

    $serial_id = $query->siswa_serial_id;

    if ($query)
    {
      $getRel = DB::table('sys_rel')->where('rel_to_id', $serial_id)
                                    ->where('rel_from_module', 'users')
                                    ->where('rel_to_module', 'siswa')
                                    ->first();

      if (@count($getRel) > 0)
      {
        $getRel = json_decode(json_encode($getRel),true);

        $query2 = DB::table('users')->where('id', $getRel['rel_to_id'])->update($dataUser);
      }

      $result = true;
    }
    return $query;
  }

  function detail($uuid)
  {
    $result = array();

    $query = $this->model_siswa::select('siswa.*',
                                        DB::raw('kelas.kelas_name as siswa_kelas_name'),
                                        DB::raw('subjurusan.subjurusan_name as siswa_sub_jurusan_name'),
                                        DB::raw('jurusan.jurusan_name as siswa_jurusan_name'),
                                        DB::raw('gen.dropdown_options_label as siswa_gender_name'),
                                        DB::raw('agama.dropdown_options_label as siswa_agama_name'),
                                        "users.photo as siswa_photo", "users.email")
                                ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
                                ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
                                ->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
                                ->leftjoin('dropdown_options as gen', 'gen.dropdown_options_serial_id', '=', 'siswa.siswa_gender')
                                ->leftjoin('dropdown_options as agama', 'agama.dropdown_options_serial_id', '=', 'siswa.siswa_agama')
                                ->leftjoin('sys_rel', function($join)
                                {
                                  $join->on('sys_rel.rel_to_id', '=', 'siswa.siswa_serial_id')
                                    ->where('sys_rel.rel_from_module', '=', 'users')
                                    ->where('sys_rel.rel_to_module', '=', 'siswa');
                                })
                                ->leftJoin('users', 'users.id', '=', 'sys_rel.rel_from_id')
                                ->where('siswa.siswa_uuid', $uuid)
                                ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;

  }

  function delete($uuid)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->model_siswa::where('siswa_uuid', $uuid)->first();
    $query->update($update);

    $serial_id = $query->siswa_serial_id;

    if ($query)
    {
      $getRel = DB::table('sys_rel')->where('rel_to_module', 'siswa')->where('rel_from_module', 'users')->where('rel_to_id', $serial_id)->first();

      if(@count($getRel) > 0)
      {
        $users = DB::table('users')->where('id', $getRel->rel_from_id)->first();
        if(@count($users) > 0)
        {
          $email = $users->email;
          $uname = explode('@', $email);
          $sys->deleteEmailSiswa($uname[0]);
        }
        DB::table('users')->where('id', $getRel->rel_from_id)->delete();
        $delRel = DB::table('sys_rel')->where('rel_to_module', 'siswa')->where('rel_from_module', 'users')->where('rel_to_id', $serial_id)->delete();
      }
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->model_siswa::whereIn('siswa_uuid', $data)->update($update);
    $get = $this->model_siswa::whereIn('siswa_uuid', $data)->get();

    if ($query)
    {
      if(@count($get) > 0)
      {
        $serial_ids = array();
        foreach($get as $key => $value)
        {
          $serial_ids[] = $value->siswa_serial_id;
        }

        $getRel = DB::table('sys_rel')->where('rel_to_module', 'siswa')->where('rel_from_module', 'users')->whereIn('rel_to_id', $serial_ids)->get();

        if(@count($getRel) > 0)
        {
          $rel_ids = array();
          foreach($getRel as $key => $value)
          {
            $rel_ids[] = $value->rel_to_id;
          }
          $users = DB::table('users')->whereIn('id', $rel_ids)->get();
          if(@count($users) > 0)
          {
            foreach ($users as $key => $value)
            {
              $email = $value->email;
              $uname = explode('@', $email);
              $sys->deleteEmailSiswa($uname[0]);
            }
          }
          DB::table('users')->whereIn('id', $rel_ids)->delete();
          $delRel = DB::table('sys_rel')->where('rel_to_module', 'siswa')->where('rel_from_module', 'users')->whereIn('rel_to_id', $serial_ids)->delete();
        }
      }
      $result = true;
    }
    return $query;
  }

  function updateAbsence($uuid)
  {
    $result = false;
    $get = $this->model_siswa::where('siswa_uuid', $uuid)->where('deleted', 0)->first();
    if(@count($get) > 0)
    {
      $get = json_decode(json_encode($get), true);

      $current_absence = $get['siswa_absence'];

      if ($current_absence !== null && $current_absence !== "")
      {
        $current_absence = json_decode($current_absence, true);

        /* cari dulu hari ini udah di absen belum */
        if(array_key_exists(date('Y-m-d'), $current_absence))
        {
          $current_absence[date('Y-m-d')] = !$current_absence[date('Y-m-d')];
        }
        else
        {
          if(@count($current_absence) >= 7)
          {
            unset($current_absence[array_key_first($current_absence)]);
          }

          $current_absence[date('Y-m-d')] = false;
        }

        $update['siswa_absence'] = json_encode($current_absence);

        $query = $this->model_siswa::where('siswa_uuid', $uuid)->update($update);
        if ($query)
        {
          $result = true;
        }
      }
      else
      {
        $current_absence[date('Y-m-d')] = false;
        $update['siswa_absence'] = json_encode($current_absence);

        $query = $this->model_siswa::where('siswa_uuid', $uuid)->update($update);
        if ($query)
        {
          $result = true;
        }
      }
    }

    return $result;
  }

  function getHasilUjian($data)
  {
    $result = array();

    $sys_ujian = new ujian();

    $query = DB::table('hasil_ujian as hu')->select('hu.*', 'ujian.ujian_name', 'mp.mata_pelajaran_name', "guru.guru_name")
              ->leftjoin('sys_rel', function($join) use ($data)
              {
                $join->on('sys_rel.rel_from_id', '=', 'hu.hasil_ujian_serial_id')
                  ->where('sys_rel.rel_from_module', '=', 'hasil_ujian')
                  ->where('sys_rel.rel_to_module', '=', 'siswa');
                })
              ->where('sys_rel.rel_to_id', '=', $data['siswa_serial_id'])
              ->leftJoin('ujian', 'ujian.ujian_serial_id', '=', 'hu.ujian_serial_id')
              ->leftJoin('mata_pelajaran as mp', 'mp.mata_pelajaran_serial_id', '=', 'ujian.ujian_mata_pelajaran_id')
              ->leftjoin('guru', function($join) use ($data)
              {
                $join->on('guru.guru_mata_pelajaran_id', '=', 'mp.mata_pelajaran_serial_id')
                  ->where('guru.guru_kelas_id', '=', $data['siswa_kelas_id']);
              })
              ->where('hu.deleted', 0)
              ->get();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
      foreach($result as $key => $value)
      {
        $result[$key]['total_soal'] = $sys_ujian->getSoal($value, 'count');
      }
      // dd($result);
    }

    return $result;
  }

  function generateTokenSiswa($type)
  {
    $result = false;

    $sys = new sys();

    if($type == 'empty')
    {
      $get = $this->model_siswa::whereNull('siswa_token')->orWhere('siswa_token', '')->where('deleted', 0)->get();
    }
    else
    {
      $get = $this->model_siswa::where('deleted', 0)->get();
    }

    if(@count($get) > 0)
    {
      $get = json_decode(json_encode($get), true);

      foreach($get as $key => $value)
      {
        $token = $sys->generateRandomToken();
  
        $update = $this->model_siswa::where('siswa_serial_id', $value['siswa_serial_id'])->update(['siswa_token' => $token]);
      }
      $result = true;
    }

    return $result;
  }

  function resetSingleToken($uuid)
  {
    $result = false;

    $sys = new sys();

    $token = $sys->generateRandomToken();

    $reset = $this->model_siswa::where('siswa_uuid', $uuid)->update(['siswa_token' => $token]);

    if($reset)
    {
      $result = true;
    }

    return $result;
  }

}
