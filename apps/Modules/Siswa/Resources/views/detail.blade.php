@extends('app')
@section('title', 'Siswa')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Detail Siswa</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Siswa</li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="bg-primary div-profile-user px-3 py-4">
              <a href="{{ url('smkbisa/siswa/absence/' . $data[$table_module.'_uuid'])}}" class="btn btn-info btn-sm" style="position: absolute; right: 130px; margin-top: -8px; ">
                @php
                    $absence = json_decode($data[$table_module.'_absence'], true);
                    if (empty($absence[date('Y-m-d')])) 
                    {
                      $absence[date('Y-m-d')] = false;
                    }
                @endphp
                {{ ( $data[$table_module.'_absence'] !== null && !empty($data[$table_module.'_absence']) && $absence[date('Y-m-d')] == TRUE ) ? "Tandai Siswa Tidak Masuk" : "Tandai Siswa Masuk" }}
              </a>
              <a href="{{ url('smkbisa/siswa/delete/' . $data[$table_module.'_uuid'])}}" class="btn btn-danger btn-sm" style="position: absolute; right: 65px; margin-top: -8px; ">
                Hapus
              </a>
              <a href="{{ url('smkbisa/siswa/edit/' . $data[$table_module.'_uuid'])}}" class="btn btn-light btn-sm" style="position: absolute; right: 15px; margin-top: -8px; ">
                Edit
              </a>
              <div class="media align-items-center d-flex ">
                <img class="img-thumbnail rounded-circle avatar-lg bg-white mr-4" alt="" src="{{ URL('apps/public/'.$data['siswa_photo']) }}" data-holder-rendered="true">
                <div class="media-body">
                    <h4 class="mt-0">{{$data['siswa_name']}}</h4>
                    <p class="mb-0 text-white">
                      <span>{{ $data['siswa_kelas_name']." ".$data['siswa_jurusan_name']." ".$data['siswa_sub_jurusan_name'] }}</span>
                    </p>
                </div>
              </div>
            </div>
            <div class="card-body">
              <ul class="list-unstyled">
                @if (@count($fields) > 0)
                  @foreach ($fields as $fk => $f)
                    @if ($f[$table_module.'_fields_name'] !== $table_module.'_photo')
                    <li class="list-item border-bottom pb-2 mb-1">
                      <p class="mb-1">{{ $f[$table_module.'_fields_label'] }}</p>
                      <h6>
                        @if ($f[$table_module.'_fields_input_type'] == 'date')
                        {{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($data[$f[$table_module.'_fields_name']]))) }}
                        @elseif($f[$table_module.'_fields_name'] == 'siswa_gender')
                        {{ $data['siswa_gender_name'] }}
                        @elseif($f[$table_module.'_fields_name'] == 'siswa_kelas_id')
                        {{ $data['siswa_kelas_name'] }}
                        @elseif($f[$table_module.'_fields_name'] == 'siswa_agama')
                        {{ $data['siswa_agama_name'] }}
                        @elseif($f[$table_module.'_fields_name'] == 'siswa_jurusan_id')
                        {{ $data['siswa_jurusan_name'] }}
                        @elseif($f[$table_module.'_fields_name'] == 'siswa_subjurusan_id')
                        {{ $data['siswa_sub_jurusan_name'] }}
                        @else
                        {{ $data[$f[$table_module.'_fields_name']] }}
                        @endif
                      </h6>
                    </li>
                    @endif
                  @endforeach
                @endif
                <li class="list-item border-bottom pb-2 mb-1">
                  <p class="mb-1">Email</p>
                  <h6> {{ $data['email'] }} </h6>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="card">
            <div class="card-body">
              {{-- <div class="float-right btn-group align-items-center d-flex">
                <div class="dropdown mr-2">
                  <button class="btn btn-light dropdown-toggle btn-sm d-flex justify-content-between align-items-center type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    All Course <i class=" ri-arrow-down-s-line ml-3"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Matematika</a>
                    <a class="dropdown-item" href="#">Bahasa Indonesia</a>
                  </div>
                </div>
                <div class="dropdown">
                  <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="mdi mdi-dots-vertical"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a href="javascript:void(0);" class="dropdown-item">Close</a>
                  </div>
                </div>
              </div> --}}
              <h4 class="card-title mb-3">Daftar Hasil Ujian</h4>

              <!-- start kalo kosong -->
              @if (@count($list_ujian) == 0)
              <div class="d-flex align-items-center w-100 bg-light h-300">
                <div class="m-auto text-center">
                  <div class="avatar-sm m-auto rounded-circle bg-secondary align-items-center d-flex">
                    <div class="avatar-content m-auto text-white">
                      <i class=" ri-book-read-line"></i>
                    </div>
                  </div>
                  <h6 class="mt-3 mb-0">Belum ada ujian yang dikerjakan</h6>
                </div>
              </div>
              @endif
              <!-- end kalo kosong -->

              @if (@count($list_ujian) > 0)
                <ul class="list-unstyled mt-4 h-550">
                  @foreach ($list_ujian as $key => $item)
                  <li class="list-item align-items-center d-flex justify-content-between border-bottom mb-2 pb-3">
                    <div class="media align-items-center d-flex w-100">
                      <div class="avatar-sm mr-3 rounded-circle bg-primary align-items-center d-flex">
                        <div class="avatar-content m-auto text-white">
                          <i class=" ri-book-read-line"></i>
                        </div>
                      </div>
                      <div class="media-body">
                        <small>{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</small>
                        <h6 class="mt-1 mb-1">{{ $item['ujian_name'] }} - {{ $item['mata_pelajaran_name'] }}</h6>
                        <p class="mb-0">{{ $item['guru_name'] }}</p>
                      </div>
                    </div>
                    <div class="d-flex align-items-center">
                      <div class="text-right mr-3">
                        <span>Hasil</span>
                        <h6 class="mb-0 text-primary">{{ json_decode($item['hasil_ujian_result'], true)['true'] }}/{{ json_decode($item['hasil_ujian_result'], true)['false'] }}/{{ json_decode($item['hasil_ujian_result'], true)['none'] }}</h6>
                      </div>
                      <div class="text-right mr-3">
                        <span>Nilai</span>
                        @php
                            $nilai = (json_decode($item['hasil_ujian_result'], true)['true']/$item['total_soal']) * 100;
                        @endphp
                        <h6 class="mb-0 text-{{($nilai <= 60) ? 'danger' : 'primary'}}">{{ $nilai }}</h6>
                      </div>
                      
                    </div> 
                  </li>
                  @endforeach
                </ul>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
</div>
@endsection