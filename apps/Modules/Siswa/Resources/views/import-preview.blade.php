@extends('app')
@section('title', 'Import Siswa')
@section('content')
<div class="main-content">
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Import Siswa Preview</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Siswa</li>
                <li class="breadcrumb-item">Import</li>
                <li class="breadcrumb-item active">Preview</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="card shadow mb-4">
        <div class="card-body">
          <h6>Total data to be imported : {{ @count($data) }}</h6>
          <hr>
          <h6>Silahkan tentukan info data yang akan ada import.</h6>
          <form method="post" action="{{ URL('smkbisa/siswa/import_save') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="data_siswa" value="{{ json_encode($data, true) }}">
            <div class="form-group">
              <label>Kelas</label>
              <select class="form-control" name="siswa_kelas_id">
                @foreach ($kelas as $item)
                  <option value="{{ $item['value'] }}">{{ $item['label'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Jurusan</label>
              <select class="form-control" name="siswa_jurusan_id">
                @foreach ($jurusan as $item)
                  <option value="{{ $item['value'] }}">{{ $item['label'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Sub Jurusan</label>
              <select class="form-control" name="siswa_subjurusan_id">
                @foreach ($subjurusan as $item)
                  <option value="{{ $item['value'] }}">{{ $item['label'] }}</option>
                @endforeach
              </select>
            </div>
            <a href="{{ URL('smkbisa/siswa')}}"type="button" class="btn btn-secondary">Batal</a>
            <button type="submit" class="btn btn-primary">Proses Import</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection