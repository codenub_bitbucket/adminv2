<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/siswa')->group(function() {
    Route::get('/', 'SiswaController@index');
    Route::get('/tambah', 'SiswaController@create');
    Route::post('/save', 'SiswaController@save');
    Route::get('/delete/{uuid}', 'SiswaController@delete');
    Route::get('/edit/{uuid}', 'SiswaController@edit');
    Route::post('/update/{uuid}', 'SiswaController@update');
    Route::get('/detail/{uuid}', 'SiswaController@detail');
    Route::get('/absence/{uuid}', 'SiswaController@absence');
    
    Route::get('/import', 'SiswaController@import');
    Route::post('/import_preview', 'SiswaController@importPreview');
    Route::post('/import_save', 'SiswaController@importSave');

    Route::post('/mass_delete', 'SiswaController@mass_delete');
    
    Route::get('/generate-token/{type}', 'SiswaController@generate_token');
    Route::get('/token/reset/{uuid}', 'SiswaController@reset_single_token');
});
