<?php

namespace Modules\Soal\Entities;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table 		= "soal";
		protected $primaryKey 	= "soal_serial_id";
		protected $guarded = array('soal_serial_id');
		public $timestamps = false;
}
