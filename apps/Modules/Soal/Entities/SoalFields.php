<?php

namespace Modules\Soal\Entities;

use Illuminate\Database\Eloquent\Model;

class SoalFields extends Model
{
    protected $table 		= "soal_fields";
    protected $primaryKey 	= "soal_fields_serial_id";
    protected $guarded = array('soal_fields_serial_id');
    public $timestamps = false;
}
