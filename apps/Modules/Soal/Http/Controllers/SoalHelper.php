<?php
namespace Modules\Soal\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class SoalHelper
{
  var $table_module = 'soal';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_soal = 'Modules\Soal\Entities\Soal';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_guru::select("guru.*", DB::raw('kelas.kelas_name as guru_kelas_name'), DB::raw('dropdown_options.dropdown_options_label as guru_gender'), "mata_pelajaran.mata_pelajaran_name")
                              ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'guru.guru_kelas_id')
                              ->leftjoin('mata_pelajaran', 'mata_pelajaran.mata_pelajaran_serial_id', '=', 'guru.guru_mata_pelajaran_id')
                              ->leftjoin('dropdown_options', 'dropdown_options.dropdown_options_serial_id', '=', 'guru.guru_gender');
                              if (isset($criteria['search']) && $criteria['search'] != '')
                              {
                                $query->where('guru.guru_name',"LIKE", "%".$criteria['search']."%");
                              }
                              $query->where('guru.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("guru.guru_name as label", "guru.guru_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_guru::select('guru.*', 'mata_pelajaran.mata_pelajaran_name as guru_mata_pelajaran_id_label')->where('guru.guru_uuid', $uuid)
                              ->leftjoin('mata_pelajaran', 'mata_pelajaran.mata_pelajaran_serial_id', '=', 'guru.guru_mata_pelajaran_id')
                              ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function save($data)
  {
    $result = false;
    unset($data['_token']);

    $data['guru_dob'] = isset($data['guru_dob']) ? date("Y-m-d", strtotime($data['guru_dob'])) : '1111-11-11';
    $data['guru_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_guru::insert($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['guru_dob'] = isset($data['guru_dob']) ? date("Y-m-d", strtotime($data['guru_dob'])) : '1111-11-11';
    $query = $this->model_guru::where('guru_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_guru::where('guru_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
}