@extends('app')
@section('title', 'Import Soal')
@section('content')
<div class="main-content">
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Import Soal Preview</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Soal</li>
                <li class="breadcrumb-item">Import</li>
                <li class="breadcrumb-item active">Preview</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="card shadow mb-4">
        <div class="card-body">
          <h6>Total data to be imported : {{ @count($data) }}</h6>
          <hr>
          <h6>Silahkan klik proses untuk melanjutkan import data.</h6>
          <form method="post" action="{{ URL('smkbisa/ujian/soal/import_save') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="data_soal" value="{{ json_encode($data, true) }}">
            <input type="hidden" name="ujian_uuid" value="{{ $ujian_uuid }}">
            <a href="{{ URL('smkbisa/ujian/detail/'.$ujian_uuid)}}"type="button" class="btn btn-secondary">Batal</a>
            <button type="submit" class="btn btn-primary">Proses Import</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection