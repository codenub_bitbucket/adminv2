<?php

namespace Modules\SubJurusan\Entities;

use Illuminate\Database\Eloquent\Model;

class SubJurusan extends Model
{
    protected $table 		= "subjurusan";
    protected $primaryKey 	= "subjurusan_serial_id";
    protected $guarded = array('subjurusan_serial_id');
    public $timestamps = false;
}
