<?php

namespace Modules\SubJurusan\Entities;

use Illuminate\Database\Eloquent\Model;

class SubJurusanFields extends Model
{
    protected $table 		= "subjurusan_fields";
    protected $primaryKey 	= "subjurusan_fields_serial_id";
    protected $guarded = array('subjurusan_fields_serial_id');
    public $timestamps = false;
}
