<?php

namespace Modules\SubJurusan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\SubJurusan\Http\Controllers\SubJurusanHelper as subjurusan_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class SubJurusanController extends Controller
{
	var $module       = 'Sub Jurusan';
	var $table_module = 'subjurusan';

	function __construct(Request $request)
	{
			$this->middleware('auth');
			session()->forget('redirect');
			session()->put('redirect', $request->path());
			
	}

	public function index(Request $request)
	{
		$criteria = $request->all();
		$sys = new sys();
		$sj_helper = new subjurusan_helper();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		$list_subjurusan = $sj_helper->list_data('get', $criteria);
		$count_subjurusan = $sj_helper->list_data('count');

		$fields = $sys->getFields($this->table_module);

		$view = 'subjurusan::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_subjurusan,
			'count_data'     => $count_subjurusan,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function create()
	{
		$sys = new sys();

		$fields = $sys->getFields($this->table_module);

		$view = 'subjurusan::create';

		$view_data = array(
				'table_module'  => $this->table_module,
				'fields'        => $fields,
				'function'   				 => 'save'	
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function save(Request $request)
	{
		$sj_helper = new subjurusan_helper();
		$sys = new sys();

		$status       = 500;
		$msg          = "Gagal Menyimpan Data Sub Jurusan";

		$data = $request->all();
		
		$save = $sj_helper->save($data);

		if ($save)
		{
			return redirect('smkbisa/subjurusan')->with(['success' => 'Sub Jurusan Berhasil Ditambah']);
		}
		else
		{
			return redirect('smkbisa/subjurusan')->with('failed', 'Sub Jurusan Gagal Ditambah');
		}
	}

	public function edit($uuid)
	{
		$sj_helper = new subjurusan_helper();
		$sys = new sys();
		$detail = $sj_helper->get_detail($uuid);

		
		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
		{
			if ($value['subjurusan_fields_input_type'] == 'singleoption' && $value['subjurusan_fields_options'] !== null) 
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['subjurusan_fields_options']);
			}
		}

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		$view = 'subjurusan::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function update(Request $request, $uuid)
	{
		$sys = new sys();
		$criteria = $request->all();

		$sj_helper = new subjurusan_helper();

		$update = $sj_helper->update($criteria, $uuid);

		if ($update)
		{
			return redirect('smkbisa/subjurusan')->with(['success' => 'Sub Jurusan Berhasil Diperbaharui']);
		}
		else
		{
			return redirect('smkbisa/subjurusan')->with('failed', 'Sub Jurusan Gagal Diperbaharui');
		}
	}
		
	public function delete($uuid)
	{
		$sys = new sys();

		$sj_helper = new subjurusan_helper();

		$delete = $sj_helper->delete($uuid);

		if ($delete)
		{
				return redirect()->back()->with(['success' => 'Sub Jurusan Berhasil Dihapus']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Sub Jurusan Gagal Dihapus');
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new subjurusan_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
				return redirect('smkbisa/subjurusan')->with(['success' => 'SubJurusan Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/subjurusan')->with('failed', 'SubJurusan Gagal Dihapus');
		}
	}
}
