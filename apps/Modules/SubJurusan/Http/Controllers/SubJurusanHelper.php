<?php
namespace Modules\SubJurusan\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class SubJurusanHelper
{
  var $table_module = 'subjurusan';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_subjurusan = 'Modules\SubJurusan\Entities\SubJurusan';
  
  function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_subjurusan::where('subjurusan.deleted', 0);
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('subjurusan.subjurusan_name',"LIKE", "%".$criteria['search']."%");
                                }

    if ($type == 'get') 
    {
      $result = $query->paginate($criteria['show']);
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("subjurusan.subjurusan_name as label", "subjurusan.subjurusan_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_subjurusan::where('subjurusan_uuid', $uuid)->first();
    
    if (@count($query) > 0) 
    {
      $result = json_decode(json_encode($query),true);
    }

    return $result;
  }

  function save($data)
  {
    $result = false;
    unset($data['_token']);
    
    $data['subjurusan_uuid'] = Uuid::uuid4()->toString();
    $query = $this->model_subjurusan::insert($data);
    if (@count($query) > 0)
    {
      $result = true;
    }
    return $query;
        
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_subjurusan::where('subjurusan_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $uuid)
  {
    $result = false;
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_subjurusan::where('subjurusan_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
}