<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('subjurusan')->group(function() {
//     Route::get('/', 'SubJurusanController@index');
//     Route::get('/create', 'SubJurusanController@create');
//     Route::post('/save', 'SubJurusanController@save');
// });

Route::prefix('smkbisa/subjurusan')->group(function() {
    Route::get('/', 'SubJurusanController@index');
    Route::get('/tambah', 'SubJurusanController@create');
    Route::post('/add', 'SubJurusanController@save');
    Route::get('/delete/{uuid}', 'SubJurusanController@delete');
    Route::get('/edit/{uuid}', 'SubJurusanController@edit');
    Route::post('/update/{uuid}', 'SubJurusanController@update');

    Route::post('/mass_delete', 'SubJurusanController@mass_delete');
});
