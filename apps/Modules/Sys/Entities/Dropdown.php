<?php

namespace Modules\Sys\Entities;

use Illuminate\Database\Eloquent\Model;

class Dropdown extends Model
{
    protected $table 		= "dropdown";
    protected $primaryKey 	= "dropdown_serial_id";
    protected $guarded = array('dropdown_serial_id');
    public $timestamps = false;
}
