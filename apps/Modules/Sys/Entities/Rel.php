<?php

namespace Modules\Sys\Entities;

use Illuminate\Database\Eloquent\Model;

class Rel extends Model
{
    protected $table 		= "sys_rel";
    protected $primaryKey 	= "rel_serial_id";
    protected $guarded = array('rel_serial_id');
    public $timestamps = false;
}
