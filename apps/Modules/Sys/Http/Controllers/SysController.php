<?php

namespace Modules\Sys\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Siswa\Http\Controllers\SiswaHelper as sys_siswa;
use Modules\MataPelajaran\Http\Controllers\MataPelajaranHelper as sys_mata_pelajaran;

class SysController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function uploadFile(Request $request)
    {
        $response = null;
        $file = (object) ['file' => ""];

        if ($request->hasFile('file')) {
            $original_filename = $request->file('file')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $destination_path = 'apps/uploads/';
            $image = $original_filename;

            if ($request->file('file')->move($destination_path, $image)) {
                $file->image = 'uploads/' . $image;

                $status = 200;
                $response = array(
                    'status'    => $status,
                    'message'   => 'OK',
                    'file'      => $file
                );
            } else {

                $status = 500;
                $response = array(
                    'status'    => $status,
                    'message'   => 'Cannot upload file'
                );
            }
        } else {
            $status = 404;
            $response = array(
                'status'    => $status,
                'message'   => 'File not found'
            );
        }

        return response()->json($response, $status);
    }

    public function deleteFile(Request $request)
    {
        $filename =  $request->get('filename');
        $path='apps/uploads/'.$filename;
        if (file_exists($path)) 
        {
            unlink($path);
            $status = 200;
            $response = array(
                'status'    => $status,
                'message'   => 'OK',
            );
        }
        else
        {
            $status = 500;
            $response = array(
                'status'    => $status,
                'message'   => 'Failed Delete file'
            );
        }
        return response()->json($response, $status);
    }

    public function autocomplete(Request $request, $table_module)
    {
        $data = array();
        if($table_module == 'siswa')
        {
            $sys_module = new sys_siswa();
        }
        else if($table_module == 'mata_pelajaran')
        {
            $sys_module = new sys_mata_pelajaran();
        }
        
        if(isset($sys_module) && gettype($sys_module) == 'object')
        {
            $criteria['search'] = $request->has('q') ? $request->q : "";
            $data = $sys_module->list_data('get_dropdown', $criteria);
        }

		return response()->json($data);
    }

    public function savesettingsoal(Request $request)
		{
			if (!Auth::user()) 
			{
					return "ngapain bosss!!!";
			}

			$sys = new sys();

			$status = 500;
			$response = array(
					'status'    => $status,
					'message'   => 'Error'
			);

			$save = $sys->saveSettingSoal($request->all());

			if ($save)
			{
				$status = 200;
				$response = array(
						'status'    => $save,
						'message'   => 'Success'
				);
			}

			return response()->json($response, $status);
		}
}
