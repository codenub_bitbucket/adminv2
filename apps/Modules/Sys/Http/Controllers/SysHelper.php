<?php
namespace Modules\Sys\Http\Controllers;

use App\Classes\xmlapi;

use DB;
use Intervention\Image\Facades\Image;

class SysHelper
{
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_siswa = 'Modules\Siswa\Entities\Siswa';
  var $model_fields_siswa = 'Modules\Siswa\Entities\SiswaFields';
  var $model_fields_kelas = 'Modules\Kelas\Entities\KelasFields';
  var $model_fields_jurusan = 'Modules\Jurusan\Entities\JurusanFields';
  var $model_fields_subjurusan = 'Modules\SubJurusan\Entities\SubJurusanFields';
  var $model_fields_guru = 'Modules\Guru\Entities\GuruFields';
  var $model_fields_berita_sekolah = 'Modules\Blog\Entities\BlogFields';
  var $model_fields_pendaftar = 'Modules\Pendaftar\Entities\PendaftarFields';
  var $model_fields_extracurricular = 'Modules\Extracurricular\Entities\ExtracurricularFields';
  var $model_fields_mailbox = 'Modules\ContactsUs\Entities\ContactsUsFields';
  var $model_fields_mata_pelajaran = 'Modules\MataPelajaran\Entities\MataPelajaranFields';
  var $model_fields_wali = 'Modules\Wali\Entities\WaliFields';
  var $model_fields_admin = 'Modules\Admin\Entities\AdminFields';
  var $model_fields_ujian = 'Modules\Ujian\Entities\UjianFields';
  var $model_fields_soal = 'Modules\Soal\Entities\SoalFields';
  var $model_fields_pengumuman = 'Modules\Pengumuman\Entities\PengumumanFields';
  var $model_dropdown = 'Modules\Sys\Entities\Dropdown';

	public function CORS()
  {
    if (isset($_SERVER['HTTP_ORIGIN']))
    {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    # cache for 1 day
    }
    # Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
    {
      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      {
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
      }

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      {
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
      }

      exit(0);
    }
  }

  public function getFields($module = '')
  {
    $model_fields_class = 'model_fields_'.$module;
    $result = $this->$model_fields_class::where($module.'_fields_status', 1)
                                        ->orderby($module.'_fields_sorting', 'ASC')
                                        ->get()
                                        ->toArray();

    return $result;
  }

  public function getDropdown($name)
  {
    $result = $this->model_dropdown::select('dropdown_options_serial_id as value', 'dropdown_options_label as label')
                                  ->where('dropdown_name', $name)
                                  ->leftjoin('dropdown_options as do', 'do.dropdown_serial_id', '=', 'dropdown.dropdown_serial_id')
                                  ->get()->toArray();

		return $result;
  }

  public function uploadFile($request)
	{
			$response = null;
			$file = (object) ['file' => ""];

			if ($request['file']) {
					$original_filename = $request['file']->getClientOriginalName();
					$destination_path = 'apps/public/uploads/';
					$image = "SMK_UPLOAD-".$original_filename;

					if ($request['file']->move($destination_path, $image)) {
						$filePath = "/uploads/$image";
						$upload = Image::make($destination_path.$image)
							/** encode file to the specified mimeType */
							->encode('jpeg')
							/** public_path - points directly to public path */
							->save(public_path($filePath), 30);

							$file->image = 'uploads/' . $image;

							$status = 200;
							$response = array(
									'status'    => $status,
									'message'   => 'OK',
									'file'      => $file
							);
					} else {

							$status = 500;
							$response = array(
									'status'    => $status,
									'message'   => 'Cannot upload file'
							);
					}
			} else {
					$status = 404;
					$response = array(
							'status'    => $status,
							'message'   => 'File not found'
					);
			}

			return $response;
	}

	public function createEmailSiswa($data)
	{
		/* get siswa first name */
		$siswa_name = $data['siswa_name'];

		$siswa_name = str_replace(" ", "", $siswa_name);
		$siswa_name = str_replace(".", "", $siswa_name);
		$siswa_name = str_replace(",", "", $siswa_name);
		$siswa_name = strtolower($siswa_name);
		// if ($siswa_name[0] == "")
		// {
		// }
		// else
		// {
		// 	$siswa_name = strtolower($siswa_name[0]);
		// }

		$siswa_pass = "Demo999123#!#";

		if(strlen($siswa_name) >= 5)
		{
			$siswa_pass = ucfirst(substr($siswa_name,0 , 5).$data['siswa_nis']."#!#");
		}

		if(!isset($data['siswa_nis']) || empty($data['siswa_nis']))
		{
			$data['siswa_nis'] = rand(9,99999999);
		}

		$ip 					= "103.147.154.52";            // should be server IP address or 127.0.0.1 if local server
		$account 			= "smkkarti";        // cpanel user account name
		$passwd 			= "Rendi123!";          // cpanel user password
		$port 				= 2083;                  // cpanel secure authentication port unsecure port# 2082
		$email_domain = "smkkartikatama.sch.id";
		$email_user 	= $siswa_name;
		$email_pass 	= $siswa_pass;
		$email_quota 	= 2;             // 0 is no quota, or set a number in mb

		$xmlapi = new xmlapi($ip);
		$xmlapi->set_port($port);     //set port number.
		$xmlapi->password_auth($account, $passwd);
		$xmlapi->set_debug(0);        //output to error file  set to 1 to see error_log.
		$xmlapi->set_output('json');

		$call = array('domain'=>$email_domain, 'email'=>$email_user, 'password'=>$email_pass, 'quota'=>$email_quota);

		$result = $xmlapi->api2_query($account, "Email", "addpop", $call );
		$result = json_decode($result, true);

		if ($result['cpanelresult']['data'][0]['result'])
		{
			return array(
				'email' => $email_user."@".$email_domain,
				'password' => $siswa_pass
			);
		}
		else
		{
			return null;
		}
	}

	public function deleteEmailSiswa($name)
	{
		$ip 					= "103.147.154.52";            // should be server IP address or 127.0.0.1 if local server
		$account 			= "smkkarti";        // cpanel user account name
		$passwd 			= "Rendi123!";          // cpanel user password
		$port 				= 2083;                  // cpanel secure authentication port unsecure port# 2082
		$email_domain = "smkkartikatama.sch.id";

		$xmlapi = new xmlapi($ip);
		$xmlapi->set_port($port);     //set port number.
		$xmlapi->password_auth($account, $passwd);
		$xmlapi->set_debug(0);        //output to error file  set to 1 to see error_log.
		$xmlapi->set_output('json');

		$args = array(
			'domain'=>$email_domain,
			'email'=>$name
		);

		$result = $xmlapi->api2_query($account, "Email", "delpop", $args );
		$result = json_decode($result, true);

		return true;
	}

	public function createEmailAdmin($data)
	{
		/* get siswa first name */
		$admin_name = strtolower(str_replace(" ", "", $data['admin_name']).rand(1,10));

		$ip 					= "103.147.154.52";            // should be server IP address or 127.0.0.1 if local server
		$account 			= "smkkarti";        // cpanel user account name
		$passwd 			= "Rendi123!";          // cpanel user password
		$port 				= 2083;                  // cpanel secure authentication port unsecure port# 2082
		$email_domain = "smkkartikatama.sch.id";
		$email_user 	= $admin_name;
		$email_pass 	= isset($data['password']) ? $data['password'] : "123qwerty123!";
		$email_quota 	= 2;             // 0 is no quota, or set a number in mb

		$xmlapi = new xmlapi($ip);
		$xmlapi->set_port($port);     //set port number.
		$xmlapi->password_auth($account, $passwd);
		$xmlapi->set_debug(0);        //output to error file  set to 1 to see error_log.
		$xmlapi->set_output('json');

		$call = array('domain'=>$email_domain, 'email'=>$email_user, 'password'=>$email_pass, 'quota'=>$email_quota);

		$result = $xmlapi->api2_query($account, "Email", "addpop", $call );
		$result = json_decode($result, true);

		if ($result['cpanelresult']['data'][0]['result'] == 1)
		{
			return $email_user."@".$email_domain;
		}
		else if (strpos($result['cpanelresult']['data'][0]['reason'], "The password that you entered has a strength rating of") !== FALSE)
		{
			$data['password'] = "123qwerty123!";
			return $this->createEmailAdmin($data);
		}
		else
		{
			return null;
		}
	}

	public function deleteEmailAdmin($name)
	{
		$ip 					= "103.147.154.52";            // should be server IP address or 127.0.0.1 if local server
		$account 			= "smkkarti";        // cpanel user account name
		$passwd 			= "Rendi123!";          // cpanel user password
		$port 				= 2083;                  // cpanel secure authentication port unsecure port# 2082
		$email_domain = "smkkartikatama.sch.id";

		$xmlapi = new xmlapi($ip);
		$xmlapi->set_port($port);     //set port number.
		$xmlapi->password_auth($account, $passwd);
		$xmlapi->set_debug(0);        //output to error file  set to 1 to see error_log.
		$xmlapi->set_output('json');

		$args = array(
			'domain'=>$email_domain,
			'email'=>$name
		);

		$result = $xmlapi->api2_query($account, "Email", "delpop", $args );
		$result = json_decode($result, true);

		return true;
	}

	public function getDropdownValueByLabel($label, $dropdown)
	{
		$result = null;

		$getDID = $this->model_dropdown::where('dropdown_name', $dropdown)
                                  ->first();

		if(@count($getDID) > 0)
		{
			$getDID = json_decode(json_encode($getDID), true);
			$getValD = DB::table('dropdown_options')->where('dropdown_serial_id', $getDID['dropdown_serial_id'])->where('dropdown_options_label', $label)->first();

			if(@count($getValD) > 0)
			{
				$getValD = json_decode(json_encode($getValD), true);
				$result = $getValD['dropdown_options_serial_id'];
			}
		}

		return $result;
	}

	function generateRandomString($length = 7) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function generateRandomToken($length = 6) 
	{
		$characters = '23456789ABCDEFGHJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function saveSettingSoal($data) 
	{
		unset($data['_token']);

		$save = DB::table('settings')->where('settings_name', 'show_soal')->update(['settings_value' => $data['total_show']]);

		if($save)
		{
			return true;
		}

		return false;
	}

	function getShowSoal()
	{
		$result = 0;

		$query = DB::table('settings')->where('settings_name', 'show_soal')->select('settings_value')->first();

		if(@count($query) > 0)
		{
			$result = json_decode(json_encode($query), true);
			$result = $result['settings_value'];
		}

		return $result;
	}

}
