<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('/upload', 'SysController@uploadFile');
// Route::post('/upload/delete', 'SysController@deleteFile');
// Route::prefix('sys')->group(function() {
  //     Route::get('/', 'SysController@index');
  // });


  Route::get('/autocomplete/{table_module}', 'SysController@autocomplete');
  Route::post('/ujian/save_setting_soal', 'SysController@savesettingsoal');
