<?php

namespace Modules\Ujian\Entities;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    protected $table 		= "ujian";
    protected $primaryKey 	= "ujian_serial_id";
    protected $guarded = array('ujian_serial_id');
    public $timestamps = false;
}
