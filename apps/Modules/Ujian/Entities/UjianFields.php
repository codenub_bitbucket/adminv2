<?php

namespace Modules\Ujian\Entities;

use Illuminate\Database\Eloquent\Model;

class UjianFields extends Model
{
    protected $table 		= "ujian_fields";
    protected $primaryKey 	= "ujian_fields_serial_id";
    protected $guarded = array('ujian_fields_serial_id');
    public $timestamps = false;
}
