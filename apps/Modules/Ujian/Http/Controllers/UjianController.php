<?php

namespace Modules\Ujian\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportCallBack;
use DB;

use App\Exports\CollectionExport;

use Modules\Ujian\Http\Controllers\UjianHelper as ujian_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;
use Modules\Kelas\Http\Controllers\KelasHelper as kelas_helper;
use Modules\MataPelajaran\Http\Controllers\MataPelajaranHelper as matapelajaran_helper;
use Modules\Jurusan\Http\Controllers\JurusanHelper as jurusan_helper;

class UjianController extends Controller
{

	var $module       = 'Ujian';
	var $table_module = 'ujian';

	function __construct(Request $request)
	{
		$this->middleware('auth');
		session()->forget('redirect');
		session()->put('redirect', $request->path());
	}


	public function index(Request $request)
	{
		$criteria = $request->all();
		$sys = new sys();
		$helper = new ujian_helper();

		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;
		$list = $helper->list_data('get', $criteria);
		$count = $helper->list_data('count');
		$show_soal = $sys->getShowSoal();

		$fields = $sys->getFields($this->table_module);

		$view = 'ujian::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list,
			'count_data'     => $count,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields,
			'show_soal'             => $show_soal,
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function create()
	{
			$sys = new sys();
			$k_helper = new kelas_helper();
			$j_helper = new jurusan_helper();

			$fields = $sys->getFields($this->table_module);

			foreach ($fields as $key => $value)
			{
				if ($value['ujian_fields_input_type'] == 'singleoption' && $value['ujian_fields_options'] !== null)
				{
					$fields[$key]['dropdown'] = $sys->getDropdown($value['ujian_fields_options']);
				}
				elseif ($value['ujian_fields_input_type'] == 'singleoption')
				{
					if ($value['ujian_fields_name'] == 'ujian_kelas_id')
					{
						$list_kelas = $k_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_kelas;
					}
					else if ($value['ujian_fields_name'] == 'ujian_jurusan_id')
					{
						$list_jurusan = $j_helper->list_data('get_dropdown');
						$fields[$key]['dropdown'] = $list_jurusan;
					}
				}
			}

			$view = 'ujian::create';

			$view_data = array(
					'function' => 'save',
					'table_module'  => $this->table_module,
					'fields'        => $fields,
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
	}

	public function save(Request $request)
	{
			$helper = new ujian_helper();
			$sys = new sys();

			$users = Auth::user();

			$data = $request->all();

			$save = $helper->save($data, $users);

			if ($save)
			{
					return redirect('smkbisa/ujian')->with(['success' => 'Ujian Berhasil Disimpan']);
			}
			else
			{
					return redirect('smkbisa/ujian')->with('failed', 'Ujian Gagal Disimpan');
			}
	}

	public function update(Request $request, $uuid)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$helper = new ujian_helper();

		$update = $helper->update($criteria, $users, $uuid);

		if ($update)
		{
			return redirect('smkbisa/ujian')->with(['success' => 'Ujian Berhasil Diperbaharui']);
		}
		else
		{
			return redirect('smkbisa/ujian')->with('failed', 'Ujian Gagal Diperbaharui');
		}
	}

	public function delete($uuid)
	{
		$sys = new sys();

		$helper = new ujian_helper();

		$delete = $helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/ujian')->with(['success' => 'Ujian Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/ujian')->with('failed', 'Ujian Gagal Dihapus');
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new ujian_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
				return redirect('smkbisa/ujian')->with(['success' => 'Ujian Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/ujian')->with('failed', 'Ujian Gagal Dihapus');
		}
	}

	public function detail(Request $request, $uuid)
	{
		$sys = new sys();
		$helper = new ujian_helper();

		$data = $helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);
		$soal = $helper->getSoal($data);
		$report = method_exists($helper, "getReport") ? $helper->getReport($data) : array();

		$view = 'ujian::detail';

		$view_data = array(
				'data' => $data,
				'soal' => $soal,
				'report' => $report,
				'fields' => $fields,
				'table_module' => $this->table_module
		);

		// dd($view_data);

		session()->forget('back');
		session()->put('back', 'smkbisa/siswa/detail/'.$uuid);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function duplicate(Request $request)
	{
		$sys = new sys();
		$helper = new ujian_helper();

		$k_helper = new kelas_helper();
		$j_helper = new jurusan_helper();
		$mp_helper = new matapelajaran_helper();

		$criteria['from'] = 'duplicate';
		$data = $helper->list_data('get_dropdown',$criteria);
		$list_kelas = $k_helper->list_data('get_dropdown');
		$list_jurusan = $j_helper->list_data('get_dropdown');
		$list_mata_pelajaran = $mp_helper->list_data('get_dropdown');

		$view = 'ujian::duplicate';

		$view_data = array(
				'data' => $data,
				'list_kelas' => $list_kelas,
				'list_jurusan' => $list_jurusan,
				'list_mata_pelajaran' => $list_mata_pelajaran,
				'table_module' => $this->table_module
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function copy(Request $request)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$helper = new ujian_helper();

		$copy = $helper->copy($criteria, $users);

		if ($copy)
		{
			return redirect('smkbisa/ujian')->with(['success' => 'Ujian Berhasil Dicopy']);
		}
		else
		{
			return redirect()->back()->with('failed', 'Ujian Gagal Dicopy');
		}
	}

	public function edit(Request $request, $uuid)
	{
		$sys = new sys();
		$k_helper = new kelas_helper();
		$j_helper = new jurusan_helper();
		$helper = new ujian_helper();

		$data = $helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);

		foreach ($fields as $key => $value)
		{
			if ($value['ujian_fields_input_type'] == 'singleoption' && $value['ujian_fields_options'] !== null)
			{
				$fields[$key]['dropdown'] = $sys->getDropdown($value['ujian_fields_options']);
			}
			elseif ($value['ujian_fields_input_type'] == 'singleoption')
			{
				if ($value['ujian_fields_name'] == 'ujian_kelas_id')
				{
					$list_kelas = $k_helper->list_data('get_dropdown');
					$fields[$key]['dropdown'] = $list_kelas;
				}
				else if ($value['ujian_fields_name'] == 'ujian_jurusan_id')
				{
					$list_jurusan = $j_helper->list_data('get_dropdown');
					$fields[$key]['dropdown'] = $list_jurusan;
				}
			}
		}

		$view = 'ujian::create';

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $data,
			'function'   				 => 'edit'
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function createSoal($ujian_uuid)
	{
			$sys = new sys();

			$fields = $sys->getFields('soal');

			$view = 'soal::create';

			$view_data = array(
					'function' => 'save',
					'table_module'  => 'soal',
					'fields'        => $fields,
					'ujian_uuid'		=> $ujian_uuid
			);

			if(view()->exists($view))
			{
					return view($view, $view_data)->render();
			}
	}

	public function saveSoal(Request $request)
	{
		$helper = new ujian_helper();
		$sys = new sys();

		$users = Auth::user();

		$data = $request->all();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($data['soal_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}
		$data['soal_description'] = $dom->saveHTML();

		$save = $helper->saveSoal($data, $users);

		if ($save)
		{
				return redirect('smkbisa/ujian/detail/'.$data['ujian_uuid'])->with(['success' => 'Soal Berhasil Disimpan']);
		}
		else
		{
				return redirect('smkbisa/ujian/detail/'.$data['ujian_uuid'])->with('failed', 'Soal Gagal Disimpan');
		}
	}

	public function importSoal($ujian_uuid)
	{
		$sys = new sys();

		$view = 'soal::import';

		$view_data = array(
				'ujian_uuid' => $ujian_uuid
		);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function importSoalPreview(Request $request)
	{
		session()->forget('redirect');

		$request->validate([
				'file' => 'required|mimes:csv,xls,xlsx'
		]);

		$criteria = $request->all();

		$array = Excel::toArray(new ImportCallBack, $request->file('file'));

		$fieldsHeader = $array[0][0];
		$dataReal = array();
		unset($array[0][0]);

		foreach ($fieldsHeader as $key => $value)
		{
			if($value == 'PERTANYAAN')
			{
				$fieldsHeader[$key] = 'soal_description';
			}
			if($value == 'PILIHAN A' || $value == 'PILIHAN B' || $value == 'PILIHAN C' || $value == 'PILIHAN D' || $value == 'PILIHAN E')
			{
				$fieldsHeader[$key] = 'soal_choices';
			}
			else if($value == 'KUNCI JAWABAN')
			{
				$fieldsHeader[$key] = 'soal_answer_key';
			}
		}

		$option = ['a', 'b', 'c', 'd', 'e'];

		foreach ($array[0] as $key => $value)
		{
			foreach ($value as $k => $v)
			{
				if($fieldsHeader[$k] == 'soal_choices')
				{
					$newValue[$fieldsHeader[$k]][$option[$k-1]] = $v;
				}
				else
				{
					$newValue[$fieldsHeader[$k]] = $v;
				}
				if(!empty($newValue['soal_description']))
				{
					$dataReal[$key] = $newValue;
				}
			}
		}

		$dataReal = array_values($dataReal);

		$view_data = array(
			'data' => $dataReal,
			'ujian_uuid' => $criteria['ujian_uuid']
		);

		$view = 'soal::import-preview';

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function importSoalSave(Request $request)
	{
		$sys = new sys();
		$helper = new ujian_helper();
		$users = Auth::user();

		$data = $request->all();

		$data_soal = json_decode($data['data_soal'], true);
		unset($data['_token']);
		unset($data['data_soal']);


		$success = 0;
		$failed = 0;


		foreach ($data_soal as $key => $value)
		{
			foreach ($data as $k => $v)
			{
				$value[$k] = $v;
			}

			$save = $helper->saveSoal($value, $users);
			if ($save)
			{
				$success++;
			}
			else
			{
				$failed++;
			}
		}

		if ($success >= 0 || $failed >= 0)
		{
				return redirect('smkbisa/ujian/detail/'.$data['ujian_uuid'])->with(['success' => 'Soal Berhasil Diimport : '.$success.' Berhasil, '.$failed.' Gagal']);
		}
		else
		{
				return redirect('smkbisa/ujian/detail/'.$data['ujian_uuid'])->with('failed', 'Soal Gagal Diimport');
		}
	}

	public function editSoal($uuid_soal, $uuid_ujian)
	{
		$helper = new ujian_helper();
		$sys = new sys();

		$data = $helper->detailSoal($uuid_soal);
		$fields = $sys->getFields('soal');

		$view = 'soal::create';

		$view_data = array(
				'data' => $data,
				'fields' => $fields,
				'table_module' => 'soal',
				'function'   				 => 'edit',
				'uuid_soal'		=> $uuid_soal,
				'uuid_ujian'		=> $uuid_ujian,
			);

		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
	}

	public function updateSoal(Request $request, $uuid_soal, $uuid_ujian)
	{
		$users = Auth::user();

		$sys = new sys();
		$criteria = $request->all();

		$dom = new \DomDocument();
		libxml_use_internal_errors(true);
		$dom->loadHtml($criteria['soal_description'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
			$imageSrc = $image->getAttribute('src');
			/** if image source is 'data-url' */
			if (preg_match('/data:image/', $imageSrc)) {
				/** etch the current image mimetype and stores in $mime */
				preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
				$mimeType = $mime['mime'];
				/** Create new file name with random string */
				$filename = uniqid();
				/** Public path. Make sure to create the folder
				 * public/uploads
				 */
				$filePath = "/uploads/$filename.$mimeType";

				if (!file_exists(public_path('/uploads'))) {
						mkdir(public_path('/uploads'), 666, true);
				}
				/** Using Intervention package to create Image */
				Image::make($imageSrc)
						/** encode file to the specified mimeType */
						->encode($mimeType, 70)
						/** public_path - points directly to public path */
						->save(public_path($filePath));

				$newImageSrc = URL('apps/public/'.$filePath);
				$image->removeAttribute('src');
				$image->setAttribute('src', $newImageSrc);
			}
		}
		$criteria['soal_description'] = $dom->saveHTML();


		$helper = new ujian_helper();

		$update = $helper->updateSoal($criteria, $users, $uuid_soal);

		if ($update)
		{
			return redirect('smkbisa/ujian/detail/'.$uuid_ujian)->with(['success' => 'Soal Berhasil Diperbaharui']);
		}
		else
		{
			return redirect('smkbisa/ujian/detail/'.$uuid_ujian)->with('failed', 'Soal Gagal Diperbaharui');
		}
	}

	public function deleteSoal($uuid, $uuid_ujian)
	{
		$sys = new sys();

		$helper = new ujian_helper();

		$delete = $helper->deleteSoal($uuid);

		if ($delete)
		{
			return redirect('smkbisa/ujian/detail/'.$uuid_ujian)->with(['success' => 'Soal Berhasil Dihapus']);
		}
		else
		{
			return redirect('smkbisa/ujian/detail/'.$uuid_ujian)->with(['success' => 'Soal Berhasil Dihapus']);
		}
	}

	public function resetResult($uuid_result, $users_id)
	{
		$sys = new sys();

		$helper = new ujian_helper();

		$reset = $helper->resetResult($uuid_result, $users_id);

		if ($reset)
		{
				return redirect()->back()->with(['success' => 'Hasil Ujian Berhasil Direset']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Hasil Ujian Gagal Direset');
		}
	}

	public function resetUjian($uuid)
	{
		$sys = new sys();

		$helper = new ujian_helper();

		$reset = $helper->resetUjian($uuid);

		if ($reset)
		{
				return redirect()->back()->with(['success' => 'Ujian Berhasil Direset']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Ujian Gagal Direset');
		}
	}

	public function resetUjianAll()
	{
		$sys = new sys();

		$helper = new ujian_helper();

		$reset = $helper->resetUjianAll();

		if ($reset)
		{
				return redirect()->back()->with(['success' => 'Semua Data Ujian Berhasil Direset']);
		}
		else
		{
				return redirect()->back()->with('failed', 'Semua Data Ujian Gagal Direset');
		}
	}

	public function exportResult(Request $request, $uuid_ujian)
	{
		// new CollectionExport($ujian_uuid);exit;
		// $col = collect([
        //     [
        //         'name' => 'Povilas',
        //         'surname' => 'Korop',
        //         'email' => 'povilas@laraveldaily.com',
        //         'twitter' => '@povilaskorop'
        //     ],
        //     [
        //         'name' => 'Taylor',
        //         'surname' => 'Otwell',
        //         'email' => 'taylor@laravel.com',
        //         'twitter' => '@taylorotwell'
        //     ]
        // ]);
		$helper = new ujian_helper();

		$name = $uuid_ujian;
		$data = $helper->get_detail($uuid_ujian);
		if(@count($data) > 0)
		{
			$name = $data['ujian_mata_pelajaran_id_label']."-".$data['ujian_kelas_name']."-".$data['ujian_jurusan_name'];
		}
		return Excel::download(new CollectionExport($uuid_ujian), $name.'.xlsx');
		Excel::create('Filename', function($excel) {

			$excel->sheet('Sheetname', function($sheet) {

				$sheet->fromArray(array(
					array('data1', 'data2'),
					array('data3', 'data4')
				));

			});

		})->download('xls');
	}
}
