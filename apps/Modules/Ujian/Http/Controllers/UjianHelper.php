<?php
namespace Modules\Ujian\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Api\Entities\Absensi;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class UjianHelper
{
  var $table_module = 'ujian';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_ujian = 'Modules\Ujian\Entities\Ujian';

  public function list_data($type='get', $criteria = array())
  {
    $result = array();

    $select = array(
      "ujian.*",
      DB::raw('mp.mata_pelajaran_name'),
      DB::raw('kelas.kelas_name as ujian_kelas_name'),
      DB::raw('jurusan.jurusan_name as ujian_jurusan_name')
    );

    $query = $this->model_ujian::leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'ujian.ujian_kelas_id')
                              ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'ujian.ujian_jurusan_id')
                              ->leftjoin('mata_pelajaran as mp', 'mp.mata_pelajaran_serial_id', '=', 'ujian.ujian_mata_pelajaran_id');

                              if (isset($criteria['search']) && $criteria['search'] != '')
                              {
                                // $query->where('ujian.ujian_name',"LIKE", "%".$criteria['search']."%");
                                // $query->orWhere('mp.mata_pelajaran_name',"LIKE", "%".$criteria['search']."%");
                                // $query->orWhere('kelas.kelas_name',"LIKE", "%".$criteria['search']."%");
                                // $query->orWhere('jurusan.jurusan_name',"LIKE", "%".$criteria['search']."%");
                                // $query->where('ujian.deleted', 0);
                                $query = $query->where(function ($query) use ($criteria){
                                  $query->where('ujian.ujian_name',"LIKE", "%".$criteria['search']."%")
                                  ->orWhere('mp.mata_pelajaran_name',"LIKE", "%".$criteria['search']."%")
                                  ->orWhere('kelas.kelas_name',"LIKE", "%".$criteria['search']."%")
                                  ->orWhere('jurusan.jurusan_name',"LIKE", "%".$criteria['search']."%");
                                });

                              }

                              if(isset($criteria['who_get']) && $criteria['who_get'] == 'users')
                              {
                                $select[] = DB::raw('g.guru_name');
                                $query->leftjoin('guru as g', function($join)
                                {
                                  $join->on('g.guru_mata_pelajaran_id', '=', 'ujian.ujian_mata_pelajaran_id');
                                  $join->on('g.guru_kelas_id', '=', 'ujian.ujian_kelas_id');
                                });

                                $get_class = DB::table('siswa')->where('siswa_serial_id', $criteria['siswa_id'])->select("siswa_kelas_id", "siswa_jurusan_id")->first();

                                if(@count($get_class) > 0)
                                {
                                  $get_class = json_decode(json_encode($get_class), true);

                                  $query = $query->where(function ($query) {
                                    $query->where('ujian.ujian_start_date', '<=', date('Y-m-d', strtotime(date("Y-m-d"). "+7 HOURS")))
                                    ->orWhere('ujian.ujian_start_date', '>', date('Y-m-d', strtotime(date("Y-m-d"). "+7 HOURS")))
                                    ->where('ujian.ujian_start_date', '!>', date('Y-m-d', strtotime(date("Y-m-d"). "+7 HOURS")));
                                  });
                                  // $query->where('ujian.ujian_start_date', '<=', date('Y-m-d'));
                                  $query->where('ujian.ujian_end_date', '>=', date('Y-m-d'));
                                  $query->where('ujian.ujian_kelas_id', $get_class['siswa_kelas_id'])
                                  ->where('ujian.ujian_jurusan_id', $get_class['siswa_jurusan_id']);
                                }
                                else
                                {
                                  return array();
                                }

                              }
                              $query->where('ujian.deleted', 0);

                              $query = $query->select($select);

    if ($type == 'get')
    {
      // echo "<pre>";
      //   print_r($query->toSql());
      // exit();
        $result = $query->paginate($criteria['show']);
        $result->appends($criteria);
        if(isset($criteria['who_get']) && $criteria['who_get'] == 'users' && @count(json_decode(json_encode($result), true)['data']) > 0)
        {
          $result = json_decode(json_encode($result), true);
          $rel = DB::table('sys_rel')->where('rel_from_module', 'hasil_ujian')
                                      ->where('rel_to_module', 'siswa')
                                      ->where('rel_to_id', $criteria['siswa_id'])
                                      ->get();

          foreach ($rel as $key => $value)
          {
              $hasil_ujian = DB::table('hasil_ujian')->where('hasil_ujian_serial_id', $value->rel_from_id)->where('deleted', 0)->first();

              $temp = json_decode(json_encode($result), true);
              if(@count($hasil_ujian) > 0)
              {
                  $key = array_search($hasil_ujian->ujian_serial_id, array_column($temp['data'], 'ujian_serial_id'));

                  if($key !== FALSE)
                  {
                      $temp['data'][$key]['status'] = true;
                      $temp['data'][$key]['result'] = json_decode($hasil_ujian->hasil_ujian_result);
                  }
                  else
                  {
                      $temp['data'][$key]['status'] = false;
                      $temp['data'][$key]['result'] = json_encode(array());
                  }
              }

              $result = $temp;
          }
          foreach ($result['data'] as $k => $v)
          {
              $total = $this->getSoal($v, 'count');
              $result['data'][$k]['total'] = $total;
          }
        }

    }
    else if ($type == 'get_dropdown')
    {
      $select = array("ujian.ujian_name as label", "ujian.ujian_serial_id as value");
      if ($criteria['from'] == 'duplicate')
      {
        $select = array(
          "ujian.ujian_name as label",
          "ujian.ujian_serial_id as value",
          "kelas.kelas_name",
          "jurusan.jurusan_name",
          "mp.mata_pelajaran_name",
        );
      }
      $query = $query->select($select)->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_ujian::select("ujian.*", DB::raw('mp.mata_pelajaran_name as ujian_mata_pelajaran_id_label'), DB::raw('kelas.kelas_name as ujian_kelas_name'), DB::raw('jurusan.jurusan_name as ujian_jurusan_name'), DB::raw('g.guru_name'))
                              ->where('ujian.ujian_uuid', $uuid)
                              ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'ujian.ujian_kelas_id')
                              ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'ujian.ujian_jurusan_id')
                              ->leftjoin('mata_pelajaran as mp', 'mp.mata_pelajaran_serial_id', '=', 'ujian.ujian_mata_pelajaran_id')
                              ->leftjoin('guru as g', function($join)
                                {
                                  $join->on('g.guru_mata_pelajaran_id', '=', 'ujian.ujian_mata_pelajaran_id');
                                  $join->on('g.guru_kelas_id', '=', 'ujian.ujian_kelas_id');
                                })
                              ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function save($data, $users)
  {
    $result = false;
    unset($data['_token']);

    $data['created_by'] = $users->id;
    $data['ujian_start_date'] = isset($data['ujian_start_date']) ? date("Y-m-d", strtotime($data['ujian_start_date'])) : date('Y-m-d');
    $data['ujian_end_date'] = isset($data['ujian_end_date']) ? date("Y-m-d", strtotime($data['ujian_end_date'])) : date('Y-m-d');
    $data['ujian_uuid'] = Uuid::uuid4()->toString();

    $query = $this->model_ujian::insert($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);


    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['ujian_start_date'] = isset($data['ujian_start_date']) ? date("Y-m-d", strtotime($data['ujian_start_date'])) : date('Y-m-d');
    $data['ujian_end_date'] = isset($data['ujian_end_date']) ? date("Y-m-d", strtotime($data['ujian_end_date'])) : date('Y-m-d');

    $data['ujian_start_access'] = isset($data['ujian_start_access']) ? date("H:i:s", strtotime($data['ujian_start_access'])) : date('H:i:s');
    $data['ujian_end_access'] = isset($data['ujian_end_access']) ? date("H:i:s", strtotime($data['ujian_end_access'])) : date('H:i:s');

    $query = $this->model_ujian::where('ujian_uuid', $uuid)->update($data);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function copy($data, $users)
  {
    $result = false;
    unset($data['_token']);

    /* Get ujian first by jurusan and kelas and mapel */
    $ujian = DB::table('ujian')->where($this->table_module.'_serial_id', $data['ujian_id'])->first();

    if(@count($ujian) > 0)
    {
      $insert = json_decode(json_encode($ujian), true);
      unset($insert['ujian_serial_id']);

      if($data['to_jurusan_id'] == $insert['ujian_jurusan_id'])
      {
        return false;
      }

      /* create copy of ujian first */
      $insert['created_by'] = $users->id;
      $insert['ujian_start_date'] = isset($insert['ujian_start_date']) ? date("Y-m-d", strtotime($insert['ujian_start_date'])) : date('Y-m-d');
      $insert['ujian_end_date'] = isset($insert['ujian_end_date']) ? date("Y-m-d", strtotime($insert['ujian_end_date'])) : date('Y-m-d');
      $insert['ujian_uuid'] = Uuid::uuid4()->toString();
      $insert['ujian_kelas_id'] = $data['to_kelas_id'];
      $insert['ujian_jurusan_id'] = $data['to_jurusan_id'];

      /* get list rel to copy */
      $rel = DB::table("sys_rel")->where("rel_to_module", $this->table_module)
                                ->where("rel_to_id", $data['ujian_id'])
                                ->where("rel_from_module", "soal")
                                ->get();

      if(@count($rel) > 0)
      {
        $rel = json_decode(json_encode($rel), true);

        /* jika ada soal maka insert new ujian, jika tidak return false */
        $query = $this->model_ujian::insertGetId($insert);
        if ($query)
        {
          $insertRel = array();

          foreach ($rel as $key => $value)
          {
            $insertRel[$key]['rel_from_module'] = 'soal';
            $insertRel[$key]['rel_from_id'] = $value['rel_from_id'];
            $insertRel[$key]['rel_to_module'] = $this->table_module;
            $insertRel[$key]['rel_to_id'] = $query;
          }

          $queryRel = DB::table('sys_rel')->insert($insertRel);
          if($queryRel)
          {
            return true;
          }
        }
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  function delete($uuid)
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $update['deleted'] = 1;
    $query = $this->model_ujian::where('ujian_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function getSoal($data, $type='get')
  {
    $sys = new sys();
    $result = array();

    $limit = $sys->getShowSoal();

    $rel = DB::table("sys_rel")->where("rel_to_module", $this->table_module)
                               ->where("rel_to_id", $data['ujian_serial_id'])
                               ->where("rel_from_module", "soal");
                               if(isset($data['who_get']) && $data['who_get'] == 'siswa')
                               {
                                $rel->limit($limit);
                               }
                               $rel = $rel->get();

    if(@count($rel) > 0)
    {
      $rel = json_decode(json_encode($rel), true);

      $ids = array();

      foreach ($rel as $key => $value)
      {
        $ids[] = $value['rel_from_id'];
      }

      $getSoal = DB::table("soal as s")->whereIn("s.soal_serial_id", $ids);

                                      if(isset($data['who_get']) && $data['who_get'] == 'users')
                                      {
                                        $getSoal = $getSoal->select("s.soal_uuid", "s.soal_description", "s.soal_choices")
                                                // ->where("s.deleted", 0)->take($limit)->inRandomOrder()
                                                ->where("s.deleted", 0);
                                      }
                                      else
                                      {
                                        $getSoal->select("s.*", "u.name as created_by_name");
                                        $getSoal = $getSoal->leftjoin("users as u", "u.id", "=", "s.created_by")
                                                            ->where("s.deleted", 0);
                                      }

      if($type == 'count')
      {
        return $result = $getSoal->count();
      }
      else
      {
        $getSoal = $getSoal->inRandomOrder()->get();
      }

      if (@count($getSoal) > 0)
      {
        $result = json_decode(json_encode($getSoal), true);
      }
    }

    return $result;
  }

  function getSerialIdByUuid($uuid)
  {
    $result = 0;

    $query = $this->model_ujian::where($this->table_module."_uuid", $uuid)->select($this->table_module."_serial_id")->first();

    if(@count($query) > 0)
    {
      $result = $query->{$this->table_module."_serial_id"};
    }

    return $result;
  }

  function saveSoal($data, $users)
  {
    unset($data['files']);
    unset($data['_token']);

    $result = false;

    $insertSoal = array(
      'soal_uuid' => Uuid::uuid4()->toString(),
      'soal_description' => $data['soal_description'],
      'soal_choices' => json_encode($data['soal_choices']),
      'soal_answer_key' => strtolower($data['soal_answer_key']),
      'created_by' => $users->id
    );
    $saveSoal = DB::table('soal')->insertGetId($insertSoal);

    if($saveSoal > 0)
    {
      $ujianId = $this->getSerialIdByUuid($data['ujian_uuid']);

      $rel = array(
        'rel_from_module' => 'soal',
        'rel_from_id' => $saveSoal,
        'rel_to_module' => 'ujian',
        'rel_to_id' => $ujianId
      );
      $query3 = DB::table('sys_rel')->insert($rel);

      if($query3)
      {
        $result = true;
      }

      return $result;
    }
  }

  function detailSoal($uuid)
  {
    $result = array();

    $query = DB::table('soal')->where('soal_uuid', $uuid)->first();

    if(@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function updateSoal($data, $users, $uuid)
  {
    $result = false;

    unset($data['files']);
    unset($data['_token']);

    $data['updated_at'] = date('Y-m-d H:i:s');

    $data['soal_choices'] = json_encode($data['soal_choices']);

    $update = DB::table('soal')->where('soal_uuid', $uuid)->update($data);

    if($update)
    {
      $result = true;
    }

    return $result;
  }

  function deleteSoal($uuid)
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $update['deleted'] = 1;
    $query = DB::table('soal')->where('soal_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function saveUjianSiswa($data)
  {
    $result = false;

    $insert['hasil_ujian_uuid'] = Uuid::uuid4()->toString();
    $insert['ujian_serial_id'] = $data['ujian_serial_id'];
    $insert['hasil_ujian_info'] = json_encode($data['answer']);
    $insert['hasil_ujian_result'] = json_encode($this->getResultExam($data['answer']));
    $insert['created_at'] = date('Y-m-d H:i:s');
    $insert['updated_at'] = date('Y-m-d H:i:s');

    // echo "<pre>";
    //   print_r('as');
    // exit();
    $query = DB::table('hasil_ujian')->insertGetId($insert);
    if(@count($query) > 0)
    {
        $result = true;

        $rel = array(
            'rel_from_module' => 'hasil_ujian',
            'rel_from_id' => $query,
            'rel_to_module' => 'siswa',
            'rel_to_id' => $data['siswa_id'],
        );
        $query3 = DB::table('sys_rel')->insert($rel);
    }

    return $result;
  }

  function getResultExam($data)
  {
      $result = array(
          'true' => 0,
          'false' => 0,
          'none' => 0,
      );

      foreach ($data as $key => $value)
      {
          $getAnswer = DB::table('soal')->where('soal_uuid', $key)->select('soal_answer_key')->first();

          if(@count($getAnswer) > 0)
          {
            if($getAnswer->soal_answer_key == $value)
            {
                $result['true']++;
            }
            else if($value == NULL || empty($value))
            {
                $result['none']++;
            }
            else
            {
                $result['false']++;
            }
          }
          else
          {
            $result['none']++;
          }
      }

      return $result;
  }

  function getReport($data)
  {
    $result = array();

    $getHasilUjian = DB::table('hasil_ujian as hu')
                        ->select('hu.*', 's.siswa_serial_id', 's.siswa_name', 'sj.subjurusan_name', 'k.kelas_name', 'j.jurusan_name')
                        ->rightjoin('sys_rel', function($join)
                        {
                          $join->on('sys_rel.rel_from_id', '=', 'hu.hasil_ujian_serial_id')
                            ->where('sys_rel.rel_from_module', '=', 'hasil_ujian')
                            ->where('sys_rel.rel_to_module', '=', 'siswa');
                        })
                        ->leftJoin('siswa as s', 's.siswa_serial_id', '=', 'sys_rel.rel_to_id')
                        ->leftJoin('kelas as k', 'k.kelas_serial_id', '=', 's.siswa_kelas_id')
                        ->leftJoin('jurusan as j', 'j.jurusan_serial_id', '=', 's.siswa_jurusan_id')
                        ->leftjoin('subjurusan as sj', 'sj.subjurusan_serial_id', '=', 's.siswa_subjurusan_id')
                        ->where('hu.deleted', 0)
                        ->where("hu.ujian_serial_id", $data['ujian_serial_id'])
                        ->get();

    if(@count($getHasilUjian) > 0)
    {
        $result = json_decode(json_encode($getHasilUjian), true);
        foreach ($result as $key => $value)
        {
            $result[$key]['absensi'] = Absensi::where("absensi_siswa_id", $value['siswa_serial_id'])->where("absensi_ujian_id", $value['ujian_serial_id'])->first();
        }
    }

    return $result;
  }

  function resetResult($uuid, $users_id)
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $update['deleted'] = 1;
    $query = DB::table('hasil_ujian')->where('hasil_ujian_uuid', $uuid)->update($update);
    $get = DB::table('hasil_ujian')->where('hasil_ujian_uuid', $uuid)->first();
    if ($query)
    {
      $get = json_decode(json_encode($get), true);
      DB::table('sys_rel')->where('rel_from_module', 'hasil_ujian')->where('rel_from_id', $get['hasil_ujian_serial_id'])->where('rel_to_module', 'siswa')->where('rel_to_id', $users_id)->delete();
      DB::table('absensi')->where("absensi_siswa_id", $users_id)->where("absensi_ujian_id", $get['ujian_serial_id'])->delete();
      $result = true;
    }
    return $query;
  }

  function resetUjian($uuid)
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_ujian::where('ujian_uuid', $uuid)->update($update);
    $get = $this->model_ujian::where('ujian_uuid', $uuid)->first();
    if ($query)
    {
      $get = json_decode(json_encode($get), true);
      DB::table('sys_rel')->where('rel_to_module', 'ujian')->where('rel_to_id', $get['ujian_serial_id'])->where('rel_from_module', 'soal')->delete();
      DB::table('hasil_ujian')->where('ujian_serial_id', $get['ujian_serial_id'])->update(['deleted' => 1]);
      $result = true;
    }
    return $query;
  }

  function resetUjianAll()
  {
    $result = false;
    $update['updated_at'] = date('Y-m-d H:i:s');
    $query = DB::table('ujian')->update($update);
    $get = $this->model_ujian::get();

    if ($query)
    {
      $get = json_decode(json_encode($get), true);
      $serial_id = array();
      if(@count($get) > 0)
      {
        foreach($get as $key => $value)
        {
          $serial_id[] = $value['ujian_serial_id'];
        }
      }
      DB::table('sys_rel')->where('rel_to_module', 'ujian')->whereIn('rel_to_id', $serial_id)->where('rel_from_module', 'soal')->delete();
      DB::table('hasil_ujian')->whereIn('ujian_serial_id', $serial_id)->update(['deleted' => 1]);
      $result = true;
    }
    return $query;
  }

}
