@extends('app')
@section('title', 'Siswa')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama | Ujian Detail</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Ujian</li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="bg-primary div-profile-user px-3 py-4 mb-3">
            <div class="media align-items-center d-flex ">
              <div class="avatar-sm mr-3 rounded-circle bg-dark align-items-center d-flex">
                <div class="avatar-content m-auto text-white">
                  <i class=" ri-book-read-line"></i>
                </div>
              </div>
              <div class="media-body">
                  <h4 class="mt-0">{{ $data['ujian_name'] }}</h4>
                  <p class="mb-0 text-white">
                    <span>{{ '[ '.$data['ujian_kelas_name'].' - '. $data['ujian_jurusan_name'] .' ]' }} {{ $data['ujian_mata_pelajaran_id_label'] }}</span>
                    <span class="m-2">- {{ $data['guru_name'] }}</span>
                  </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-body px-0 pb-0">
              <div class="align-items-center d-flex p-3 w-100 justify-content-between border-bottom">
                <h4 class="card-title">Questions List</h4>
                <div class="btn-group d-flex align-items-center">
                  <a href="{{ URL('smkbisa/ujian/soal/tambah/'.$data['ujian_uuid']) }}" class="btn btn-primary mr-2 btn-sm px-4 mb-0">
                    Tambah
                  </a>
                  <a href="{{ URL('smkbisa/ujian/soal/import/'.$data['ujian_uuid']) }}" class="btn btn-info mr-2 btn-sm px-4 mb-0">
                    Import
                  </a>
                  <a href="{{ URL('smkbisa/ujian/reset/'.$data['ujian_uuid']) }}" class="btn btn-danger mr-2 btn-sm px-4 mb-0">
                    Reset Ujian
                  </a>
                  {{-- <div class="dropdown">
                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                      <i class="mdi mdi-dots-vertical"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="javascript:void(0);" class="dropdown-item">Close</a>
                    </div>
                  </div> --}}
                </div>
              </div>

              @if (@count($soal) == 0)
              <div class="d-flex align-items-center w-100 bg-light h-300">
                <div class="m-auto text-center">
                  <div class="avatar-sm m-auto rounded-circle bg-secondary align-items-center d-flex">
                    <div class="avatar-content m-auto text-white">
                      <i class=" ri-book-read-line"></i>
                    </div>
                  </div>
                  <h6 class="mt-3 mb-0">Belum ada soal</h6>
                </div>
              </div>
              @endif

              @if (@count($soal) > 0)
                <ul class="list-unstyled list-course h-550">
                  @foreach ($soal as $key => $value)
                  <li class="pointer list-item align-items-center d-flex justify-content-between border-bottom  pt-3 pb-3 px-3 modalSoal" data-toggle="modal" data-content="{{ json_encode($value) }}" data-parent="{{ json_encode($data) }}" data-target="#detailSoalModal">
                    <div class="media">
                      <div class="media-body">
                        <h6 class="mt-1 mb-1">Soal {{ $data['ujian_mata_pelajaran_id_label'] }}, Nomor {{ $key+1 }}</h6>
                        <p class="mb-0 font-size-12">Created by : {{ $value['created_by_name'] }}</p>
                      </div>
                    </div>
                    <div class="text-right">
                      <i class=" ri-arrow-right-s-line"></i>
                    </div>
                  </li>
                  @endforeach
                </ul>
              @endif

              {{-- Modal detail soal --}}
              <div class="modal fade" id="detailSoalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Detail Soal</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h6><b>Pertanyaan : </b></h6>
                      <p class="soal_description"></p>

                      <h6><b>Pilihan : </b></h6>
                      <p class="soal_choices"></p>

                      <h6><b>Jawaban : </b></h6>
                      <p class="soal_answer_key"></p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <a type="button" class="btn btn-danger btn_delete">Hapus</a>
                      <a href="#" type="button" class="btn btn-primary edit_btn">Edit</a>
                    </div>
                  </div>
                </div>
              </div>
              {{-- End Modal detail soal --}}
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-body px-0 pb-0">
              <div class="align-items-center d-flex p-3 w-100 justify-content-between border-bottom">
                <h4 class="card-title">Exams Result</h4>
                <div class="btn-group d-flex align-items-center">
                  <a href="{{ URL('smkbisa/ujian/result/export/'.$data['ujian_uuid']) }}" class="btn btn-info mr-2 btn-sm px-4 mb-0">
                    Export
                  </a>
                </div>
              </div>

              @if (@count($report) == 0)
              <div class="d-flex align-items-center w-100 bg-light h-300">
                <div class="m-auto text-center">
                  <div class="avatar-sm m-auto rounded-circle bg-secondary align-items-center d-flex">
                    <div class="avatar-content m-auto text-white">
                      <i class=" ri-book-read-line"></i>
                    </div>
                  </div>
                  <h6 class="mt-3 mb-0">Belum ada hasil ujian</h6>
                </div>
              </div>
              @endif

              @if (@count($report) > 0)
              <ul class="list-unstyled list-course h-550">
                @foreach ($report as $key => $value)
                <li class="pointer list-item align-items-center d-flex justify-content-between border-bottom  pt-3 pb-3 px-3 modalResult" data-toggle="modal" data-content="{{ json_encode($value) }}" data-parent="{{ json_encode($data) }}" data-soal="{{ json_encode($soal) }}" data-target="#detailResultModal">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="mt-1 mb-1">{{ $value['siswa_name'] }}</h6>
                      <span class="mb-0">{{ $value['kelas_name'] }} - {{ $value['jurusan_name'] }} {{ $value['subjurusan_name'] }}</span>
                    </div>
                  </div>
                  <div class="d-flex align-items-center">
                    <div class="text-right mr-3">
                      <span>Hasil</span>
                      <h6 class="mb-0 text-primary">{{ json_decode($value['hasil_ujian_result'], true)['true'] }}/{{ json_decode($value['hasil_ujian_result'], true)['false'] }}/{{ json_decode($value['hasil_ujian_result'], true)['none'] }}</h6>
                    </div>
                    <div class="text-right mr-3">
                      <span>Nilai</span>
                      @php
                          $nilai = (json_decode($value['hasil_ujian_result'], true)['true']/@count($soal)) * 100;
                      @endphp
                      <h6 class="mb-0 text-{{($nilai <= 60) ? 'danger' : 'primary'}}">{{ $nilai }}</h6>
                    </div>
                    <div class="text-right">
                      <i class=" ri-arrow-right-s-line"></i>
                    </div>
                  </div>
                </li>
                @endforeach
              </ul>
              {{-- Modal detail result --}}
              <div class="modal fade" id="detailResultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Detail Result</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h6><b>Siswa Detail : </b></h6>
                      <p class="siswa_detail"></p>

                      <h6><b>Nilai : </b></h6>
                      <p class="report_score"></p>

                      <h6><b>Detail Nilai : </b></h6>
                      <p class="report_detail_score"></p>

                      <h6><b>Detail Jawaban : </b></h6>
                      <p class="report_detail_answer">

                      </p>
                      <h6><b>Photo Absen Ujian : </b></h6>
                      <p class="absen_ujian">

                    </p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <a type="button" class="btn btn-danger btn_reset_score">Reset</a>
                    </div>
                  </div>
                </div>
              </div>
              {{-- End Modal detail soal --}}
              @endif
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>

<script>
  $(document).on("click", ".modalSoal", function () {
     var content = $(this).data('content');
     var parent = $(this).data('parent');

     console.log(parent);

     let choices = JSON.parse(content.soal_choices);
     let htmlChoices = `<ul>`;
      Object.keys(choices).forEach(key => {
        htmlChoices += `
          <li style="list-style-type:'${key.toUpperCase()} . '">${choices[key]}</li>
        `;
      });
     $(".modal-body .soal_description").html( content.soal_description );
     $(".modal-body .soal_choices").html(htmlChoices);
     $(".modal-body .soal_answer_key").html( content.soal_answer_key.toUpperCase() );

     $(".edit_btn").attr("href", "{{ URL('smkbisa/ujian/soal/edit') }}/" + content.soal_uuid+ "/" + parent.ujian_uuid);
     $(".btn_delete").attr("href", "{{ URL('smkbisa/ujian/soal/delete') }}/" + content.soal_uuid+ "/" + parent.ujian_uuid);
  });

  $(document).on("click", ".modalResult", function () {
     var content = $(this).data('content');
     var parent = $(this).data('parent');
     var soal = $(this).data('soal');

     let htmlSiswaDetail = `
        <table>
          <tbody>
            <tr>
              <td>Nama Siswa</td>
              <td>: ${content['siswa_name']}</td>
            </tr>
            <tr>
              <td>Kelas</td>
              <td>: ${content['kelas_name']}</td>
            </tr>
            <tr>
              <td>Jurusan</td>
              <td>: ${content['jurusan_name']}</td>
            </tr>
            <tr>
              <td>Sub Jurusan</td>
              <td>: ${content['subjurusan_name']}</td>
            </tr>
          <tbody>
        </table>
     `;
     $(".siswa_detail").html( htmlSiswaDetail );

     let result = JSON.parse(content.hasil_ujian_result);
     let nilai = (result['true'] / soal.length) * 100;

     $(".report_score").html( nilai );

     let htmlResult = `
        <table>
          <tbody>
            <tr>
              <td>Benar</td>
              <td>: ${result['true']}</td>
            </tr>
            <tr>
              <td>Salah</td>
              <td>: ${result['false']}</td>
            </tr>
            <tr>
              <td>Tidak Dijawab</td>
              <td>: ${result['none']}</td>
            </tr>
          <tbody>
        </table>
     `;
     $(".report_detail_score").html( htmlResult );

     let resultInfo = JSON.parse(content.hasil_ujian_info);
     let htmlResultInfo = `
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <ul id="list1">
            </ul>
          </div>
          <div class="col-sm">
            <ul id="list2">
            </ul>
          </div>
          <div class="col-sm">
            <ul id="list3">
            </ul>
          </div>
          <div class="col-sm">
            <ul id="list4">
            </ul>
          </div>
          <div class="col-sm">
            <ul id="list5">
            </ul>
          </div>
        </div>
      </div>
     `;

     $(".report_detail_answer").html( htmlResultInfo );

     let currentList = 1;
     let currentLoop = 0;

     let htmlListAnswer = {
       list1 : ``,
       list2 : ``,
       list3 : ``,
       list4 : ``,
       list5 : ``,
     };
     Object.keys(resultInfo).forEach((key, subKey) => {
      currentLoop++;
      if(resultInfo[key] == null) resultInfo[key] = "";
      if (currentLoop == 11)
      {
        currentList++;
        currentLoop = 1;
      }

      /* defind true or false */
      let color = '';
      let find = soal.findIndex(s => s['soal_uuid'] == key.toString());
      if(find >= 0)
      {
        if(soal[find]['soal_answer_key'] !== resultInfo[key] && (resultInfo[key] !== null && resultInfo[key] !== ''))
        {
          color = 'red';
        }
        else if(resultInfo[key] == "")
        {
          color = 'blue';
        }
      }
      htmlListAnswer['list'+currentList] += `
        <li style="list-style-type:'${subKey+1} . '; color:${color}">${resultInfo[key].toUpperCase()}</li>
      `;
    });

    Object.keys(htmlListAnswer).forEach(key => {
      $("#"+key).html( htmlListAnswer[key] );
    })

    let htmlAbsensi = "<h2>Photo Tidak Ditemukan</h2>"
    if(content.absensi !== null && content.absensi.absensi_document_url !== null)
    {
        htmlAbsensi = `
            <img src="${content.absensi.absensi_document_url}" style="width:100%">
         `;
    }

     $(".absen_ujian").html( htmlAbsensi );

    $(".btn_reset_score").attr("href", "{{ URL('smkbisa/ujian/result/reset') }}/" + content.hasil_ujian_uuid + "/" + content.siswa_serial_id);
  });
</script>
@endsection
