@extends('app')
@section('title', 'Duplikat')
@section('content')
<div class="main-content">
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Duplikat Ujian</h4>
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item">Ujian</li>
                <li class="breadcrumb-item active">Duplikat</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

  
      <div class="card shadow mb-4">
        <div class="card-body">
          <form method="post" action="{{ URL('smkbisa/ujian/copy') }}">
            {{ csrf_field() }}
						<div class="form-group">
              <label>Ujian</label>
              <select class="form-control" name="ujian_id" required>
								<option value="" selected disabled>-- Pilih Ujian --</option>
                @foreach ($data as $item)
                  <option value="{{ $item['value'] }}">{{ $item['label'] }} {{($item['mata_pelajaran_name'])}} ({{ $item['kelas_name'] }} - {{ $item['jurusan_name'] }})</option>
                @endforeach
              </select>
            </div>
						<div class="p-2 text-center h2">
							<i class="ri-arrow-up-down-line"></i>
						</div>
            <div class="form-group">
              <label>Ke Kelas</label>
              <select class="form-control to_kelas_id" name="to_kelas_id" required>
								<option value="" selected disabled>-- Pilih Kelas --</option>
                @foreach ($list_kelas as $item)
								<option value="{{ $item['value'] }}" class="to-kelas-{{ $item['value'] }}">{{ $item['label'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
							<label>Ke Jurusan</label>
              <select class="form-control to_jurusan_id" name="to_jurusan_id" required>
								<option value="" selected disabled>-- Pilih Jurusan --</option>
                @foreach ($list_jurusan as $item)
                  <option value="{{ $item['value'] }}" class="to-jurusan-{{ $item['value'] }}">{{ $item['label'] }}</option>
                @endforeach
              </select>
            </div>
            <a href="{{ URL('smkbisa/ujian')}}"type="button" class="btn btn-secondary">Batal</a>
            <button type="submit" class="btn btn-primary">Proses Duplikat</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- <script>
	$('.to_kelas_id').prop('disabled', true);
	$('.to_kelas_id').css('background', '#eee');

	$('.to_jurusan_id').prop('disabled', true);
	$('.to_jurusan_id').css('background', '#eee');

	let kelas = null;
	let jurusan = null;

	$('.from_kelas_id').on('change', function() {
		kelas = $(this).val();
		checkStatusSelection();
		if($(this).val() !== $('.to_kelas_id').val())
		{
			$('.to-jurusan-'+jurusan).prop('disabled', false);
			$('.to-jurusan-'+jurusan).css('background', '');
		}

		if($(this).val() == $('.to_kelas_id').val())
		{
			$('.to_jurusan_id').prop('selectedIndex', 0);
			$('.to-jurusan-'+jurusan).prop('disabled', true);
			$('.to-jurusan-'+jurusan).css('background', '#eee');
		}
	});

	$('.to_kelas_id').on('change', function() {
		if($(this).val() == kelas)
		{
			$('.to-jurusan-'+jurusan).prop('disabled', true);
			$('.to-jurusan-'+jurusan).css('background', '#eee');
		}
		else
		{
			$('.to-jurusan-'+jurusan).prop('disabled', false);
			$('.to-jurusan-'+jurusan).css('background', '');
		}
	});
	
	$('.from_jurusan_id').on('change', function() {
		jurusan = $(this).val();
		checkStatusSelection();
	});

	function checkStatusSelection()
	{
		if (kelas !== null && jurusan !== null ) 
		{
			$('.to_kelas_id').prop('disabled', false);
			$('.to_kelas_id').css('background', '');

			$('.to_jurusan_id').prop('disabled', false);
			$('.to_jurusan_id').css('background', '');
		}
	}

</script> --}}

@endsection