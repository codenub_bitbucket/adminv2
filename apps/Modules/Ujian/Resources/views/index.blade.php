@extends('app')
@section('title', 'Guru')
@section('content')
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">SMK Kartikatama | Ujian</h4>
            @if (\Session::has('success'))
              <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Berhasil!.</strong>
                      {!! \Session::get('success') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#success-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            @if (\Session::has('failed'))
              <div class="alert alert-danger" id="failed-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong>Opps!.</strong>
                      {!! \Session::get('failed') !!}
              </div>
              <script>
                  $(document).ready(function() {
                      setTimeout(() => {
                          $("#failed-alert").fadeTo(2000, 500).slideUp(500, function() {
                              $("#failed-alert").slideUp(500);
                          });
                      }, 30000);
                  });
              </script>
            @endif
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Ujian</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="d-lg-flex align-items-center justify-content-between mb-4">
                <section class="d-flex align-items-center w-100" id="select-area" style="display:none!important">
                  <div class="w-50">
                    <span style="font-weight: bold">Total Selected : <span id="count_select"></span></span>
                  </div>
                  <div class="w-50">
                    <form name="form_mass_delete" action="{{ URL('smkbisa/ujian/mass_delete') }}" method="post">
                      @csrf
                      <input type="hidden" name="data_selected" value="">
                    </form>
                    <button type="button" class="btn btn-danger float-right" onclick="confirmDelete()">Delete</button>
                    <button type="button" class="btn btn-secondary float-right mr-2 cancel_mass_delete">Cancel</button>
                  </div>
                </section>
                <div class="align-items-center d-lg-flex d-none w-50 unshow-select">
                  @include('component.filter_show')
                  @include('component.search')
                </div>
                <div class="d-flex align-items-center unshow-select">
                  <a class="btn btn-danger px-4 mr-3" id="resetUjianAll" href="#"
                    <i class="ri-file-copy-line p-0 m-0"></i> Reset Semua Ujian
                  </a>
                  <a class="btn btn-info px-4 mr-3" href="{{ url('smkbisa/ujian/duplikat') }}">
                    <i class="ri-file-copy-line p-0 m-0"></i> Duplikat
                  </a>
                  <a class="btn btn-primary px-4 mr-2" href="{{ url('smkbisa/ujian/tambah') }}">
                    + Ujian
                  </a>
                  <button type="button" class="btn btn-dark px-4" data-toggle="modal" data-target="#exampleModal">
                    <i class="ri-settings-2-line"></i>
                  </button>
                  <!-- Modal -->
                  <div class="modal fade modal-setting" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Setting Ujian</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Jumlah Soal Ditampilkan :</label>
                            <input type="number" class="form-control" id="exampleInputEmail1" name="total_show" value="{{ $show_soal }}">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary btn-save-setting">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table mb-0">
                  <thead>
                    <tr>
                      <th style="white-space:nowrap">Action</th>
                      <th>
                        <input type="checkbox" class="check_all">
                      </th>
                      @foreach ($fields as $f)
                        @if ($f[$table_module.'_fields_name'] !== $table_module . '_photo' && $f[$table_module.'_fields_name'] !== $table_module . '_desc')
                            <th style="white-space:nowrap">{{ $f[$table_module.'_fields_label'] }}</th>
                        @endif
                      @endforeach
                      <th style="white-space:nowrap">Created</th>
                      <th style="white-space:nowrap">Updated</th>
                    </tr>
                  </thead>
                  <tbody>
										@if (@count($list_data) > 0)
												@foreach ($list_data as $key => $item)
													<tr>
                            <td style="white-space:nowrap">
                                <a title="Lihat" class="btn btn-success" href="{{ url('smkbisa/ujian/detail/' . $item[$table_module.'_uuid'])}}"><i class="ri-eye-line"></i></a>
                                <a title="Edit" class="btn btn-info" href="{{ url('smkbisa/ujian/edit/' . $item[$table_module.'_uuid'])}}"><i class="ri-pencil-line"></i></a>
                                <a title="Hapus" class="btn btn-danger" href="{{ url('smkbisa/ujian/delete/' . $item[$table_module.'_uuid'])}}"><i class="ri-delete-bin-line"></i></a>
                            </td>
														<td>
															<input type="checkbox" class="checkbox_data" multiple="true" onclick="doSelect()" value="{{$item[$table_module.'_uuid']}}" autocomplete="off">
														</td>
														@foreach ($fields as $f)
															@if ($f[$table_module.'_fields_input_type'] == 'date')
																<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item[$f[$table_module.'_fields_name']]))) }}</td>
															@else
                                @if ($f[$table_module.'_fields_name'] == 'ujian_mata_pelajaran_id')
  																<td style="white-space:nowrap">{{ $item['mata_pelajaran_name'] }}</td>
                                @elseif($f[$table_module.'_fields_name'] == 'ujian_kelas_id')
                                  <td style="white-space:nowrap">{{ $item['ujian_kelas_name'] }}</td>
                                @elseif($f[$table_module.'_fields_name'] == 'ujian_jurusan_id')
                                  <td style="white-space:nowrap">{{ $item['ujian_jurusan_name'] }}</td>
                                @else
  																<td style="white-space:nowrap">{{ $item[$f[$table_module.'_fields_name']] }}</td>
                                @endif
															@endif
														@endforeach
														<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['created_at']))) }}</td>
														<td style="white-space:nowrap">{{ \App\Helpers\GlobalHelper::instance()->dateIndo(date("d-m-Y", strtotime($item['updated_at']))) }}</td>
													</tr>
												@endforeach
										@else
											<tr>
												<td colspan="11">
													<h5 class="text-center">Belum Ada Ujian</h5>
												</td>
											</tr>
										@endif
                  </tbody>
                </table>
                @include('component.pagination')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
  <style>
    .bd-example-modal-lg .modal-dialog{
      display: table;
      position: relative;
      margin: 0 auto;
      top: calc(50% - 24px);
    }
    
    .bd-example-modal-lg .modal-dialog .modal-content{
      background-color: transparent;
      border: none;
    }
  </style>
  <div class="modal fade bd-example-modal-lg modal-spinner" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 48px">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
  </div>
</div>
<script>
  function doSelect()
  {
    $('#count_select').text($('.checkbox_data:checked').length);
    if($('.checkbox_data:checked').length > 0)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });

      $('input[name=data_selected]').val(JSON.stringify(val, true));
      
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

    let checked_data = $('.checkbox_data:checked').length;
    let check_all = $('.check_all').is(":checked");
    let count_all = '{{ @count($list_data) }}';

    console.log(check_all,checked_data,parseInt(count_all));
    if(check_all && checked_data < parseInt(count_all))
    {
      console.log('masuk');
      $('.check_all').prop('checked', false); 
    }
  }

  $(".check_all").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);

    let checked = $('.check_all').is(":checked");
    if(checked)
    {
      let val = [];
      $('.checkbox_data:checked').each(function() {
        val.push(this.value);
      });
      $('input[name=data_selected]').val(JSON.stringify(val, true));

      $('#count_select').text($('.checkbox_data:checked').length);
      $('.unshow-select').attr('style', 'display: none !important');
      $('#select-area').show();
    }
    else
    {
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
    }

  });

  $('.cancel_mass_delete').click(function () {
      $('input:checkbox').prop('checked', false);
      $('input[name=data_selected]').val(JSON.stringify([], true));
      $('.unshow-select').attr('style', '');
      $('#select-area').attr('style', 'display: none !important');
  })
  
  function confirmDelete()
  {
    swal({
      title: "Are you sure?",
      text: "You will deleting selected data?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        if($('.checkbox_data:checked').length == 0)
        {
          swal("No data selected!");
        }
        else
        {
          $('form[name=form_mass_delete]').submit();
        }
      }
    });
  }

  $('#resetUjianAll').click(function () {
    swal({
      title: "Are you sure?",
      text: "You will reset all data Ujian?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "{{url('smkbisa/ujian/reset-all')}}";
      }
    });
  })

  $('.btn-save-setting').click(function () {
    $('.modal-setting').modal('hide');

    let total = $('input[name=total_show]').val();
    console.log("🚀 ~ file: index.blade.php ~ line 294 ~ total", total)

	  var token = "{{ csrf_token() }}";

    $.ajax({
		url : '{{ URL($table_module.'/save_setting_soal') }}',
		type    : "POST",
		data    : "_token="+token+"&total_show="+total,
		dataType : 'json',
		beforeSend : function(responses){
    console.log("🚀 ~ file: index.blade.php ~ line 318 ~ responses", responses)
			$('.modal-spinner').modal('show');
		},
		success : function(responses){
    console.log("🚀 ~ file: index.blade.php ~ line 322 ~ responses", responses)
      $('.modal-spinner').modal('hide');
      swal("Berhasil Menyimpan Setting!");
		},
    error : function(responses){
    console.log("🚀 ~ file: index.blade.php ~ line 326 ~ responses", responses)
      $('.modal-spinner').modal('hide');
      swal("Error!");
    }
	});
  })
</script>
@endsection