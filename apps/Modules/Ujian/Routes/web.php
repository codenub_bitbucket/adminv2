<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/ujian')->group(function() {
    Route::get('/', 'UjianController@index');
    Route::get('/tambah', 'UjianController@create');
    Route::post('/save', 'UjianController@save');
    Route::get('/delete/{uuid}', 'UjianController@delete');
    Route::get('/edit/{uuid}', 'UjianController@edit');
    Route::post('/update/{uuid}', 'UjianController@update');
    Route::get('/detail/{uuid}', 'UjianController@detail');
    Route::post('/mass_delete', 'UjianController@mass_delete');
    
    Route::get('/duplikat', 'UjianController@duplicate');
    Route::post('/copy', 'UjianController@copy');
    
    Route::get('/soal/tambah/{ujian_uuid}', 'UjianController@createSoal');
    Route::post('/soal/save', 'UjianController@saveSoal');
    Route::get('/soal/import/{ujian_uuid}', 'UjianController@importSoal');
    Route::post('/soal/import_preview', 'UjianController@importSoalPreview');
    Route::post('/soal/import_save', 'UjianController@importSoalSave');
    
    Route::get('/soal/delete/{uuid_soal}/{uuid_ujian}', 'UjianController@deleteSoal');
    Route::get('/soal/edit/{uuid_soal}/{uuid_ujian}', 'UjianController@editSoal');
    Route::post('/soal/update/{uuid_soal}/{uuid_ujian}', 'UjianController@updateSoal');

    Route::get('/result/reset/{uuid_result}/{users_id}', 'UjianController@resetResult');
    Route::get('/result/export/{ujian_uuid}', 'UjianController@exportResult');
    Route::get('/reset/{ujian_uuid}', 'UjianController@resetUjian');
    Route::get('/reset-all', 'UjianController@resetUjianAll');

});
