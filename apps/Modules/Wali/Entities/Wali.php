<?php

namespace Modules\Wali\Entities;

use Illuminate\Database\Eloquent\Model;

class Wali extends Model
{
    protected $table 		= "wali";
    protected $primaryKey 	= "wali_serial_id";
    protected $guarded = array('wali_serial_id');
    public $timestamps = false;
}
