<?php

namespace Modules\Wali\Entities;

use Illuminate\Database\Eloquent\Model;

class WaliFields extends Model
{
    protected $table 		= "wali_fields";
    protected $primaryKey 	= "wali_fields_serial_id";
    protected $guarded = array('wali_fields_serial_id');
    public $timestamps = false;
}
