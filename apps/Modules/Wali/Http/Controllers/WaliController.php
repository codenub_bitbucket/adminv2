<?php

namespace Modules\Wali\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Auth;

use Modules\Wali\Http\Controllers\WaliHelper as wali_helper;
use Modules\Sys\Http\Controllers\SysHelper as sys;

class WaliController extends Controller
{
	var $module       = 'Wali';
	var $table_module = 'wali';

	function __construct(Request $request)
	{
		$this->middleware('auth');
		session()->forget('redirect');
		session()->put('redirect', $request->path());
			
	}

	public function index(Request $request)
	{
		$criteria = $request->all();
		$helper = new wali_helper();
		$sys = new sys();
		
		$criteria['show'] = isset($criteria['show']) ? $criteria['show'] : 15;
		$data_per_page = isset($criteria['show']) ? $criteria['show'] : 15;

		$list_wali = $helper->list_data('get', $criteria);
		$count_wali = $helper->list_data('count');

		$fields = $sys->getFields($this->table_module);

		$view = 'wali::index';

		$view_data = array(
			'table_module'   => $this->table_module,
			'list_data'      => $list_wali,
			'count_data'     => $count_wali,
			'data_per_page'  => $data_per_page,
			'fields'         => $fields
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function create()
	{
		$sys = new sys();

		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
		{
				if ($value['wali_fields_input_type'] == 'singleoption' && $value['wali_fields_options'] !== null) 
				{
						$fields[$key]['dropdown'] = $sys->getDropdown($value['wali_fields_options']);
				}
		}

		$view = 'wali::create';

		$view_data = array(
				'table_module'  => $this->table_module,
				'fields'        => $fields,
				'function' => 'save'
		);
			
		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function add(Request $request)
	{
		$criteria = $request->all();
		$helper = new wali_helper();
		$sys = new sys();

		$status       = 500;
		$msg          = "Gagal Menyimpan Data Wali";

		$data = $request->all();
        
		$save = $helper->save($data);

		if ($save)
		{
			return redirect('smkbisa/wali')->with(['success' => 'Wali Berhasil Ditambah']);
		}
		else
		{
			return redirect('smkbisa/wali')->with('failed', 'Wali Gagal Ditambah');
		}
	}

    public function update(Request $request, $uuid)
		{
			$users = Auth::user();

			$sys = new sys();
			$criteria = $request->all();

			if ($request->file('wali_photo')) 
			{
					$temp = new \Illuminate\Http\Request();
					$temp->replace(['file' => $criteria['wali_photo']]);
					
					$upload = $sys->uploadFile($temp);

					if ($upload['status'] == 200) 
					{
							$criteria['wali_photo'] = $upload['file']->image;
					}
			}

			$helper = new wali_helper();

			$update = $helper->update($criteria, $users, $uuid);

			if ($update)
			{
				return redirect('smkbisa/wali')->with(['success' => 'Wali Berhasil Diperbaharui']);
			}
			else
			{
				return redirect('smkbisa/wali')->with('failed', 'Wali Gagal Diperbaharui');
			}
    }
    
	public function delete($uuid)
	{
		$sys = new sys();

		$helper = new wali_helper();

		$delete = $helper->delete($uuid);

		if ($delete)
		{
				return redirect('smkbisa/wali')->with(['success' => 'Wali Berhasil Dihapus']);
		}
		else
		{
				return redirect('smkbisa/wali')->with('failed', 'Wali Gagal Dihapus');
		}
	}
    
	public function detail($uuid)
	{
		$sys = new sys();
		$helper = new wali_helper();
		
		$detail = $helper->get_detail($uuid);
		$fields = $sys->getFields($this->table_module);

		if (@count($detail) == 0) 
		{
				return redirect('smkbisa/wali')->with('failed', "Wali Tidak Valid!");
		}

		$view = 'wali::detail';
		$view_data = array(
			'table_module'   => $this->table_module,
			'content'        => $detail,
			'fields'				=> $fields
		);
		session()->forget('back');
		session()->put('back', 'smkbisa/wali/detail/'.$uuid);

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
    }
    
    public function edit($uuid)
	{
		$helper = new wali_helper();
		$sys = new sys();
		$detail = $helper->get_detail($uuid);

		$fields = $sys->getFields($this->table_module);
		foreach ($fields as $key => $value) 
        {
            if ($value['wali_fields_input_type'] == 'singleoption' && $value['wali_fields_options'] !== null) 
            {
                $fields[$key]['dropdown'] = $sys->getDropdown($value['wali_fields_options']);
            }
        }

		$view_data = array(
			'table_module'   => $this->table_module,
			'fields'         => $fields,
			'data' 					 => $detail,
			'function'   				 => 'edit'	
		);

		$view = 'wali::create';

		if(view()->exists($view))
		{
			return view($view, $view_data)->render();
		}
	}

	public function mass_delete(Request $request)
	{
		$data = $request->all();
		$helper = new wali_helper();

		$data = json_decode($data['data_selected'], true);
		$delete = $helper->mass_delete($data);

		if ($delete)
		{
			return redirect('smkbisa/wali')->with(['success' => 'Wali Berhasil Dihapus']);
		}
		else
		{
			return redirect('smkbisa/wali')->with('failed', 'Wali Gagal Dihapus');
		}
	}

}
