<?php
namespace Modules\Wali\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;

use Modules\Sys\Http\Controllers\SysHelper as sys;

class WaliHelper
{
  var $table_module = 'wali';
	var $paginate_manually = 'Illuminate\Pagination\LengthAwarePaginator';
  var $model_wali = 'Modules\Wali\Entities\Wali';
  
  public function list_data($type='get', $criteria = array())
  {
    $result = array();
    
    $query = $this->model_wali::orderby('wali.wali_name', 'ASC');
                                if (isset($criteria['search']) && $criteria['search'] != '')
                                {
                                  $query->where('wali.wali_name',"LIKE", "%".$criteria['search']."%");
                                }
                                $query->where('wali.deleted', 0);

    if ($type == 'get') 
    {
      $result = $query->select('wali.*', 'siswa.siswa_name', DB::raw('kelas.kelas_name as siswa_kelas_name'), 
                                                DB::raw('subjurusan.subjurusan_name as siswa_sub_jurusan_name'), 
                                                DB::raw('jurusan.jurusan_name as siswa_jurusan_name'))
                ->leftJoin('siswa', 'siswa.siswa_serial_id', "wali.wali_siswa_id")
                ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
                ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
                ->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
                ->paginate($criteria['show']);
                
      $result->appends($criteria);
    }
    else if ($type == 'get_dropdown') 
    {
      $query = $query->select("wali.wali_name as label", "wali.wali_serial_id as value")->get();
      if(@count($query) > 0)
			{
				$result 	= $query->toArray();
			}
    }
    else
    {
      $result = $query->count();
    }

    return $result;
  }

  function get_detail($uuid)
  {
    $result = array();

    $query = $this->model_wali::select('wali.*', 'siswa.siswa_name', DB::raw('kelas.kelas_name as siswa_kelas_name'), 
                                                DB::raw('subjurusan.subjurusan_name as siswa_sub_jurusan_name'), 
                                                DB::raw('jurusan.jurusan_name as siswa_jurusan_name'))
                              ->leftJoin('siswa', 'siswa.siswa_serial_id', "wali.wali_siswa_id")
                              ->leftjoin('kelas', 'kelas.kelas_serial_id', '=', 'siswa.siswa_kelas_id')
                              ->leftjoin('jurusan', 'jurusan.jurusan_serial_id', '=', 'siswa.siswa_jurusan_id')
                              ->leftjoin('subjurusan', 'subjurusan.subjurusan_serial_id', '=', 'siswa.siswa_subjurusan_id')
                              ->where('wali.wali_uuid', $uuid)
                              ->first();

    if (@count($query) > 0)
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function save($data)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

    $data['wali_uuid'] = Uuid::uuid4()->toString();
    $data['wali_username'] = strtoupper(substr(str_shuffle($permitted_chars), 0, 7));
    $data['wali_password'] = substr(str_shuffle($permitted_chars), 0, 7);
    $query = $this->model_wali::insert($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function update($data, $users, $uuid)
  {
    $result = false;
    unset($data['_token']);
    unset($data['files']);

    $data['updated_at'] = date('Y-m-d H:i:s');
    $query = $this->model_wali::where('wali_uuid', $uuid)->update($data);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function delete($uuid)
  {
    $result = false;
    $update['deleted'] = 1;
    $query = $this->model_wali::where('wali_uuid', $uuid)->update($update);
    if ($query)
    {
      $result = true;
    }
    return $query;
  }

  function mass_delete($data)
  {
    $sys = new sys();

    $result = false;
    $update['deleted'] = 1;
    $update['updated_at'] = date('Y-m-d H:i:s');

    $query = $this->{'model_'.$this->table_module}::whereIn($this->table_module.'_uuid', $data)->update($update);

    if ($query)
    {
      $result = true;
    }
    return $query;
  }
  
}