<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('smkbisa/wali')->group(function() {
    Route::get('/', 'WaliController@index');
    Route::get('/tambah', 'WaliController@create');
    Route::post('/add', 'WaliController@add');
    Route::get('/delete/{uuid}', 'WaliController@delete');
    Route::get('/edit/{uuid}', 'WaliController@edit');
    Route::post('/update/{uuid}', 'WaliController@update');
    Route::get('/detail/{uuid}', 'WaliController@detail');

    Route::post('/mass_delete', 'WaliController@mass_delete');
});
