<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($e instanceof NotFoundHttpException)
		{
            return redirect('home');
            // return response()->view('errors.404', [], 404);

			// $response  = array(
			// 	"status"    => 404,
			// 	"msg"       => 'Route Not Found!',
			// );

			// return response()->json($response);
		}
		else if(app()->environment() == 'production')
		{
			if($e instanceof \ErrorException)
			{
                $this->error_information_save($e);
                return redirect('home');
                // return response()->view('errors.500', [], 500);
                
				// $response  = array(
				// 	"status"    => 500,
				// 	"msg"       => 'Something Went Wrong!',
				// );

				// return response()->json($response);
			}
			elseif($e instanceof \Symfony\Component\Debug\Exception\FatalErrorException)
			{
                # save information log into tbl error_log
                return redirect('home');
                // return response()->view('errors.500', [], 500);
                
				// $this->error_information_save($e);
				// $response  = array(
				// 	"status"    => 500,
				// 	"msg"       => 'Fatal Error!',
				// );

				// return response()->json($response);
			}
			else if($e instanceof QueryException)
			{
				$this->error_information_save($e);
				$response  = array(
					"status"    => 'SQLSTATE',
					"msg"       => 'SQLSTATE ERROR!',
				);

				return response()->json($response);
			}
		}
        return parent::render($request, $e);
    }

    function error_information_save($e)
    {
        # code...
    }
}
