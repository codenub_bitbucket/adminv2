<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use Modules\Ujian\Http\Controllers\UjianHelper as ujian_helper;

class CollectionExport implements FromCollection, WithHeadings, WithColumnWidths, WithStyles
{
    use Exportable;

    protected $uuid_ujian;

    function __construct($uuid_ujian) {
            $this->uuid_ujian = $uuid_ujian;
    }

    public function collection()
    {
		$helper = new ujian_helper();
		$data = $helper->get_detail($this->uuid_ujian);
        $col = $helper->getReport($data);

        $newArray = array();

        foreach ($col as $key => $val)
        {
            $hasil_ujian_result = json_decode($val['hasil_ujian_result'], true);
            $total_soal = $hasil_ujian_result['true'] + $hasil_ujian_result['false'] + $hasil_ujian_result['none'];
            $newArray[] = array(
                'name'   => $val['siswa_name'],
                'kelas'   => $val['kelas_name'],
                'jurusan'   => $val['jurusan_name'],
                'subjurusan'   => $val['subjurusan_name'],
                'true'   => $hasil_ujian_result['true'],
                'false'   => $hasil_ujian_result['false'],
                'none'   => $hasil_ujian_result['none'],
                'score'   => ($hasil_ujian_result['true'] / $total_soal) * 100,
            );
        }

        return collect($newArray);
    }

    public function headings(): array
    {
        return [
            'Nama Siswa',
            'Kelas',
            'Jurusan',
            'Sub',
            'Benar',
            'Salah',
            'Kosong',
            'Nilai',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 10,
            'C' => 20,
            'D' => 7,
            'E' => 7,
            'F' => 7,
            'G' => 7,
            'H' => 7,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
    }

}