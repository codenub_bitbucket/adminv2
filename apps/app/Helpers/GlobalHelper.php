<?php
namespace App\Helpers;
use Modules\AboutUs\Http\Controllers\AboutUsHelper as aboutus_helper;

class GlobalHelper
{
      public static function instance()
      {
				return new GlobalHelper();
      }

      function dateIndo($date, $dayPrint = true)
      {
				$hari = array ( 1 =>    'Senin',
													'Selasa',
													'Rabu',
													'Kamis',
													'Jumat',
													'Sabtu',
													'Minggu'
										);
                        
				$bulan = array (1 =>   'Januari',
													'Februari',
													'Maret',
													'April',
													'Mei',
													'Juni',
													'Juli',
													'Agustus',
													'September',
													'Oktober',
													'November',
													'Desember'
										);
				$split 	  = explode('-', $date);

				$dateIndo = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];
				
				if ($dayPrint) {
							$num = date('N', strtotime($date));
							return $hari[$num] . ', ' . $dateIndo;
				}
				return $dateIndo;
      }

      public function getLogo()
      {
				$auh = new aboutus_helper();

				$logo = $auh->getInfoAbout('school_logo');

				if (@count($logo) > 0) 
				{
					$logo = 'apps/public/'.$logo['about_us_description'];
				}
				else 
				{
					$logo = 'resources/images/logo-kartikatama.png';
				}

				return $logo;
      }
}