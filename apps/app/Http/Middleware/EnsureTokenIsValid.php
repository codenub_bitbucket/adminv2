<?php

namespace App\Http\Middleware;

use Closure;
use Response;

use DB;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();

        if(isset($data['users_id']) && $data['users_id'] > 0)
        {
            $token = DB::table('users')->where('id', '=', $data['users_id'])->first();
            $token = $token->api_token;

            if(isset($data['api_token']) && $data['api_token'] == $token)
            {
                return $next($request);
            }
            else
            {
                return response()->json(['msg' => 'unauthorized'], 401);
            }
        }
        else if(isset($data['isParent']) && $data['isParent'] == true)
        {
            return $next($request);
        }
        else 
        {
            return response()->json(['msg' => 'unauthorized'], 401);
        }
    }
}
