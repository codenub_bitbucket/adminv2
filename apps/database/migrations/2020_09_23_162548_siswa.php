<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Siswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->increments('siswa_serial_id');
            $table->string('siswa_name');
            $table->string('siswa_username');
            $table->string('siswa_password');
            $table->string('siswa_nisn');
            $table->string('siswa_photo');
            $table->datetime('siswa_dob')->default(NULL);
            $table->string('siswa_gender');
            $table->bigInteger('siswa_kelas_id');
            $table->bigInteger('siswa_jurusan_id');
            $table->string('siswa_sub_jurusan');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswa');
    }
}
