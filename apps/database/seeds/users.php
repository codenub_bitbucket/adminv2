<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        
        DB::table('users')->insert([
            [
                'name'          => 'Admin Rendi',
                'email'         => 'admin@admin.com',
                'password'      => '$2y$12$d9QaMBWci2uHv/xfIiagDu8y1iV0AVgKbmcbXwf5PX1ByFOt6p/X2',
                'role'          => 1,
                'photo'         => 'assets/img/rendi.png',
            ],
                    
      ]);
    }
}
