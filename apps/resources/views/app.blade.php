@php
    $segment = Request::segment(2);
@endphp
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>SMK Kartikatama Metro</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- App favicon -->
	<link rel="shortcut icon" href="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" />

  <!-- Bootstrap Css -->
  <link href="{{ URL('resources_admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />

  <!-- Icons Css -->
  <link href="{{ URL('resources_admin/css/icons.css') }}" rel="stylesheet" type="text/css" />

  <!-- App Css-->
  <link href="{{ URL('resources_admin/css/app.css') }}" id="app-style" rel="stylesheet" type="text/css" />
  <link href="{{ URL('resources_admin/timepicker.css') }}" id="app-style" rel="stylesheet" type="text/css" />

  {{-- Auto complete css --}}
  <link href="{{ URL('assets/select2/select2.min.css') }}" rel="stylesheet" />

  <script src="{{ URL('resources_admin/libs/jquery/jquery.min.js') }}"></script>
  <link href="{{ URL('resources_admin/summernote/summernote.css') }}" id="app-style" rel="stylesheet" type="text/css" />
  <script src="{{ URL('resources_admin/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL('resources_admin/libs/metismenu/metisMenu.min.js') }}"></script>
  <script src="{{ URL('resources_admin/libs/simplebar/simplebar.min.js') }}"></script>
  <script src="{{ URL('resources_admin/libs/node-waves/waves.min.js') }}"></script>
  <script src="{{ URL('resources_admin/summernote/summernote.js') }}"></script>
  <script src="{{ URL('resources_admin/datepicker.js') }}"></script>
  <script src="{{ URL('resources_admin/timepicker.js') }}"></script>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script src="{{ URL('assets/select2/select2.min.js') }}"></script>
</head>

<body  data-layout="horizontal">
  <!-- Begin page -->
  <div>
    <header id="page-topbar">
      <div class="navbar-header">
        <div class="d-flex w-50">
          <!-- LOGO -->
          <div class="navbar-brand-box">
            <a href="index.html" class="logo logo-dark">
              <span class="logo-sm">
                <img src="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" alt="" height="22">
              </span>
              <span class="logo-lg">
                <img src="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" alt="" style="width: 120px!important; position: absolute;">
              </span>
            </a>
          </div>

          <button type="button" class="btn btn-sm px-3 font-size-24 d-lg-none header-item" data-toggle="collapse"
            data-target="#topnav-menu-content">
            <i class="ri-menu-2-line align-middle"></i>
          </button>

          <!-- App Search-->
          {{-- <form class="app-search d-none d-lg-block w-50" style="margin-left: 22%;">
            <div class="position-relative">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="ri-search-line"></span>
            </div>
          </form> --}}
        </div>

        <div class="d-flex">
          {{-- <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="ri-notification-3-line"></i>
              <span class="noti-dot"></span>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
              aria-labelledby="page-header-notifications-dropdown">
              <div class="p-3">
                <div class="row align-items-center">
                  <div class="col">
                    <h6 class="m-0"> Notifications </h6>
                  </div>
                  <div class="col-auto">
                    <a href="#!" class="small"> View All</a>
                  </div>
                </div>
              </div>
              <div data-simplebar style="max-height: 230px;">
                <a href="" class="text-reset notification-item">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="mt-0 mb-1">New Register</h6>
                      <div class="font-size-12 text-muted">
                        <p class="mb-1">If several languages coalesce the grammar</p>
                        <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="p-2 border-top">
                <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                  <i class="mdi mdi-arrow-right-circle mr-1"></i> View More..
                </a>
              </div>
            </div>
          </div> --}}
          <div class="dropdown d-inline-block user-dropdown">
            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="rounded-circle header-profile-user" src="{{ URL('apps/public/'.session()->get('users_detail')['photo']) }}"
                alt="Header Avatar">
              <span class="d-none d-xl-inline-block ml-1">{{ session()->get('users_detail')['name'] }}</span>
              <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <!-- item-->
              {{-- <a class="dropdown-item" href="#"><i class="ri-user-line align-middle mr-1"></i> Profile</a> --}}
              <div class="dropdown-divider"></div>
              <a class="dropdown-item text-danger" href="{{ URL('logout') }}"><i
                  class="ri-shut-down-line align-middle mr-1 text-danger"></i> Logout</a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="topnav">
      <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">
          <div class="collapse navbar-collapse" id="topnav-menu-content">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item {{ ($segment == 'beranda') ? ' active' : ''}}">
                <a class="nav-link" href="{{ URL('smkbisa') }}">
                  <i class="ri-dashboard-line mr-2"></i> Dashboard
                </a>
              </li>

              <li class="nav-item dropdown {{ ($segment == 'landing' || $segment == 'extracurricular' || $segment == 'fasilitas' || $segment == 'berita-sekolah' || $segment == 'pendaftar') ? ' active' : ''}}">
                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-apps" role="button"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ri-macbook-line mr-2"></i>Website <div class="arrow-down"></div>
                </a>
                <div class="dropdown-menu" aria-labelledby="topnav-apps">
                  <a href="{{ URL('smkbisa/landing') }}" class="dropdown-item">Pengaturan</a>
                  <a href="{{ URL('smkbisa/fasilitas') }}" class="dropdown-item">Fasilitas</a>
                  <a href="{{ URL('smkbisa/extracurricular') }}" class="dropdown-item">Extracurricular</a>
                  <a href="{{ URL('smkbisa/berita-sekolah') }}" class="dropdown-item">Berita Sekolah</a>
                  <a href="{{ URL('smkbisa/pendaftar') }}" class="dropdown-item">Pendaftar (PPDB)</a>
                </div>
              </li>

              <li class="nav-item dropdown {{ ($segment == 'jurusan' || $segment == 'subjurusan' || $segment == 'siswa' || $segment == 'approval-absensi') ? ' active' : ''}}">
                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-apps" role="button"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ri-building-fill mr-2"></i>School Management <div class="arrow-down"></div>
                </a>
                <div class="dropdown-menu" aria-labelledby="topnav-apps">
                  <a href="{{ URL('smkbisa/siswa') }}" class="dropdown-item">Siswa</a>
                  <a href="{{ URL('smkbisa/jurusan') }}" class="dropdown-item">Jurusan</a>
                  <a href="{{ URL('smkbisa/subjurusan') }}" class="dropdown-item">Sub Jurusan</a>
                  <a href="{{ URL('smkbisa/approval-absensi') }}" class="dropdown-item">Approval Absensi</a>
                </div>
              </li>

              <li class="nav-item dropdown {{ ($segment == 'mata-pelajaran' || $segment == 'soal' || $segment == 'elearning') ? ' active' : ''}}">
                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-apps" role="button"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ri-building-fill mr-2"></i>Education <div class="arrow-down"></div>
                </a>
                <div class="dropdown-menu" aria-labelledby="topnav-apps">
                  <a href="{{ URL('smkbisa/mata-pelajaran') }}" class="dropdown-item">Mata Pelajaran</a>
                  <a href="{{ URL('smkbisa/ujian') }}" class="dropdown-item">Ujian</a>
                  <a href="{{ URL('smkbisa/elearning') }}" class="dropdown-item">E-Learning</a>
                </div>
              </li>

              <li class="nav-item dropdown {{ ($segment == 'staff' || $segment == 'guru' || $segment == 'wali') ? ' active' : ''}}">
                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-apps" role="button"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ri-book-2-line mr-2"></i>User Management <div class="arrow-down"></div>
                </a>
                <div class="dropdown-menu" aria-labelledby="topnav-apps">
                  <a href="{{ URL('smkbisa/staff') }}" class="dropdown-item">Admin</a>
                  <a href="{{ URL('smkbisa/guru') }}" class="dropdown-item">Guru</a>
                  <a href="{{ URL('smkbisa/wali') }}" class="dropdown-item">Wali</a>
                </div>
              </li>
              <li class="nav-item {{ ($segment == 'pengumuman') ? ' active' : ''}}">
                <a class="nav-link" href="{{ URL('smkbisa/pengumuman') }}">
                  <i class="ri-information-line mr-2"></i> Pengumuman
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    <!-- start main content-->
    @yield('content')
    <!-- end main content-->
    <footer class="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            © 2021 All Rights Reserved || SMK Kartikatama Metro
          </div>
          <div class="col-sm-6">
            <div class="text-sm-right d-none d-sm-block">
                Created with ☕ by <a href="https://www.instagram.com/rendifebrian15">RENDI FEBRIAN</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <!-- END layout-wrapper -->

  <!-- Right bar overlay-->
  <div class="rightbar-overlay"></div>

  <!-- JAVASCRIPT -->
  </body>

</html>
