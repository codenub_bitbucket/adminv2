<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title') - SMK KARTIKATAMA METRO</title>

  <link rel="icon" href="{{ URL('assets/img/logo.png') }}" type="image/gif" sizes="16x16">

  <!-- Custom fonts for this template-->
  <link href="{{ URL('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ URL('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL('assets/dropzone/basic.css') }}">
  <link rel="stylesheet" href="{{ URL('assets/dropzone/dropzone.css') }}">
  <meta name="csrf-token" content="{!! csrf_token() !!}">

</head>

@if (!empty(Auth::user()))
  <body id="page-top">
@else
  <body class="bg-gradient-primary">
@endif

@if (!empty(Auth::user()))
<div id="wrapper">
  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon">
        <img src="{{ URL('assets/img/logo.png') }}" width="40px" height="40px">
      </div>
      <div class="sidebar-brand-text mx-3">SMK KARTIKATAMA METRO</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
      <a class="nav-link" href="index.html">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      User Management
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('siswa') }}">
      <i class="fas fa-users"></i>
      <span>Siswa</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('guru') }}">
      <i class="fas fa-chalkboard-teacher"></i>
      <span>Guru</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('alumni') }}">
      <i class="fas fa-user-graduate"></i>
      <span>Alumni</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('admin') }}">
      <i class="fas fa-user-secret"></i>
      <span>Admin</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      SCHOOL MANAGEMENT
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('matapelajaran') }}">
      <i class="fas fa-book"></i>
      <span>Mata Pelajaran</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('soal') }}">
      <i class="fas fa-list-alt"></i>
      <span>Soal</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('hasil_ujian') }}">
      <i class="fas fa-clipboard-list"></i>
      <span>Hasil Ujian</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ URL('kelas') }}">
      <i class="fas fa-school"></i>
      <span>Kelas</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="false" aria-controls="collapseUtilities">
      <i class="fas fa-briefcase"></i>
        <span>Jurusan</span>
      </a>
      <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="{{ URL('jurusan') }}">Jurusan</a>
          <a class="collapse-item" href="{{ URL('subjurusan') }}">Sub Jurusan</a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>

  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars"></i>
        </button>

        <div class="text-center w-100">
          <span class="h4 d-none d-lg-block d-md-block d-sm-none d-xs-none">ADMIN PANEL SMK KARTIKATAMA METRO</span>
        </div>
  
  
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
  
          <!-- Nav Item - Search Dropdown (Visible Only XS) -->
          <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
              <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>
  
          <div class="topbar-divider d-none d-sm-block"></div>
  
          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ session()->get('users_detail')['name'] }}</span>
              <img class="img-profile rounded-circle" src="{{ URL(session()->get('users_detail')['photo']) }}">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="#">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profile
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
              </a>
            </div>
          </li>
  
        </ul>
  
      </nav>
@endif

  @yield('content')

@if (!empty(Auth::user()))
      </div>
    <footer class="sticky-footer bg-white">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          <span>Copyright &copy; Rendi Febrian 2019-{{ date('Y') }} || Made with &#9749; by <b>Rendi Febrian</b></span>
        </div>
      </div>
    </footer>
  </div>
</div>
@endif

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="{{ URL('logout') }}">Logout</a>
      </div>
    </div>
  </div>
</div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ URL('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ URL('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ URL('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ URL('assets/js/sb-admin-2.min.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>

</html>
