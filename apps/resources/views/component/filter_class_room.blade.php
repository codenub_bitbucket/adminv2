<?php
$class = json_decode($class); 
unset($_GET['class']); 
if ($class > 30) 
{
  $link = "?".http_build_query($_GET)."&class";
}
else 
{
  $link = $table_module."?".http_build_query($_GET)."&class";
}
?>

<div class="d-flex align-items-center unshow-select mr-4">
  <label class="mb-0">Kelas</label>
  <select class="form-control ml-3 " onChange="javascript:window.location.href='{{ $link }}='+btoa(this.value)">
    <option value="All" {{($class == 'All') ? 'selected' : ''}}>Semua</option>
    @foreach ($list_kelas as $item)
      <option value="{{ $item['value'] }}" {{($class == $item['value']) ? 'selected' : ''}}>{{ $item['label'] }}</option>
    @endforeach
  </select>
</div>