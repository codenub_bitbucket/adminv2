<?php
$show = $data_per_page; 
unset($_GET['show']); 
if ($show > 30) 
{
  $link = "?".http_build_query($_GET)."&show";
}
else 
{
  $link = $table_module."?".http_build_query($_GET)."&show";
}
?>

<div class="form-group mb-0 align-items-center d-flex" style="width: 180px">
  <label class="mb-0">Show</label>
  <select class="form-control ml-3 " onChange="javascript:window.location.href='{{ $link }}='+this.value">
    <option value="15" {{($show == '15') ? 'selected' : ''}}>15</option>
    <option value="30" {{($show == '30') ? 'selected' : ''}}>30</option>
    <option value="50" {{($show == '50') ? 'selected' : ''}}>50</option>
    <option value="100" {{($show == '100') ? 'selected' : ''}}>100</option>
  </select>
</div>