@php
    $interval = isset($interval) ? abs(intval($interval)) : 1 ;
    $from = $list_data->currentPage() - $interval;
    if($from < 1){
        $from = 1;
    }
    $to = $list_data->currentPage() + $interval + 1;
    if($to > $list_data->lastPage()){
        $to = $list_data->lastPage();
    }
@endphp
<div class="container mb-6 order-3 order-md-3">
  <div class="row justify-content-center">
      <div class="col-md-12 ml-auto">
          <nav>
              <ul class="pagination justify-content-center">
                  <li class="page-item {{ ($list_data->currentPage() <= 1) ? 'disabled' : '' }}"> <a class="page-link" href="{{ ($list_data->currentPage() > 1) ? $list_data->url($list_data->currentPage() - 1) : '#' }}">Sebelumnya</a> </li>
                  @for ($i = $from; $i <= $to; $i++)
                  <li class="page-item {{ ($list_data->currentPage() == $i) ? 'active' : '' }}"> <a class="page-link bg-grad" href="{{ $list_data->url($i) }}"> {{ $i }} </a> </li>
                  @endfor
                  <li class="page-item {{ ($list_data->lastPage() <= $list_data->currentPage()) ? 'disabled' : '' }}"><a class="page-link" href="{{ $list_data->url($list_data->currentPage() + 1) }}">Selanjutnya</a> </li>
              </ul>
          </nav>
      </div>
  </div>
</div>