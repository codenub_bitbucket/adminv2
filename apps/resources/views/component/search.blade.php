<div class="input-group mb-0 ml-3">
  <input type="text" class="form-control" placeholder="Search here..."  aria-describedby="basic-addon2" id="keyword" value="{{ (isset($_GET['search']) && $_GET['search'] !== '') ? $_GET['search'] : '' }}">
  <div class="input-group-append">
    <button class="btn btn-secondary" style="padding-bottom: 5px;" type="button" onclick="search()">
      <i class="ri-search-line"></i>
    </button>
  </div>
</div>

<?php

$lastKeyword = (isset($_GET["search"]) && $_GET["search"] !== '') ? $_GET["search"] : '';
$table_module = str_replace("_", "-", $table_module);
?>
<script>
  var input = document.getElementById("keyword");

  // Execute a function when the user releases a key on the keyboard
  input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      search();
    }
  });

  function search()
  {
    let keyword = document.getElementById('keyword').value;

    if (keyword.length > 0) 
    {
      let link = '';
      if ('{{ isset($_GET["search"]) && $_GET["search"] !== '' }}') 
      {
        link = '{{ $table_module."?".str_replace("search=".$lastKeyword, "",http_build_query($_GET)) }}&search=' + keyword;
      }
      else
      {
        link = '{{ $table_module."?".http_build_query($_GET)."&search=" }}' + keyword;
      }

      if (link !== '') 
      {
        link = link.replace("&amp;", "&");
        window.location.href = link;
      }
    }
    else
    {
      window.location.href = '{{ $table_module }}';
    }
  }
</script>