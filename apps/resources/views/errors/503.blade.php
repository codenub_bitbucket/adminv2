
<!doctype html>
<html lang="en">

<head>
	<title>SMK Kartikatama Metro</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="">
	<meta name="description" content="SMK Kartikatama Metro">
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/logo.png">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CPlayfair+Display:400,400i,700,700i%7CRoboto:400,400i,500,700" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/fontawesome-free/css/fontawesome.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/themify-icons/css/themify-icons.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/animate/animate.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/fancybox/css/jquery.fancybox.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/owlcarousel/css/owl.carousel.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendor/swiper/css/swiper.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/three-dots.css') }}" />

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}" />

</head>

<body>

	<section class="error-page maintenance pt-5 mt-3" >
		<div class="container">
			<div class="row">
				<div class="col-md-8 mx-auto text-center">
					<img src="{{ url('assets/img/logo.png') }}" style="height: 200px; margin-top: 30px;" >
					<h3 class="mb-4 font-weight-bold">Maaf, website SMK Kartikatama Metro sedang dalam proses perbaikan</h3>
					<p>Kami akan segera live kembali.</p>
				</div>
			</div>
		</div>
	</section>


	<!--Global JS-->
	<script src="{{ url('assets/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ url('assets/vendor/popper.js/umd/popper.min.js') }}"></script>
	<script src="{{ url('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

	<!--Vendors-->
	<script src="{{ url('assets/vendor/fancybox/js/jquery.fancybox.min.js') }}"></script>
	<script src="{{ url('assets/vendor/owlcarousel/js/owl.carousel.min.js') }}"></script>
	<script src="{{ url('assets/vendor/swiper/js/swiper.js') }}"></script>
	<script src="{{ url('assets/vendor/wow/wow.min.js') }}"></script>

	<!--Template Functions-->
	<script src="{{ url('assets/js/functions.js') }}"></script>

</body>

</html>
