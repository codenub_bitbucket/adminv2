@php
    $val = '';

    if (isset($data) && @count($data) > 0) 
    {
      $val = date("d-m-Y", strtotime($data[$fields_name]));
    }
    else {
      $val = date("d-m-Y");
    }

@endphp
{{-- 
<input 
  type='text' 
  class='form-control text-date' 
  placeholder="dd/mm/yyyy"
  onkeyup="onlyNumberKey(event); 
      var v = this.value;
      if (v.match(/^\d{2}$/) !== null) {
          this.value = v + '/';
      } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
          this.value = v + '/';
      }"
  maxlength="10"
  name="{{ $fields_name }}"  
  value="{{ $val }}"
  id="text-date"
  required="{{ $required}}"
  autocomplete="off"
>
<script>
  $(document).ready(function(){
    setDate();
  });
  function onlyNumberKey(evt) { 
          
          // Only ASCII charactar in that range allowed 
          var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
          if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
          {
            let val = $("#text-date").val();
            let replaced = val.replace(evt.key, '')
            $("#text-date").val(replaced)
          }
      } 

  function setDate()
  {
    if("{{ $fields_name }}" !== "{{ $table_module }}_dob")
    {
      const dateObj = new Date();
      const month = ("0" + (dateObj.getMonth() + 1)).slice(-2)
      const day = ("0" + dateObj.getDate()).slice(-2)
      const year = dateObj.getFullYear();
      const output = month  + '/'+ day  + '/' + year;

      $(".text-date").val(output)
    }
  }
</script> --}}
<!-- Input Mask-->
{{-- <script src="{{ URL('assets/jasny/jasny-bootstrap.min.js') }}"></script> --}}

<input 
  class="form-control" 
  type="text"
  placeholder="dd-mm-yyyy"
  name="{{ $fields_name }}"  
  required="{{ $required}}"
  autocomplete="off"
  >

<script type="text/javascript">
  $('input[name="{{ $fields_name }}"]').datepicker({  
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", "{{ $val }}");
</script> 