<textarea 
  class="form-control" 
  id="textarea-input"
  name="{{ $fields_name }}" 
  cols="30" 
  rows="10" maxlength="500">{{ (isset($data) && @count($data) > 0) ? $data[$fields_name] : '' }}</textarea>
  <small class="float-right" id="length-text">0/500</small>

  <script>
    $(document).ready(function() {
      let p = $('#textarea-input').val().length;
      $('#length-text').html(p + '/500');
  
      $('#textarea-input').keyup(function() {

        let p = $('#textarea-input').val().length;
        $('#length-text').html(p + '/500');
        if (p >= 500) 
        {
          $('#length-text').css('color', 'red');
        }
  
      })
    })
  </script>