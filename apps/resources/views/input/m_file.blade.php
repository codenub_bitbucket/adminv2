<div class="dropzone" id="divdropzone"></div>

<script src="{{ URL('assets/dropzone/dropzone.js') }}"></script>
<script>
  Dropzone.autoDiscover = false;
  var myDropzone = new Dropzone("div#divdropzone", 
                  { 
                    url: '{{ URL('upload') }}', 
                    addRemoveLinks: true,
                  });
                  // myDropzone.hiddenFileInput.removeAttribute('multiple');
  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("_token", "{{ csrf_token() }}");
  });
  myDropzone.on("error", function(file, xhr) {
    setTimeout(() => {
      myDropzone.removeFile(file);
    }, 3000);
  });
  myDropzone.on("removedfile", function(file) {
    var name = file.upload.filename;
    $.ajax({
        type: 'POST',
        url: '{{ url("upload/delete") }}',
        data: {filename: name, _token:'{{ csrf_token() }}'},
        success: function (data){
          if (data.status == 200) 
          {
            console.log("File has been successfully removed!!");
            file.previewElement.remove();
          }
        },
        error: function(e) {
            console.log(e);
        }});
  });
</script>