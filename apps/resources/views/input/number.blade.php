<input
  class="form-control"
  id="{{ $fields_name }}"
  type="number"
  placeholder="{{ $fields_label }}"
  name="{{ $fields_name }}"
  min="1"
  value="{{ (isset($data) && @count($data) > 0) ? $data[$fields_name] : '' }}"
  required="{{ $required}}"
  >
