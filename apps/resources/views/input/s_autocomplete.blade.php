<?php
$table_module = str_replace("_", "-", $table_module);
$module = $table_module;

if($table_module == 'wali')
{
  $module = 'siswa';
}

if($fields_name == $table_module.'_mata_pelajaran_id')
{
  $module = 'mata_pelajaran';
}
?>

<select class="select2class form-control" name="{{ $fields_name }}">
  @if ($function == 'edit')
    <option value="{{ $data[$fields_name] }}" selected="selected">
      <?php
        if($table_module == 'wali')
        {
          echo($data['siswa_name'].' - '.$data['siswa_kelas_name'].' '.$data['siswa_jurusan_name']);
        }
        else {
          echo($data[$fields_name."_label"]);
        }
      ?>
    </option>
  @endif
</select>

<script type="text/javascript">
  $('.select2class').select2({
    placeholder: 'Cari...',
    ajax: {
      url: '{{ URL("autocomplete/$module") }}',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: `${item.label} ${ ('{{ $table_module }}' == 'wali') ? '-' + item.siswa_kelas_name +' '+item.siswa_jurusan_name : ''}`,
              id: item.value
            }
          })
        };
      },
      cache: true
    }
  });

</script>