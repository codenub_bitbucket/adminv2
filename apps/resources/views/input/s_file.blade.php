<input type='file' id="{{ $fields_name }}" name="{{ $fields_name }}" class="file form-control" {{ (isset($function) && $function == 'edit') ? false : 'required='.$required }}/>

@if (isset($function) && $function == 'edit')
<img id="preview_img_s" src="{{ URL('apps/public/'.$data[$fields_name]) }}"  style="height: 100px; min-width: 100px"/>
@elseif(isset($function) && $function == 'save')
<div class="container-preview" style="display: none">
  <img id="preview_img_s" src="#"  style="height: 100px; min-width: 100px"/>
</div>
@endif

<script>
  let id = '{{ $fields_name }}';
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.container-preview').show();
              $('#preview_img_s').attr('src', e.target.result);
              if ('{{$table_module}}' == 'berita_sekolah') 
              {
                $('#img-preview').attr('src', e.target.result)  
              }
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $("#"+id).change(function(){
    console.log("{{ $table_module }}");
    if("{{ $table_module }}" != 'elearning')
    {
      readURL(this);
    }
  });
</script>