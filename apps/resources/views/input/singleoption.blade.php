<select class="form-control" id="{{ $fields_name }}" name="{{ $fields_name }}" required="{{ $required }}">
  <option value="">-- Pilih {{ $fields_label }} --</option>
  @if (@count($dropdown) > 0)
    @foreach ($dropdown as $key => $item)
      <option value="{{ $item['value'] }}" <?= (isset($data) && $item['value'] == $data[$fields_name]) ? "selected" : "" ?>>{{ $item['label'] }}</option>
    @endforeach
  @elseif($table_module == 'soal')
  <option value="a" <?= (isset($data) && $data['soal_answer_key'] == 'a') ? "selected" : "" ?>>A</option>
  <option value="b" <?= (isset($data) && $data['soal_answer_key'] == 'b') ? "selected" : "" ?>>B</option>
  <option value="c" <?= (isset($data) && $data['soal_answer_key'] == 'c') ? "selected" : "" ?>>C</option>
  <option value="d" <?= (isset($data) && $data['soal_answer_key'] == 'd') ? "selected" : "" ?>>D</option>
  <option value="e" <?= (isset($data) && $data['soal_answer_key'] == 'e') ? "selected" : "" ?>>E</option>
  @else
    <option disabled>Tidak Ada {{ $fields_label }}</option>
  @endif
</select>