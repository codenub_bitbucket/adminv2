@if ($table_module == 'soal' && $fields_name == 'soal_choices')
<div class="row">
  @php
      $option = ['A', 'B', 'C', 'D', 'E'];

      if ($function == 'edit') 
      {
        $data['soal_choices'] = json_decode($data['soal_choices'], true);
      }
  @endphp
  @for ($i = 0; $i < 5; $i++)
    <div class="col-{{floor(12/5)}} input-group" style="max-width: calc(100% / 5); flex: calc(100% / 5);">
      <div class="input-group-prepend">
        <span class="input-group-text">{{ $option[$i] }}</span>
      </div>
      <input type="text" required class="form-control" name="soal_choices[{{strtolower($option[$i])}}]" value="{{ (isset($data) && @count($data['soal_choices']) > 0) ? $data['soal_choices'][strtolower($option[$i])] : '' }}">
    </div>
  @endfor
</div> 
@else
<input 
  class="form-control" 
  id="{{ $fields_name }}" 
  type="text" 
  placeholder="{{ $fields_label }}"
  name="{{ $fields_name }}" 
  @if ($table_module == 'about_us')
  value="{{$data['about_us_description']}}"
  @else
  value="{{ (isset($data) && @count($data) > 0) ? $data[$fields_name] : '' }}"
  @endif
  required="{{ $required }}"
  >
@endif