<textarea 
  class="form-control" 
  name="{{ $fields_name }}" 
  id="compose" 
  cols="30" 
  rows="30">
  @if ($table_module == 'about_us')
  {{$data['about_us_description']}}
  @else
  {{ (isset($data) && @count($data) > 0) ? $data[$fields_name] : '' }}
  @endif
</textarea>

<script>
  $(document).ready(function() {
    $('#compose').summernote({
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['picture'],
        ['insert', ['link']],
      ],
      height: 250
    });
  });
</script>