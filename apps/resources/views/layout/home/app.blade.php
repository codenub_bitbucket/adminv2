
<!doctype html>
<html lang="en">

<head>
	<title>{{ (isset($data['school_name']) ? $data['school_name']['description'] : 'SMK Kartikatama Metro') }}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="">
	<meta name="description" content="{{ (isset($data['school_name']) ? $data['school_name']['description'] : 'SMK Kartikatama Metro') }}">
	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}"/>
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CPlayfair+Display:400,400i,700,700i%7CRoboto:400,400i,500,700" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/font-awesome/css/font-awesome.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/themify-icons/css/themify-icons.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/animate/animate.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/fancybox/css/jquery.fancybox.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/owlcarousel/css/owl.carousel.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/vendor/swiper/css/swiper.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/css/three-dots.css') }}" />

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL('resources/css/style.css') }}"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
	<script src="{{ URL('resources/download.js') }}"></script>
</head>

<body>
	<div class="preloader">
		<div class="d-flex align-items-center w-100">
			<img src="{{ URL('resources/images/logo-kartikatama.png') }}" >
			<div class="snippet" data-title=".dot-typing">
				<div class="stage">
					<div class="dot-typing"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- =======================
	header Start-->
	<header class="header-static navbar-sticky navbar-light">
		<!-- Logo Nav Start -->
		<nav class="navbar navbar-expand-lg">
			<div class="container justify-content-between d-lg-flex py-3">
				<!-- Logo -->
				<a class="navbar-brand" href="index.html">
					<img src="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}">
				</a>
				<!-- Menu opener button -->
				<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"> </span>
			  </button>
				<!-- Main Menu Start -->
				<div class="collapse navbar-collapse" id="navbarCollapse" style="margin-left: 13%;">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown active">
							<a class="nav-link " href="{{ URL('home') }}">Halaman Utama</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="pagesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tentang</a>
							<ul class="dropdown-menu" aria-labelledby="pagesMenu">
								<li><a class="dropdown-item" href="{{ URL('about/profile') }}">Profil</a></li>
								<li><a class="dropdown-item" href="{{ URL('about/facility') }}">Fasilitas</a></li>
							</ul>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="pagesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akademis</a>
							<ul class="dropdown-menu" aria-labelledby="pagesMenu">
								<li><a class="dropdown-item" href="{{ URL('academic/major') }}">Jurusan</a></li>
								<li><a class="dropdown-item" href="{{ URL('academic/extracurricular') }}">Ekstrakurikuler</a></li>
							</ul>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link " href="{{ URL('blog/news') }}">Berita Sekolah</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link " href="{{ URL('contacts-us') }}">Hubungi</a>
						</li>

					</ul>
				</div>
				<!-- Main Menu End -->
				<!-- Header Extras Start-->
				<div class="navbar-nav">
					{{-- <div class="nav-item border-0 d-none d-lg-inline-block align-self-center">
						<a href="#" class=" btn btn-sm btn-white text-white mb-0 mr-3">Login</a>
					</div> --}}
					<div class="nav-item border-0 d-none d-lg-inline-block align-self-center">
						<a href="{{ URL('registration/new-student') }}" class=" btn btn-sm btn-grad text-white mb-0 px-4">Pendaftaran</a>
					</div>
				</div>
				<!-- Header Extras End-->
			</div>
		</nav>
		<!-- Logo Nav End -->
	</header>
	<!-- =======================
	header End-->

	@yield('content')

	<!-- =======================
	footer  -->
	<footer class="footer bg-light pt-6">
		<div class="footer-content pb-3">
			<div class="container">
				<div class="row">
					<!-- Footer widget 1 -->
					<div class="col-md-4">
						<div class="widget">
							<a href="index.html" class="footer-logo">
								<img src="{{ URL(\App\Helpers\GlobalHelper::instance()->getLogo()) }}" style="width: 125px; height: auto;">
							</a>
							<h4 class="mt-4">
								{{ (isset($data['school_name']) ? $data['school_name']['description'] : 'SMK Kartikatama Metro') }}
							</h4>
							<ul class="social-icons light si-colored-bg-on-hover no-pb mt-4">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="{{ (isset($data['facebook']) ? $data['facebook']['description'] : '#') }}"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="{{ (isset($data['instagram']) ? $data['instagram']['description'] : '#') }}"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="{{ (isset($data['twitter']) ? $data['twitter']['description'] : '#') }}"><i class="fa fa-twitter"></i></a></li>
								<li class="social-icons-item social-youtube"><a class="social-icons-link" href="{{ (isset($data['youtube']) ? $data['youtube']['description'] : '#') }}"><i class="fa fa-youtube"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-6">
						<div class="widget">
							<h6>Menu</h6>
							<ul class="nav flex-column primary-hover">
								<li class="nav-item"><a class="nav-link" href="{{ URL('home') }}">Home</a></li>
								<li class="nav-item"><a class="nav-link" href="{{ URL('blog/news') }}">Berita Sekolah</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-6">
						<div class="widget">
							<h6>Tentang</h6>
							<ul class="nav flex-column primary-hover">
								<li class="nav-item"><a class="nav-link" href="{{ URL('about/profile') }}">Profil</a></li>
								<li class="nav-item"><a class="nav-link" href="">Fasilitas</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-6">
						<div class="widget">
							<h6>Akademis</h6>
							<ul class="nav flex-column primary-hover">
								<li class="nav-item"><a class="nav-link" href="{{ URL('academic/major') }}">Jurusan</a></li>
								<li class="nav-item"><a class="nav-link" href="{{ URL('academic/extracurricular') }}">Ekstrakurikuler</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-6">
						<div class="widget">
							<h6>Lain-Lain</h6>
							<ul class="nav flex-column primary-hover">
								<li class="nav-item"><a class="nav-link" href="{{ URL('registration/new-student') }}">Pendaftaran (PPDB)</a></li>
								<li class="nav-item"><a class="nav-link" href="{{ URL('contacts-us') }}">Hubungi Kami</a></li>
							</ul>
						</div>
					</div>

				</div>
				<!-- Footer widget 4 -->
			</div>
		</div>
		<div class="divider mt-3"></div>
		<!--footer copyright -->
		<div class="footer-copyright py-3">
			<div class="container">
				<div class=" py-3 text-center ">
					<!-- copyright text -->
					<div class="copyright-text">©2021 All Rights Reserved || Created with ☕ by <a href="https://www.instagram.com/rendifebrian15">RENDI FEBRIAN</a></div>
					<!-- copyright links-->
				</div>
			</div>
		</div>
	</footer>
	<!-- =======================
	footer  -->

	<div> <a href="#" class="back-top btn btn-grad"><i class="ti-angle-up"></i></a> </div>

	<!--Global JS-->
	<script src="{{ URL('resources/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/popper.js/umd/popper.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

	<!--Vendors-->
	<script src="{{ URL('resources/vendor/fancybox/js/jquery.fancybox.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/owlcarousel/js/owl.carousel.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/swiper/js/swiper.js') }}"></script>
    <script src="{{ URL('resources/vendor/wow/wow.min.js') }}"></script>

    <script src="{{ URL('resources/vendor/fancybox/js/jquery.fancybox.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ URL('resources/vendor/isotope/isotope.pkgd.min.js') }}"></script>

	<!--Template Functions-->
	<script src="{{ URL('resources/js/functions.js') }}"></script>

</body>

</html>
