﻿# Host: localhost  (Version 5.5.5-10.4.18-MariaDB)
# Date: 2021-04-18 00:51:48
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "about_us"
#

DROP TABLE IF EXISTS `about_us`;
CREATE TABLE `about_us` (
  `about_us_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `about_us_name` varchar(255) DEFAULT NULL,
  `about_us_label` varchar(255) DEFAULT NULL,
  `about_us_description` text DEFAULT NULL,
  `about_us_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`about_us_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

#
# Data for table "about_us"
#

INSERT INTO `about_us` VALUES (1,'our_vision','Visi Kami','TERWUJUDNYA PESERTA DIDIK YANG UNGGUL DALAM PRESTASI, INOVATIF, TERAMPIL, MANDIRI, BERBUDI LUHUR, DAN BERWAWASAN LINGKUNGAN','longtext','2021-01-27 20:25:30','2021-02-06 19:23:15',0),(2,'our_mision','Misi SMK Kartikatama','1. misi kesatu\r\n2. misi kedua\r\n3. misi ketiga','longtext','2021-01-27 20:25:50','2021-03-16 21:04:06',0),(3,'location','Alamat','Jl. Kapten Tendean No.25, Margorejo, Metro Sel., Kota Metro, Lampung 34125','text','2021-01-27 20:28:08','2021-01-27 20:28:08',0),(4,'phone','Telepon','082371820023','text','2021-01-27 20:29:07','2021-01-27 20:29:07',0),(5,'email','Email','rendifebrian441@gmail.com','text','2021-01-27 20:29:26','2021-01-27 20:29:26',0),(6,'instagram','Instagram','https://www.instagram.com/rendifebrian15','text','2021-01-27 20:30:50','2021-01-27 20:30:50',0),(7,'school_name','Nama Sekolah','SMK KARTIKATAMA METRO','text','2021-01-27 20:30:57','2021-01-27 20:30:57',0),(8,'about_school','Tentang Sekolah','<p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\"><img data-filename=\"125px-Flag_of_Indonesia.svg.webp\" style=\"width: 125px; float: left;\" src=\"http://localhost/AdminKu/apps/public//uploads/6026560a1d5f7.webp\" class=\"note-float-left\"><img style=\"width: 25%; float: right;\" data-filename=\"3b8ad2c7b1be2caf24321c852103598a.jpg\" class=\"note-float-right\" src=\"http://localhost/AdminKu/apps/public//uploads/602674aa7f74d.jpeg\"><b><br></b><p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\"><b><br></b></p><p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\"><b>Indonesia</b>&nbsp;(<span class=\"rt-commentedText nowrap\" style=\"white-space: nowrap;\"><span class=\"IPA nopopups noexcerpt\"><a href=\"https://en.wikipedia.org/wiki/Help:IPA/English\" title=\"Help:IPA/English\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">/<span style=\"border-bottom: 1px dotted;\"><span title=\"/&Atilde;&#139;&Aring;&#146;/: secondary stress follows\">&Atilde;&#139;&Aring;&#146;</span><span title=\"/&Atilde;&#137;&Acirc;&ordf;/: \'i\' in \'kit\'\">&Atilde;&#137;&Acirc;&ordf;</span><span title=\"\'n\' in \'nigh\'\">n</span><span title=\"\'d\' in \'dye\'\">d</span><span title=\"/&Atilde;&#137;&acirc;&#132;&cent;/: \'a\' in \'about\'\">&Atilde;&#137;&acirc;&#132;&cent;</span><span title=\"/&Atilde;&#139;&Euml;&#134;/: primary stress follows\">&Atilde;&#139;&Euml;&#134;</span><span title=\"\'n\' in \'nigh\'\">n</span><span title=\"/i&Atilde;&#139;&Acirc;&#144;/: \'ee\' in \'fleece\'\">i&Atilde;&#139;&Acirc;&#144;</span><span title=\"/&Atilde;&#138;&acirc;&#128;&#153;/: \'s\' in \'pleasure\'\">&Atilde;&#138;&acirc;&#128;&#153;</span><span title=\"/&Atilde;&#137;&acirc;&#132;&cent;/: \'a\' in \'about\'\">&Atilde;&#137;&acirc;&#132;&cent;</span></span>/</a></span>&nbsp;<span class=\"nowrap\" style=\"font-size: 11.9px;\">(<span class=\"unicode haudio\"><span class=\"fn\"><span style=\"margin-right: 0.25em;\"><a href=\"https://en.wikipedia.org/wiki/File:En-us-Indonesia.ogg\" title=\"About this sound\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><img alt=\"About this sound\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/11px-Loudspeaker.svg.png\" decoding=\"async\" width=\"11\" height=\"11\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/17px-Loudspeaker.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/22px-Loudspeaker.svg.png 2x\" data-file-width=\"20\" data-file-height=\"20\" style=\"border: 0px; margin: 0px;\"></a></span><a href=\"https://upload.wikimedia.org/wikipedia/commons/9/93/En-us-Indonesia.ogg\" class=\"internal\" title=\"En-us-Indonesia.ogg\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">listen</a></span></span>)</span></span>&nbsp;<a href=\"https://en.wikipedia.org/wiki/Help:Pronunciation_respelling_key\" title=\"Help:Pronunciation respelling key\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><i title=\"English pronunciation respelling\"><span style=\"font-size: 12.6px;\">IN</span>-d&Atilde;&#137;&acirc;&#132;&cent;-<span style=\"font-size: 12.6px;\">NEE</span>-zh&Atilde;&#137;&acirc;&#132;&cent;</i></a>), officially the&nbsp;<b>Republic of Indonesia</b>&nbsp;(<a href=\"https://en.wikipedia.org/wiki/Indonesian_language\" title=\"Indonesian language\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Indonesian</a>:&nbsp;<i lang=\"id\">Republik Indonesia</i>&nbsp;<small style=\"font-size: 11.9px;\"></small><span title=\"Representation in the International Phonetic Alphabet (IPA)\" class=\"IPA\"><a href=\"https://en.wikipedia.org/wiki/Help:IPA/Malay\" title=\"Help:IPA/Malay\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">[re&Atilde;&#139;&Euml;&#134;publik &Atilde;&#137;&Acirc;&ordf;ndo&Atilde;&#139;&Euml;&#134;nesia]</a></span>&nbsp;<span class=\"nowrap\" style=\"white-space: nowrap; font-size: 11.9px;\">(<span class=\"unicode haudio\"><span class=\"fn\"><span style=\"margin-right: 0.25em;\"><a href=\"https://en.wikipedia.org/wiki/File:Id-Indonesia.ogg\" title=\"About this sound\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><img alt=\"About this sound\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/11px-Loudspeaker.svg.png\" decoding=\"async\" width=\"11\" height=\"11\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/17px-Loudspeaker.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Loudspeaker.svg/22px-Loudspeaker.svg.png 2x\" data-file-width=\"20\" data-file-height=\"20\" style=\"border: 0px; margin: 0px;\"></a></span><a href=\"https://upload.wikimedia.org/wikipedia/commons/b/b8/Id-Indonesia.ogg\" class=\"internal\" title=\"Id-Indonesia.ogg\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">listen</a></span></span>)</span>),<sup id=\"cite_ref-efn0_13-0\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px;\"><a href=\"https://en.wikipedia.org/wiki/Indonesia#cite_note-efn0-13\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">[a]</a></sup>&nbsp;is a country in&nbsp;<a href=\"https://en.wikipedia.org/wiki/Southeast_Asia\" title=\"Southeast Asia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Southeast Asia</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Oceania\" title=\"Oceania\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Oceania</a>, between the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Indian_Ocean\" title=\"Indian Ocean\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Indian</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Pacific_Ocean\" title=\"Pacific Ocean\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Pacific</a>&nbsp;oceans. It consists of more than&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_islands_of_Indonesia\" title=\"List of islands of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">seventeen thousand islands</a>, including&nbsp;<a href=\"https://en.wikipedia.org/wiki/Sumatra\" title=\"Sumatra\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Sumatra</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Java\" title=\"Java\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Java</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Sulawesi\" title=\"Sulawesi\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Sulawesi</a>, and parts of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Borneo\" title=\"Borneo\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Borneo</a>&nbsp;(<a href=\"https://en.wikipedia.org/wiki/Kalimantan\" title=\"Kalimantan\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Kalimantan</a>) and&nbsp;<a href=\"https://en.wikipedia.org/wiki/New_Guinea\" title=\"New Guinea\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">New Guinea</a>&nbsp;(<a href=\"https://en.wikipedia.org/wiki/Western_New_Guinea\" title=\"Western New Guinea\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Papua</a>). Indonesia is the world\'s largest&nbsp;<a href=\"https://en.wikipedia.org/wiki/Island_country\" title=\"Island country\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">island country</a>&nbsp;and the&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_area\" title=\"List of countries and dependencies by area\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">14th-largest country by land area</a>, at 1,904,569 square kilometres (735,358&nbsp;<a href=\"https://en.wikipedia.org/wiki/Square_mile\" title=\"Square mile\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">square miles</a>). With more than 270.2 million people,<sup id=\"cite_ref-2020census_9-1\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px;\"><a href=\"https://en.wikipedia.org/wiki/Indonesia#cite_note-2020census-9\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">[9]</a></sup>&nbsp;Indonesia is the world\'s&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population\" title=\"List of countries and dependencies by population\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">fourth-most populous country</a>&nbsp;as well as the most populous&nbsp;<a href=\"https://en.wikipedia.org/wiki/Islam_by_country\" title=\"Islam by country\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Muslim-majority country</a>. Java, the world\'s&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_islands_by_population\" title=\"List of islands by population\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">most populous island</a>, is home to more than half of the country\'s population.</p><p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">The&nbsp;<a href=\"https://en.wikipedia.org/wiki/Sovereign_state\" title=\"Sovereign state\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">sovereign state</a>&nbsp;is a&nbsp;<a href=\"https://en.wikipedia.org/wiki/President_of_Indonesia\" title=\"President of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">presidential</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Republic\" title=\"Republic\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">constitutional republic</a>&nbsp;with an elected&nbsp;<a href=\"https://en.wikipedia.org/wiki/People%27s_Consultative_Assembly\" title=\"People\'s Consultative Assembly\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">legislature</a>. It has&nbsp;<a href=\"https://en.wikipedia.org/wiki/Provinces_of_Indonesia\" title=\"Provinces of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">34 provinces</a>, of which five have&nbsp;<a href=\"https://en.wikipedia.org/wiki/Autonomous_administrative_division\" title=\"Autonomous administrative division\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">special status</a>. The country\'s capital,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Jakarta\" title=\"Jakarta\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Jakarta</a>, is the&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_largest_cities\" title=\"List of largest cities\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">second-most populous urban area in the world</a>. The country shares&nbsp;<a href=\"https://en.wikipedia.org/wiki/Template:Borders_of_Indonesia\" title=\"Template:Borders of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">land borders</a>&nbsp;with&nbsp;<a href=\"https://en.wikipedia.org/wiki/Papua_New_Guinea\" title=\"Papua New Guinea\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Papua New Guinea</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/East_Timor\" title=\"East Timor\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">East Timor</a>, and the&nbsp;<a href=\"https://en.wikipedia.org/wiki/East_Malaysia\" title=\"East Malaysia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">eastern part</a>&nbsp;of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Malaysia\" title=\"Malaysia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Malaysia</a>. Other neighbouring countries include&nbsp;<a href=\"https://en.wikipedia.org/wiki/Singapore\" title=\"Singapore\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Singapore</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Vietnam\" title=\"Vietnam\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Vietnam</a>, the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Philippines\" title=\"Philippines\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Philippines</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Australia\" title=\"Australia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Australia</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Palau\" title=\"Palau\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Palau</a>, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/India\" title=\"India\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">India</a>\'s&nbsp;<a href=\"https://en.wikipedia.org/wiki/Andaman_and_Nicobar_Islands\" title=\"Andaman and Nicobar Islands\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Andaman and Nicobar Islands</a>. Despite its large population and densely populated regions, Indonesia has vast areas of wilderness that support one of the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Megadiverse_countries\" title=\"Megadiverse countries\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">world\'s highest levels of biodiversity</a>.</p><p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">The Indonesian archipelago has been a valuable region for trade since at least the 7th century when&nbsp;<a href=\"https://en.wikipedia.org/wiki/Srivijaya\" title=\"Srivijaya\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Srivijaya</a>&nbsp;and later&nbsp;<a href=\"https://en.wikipedia.org/wiki/Majapahit\" title=\"Majapahit\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Majapahit</a>&nbsp;traded with entities from&nbsp;<a href=\"https://en.wikipedia.org/wiki/Mainland_China\" title=\"Mainland China\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">mainland China</a>&nbsp;and the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Indian_subcontinent\" title=\"Indian subcontinent\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Indian subcontinent</a>. Local rulers gradually absorbed foreign influences from the early centuries and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Hinduism_in_Indonesia\" title=\"Hinduism in Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Hindu</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Buddhism_in_Indonesia\" title=\"Buddhism in Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Buddhist</a>&nbsp;kingdoms flourished.&nbsp;<a href=\"https://en.wikipedia.org/wiki/Sunni_Islam\" title=\"Sunni Islam\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Sunni</a>&nbsp;traders and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Sufism\" title=\"Sufism\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Sufi</a>&nbsp;scholars brought&nbsp;<a href=\"https://en.wikipedia.org/wiki/Islam_in_Indonesia\" title=\"Islam in Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Islam</a>, while Europeans introduced&nbsp;<a href=\"https://en.wikipedia.org/wiki/Christianity_in_Indonesia\" title=\"Christianity in Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Christianity</a>&nbsp;through&nbsp;<a href=\"https://en.wikipedia.org/wiki/European_colonisation_of_Southeast_Asia\" title=\"European colonisation of Southeast Asia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">colonisation</a>. Although sometimes interrupted by the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Portuguese_colonialism_in_the_East_Indies\" title=\"Portuguese colonialism in the East Indies\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Portuguese</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/French_and_British_interregnum_in_the_Dutch_East_Indies\" title=\"French and British interregnum in the Dutch East Indies\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">French and British</a>, the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Dutch_East_Indies#History\" title=\"Dutch East Indies\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Dutch</a>&nbsp;were the foremost&nbsp;<a href=\"https://en.wikipedia.org/wiki/Dutch_Empire\" title=\"Dutch Empire\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">colonial power</a>&nbsp;for much of their 350-year presence in the archipelago. The concept of \"Indonesia\" as a nation-state&nbsp;<a href=\"https://en.wikipedia.org/wiki/Indonesian_National_Awakening\" title=\"Indonesian National Awakening\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">emerged</a>&nbsp;in the early 20th century<sup id=\"cite_ref-14\" class=\"reference\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px;\"><a href=\"https://en.wikipedia.org/wiki/Indonesia#cite_note-14\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">[13]</a></sup>&nbsp;and the country&nbsp;<a href=\"https://en.wikipedia.org/wiki/Proclamation_of_Indonesian_Independence\" title=\"Proclamation of Indonesian Independence\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">proclaimed its independence</a>&nbsp;in 1945. However, it was not until 1949 that the Dutch recognised&nbsp;<a href=\"https://en.wikipedia.org/wiki/United_States_of_Indonesia\" title=\"United States of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Indonesia\'s sovereignty</a>&nbsp;following an&nbsp;<a href=\"https://en.wikipedia.org/wiki/Indonesian_National_Revolution\" title=\"Indonesian National Revolution\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">armed and diplomatic conflict</a>&nbsp;between the two.</p><p style=\"margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">Indonesia consists of hundreds of distinct native&nbsp;<a href=\"https://en.wikipedia.org/wiki/Ethnic_groups_in_Indonesia\" title=\"Ethnic groups in Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">ethnic</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Languages_of_Indonesia\" title=\"Languages of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">linguistic</a>&nbsp;groups, with the largest one being the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Javanese_people\" title=\"Javanese people\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Javanese</a>. A shared identity has developed with the motto&nbsp;<i>\"<a href=\"https://en.wikipedia.org/wiki/Bhinneka_Tunggal_Ika\" title=\"Bhinneka Tunggal Ika\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Bhinneka Tunggal Ika</a>\"</i>&nbsp;(\"Unity in Diversity\"&nbsp;<i>literally</i>, \"many, yet one\"), defined by a national language, ethnic diversity, religious pluralism within a Muslim-majority population, and a history of colonialism and rebellion against it. The&nbsp;<a href=\"https://en.wikipedia.org/wiki/Economy_of_Indonesia\" title=\"Economy of Indonesia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">economy of Indonesia</a>&nbsp;is the world\'s&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)\" title=\"List of countries by GDP (nominal)\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">16th</a>&nbsp;largest by nominal GDP and&nbsp;<a href=\"https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(PPP)\" title=\"List of countries by GDP (PPP)\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">7th</a>&nbsp;by GDP at&nbsp;<a href=\"https://en.wikipedia.org/wiki/Purchasing_power_parity\" title=\"Purchasing power parity\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PPP</a>. Indonesia is the only&nbsp;<a href=\"https://en.wikipedia.org/wiki/Regional_power\" title=\"Regional power\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">regional power</a>&nbsp;in&nbsp;<a href=\"https://en.wikipedia.org/wiki/Southeast_Asia\" title=\"Southeast Asia\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Southeast Asia</a>&nbsp;and is considered a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Middle_power\" title=\"Middle power\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">middle power</a>&nbsp;in global affairs. The country is a member of several multilateral organisations, including the&nbsp;<a href=\"https://en.wikipedia.org/wiki/United_Nations\" title=\"United Nations\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">United Nations</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/World_Trade_Organization\" title=\"World Trade Organization\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">World Trade Organization</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/G20\" title=\"G20\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">G20</a>, and a founding member of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Non-Aligned_Movement\" title=\"Non-Aligned Movement\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Non-Aligned Movement</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/ASEAN\" title=\"ASEAN\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Association of Southeast Asian Nations</a>&nbsp;(ASEAN),&nbsp;<a href=\"https://en.wikipedia.org/wiki/East_Asia_Summit\" title=\"East Asia Summit\" style=\"color: rgb(6, 69, 173); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">East Asia Summit</a>, and the</p><p></p></p>\n','textarea','2021-01-27 20:34:05','2021-02-12 12:29:31',0),(9,'facebook','Facebook','https://www.facebook.com/rendifebrian15','text','2021-01-27 21:16:10','2021-01-27 21:16:10',0),(10,'twitter','Twitter','https://www.twitter.com/rendifebrian__','text','2021-01-27 21:18:23','2021-01-27 21:18:23',0),(11,'banner_text','Banner Text','SMK Kartikatama 1 metro','text','2021-01-29 21:29:44','2021-03-16 20:57:59',0),(12,'banner_desc','Banner Description','gelombang 1\ngelombang 2\ngelombang 3','longtext','2021-01-29 21:31:51','2021-01-29 21:31:51',0),(13,'banner_img','Banner Image','uploads/SMK_UPLOAD-SMK_UPLOAD-DSCN0793.JPG','s_file','2021-01-29 21:33:49','2021-04-15 02:03:57',0),(14,'vm_img','Gambar Visi Misi','uploads/SMK_UPLOAD-SMK_UPLOAD-DSC_3036.JPG','s_file','2021-01-29 21:40:40','2021-04-15 05:09:26',0),(15,'school_logo','Logo Sekolah','uploads/SMK_UPLOAD-logo-kartikatama.png','s_file','2021-01-29 22:55:45','2021-02-19 05:42:59',0),(17,'new_registration_year','Tahun Ajaran Penerimaan Siswa Baru','2022','text','2021-02-01 00:26:48','2021-02-06 18:24:14',0);

#
# Structure for table "admin"
#

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_uuid` varchar(255) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`admin_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

#
# Data for table "admin"
#

INSERT INTO `admin` VALUES (1,'c7e01618-d72b-4d75-a7dc-1b5b60d5880b','Paijo Superman',1,'2021-03-21 23:32:42','2021-03-21 23:32:42',1),(2,'d4e427ad-e753-4b56-92b4-b9c5811591ac','Lyla Naga',1,'2021-03-21 23:35:29','2021-03-21 23:35:29',1),(3,'b7807516-e43f-4d30-b5e3-53573304bef2','Andra And The Back Bone',1,'2021-03-21 23:39:17','2021-03-21 23:39:17',1),(4,'95254394-f718-4c5e-bce5-f9d30d7d7660','Mas Kopet',1,'2021-03-21 23:41:50','2021-03-21 23:43:50',1),(5,'8124fed5-1f68-4d07-9623-e87e9badf9fe','Mas Toto',1,'2021-03-21 23:42:13','2021-03-21 23:43:50',1),(6,'e7e0a51c-758e-4ade-8b30-021050ca9b15','Mamang Nasgor',1,'2021-03-21 23:57:35','2021-03-22 00:07:47',1),(7,'216330ba-137f-49c3-af7f-7e4be32ecbc7','Mamang Nasgor',1,'2021-03-22 00:04:45','2021-03-22 00:07:47',1),(8,'840d3f4e-9f7a-4244-b4f5-5cf7b8cc4f3a','Mamang Nasgor',1,'2021-03-22 00:06:39','2021-03-22 00:07:47',1),(9,'eb24f5c2-458d-4da6-8205-087779cf889c','Mamang Nasgor',1,'2021-03-22 00:08:08','2021-03-22 00:10:01',1),(10,'57f3c5f1-3b97-4eb5-a1c3-ce572b9342cb','Mamang Nasgor',1,'2021-03-22 00:08:47','2021-03-22 00:10:01',1),(11,'5c0531d3-1eab-4b0a-9382-8fc26cd497bd','Mamang Nasgor',1,'2021-03-22 00:09:51','2021-03-22 00:10:01',1),(12,'e7f11361-1ea9-46f8-8bb1-b244c974eecc','Mamang Cilok',1,'2021-03-22 00:09:52','2021-03-22 00:10:01',1),(13,'479b439b-e013-47b3-aa69-c73a19c82414','Mamang Nasgor',1,'2021-03-22 00:10:37','2021-03-22 00:10:55',1),(14,'eeb95869-1d0f-4f6a-88b4-56278b5f5e30','Mamang Cilok',1,'2021-03-22 00:10:38','2021-03-22 00:10:55',1);

#
# Structure for table "admin_fields"
#

DROP TABLE IF EXISTS `admin_fields`;
CREATE TABLE `admin_fields` (
  `admin_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_fields_name` varchar(255) DEFAULT NULL,
  `admin_fields_label` varchar(255) DEFAULT NULL,
  `admin_fields_input_type` varchar(255) DEFAULT NULL,
  `admin_fields_options` varchar(255) DEFAULT NULL,
  `admin_fields_validation` varchar(255) DEFAULT NULL,
  `admin_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `admin_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`admin_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "admin_fields"
#

INSERT INTO `admin_fields` VALUES (1,'admin_name','Nama','text',NULL,'required',1,1),(2,'admin_photo','Photo','s_file',NULL,'required',2,1);

#
# Structure for table "berita_sekolah"
#

DROP TABLE IF EXISTS `berita_sekolah`;
CREATE TABLE `berita_sekolah` (
  `berita_sekolah_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `berita_sekolah_uuid` varchar(255) DEFAULT NULL,
  `berita_sekolah_thumbnail` varchar(255) DEFAULT NULL,
  `berita_sekolah_title` varchar(255) DEFAULT NULL,
  `berita_sekolah_description` text DEFAULT NULL,
  `berita_sekolah_total_seen` bigint(11) NOT NULL DEFAULT 0,
  `created_by` bigint(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  `publish` int(11) DEFAULT 0,
  PRIMARY KEY (`berita_sekolah_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

#
# Data for table "berita_sekolah"
#

INSERT INTO `berita_sekolah` VALUES (1,'6a931b36-1785-44e7-b6d4-e314b69c3c6a','uploads/SMK_UPLOAD-background_merah_1.jpg','Luciana Fitri Diawan','<p style=\"text-align: center; \">    \r\n  <img data-filename=\"Untitled-1.jpg\" style=\"width: 50%;\" src=\"http://localhost/AdminKu/apps/public//uploads/60264e90775a1.jpeg\"><p style=\"text-align: center; \"><br></p><p style=\"text-align: center; \">PACAR KU TERSAYANG</p></p>\n',4,1,'2021-02-12 16:46:56','2021-02-12 16:46:56',1,1),(2,'e916d1d2-0a71-42a2-aa7f-9dcb9ec02d45','uploads/SMK_UPLOAD-SMK_UPLOAD-accountant.png','Akuntasi','<p>Ini dia jurusan akuntansi kartikkkkkkkkkkkkkkkkkka<p><br></p><p><img data-filename=\"accountant.png\" style=\"width: 50%;\" src=\"http://localhost/AdminKu/apps/public//uploads/60264f66ba98a.png\"></p><p><br></p><p>Tapi ya begini adanya</p></p>\n',12,1,'2021-02-12 16:50:30','2021-02-15 01:50:52',0,1),(3,'a942fde2-2f3e-45c2-a866-06e68ad4e720','uploads/SMK_UPLOAD-3b8ad2c7b1be2caf24321c852103598a.jpg','Lorem ipsum','<p>adsdas&nbsp;<p><br></p><p><img data-filename=\"2008chevroletcaptiva-778b.jpg\" style=\"width: 25%;\" src=\"http://localhost/AdminKu/apps/public//uploads/602670d5c8c5e.jpeg\"><br>zxczxcn</p><p><br></p><p>zjxcbzjkxhc</p></p>\n',0,1,'2021-02-12 19:13:09','2021-02-22 06:07:50',0,1),(4,'ffce3898-9b8a-4871-b270-6813d86a2859','uploads/SMK_UPLOAD-download.jpg','Bajing','<p>asdasd</p>\n',0,1,'2021-02-22 13:05:01','2021-02-22 13:05:01',0,1),(5,'d1faafb9-eea4-40a9-ba3b-e20420a13220','uploads/SMK_UPLOAD-20180312-133927.jpg','Kantor Baru SMK Kartikatama','<p>asdasd</p>\n',0,1,'2021-02-22 13:08:06','2021-02-22 13:08:06',0,1);

#
# Structure for table "berita_sekolah_fields"
#

DROP TABLE IF EXISTS `berita_sekolah_fields`;
CREATE TABLE `berita_sekolah_fields` (
  `berita_sekolah_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `berita_sekolah_fields_name` varchar(255) DEFAULT NULL,
  `berita_sekolah_fields_label` varchar(255) DEFAULT NULL,
  `berita_sekolah_fields_input_type` varchar(255) DEFAULT NULL,
  `berita_sekolah_fields_options` varchar(255) DEFAULT NULL,
  `berita_sekolah_fields_validation` varchar(255) DEFAULT NULL,
  `berita_sekolah_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `berita_sekolah_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`berita_sekolah_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

#
# Data for table "berita_sekolah_fields"
#

INSERT INTO `berita_sekolah_fields` VALUES (1,'berita_sekolah_thumbnail','Thumbnail Berita','s_file',NULL,'required',1,1),(2,'berita_sekolah_title','Title','text',NULL,'required',2,1),(3,'berita_sekolah_description','Isi Berita','textarea',NULL,'required',3,1);

#
# Structure for table "dropdown"
#

DROP TABLE IF EXISTS `dropdown`;
CREATE TABLE `dropdown` (
  `dropdown_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `dropdown_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dropdown_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "dropdown"
#

INSERT INTO `dropdown` VALUES (1,'gender'),(2,'jenis_pendaftar'),(3,'religion');

#
# Structure for table "dropdown_options"
#

DROP TABLE IF EXISTS `dropdown_options`;
CREATE TABLE `dropdown_options` (
  `dropdown_options_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `dropdown_serial_id` bigint(1) DEFAULT 0,
  `dropdown_options_value` varchar(255) DEFAULT NULL,
  `dropdown_options_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dropdown_options_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

#
# Data for table "dropdown_options"
#

INSERT INTO `dropdown_options` VALUES (1,1,'l','L'),(2,1,'p','P'),(5,2,'siswa baru','Siswa Baru'),(6,2,'pindahan','Pindahan'),(7,3,'islam','Islam'),(8,3,'budha','Budha'),(9,3,'hindu','Hindu'),(10,3,'katholik','Katholik'),(11,3,'kristen_protestan','Kristen/Protestan'),(12,3,'khong hu chu','Khong Hu Chu'),(13,3,'other','Lainnya');

#
# Structure for table "elearning"
#

DROP TABLE IF EXISTS `elearning`;
CREATE TABLE `elearning` (
  `elearning_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `elearning_uuid` varchar(255) DEFAULT NULL,
  `elearning_name` varchar(255) DEFAULT NULL,
  `elearning_file` varchar(255) DEFAULT NULL,
  `elearning_kelas_id` bigint(10) DEFAULT NULL,
  `elearning_jurusan_id` bigint(10) DEFAULT NULL,
  `created_by` bigint(10) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`elearning_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

#
# Data for table "elearning"
#

INSERT INTO `elearning` VALUES (1,'a41505f5-77bf-45c2-9d1d-d482ac1a1679','Formulir','http://localhost/adminv2/apps/public/uploads/606f4df89a38c.pdf',NULL,NULL,1,'2021-04-09 01:39:52','2021-04-09 01:39:52',1),(2,'2792c7ce-1529-4d74-ba6b-9eaf80346aeb','CIMB','http://localhost/adminv2/apps/public/uploads/606f5fc4e6d88.PDF',3,4,1,'2021-04-09 02:55:48','2021-04-09 02:55:48',1),(3,'f43e8ec1-3e7d-441d-82ab-511fa9556692','Matematika kelas 10','http://localhost/adminv2/apps/public/uploads/607142b0d3690.pdf',1,4,1,'2021-04-10 13:16:16','2021-04-10 13:16:16',0),(4,'262f59cd-63d0-4724-b522-c3867a8e69ef','PEMBUATAN STOP MOTION','http://localhost/adminv2/apps/public/uploads/607159878487d.pdf',3,4,1,'2021-04-10 14:53:43','2021-04-10 14:53:43',0);

#
# Structure for table "extracurricular"
#

DROP TABLE IF EXISTS `extracurricular`;
CREATE TABLE `extracurricular` (
  `extracurricular_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `extracurricular_uuid` varchar(255) DEFAULT NULL,
  `extracurricular_name` varchar(255) DEFAULT NULL,
  `extracurricular_desc` text DEFAULT NULL,
  `extracurricular_photo` varchar(255) DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`extracurricular_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "extracurricular"
#

INSERT INTO `extracurricular` VALUES (1,'97b4859e-a2f1-4ea1-89e5-5685d0836872','Karate','Karate adalah seni bela diri yang berasal dari Jepang. Seni bela diri ini sedikit dipengaruhi oleh Seni bela diri Cina kenpō. Karate dibawa masuk ke Jepang lewat Okinawa dan mulai berkembang di Ryukyu Islands. Seni bela diri ini pertama kali disebut \"Tote” yang berarti seperti “Tinju China”. Wikipedia','uploads/SMK_UPLOAD-209--90bc7c3f8e2e90b11b5ee9dc68dd378e.jpg',1,'2021-02-14 14:20:43','2021-02-14 14:20:43',1),(2,'74351a17-e580-4b31-b51b-bdf537adf9c2','Karate','Karate adalah seni bela diri yang berasal dari Jepang. Seni bela diri ini sedikit dipengaruhi oleh Seni bela diri Cina kenpō. Karate dibawa masuk ke Jepang lewat Okinawa dan mulai berkembang di Ryukyu Islands. Seni bela diri ini pertama kali disebut \"Tote” yang berarti seperti “Tinju China”. Wikipedia Karate adalah seni bela diri yang berasal dari Jepang. Seni bela diri ini sedikit dipengaruhi oleh Seni bela diri Cina kenpō. Karate dibawa masuk ke Jepang lewat Okinawa dan mulai berkembang di Ryu','uploads/SMK_UPLOAD-209--90bc7c3f8e2e90b11b5ee9dc68dd378e.jpg',1,'2021-02-14 14:22:53','2021-02-14 07:58:45',0);

#
# Structure for table "extracurricular_fields"
#

DROP TABLE IF EXISTS `extracurricular_fields`;
CREATE TABLE `extracurricular_fields` (
  `extracurricular_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `extracurricular_fields_name` varchar(255) DEFAULT NULL,
  `extracurricular_fields_label` varchar(255) DEFAULT NULL,
  `extracurricular_fields_input_type` varchar(255) DEFAULT NULL,
  `extracurricular_fields_options` varchar(255) DEFAULT NULL,
  `extracurricular_fields_validation` varchar(255) DEFAULT NULL,
  `extracurricular_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `extracurricular_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`extracurricular_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "extracurricular_fields"
#

INSERT INTO `extracurricular_fields` VALUES (1,'extracurricular_name','Nama Extrakulikuler','text',NULL,'required',1,1),(2,'extracurricular_desc','Deskripsi','longtext',NULL,'reqiured',2,1),(3,'extracurricular_photo','Extrakulikuler Photo','s_file',NULL,'required',3,1);

#
# Structure for table "fasilitas"
#

DROP TABLE IF EXISTS `fasilitas`;
CREATE TABLE `fasilitas` (
  `fasilitas_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `fasilitas_uuid` varchar(255) DEFAULT NULL,
  `fasilitas_name` varchar(255) DEFAULT NULL,
  `fasilitas_kategori` varchar(255) DEFAULT NULL,
  `fasilitas_photo` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`fasilitas_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

#
# Data for table "fasilitas"
#

INSERT INTO `fasilitas` VALUES (1,'7d03b502-b091-4e8c-b8a4-741bedbc73c6','Kantor Karyawan','kantor_smk','uploads/SMK_UPLOAD-955100_720.jpg','2021-02-07 23:47:23','2021-02-22 06:09:44',1),(2,'eb545d27-2fc9-40db-b719-adeece5d292a','Mobil Chevy','mobil_dinas','uploads/SMK_UPLOAD-2008chevroletcaptiva-778b.jpg','2021-02-07 23:49:38','2021-02-22 06:09:47',1),(3,'368dca71-ff52-44d7-8006-df015b79dd8d','Kantor TU','kantor_smk','uploads/SMK_UPLOAD-20180312-133927.jpg','2021-02-07 23:53:15','2021-02-22 06:09:44',1),(4,'1629293e-db01-44d2-a2f1-0e5ec3367af7','Bajing','testing_fas_ed','uploads/SMK_UPLOAD-2008chevroletcaptiva-778b.jpg','2021-02-07 23:53:50','2021-02-22 06:09:56',1),(5,'4d57ea0a-37e0-4ef1-90a7-b971c1a683ee','Kantor Karyawan','office','uploads/SMK_UPLOAD-20180312-133927.jpg','2021-02-13 14:50:05','2021-02-22 06:09:51',1),(6,'4948ae6d-32f5-4c87-9b5f-535361da8c0f','Kantor Karyawan','office','uploads/SMK_UPLOAD-955100_720.jpg','2021-02-13 14:50:22','2021-02-22 06:09:51',1),(7,'e1371243-d535-46bb-ad5e-af7fd9846352','Kantor Karyawan','office_smk','uploads/SMK_UPLOAD-955100_720.jpg','2021-02-22 13:10:14','2021-02-22 13:10:14',0);

#
# Structure for table "guru"
#

DROP TABLE IF EXISTS `guru`;
CREATE TABLE `guru` (
  `guru_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `guru_uuid` char(36) DEFAULT NULL,
  `guru_name` varchar(255) DEFAULT NULL,
  `guru_photo` varchar(255) NOT NULL DEFAULT '',
  `guru_dob` date DEFAULT '1111-11-11',
  `guru_nip` varchar(255) DEFAULT NULL,
  `guru_mata_pelajaran_id` bigint(20) DEFAULT 0,
  `guru_kelas_id` bigint(20) DEFAULT 0,
  `guru_gender` tinyint(3) DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`guru_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

#
# Data for table "guru"
#

INSERT INTO `guru` VALUES (1,'asdasd','Luci','uploads/SMK_UPLOAD-background_merah_1.jpg','0000-00-00','123123123123',0,0,2,'2020-11-29 21:44:25','2020-11-29 21:44:25',1),(2,'080aa073-a0d4-422c-8f0e-5eb74e6cc615','Rendi','uploads/SMK_UPLOAD-photography-of-person-walking-on-road-1236701.jpg','1998-02-26','345345345',0,0,1,'2020-11-29 21:48:41','2021-03-19 03:05:48',1),(3,'2ee442b0-2fef-4160-82f1-4e833c20350c','Suparto','uploads/SMK_UPLOAD-close-up-of-fish-over-black-background-325044.jpg','1996-02-23','3242345213',0,1,1,'2020-11-29 22:47:45','2021-03-19 03:05:48',1),(4,'18327daa-f3a8-4777-9182-4404fb8d4015','Rendi Febrian SP.d S.Kom','uploads/SMK_UPLOAD-rendi.png','1998-02-26','123123123123',5,3,1,'2021-03-19 01:35:40','2021-03-19 03:03:13',0),(5,'d6e57444-7db3-4496-9926-fa9dd2d6165b','Sri wahyuni','uploads/SMK_UPLOAD-f16fedc86686146624897737cf4338d8.jpg','1983-12-26','123123123123',4,3,2,'2021-03-24 03:12:18','2021-04-02 14:38:32',0),(6,'70652653-c56a-4d3b-9dbe-c622133f88b7','Suparto','uploads/SMK_UPLOAD-Screen Shot 2021-03-18 at 1.48.12 PM.png','1221-12-12','123123123123',4,5,1,'2021-03-28 03:00:28','2021-03-28 03:00:28',0);

#
# Structure for table "guru_fields"
#

DROP TABLE IF EXISTS `guru_fields`;
CREATE TABLE `guru_fields` (
  `guru_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `guru_fields_name` varchar(255) DEFAULT NULL,
  `guru_fields_label` varchar(255) DEFAULT NULL,
  `guru_fields_input_type` varchar(255) DEFAULT NULL,
  `guru_fields_options` varchar(255) DEFAULT NULL,
  `guru_fields_validation` varchar(255) DEFAULT NULL,
  `guru_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `guru_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`guru_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

#
# Data for table "guru_fields"
#

INSERT INTO `guru_fields` VALUES (1,'guru_name','Nama Guru','text',NULL,'required',1,1),(4,'guru_photo','Photo Guru','s_file',NULL,'required',4,1),(5,'guru_dob','Tanggal Lahir','date',NULL,'required',5,1),(6,'guru_mata_pelajaran_id','Guru Mata Pelajaran','s_autocomplete','mata_pelajaran_id','required',6,1),(7,'guru_gender','Jenis Kelamin','singleoption','gender','required',7,1),(9,'guru_kelas_id','Guru Kelas','singleoption','kelas_id','required',8,1),(10,'guru_nip','NIP','text',NULL,'required',9,1);

#
# Structure for table "hasil_ujian"
#

DROP TABLE IF EXISTS `hasil_ujian`;
CREATE TABLE `hasil_ujian` (
  `hasil_ujian_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `hasil_ujian_uuid` varchar(255) NOT NULL,
  `ujian_serial_id` bigint(10) DEFAULT NULL,
  `hasil_ujian_info` text DEFAULT NULL,
  `hasil_ujian_result` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`hasil_ujian_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

#
# Data for table "hasil_ujian"
#

INSERT INTO `hasil_ujian` VALUES (1,'cd562f45-cc2f-47ea-8d6e-c7d52c93cd1f',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"c\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"d\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"c\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"c\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"d\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"c\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"b\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"d\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"c\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"d\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":\"d\",\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":\"d\",\"dda2ab89-5f1e-44ca-9409-2d3656051670\":\"d\"}','{\"true\":5,\"false\":15,\"none\":0}','2021-04-03 14:56:17','2021-04-03 14:56:17',1),(2,'f91e1f86-cc07-4adb-8624-b661cde5daf2',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"c\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"d\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"c\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"c\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"d\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"c\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"b\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"d\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"c\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"d\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":\"d\",\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":\"d\",\"dda2ab89-5f1e-44ca-9409-2d3656051670\":\"d\"}','{\"true\":5,\"false\":15,\"none\":0}','2021-04-03 17:12:05','2021-04-03 17:12:05',1),(3,'b2f49c57-6776-4334-a706-c3b411ffc987',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"d\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"d\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"c\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"b\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"d\"}','{\"true\":7,\"false\":6,\"none\":0}','2021-04-03 17:16:38','2021-04-03 17:16:38',1),(4,'cca86e09-2212-4320-87e3-11637bfaa9d6',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"d\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"c\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"d\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"c\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"d\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"e\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":null,\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"e\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":7,\"false\":13,\"none\":0}','2021-04-03 17:36:31','2021-04-03 17:36:31',1),(5,'4dec2578-9d16-490b-89b7-52bff70564ce',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"b\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"b\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"d\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"a\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"e\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":11,\"false\":9,\"none\":0}','2021-04-03 17:41:59','2021-04-03 17:41:59',1),(7,'f45872d0-4c5a-42aa-a6ee-8da98fec738d',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"b\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"b\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"d\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"a\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"e\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":11,\"false\":9,\"none\":0}','2021-04-03 17:47:47','2021-04-03 17:47:47',1),(8,'e05ed96d-1d94-4adb-a851-779f014b105f',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"b\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"b\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"d\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"a\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"e\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":11,\"false\":9,\"none\":0}','2021-04-03 17:48:14','2021-04-03 17:48:14',1),(9,'82caf4ab-090c-425e-a840-2160f88accff',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"b\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"b\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"d\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"a\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"e\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":11,\"false\":4,\"none\":5}','2021-04-03 17:49:11','2021-04-03 17:49:11',1),(10,'1d855490-eb52-4a20-80fb-60b18a471a54',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"c\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"e\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"d\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"e\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"e\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"c\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"a\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":null,\"f1946d10-56b1-4506-a105-03e767a62a5b\":null,\"190053d4-5697-489d-b5a2-946c3d5afdef\":null,\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":null,\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":null,\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":3,\"false\":7,\"none\":10}','2021-04-03 18:14:43','2021-04-03 18:14:43',1),(11,'dd43a37a-1bc6-4c57-af09-e2899877b110',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"c\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"c\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"d\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"e\",\"204c8f25-63e2-4285-9e24-582174b51c75\":null,\"f1946d10-56b1-4506-a105-03e767a62a5b\":null,\"190053d4-5697-489d-b5a2-946c3d5afdef\":null,\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":null,\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":null,\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":6,\"false\":4,\"none\":10}','2021-04-03 18:19:37','2021-04-03 18:19:37',1),(12,'8b7704a3-98db-4cf7-a9ac-def679c22590',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"c\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":null,\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"c\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"d\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"e\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"e\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"e\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"e\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"e\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"b\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"b\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"c\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"b\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":\"b\",\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":7,\"false\":10,\"none\":3}','2021-04-03 21:26:27','2021-04-03 21:26:27',1),(13,'134765bb-3e5c-42f3-9c3a-aaa4f1765f31',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"c\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"a\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":null,\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":null,\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":null,\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":null,\"e82098c2-727e-4a4f-bc6d-a631403e8859\":null,\"204c8f25-63e2-4285-9e24-582174b51c75\":null,\"f1946d10-56b1-4506-a105-03e767a62a5b\":null,\"190053d4-5697-489d-b5a2-946c3d5afdef\":null,\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"b\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":null,\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":4,\"false\":2,\"none\":14}','2021-04-03 21:29:16','2021-04-03 21:29:16',1),(14,'d895897b-3486-4cfe-8c22-0044e8982a1c',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"b\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"b\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"b\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":null,\"e82098c2-727e-4a4f-bc6d-a631403e8859\":null,\"204c8f25-63e2-4285-9e24-582174b51c75\":null,\"f1946d10-56b1-4506-a105-03e767a62a5b\":null,\"190053d4-5697-489d-b5a2-946c3d5afdef\":null,\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":null,\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":null,\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":null,\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":8,\"false\":0,\"none\":12}','2021-04-03 21:35:04','2021-04-03 21:35:04',1),(15,'2709a0a3-5e9b-4b8a-a702-d832c64bdd04',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"d\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"e\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"a\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"b\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"b\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"b\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"c\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":\"a\",\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":\"d\",\"dda2ab89-5f1e-44ca-9409-2d3656051670\":\"d\"}','{\"true\":9,\"false\":11,\"none\":0}','2021-04-03 21:39:42','2021-04-03 21:39:42',1),(16,'d77900ae-4c38-4618-a5e2-f06b0ac63c8e',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"b\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"b\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"b\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"c\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"d\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"e\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"a\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"b\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"c\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"b\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":\"b\",\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"c\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":\"a\",\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":\"d\",\"dda2ab89-5f1e-44ca-9409-2d3656051670\":\"d\"}','{\"true\":9,\"false\":11,\"none\":0}','2021-04-03 21:40:02','2021-04-03 21:40:02',1),(17,'7bcab3ec-f289-47ce-8ece-5d7b333336d8',9,'{\"89d20347-b104-4795-8645-dd67b17ea062\":\"a\",\"f1130cb7-7445-4e51-892a-4aa43e271851\":\"d\",\"23129e4c-f5b7-48f1-a817-f20fed70e422\":\"c\",\"4d0a83ae-c4c9-4f5a-b86e-626982bc2c99\":\"b\",\"76899f60-a3e5-4bf4-bac9-017e42d0e21e\":\"b\",\"44da2446-2564-4c3e-bd5d-21abdd64b8c9\":\"c\",\"29fcd00b-d297-449c-b114-3e80d1ec71d7\":\"a\",\"3e4a966e-ba5d-49de-8da4-069d1a7aa76e\":\"d\",\"4d48d5a5-0b3d-4972-90e6-ff75914237e6\":\"c\",\"e82098c2-727e-4a4f-bc6d-a631403e8859\":\"b\",\"204c8f25-63e2-4285-9e24-582174b51c75\":\"a\",\"f1946d10-56b1-4506-a105-03e767a62a5b\":\"b\",\"190053d4-5697-489d-b5a2-946c3d5afdef\":\"c\",\"f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a\":\"b\",\"be546ff6-08ea-450f-b56b-4d3cdb8143ab\":\"b\",\"621d087c-617d-438b-8aaf-5db41a78b92a\":null,\"8679ca0e-b4c1-45fb-bb3b-739dd687e157\":\"b\",\"18293a65-fd12-471f-b252-d7b248e7d40a\":null,\"ad7f005f-4133-4147-9415-5f6d58dd7e7d\":null,\"dda2ab89-5f1e-44ca-9409-2d3656051670\":null}','{\"true\":7,\"false\":9,\"none\":4}','2021-04-03 21:44:16','2021-04-07 04:55:38',0);

#
# Structure for table "jurusan"
#

DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan` (
  `jurusan_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `jurusan_uuid` varchar(255) DEFAULT NULL,
  `jurusan_name` varchar(255) DEFAULT NULL,
  `jurusan_desc` text DEFAULT NULL,
  `jurusan_photo` varchar(255) DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`jurusan_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

#
# Data for table "jurusan"
#

INSERT INTO `jurusan` VALUES (1,'ab1c3652-d40e-47b5-b812-5b1444bd67cd','TKJ','TKJ YAAAA, SIAP\" BONGKAR\" YAAAAAA> SEMANGATTTTT','D:\\XAMPPKU72\\tmp\\phpFB5A.tmp',1,'2021-02-14 15:31:45','2021-02-14 15:31:45',1),(2,'e03740f4-7628-4cba-b425-644ad290d2a9','TKJ','YAAAHHHH TKJ YAA. JANGAN LUPA CUCI TANGAN SETELAH BONGKAR.. BONGKAR','D:\\XAMPPKU72\\tmp\\phpF7E7.tmp',1,'2021-02-14 15:33:55','2021-02-14 15:33:55',1),(3,'e29fcf28-1c1b-4ed8-b8ec-d19b6a096125','TKJ (TEKNIK KOMPUTER JARINGAN)','YAAAHHHH TKJ YAA. JANGAN LUPA CUCI TANGAN SETELAH BONGKAR.. BONGKAR','uploads/SMK_UPLOAD-f16fedc86686146624897737cf4338d8.jpg',1,'2021-02-14 15:34:39','2021-03-14 04:43:36',0),(4,'d5afd602-59b2-458a-9b7a-515c9cd0f6ec','MULTIMEDIA','Multimedia hahahaha','uploads/SMK_UPLOAD-147448.jpg',1,'2021-03-14 04:42:43','2021-03-14 04:42:43',0),(5,'56fc209e-1792-4362-b00c-e45b72e8df13','asd','asd','uploads/SMK_UPLOAD-3b8ad2c7b1be2caf24321c852103598a.jpg',1,'2021-03-16 02:13:25','2021-03-16 02:13:35',1),(6,'7fe64859-bdfe-4c49-9b14-ca29d2b672a6','asd','asd','uploads/SMK_UPLOAD-3b8ad2c7b1be2caf24321c852103598a.jpg',1,'2021-03-16 02:13:25','2021-03-16 02:13:35',1);

#
# Structure for table "jurusan_fields"
#

DROP TABLE IF EXISTS `jurusan_fields`;
CREATE TABLE `jurusan_fields` (
  `jurusan_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `jurusan_fields_name` varchar(255) DEFAULT NULL,
  `jurusan_fields_label` varchar(255) DEFAULT NULL,
  `jurusan_fields_input_type` varchar(255) DEFAULT NULL,
  `jurusan_fields_options` varchar(255) DEFAULT NULL,
  `jurusan_fields_validation` varchar(255) DEFAULT NULL,
  `jurusan_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `jurusan_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`jurusan_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

#
# Data for table "jurusan_fields"
#

INSERT INTO `jurusan_fields` VALUES (1,'jurusan_name','Nama Jurusan','text',NULL,'required',1,1),(2,'jurusan_desc','Deskripsi','longtext',NULL,'required',1,1),(3,'jurusan_photo','Logo Jurusan','s_file',NULL,'required',1,1);

#
# Structure for table "kelas"
#

DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `kelas_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_uuid` varchar(255) DEFAULT NULL,
  `kelas_name` varchar(255) DEFAULT NULL,
  `kelas_sorting` tinyint(3) unsigned DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`kelas_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

#
# Data for table "kelas"
#

INSERT INTO `kelas` VALUES (1,'asdasd','Sepuluh',1,'2020-09-30 23:19:52','2020-09-30 23:19:52',0),(2,'asfdt3rgt','Sebelas',2,'2020-09-30 23:19:52','2020-09-30 23:19:52',0),(3,'asdasdvb','Dua Belas',3,'2020-09-30 23:19:52','2020-09-30 23:19:52',0),(5,NULL,'Tiga Belas',0,'2020-11-21 01:15:14','2020-11-21 01:15:14',0),(6,NULL,'Empat Belas',0,'2020-11-21 01:15:59','2020-11-21 01:15:59',0);

#
# Structure for table "kelas_fields"
#

DROP TABLE IF EXISTS `kelas_fields`;
CREATE TABLE `kelas_fields` (
  `kelas_fields_serial_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kelas_fields_name` varchar(255) DEFAULT NULL,
  `kelas_fields_label` varchar(255) DEFAULT NULL,
  `kelas_fields_input_type` varchar(255) DEFAULT NULL,
  `kelas_fields_options` varchar(255) DEFAULT NULL,
  `kelas_fields_validation` varchar(255) DEFAULT NULL,
  `kelas_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `kelas_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`kelas_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "kelas_fields"
#

INSERT INTO `kelas_fields` VALUES (1,'kelas_name','Nama Kelas','text',NULL,'required',1,1);

#
# Structure for table "mailbox"
#

DROP TABLE IF EXISTS `mailbox`;
CREATE TABLE `mailbox` (
  `mailbox_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `mailbox_uuid` varchar(255) DEFAULT NULL,
  `mailbox_from_name` varchar(255) DEFAULT NULL,
  `mailbox_from_email` varchar(255) DEFAULT NULL,
  `mailbox_subject` varchar(255) DEFAULT NULL,
  `mailbox_message` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`mailbox_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mailbox"
#

INSERT INTO `mailbox` VALUES (1,'003cbda3-e824-4af7-b536-b6ab46ceb775','Rendi Febrian','rendi@mail.com','asd','asd','2021-02-16 14:12:34','2021-02-16 14:12:34',0),(2,'12d355f6-b656-4e59-a3ca-bc037663b1b0','Rendi Febrian','rendi@mail.com','fdd','sdfsdf','2021-02-16 14:12:34','2021-02-16 14:12:34',0),(3,'848d29d6-4498-44f5-bfba-3b548a3ad1ac','Rendi Febrian','rendi@mail.com','asd','asd','2021-02-16 14:12:34','2021-02-16 14:12:34',0),(4,'4e8993a6-b30c-43dc-9651-395480e2bc7a','Rendi Febrian','rendi@mail.com','afdsdg','sdfgsdg','2021-02-16 14:12:34','2021-02-16 14:12:34',0),(5,'4ec8e94a-e63a-440f-8384-49bf1336ddc8','Rendi Febrian','rendi@mail.com','fddxzc','sdsgsdfg','2021-02-16 14:12:34','2021-02-16 14:12:34',0);

#
# Structure for table "mailbox_fields"
#

DROP TABLE IF EXISTS `mailbox_fields`;
CREATE TABLE `mailbox_fields` (
  `mailbox_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `mailbox_fields_name` varchar(255) DEFAULT NULL,
  `mailbox_fields_label` varchar(255) DEFAULT NULL,
  `mailbox_fields_input_type` varchar(255) DEFAULT NULL,
  `mailbox_fields_options` varchar(255) DEFAULT NULL,
  `mailbox_fields_validation` varchar(255) DEFAULT NULL,
  `mailbox_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `mailbox_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`mailbox_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mailbox_fields"
#

INSERT INTO `mailbox_fields` VALUES (1,'mailbox_from_name','Nama','text',NULL,'required',1,1),(2,'mailbox_from_email','Email','text',NULL,'required',2,1),(3,'mailbox_subject','Subjek','text',NULL,'required',3,1),(4,'mailbox_message','Pesan','textarea',NULL,'required',4,1);

#
# Structure for table "mata_pelajaran"
#

DROP TABLE IF EXISTS `mata_pelajaran`;
CREATE TABLE `mata_pelajaran` (
  `mata_pelajaran_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran_uuid` varchar(255) DEFAULT NULL,
  `mata_pelajaran_name` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`mata_pelajaran_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mata_pelajaran"
#

INSERT INTO `mata_pelajaran` VALUES (1,'178af327-ff18-4435-98f8-2196e0373345','Bahasa Indonesia E',1,'2021-03-16 01:58:50','2021-03-16 02:43:08',0),(2,'4db72321-8f0a-458b-87f1-336abe62beab','MTK',1,'2021-03-16 01:59:26','2021-03-16 02:08:06',1),(3,'40364677-dbed-426f-83b0-880c7cf1e18e','MTK ASW',1,'2021-03-16 02:14:13','2021-03-16 02:14:13',1),(4,'efeb3611-792f-433a-8fa3-43be608a0923','Matematika',1,'2021-03-19 01:33:26','2021-03-19 01:33:26',0),(5,'73dc2da8-987a-46fb-aa13-c187fe6e11d2','Teknik Komputer',1,'2021-03-19 01:33:43','2021-03-19 01:33:43',0),(6,'7ae31a83-01f8-4dd5-b3d4-14a5757cc27c','Teknik Mesin',1,'2021-03-19 01:33:55','2021-03-19 01:33:55',0);

#
# Structure for table "mata_pelajaran_fields"
#

DROP TABLE IF EXISTS `mata_pelajaran_fields`;
CREATE TABLE `mata_pelajaran_fields` (
  `mata_pelajaran_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran_fields_name` varchar(255) DEFAULT NULL,
  `mata_pelajaran_fields_label` varchar(255) DEFAULT NULL,
  `mata_pelajaran_fields_input_type` varchar(255) DEFAULT NULL,
  `mata_pelajaran_fields_options` varchar(255) DEFAULT NULL,
  `mata_pelajaran_fields_validation` varchar(255) DEFAULT NULL,
  `mata_pelajaran_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `mata_pelajaran_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`mata_pelajaran_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "mata_pelajaran_fields"
#

INSERT INTO `mata_pelajaran_fields` VALUES (1,'mata_pelajaran_name','Nama Mata Pelajaran','text',NULL,'required',1,1);

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (3,'2020_09_23_162548_siswa',2),(4,'2020_09_21_145049_users',3);

#
# Structure for table "pendaftar"
#

DROP TABLE IF EXISTS `pendaftar`;
CREATE TABLE `pendaftar` (
  `pendaftar_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `pendaftar_uuid` varchar(255) DEFAULT NULL,
  `pendaftar_code` varchar(255) DEFAULT NULL,
  `pendaftar_name` varchar(255) DEFAULT NULL,
  `pendaftar_jenis_id` bigint(10) NOT NULL DEFAULT 0,
  `pendaftar_jurusan_id` bigint(10) DEFAULT NULL,
  `pendaftar_asal_sekolah` varchar(255) DEFAULT NULL,
  `pendaftar_nisn` text DEFAULT NULL,
  `pendaftar_gender` bigint(10) NOT NULL DEFAULT 0,
  `pendaftar_dob` date DEFAULT '9999-12-31',
  `pendaftar_agama` bigint(10) NOT NULL DEFAULT 0,
  `pendaftar_address` varchar(255) DEFAULT NULL,
  `pendaftar_phone` varchar(255) DEFAULT NULL,
  `pendaftar_father_name` varchar(255) DEFAULT NULL,
  `pendaftar_father_salary` varchar(255) DEFAULT NULL,
  `pendaftar_mother_name` varchar(255) DEFAULT NULL,
  `pendaftar_mother_salary` varchar(255) DEFAULT NULL,
  `pendaftar_photo` varchar(255) DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`pendaftar_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4;

#
# Data for table "pendaftar"
#

INSERT INTO `pendaftar` VALUES (11,'bc8a1739-7bdf-4dfb-b87c-627357c4193d','KARTIKA-N11','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',1,'2021-02-01 12:09:17','2021-02-14 16:43:39',1),(12,'bfee3810-3925-4922-a734-47acc2e9427f','KARTIKA-N12','Rendi febrian',5,3,'smpn 1 batanghari','asd',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-2410903927.jpeg',NULL,'2021-02-01 12:11:56','2021-02-01 12:11:56',1),(13,'40064de1-d90f-44db-83ed-17e1910923ea','KARTIKA-N13','Rendi febrian',5,5,'smpn 1 batanghari','123123',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:23:15','2021-02-01 12:23:15',1),(14,'e2351f00-7310-4d9d-a3d2-f10ff3be54f6','KARTIKA-N14','Rendi febrian',5,2,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:31:10','2021-02-01 12:31:10',1),(15,'d8b42f0f-deae-4b68-a600-2bc97a87de37','KARTIKA-N15','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:34:03','2021-02-01 12:34:03',1),(16,'465d6cee-cb99-415e-a63f-39f4de39d377','KARTIKA-N16','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:36:29','2021-02-01 12:36:29',1),(17,'411758e0-4a5b-4acb-a3c2-ec32b810a271','KARTIKA-N17','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:37:40','2021-02-01 12:37:40',1),(18,'f0d089f1-b3c3-49db-9a00-a39d940d2995','KARTIKA-N18','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 12:41:41','2021-02-01 12:41:41',1),(19,'85e96d56-417e-4064-85e4-6aa22c8959ed','KARTIKA-N19','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-147448.jpg',NULL,'2021-02-01 13:10:54','2021-02-01 13:10:54',1),(20,'a0c0abb2-7c7d-451e-8bfe-b155a962e716','KARTIKA-N20','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-01 13:13:17','2021-02-01 13:13:17',1),(21,'c92dd988-5bad-4fca-852a-18b04eb8c1a1','KARTIKA-N21','Rendi febrian',5,2,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:13:16','2021-02-02 19:13:16',1),(22,'1919140d-0af2-455d-a9f1-3b86e2c9ecc5','KARTIKA-N22','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:14:56','2021-02-02 19:14:56',1),(23,'2a66bef1-266e-48ae-a66d-ce57782dc13d','KARTIKA-N23','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:16:29','2021-02-02 19:16:29',1),(24,'77d424a4-4196-450e-a2af-32ed9be19015','KARTIKA-N24','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:20:19','2021-02-02 19:20:19',1),(25,'7d912987-eacd-434c-927e-6225405f8063','KARTIKA-N25','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:22:17','2021-02-02 19:22:17',1),(26,'7dff956c-258d-4629-aaf8-5cfa1f8bd95e','KARTIKA-N26','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:28:27','2021-02-02 19:28:27',1),(27,'ff50727c-2d6a-446f-9bc8-af6ec4579afe','KARTIKA-N27','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:29:49','2021-02-02 19:29:49',1),(28,'ee61f78f-8e03-4330-a6a4-49d46f34f0c5','KARTIKA-N28','Rendi febrian',5,1,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:30:36','2021-02-02 19:30:36',1),(29,'bf263d7f-42a0-4121-98bd-ba13fbb3d9fd','KARTIKA-N29','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:37:38','2021-02-02 19:37:38',1),(30,'5bb41edf-2e30-4db3-b827-4ae7fa1b4c4d','KARTIKA-N30','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:38:49','2021-02-02 19:38:49',1),(31,'1d712130-9735-4427-b23a-c44215cc1007','KARTIKA-N31','Rendi febrian',5,5,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:40:32','2021-02-02 19:40:32',1),(32,'5aa95d63-9d87-4d08-9bcb-4aabcabd9e52','KARTIKA-N32','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:41:25','2021-02-02 19:41:25',1),(33,'29e653f6-482d-43da-8505-27b4ab5a8a57','KARTIKA-N33','Rendi febrian',5,1,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:50:31','2021-02-02 19:50:31',1),(34,'320feaf3-bdb1-45f5-a08c-0a7a6c419458','KARTIKA-N34','Rendi febrian',5,3,'smpn 1 batanghari','12312323',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 19:54:46','2021-02-02 19:54:46',1),(35,'d8b57a8e-260c-485d-94a9-d76309ad8725','KARTIKA-N35','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:01:27','2021-02-02 20:01:27',1),(36,'2766a345-5965-4bf5-85e1-2aa7d2ecd835','KARTIKA-N36','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:02:50','2021-02-02 20:02:50',1),(37,'4854b66b-4373-4415-9e73-a947a7477873','KARTIKA-N37','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:04:06','2021-02-02 20:04:06',1),(38,'d91b8aae-2d6e-4943-8575-5859c0a28284','KARTIKA-N38','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:07:37','2021-02-02 20:07:37',1),(39,'012706b8-e2ed-4d86-a7c7-0c79558336d6','KARTIKA-N39','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:08:38','2021-02-02 20:08:38',1),(40,'4618e7aa-8cc4-409d-b666-030da35776ae','KARTIKA-N40','Rendi febrian',5,1,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:10:48','2021-02-02 20:10:48',1),(41,'bdaa6a43-af21-4b39-a082-7b755e3fdb45','KARTIKA-N41','Rendi febrian',5,1,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:12:19','2021-02-02 20:12:19',1),(42,'21548907-ba48-4e93-b614-eabf95980c4f','KARTIKA-N42','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:14:07','2021-02-02 20:14:07',1),(43,'94d248f1-32ce-441d-9c83-9ea2f006a396','KARTIKA-N43','Rendi febrian',5,3,'asd','213123',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:15:00','2021-02-02 20:15:00',1),(44,'d04d6ea1-96c1-4d9c-aa8a-756fcd6fcb85','KARTIKA-N44','Rendi',5,3,'Kobang','727282',1,'1998-12-26',7,'Nana','71819',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-E348C52F-4C1A-4221-BD18-3CE9D4D17E46.jpeg',NULL,'2021-02-02 20:19:00','2021-02-02 20:19:00',1),(45,'a63a6a34-ac70-471b-8bcb-6b6364371804','KARTIKA-N45','Rendi febrian',5,1,'Demi','8182727',1,'1998-01-17',7,'Merro','828271',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-CC6B4625-2A93-49FB-9919-B77778F0A4E9.jpeg',NULL,'2021-02-02 20:22:54','2021-02-02 20:22:54',1),(46,'3a27dab8-3d71-4ea9-a126-c4ef312af079','KARTIKA-N46','Rendi febrian',5,1,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:24:03','2021-02-02 20:24:03',1),(47,'aefb1726-c6ef-49c2-97ed-1b256ead461d','KARTIKA-N47','Rendi febrian',5,3,'Akak','171717',1,'1222-12-12',7,'123 jajak','7181818',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-7F151481-181A-40AF-84E2-20F563938CDA.jpeg',NULL,'2021-02-02 20:26:16','2021-02-02 20:26:16',1),(48,'8f7c42f9-d08a-44ce-a97a-c10e5ac5a5d0','KARTIKA-N48','Re',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:28:23','2021-02-02 20:28:23',1),(49,'863fb8c5-6cb6-48ed-b0f4-455c2ae32835','KARTIKA-N49','Rendi febrian',5,3,'Jaka','1717181',1,'1222-12-12',7,'Jajak','171717',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-68C177EE-FA16-4223-908D-6F64B37296AB.jpeg',NULL,'2021-02-02 20:29:20','2021-02-02 20:29:20',1),(50,'2fbc8869-f19a-4529-bad8-00b51c531930','KARTIKA-N50','Thanks',5,2,'Thanks','232323',1,'1212-12-12',7,'sdsdds','1212121212',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:35:11','2021-02-02 20:35:11',1),(51,'8fcff440-08ab-4ac3-a1d5-416bc854c425','KARTIKA-N51','Rendi Febrian',5,2,'Smpn 1 batanghari','99987965786',1,'1998-01-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-02 20:40:00','2021-02-02 20:40:00',1),(52,'eb5ddf71-3c7f-4b2c-aa4d-4c815a94dc98','KARTIKA-N52','Paijo Superman',6,3,'Kobang ED','999945632123',2,'1992-02-26',8,'Kobang Jaya','08237182002312',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-download.jpg',1,'2021-02-14 23:02:35','2021-02-14 17:13:26',0),(58,'5e1cd69c-7104-4122-b748-adf16ba846bc','KARTIKA-N58','Rendi febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-02-15 00:19:04','2021-02-15 00:19:04',0),(59,'3f241a8e-fc49-4e6e-9f18-9f8b708cdb42','KARTIKA-N59','Mas jo',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'metro','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-3b8ad2c7b1be2caf24321c852103598a.jpg',NULL,'2021-02-15 00:34:31','2021-02-15 00:34:31',0),(60,'9b912987-417a-4d81-b131-893182a818c7','KARTIKA-N60','Luciana Fitri Diawan',5,3,'smpn 4 gunung sugih','99823456789',2,'1995-02-28',7,'Rengas','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-background_merah_1.jpg',NULL,'2021-02-15 08:17:28','2021-02-15 08:17:28',0),(61,'f00bfdd7-a404-47d1-b50d-91861e2d2e41','KARTIKA-N61','Re',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'Jajak','08237182002312',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-download.jpg',NULL,'2021-02-15 08:27:46','2021-02-15 08:27:46',0),(62,'34b6ac71-31ad-42e5-bcff-359a85774106','KARTIKA-N62','Rendi Febrian',5,3,'smpn 1 batanghari','123123',1,'1998-02-26',7,'Pesona Cinere Residance, Cluster Barcelona Block N36, Rangkapan Jaya Baru, Kec. Pancoranmas, Kota Depok, Jawa Barat 16434','082371820023',NULL,NULL,NULL,NULL,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-03-16 20:20:51','2021-03-16 20:20:51',0),(63,'2ef43ceb-b5a0-4213-91d5-382eaa12d2b3','KARTIKA-N63','Rendi Febrian',5,3,'smpn 1 batanghari','999945632',1,'1998-02-26',7,'Pesona Cinere Residance, Cluster Barcelona Block N36, Rangkapan Jaya Baru, Kec. Pancoranmas, Kota Depok, Jawa Barat 16434','082371820023','Yahmin','1000000','Darojatun','5000000','uploads/SMK_UPLOAD-rendi.png',NULL,'2021-03-16 20:34:20','2021-03-16 20:34:20',0);

#
# Structure for table "pendaftar_fields"
#

DROP TABLE IF EXISTS `pendaftar_fields`;
CREATE TABLE `pendaftar_fields` (
  `pendaftar_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `pendaftar_fields_name` varchar(255) DEFAULT NULL,
  `pendaftar_fields_label` varchar(255) DEFAULT NULL,
  `pendaftar_fields_input_type` varchar(255) DEFAULT NULL,
  `pendaftar_fields_options` varchar(255) DEFAULT NULL,
  `pendaftar_fields_validation` varchar(255) DEFAULT NULL,
  `pendaftar_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `pendaftar_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`pendaftar_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

#
# Data for table "pendaftar_fields"
#

INSERT INTO `pendaftar_fields` VALUES (1,'pendaftar_name','Nama Lengkap','text','required','required',1,1),(2,'pendaftar_jenis_id','Jenis Pendaftaran','singleoption','jenis_pendaftar','required',2,1),(3,'pendaftar_jurusan_id','Jurusan','singleoption',NULL,'required',3,1),(4,'pendaftar_asal_sekolah','Asal Sekolah','text',NULL,'required',4,1),(5,'pendaftar_nisn','NISN','text',NULL,'required',5,1),(6,'pendaftar_gender','Jenis Kelamin','singleoption','gender','required',6,1),(7,'pendaftar_dob','Tanggal Lahir','date',NULL,'required',7,1),(8,'pendaftar_agama','Agama','singleoption','religion','required',8,1),(9,'pendaftar_address','Alamat','text',NULL,'required',9,1),(10,'pendaftar_phone','Nomor Telepon','text',NULL,'required',10,1),(11,'pendaftar_photo','Photo','s_file',NULL,'required',15,1),(13,'pendaftar_father_name','Nama Ayah','text',NULL,'required',11,1),(14,'pendaftar_father_salary','Penghasilan Ayah (perbulan)','text',NULL,'required',12,1),(15,'pendaftar_mother_name','Nama Ibu','text',NULL,'required',13,1),(16,'pendaftar_mother_salary','Penghasilan Ibu (perbulan)','text',NULL,'required',14,1);

#
# Structure for table "siswa"
#

DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `siswa_serial_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siswa_uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siswa_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siswa_nis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siswa_nisn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `siswa_dob` date DEFAULT '9999-12-31',
  `siswa_agama` bigint(10) DEFAULT 0,
  `siswa_gender` tinyint(3) DEFAULT 0,
  `siswa_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siswa_kelas_id` bigint(20) DEFAULT 0,
  `siswa_jurusan_id` bigint(20) DEFAULT 0,
  `siswa_subjurusan_id` bigint(20) DEFAULT 0,
  `siswa_absence` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`siswa_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "siswa"
#

INSERT INTO `siswa` VALUES (1,'6da39060-c7c7-43bd-aa3e-28a4517398f9','Rendi Febrian','21344234','99988899998888900','1998-02-26',7,1,'082371820023',5,3,1,NULL,1,'2021-03-14 03:53:24','2021-03-14 03:53:35',1),(2,'f2adfcb9-dfdf-40cd-9930-20c39dbd8f95','Luciana Fitri Diawan','14165465','21689748949819166','1995-02-28',7,2,'082371820023',5,3,1,NULL,1,'2021-03-14 03:53:25','2021-03-14 03:53:35',1),(3,'cd5b46e7-6593-4926-94cc-10b3e267c442','asd',NULL,NULL,'1111-11-11',NULL,NULL,NULL,5,3,1,NULL,1,'2021-03-14 03:53:26','2021-03-14 03:53:35',1),(4,'d4d22ccf-eb4f-42dd-ab9c-448f4e9e02b7','Rendi Febrian E','213442342','99988899998888900','1998-02-26',7,1,'082371820023',5,3,1,'{\"2021-04-13\":false}',1,'2021-03-14 03:53:58','2021-03-27 20:42:27',0),(5,'1002146d-f109-458f-ae15-a4260427acf7','Luciana Fitri Diawan','14165465','21689748949819166','1995-02-28',7,2,'082371820023',5,3,1,NULL,1,'2021-03-14 03:53:59','2021-03-14 03:53:59',0),(6,'693ee37b-1c90-43b8-b0fe-b2cecaadbfe9','asd',NULL,NULL,'1111-11-11',NULL,NULL,NULL,5,3,1,NULL,1,'2021-03-14 03:54:00','2021-03-14 04:28:30',1),(7,'45bc8221-5a26-46d5-b2c4-64cea85a4b31','paijo','123','123','1223-12-23',7,1,'02885645646',6,3,1,NULL,1,'2021-03-16 21:27:56','2021-03-16 21:27:56',0),(8,'47ed2c1b-ee60-42d1-997a-08b0dacb2d0a','Renita','13565213','56456453132','2021-02-26',9,2,'02885645646',6,3,2,NULL,1,'2021-03-22 07:51:06','2021-03-22 07:51:06',0),(13,'163a1fac-9c5f-42ce-9e55-cebbcd1634f1','Demo','123','123','1998-02-26',7,1,'0841231544',3,4,1,NULL,1,'2021-03-31 14:28:52','2021-03-31 14:28:52',0);

#
# Structure for table "siswa_fields"
#

DROP TABLE IF EXISTS `siswa_fields`;
CREATE TABLE `siswa_fields` (
  `siswa_fields_serial_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siswa_fields_name` varchar(255) DEFAULT NULL,
  `siswa_fields_label` varchar(255) DEFAULT NULL,
  `siswa_fields_input_type` varchar(255) DEFAULT NULL,
  `siswa_fields_options` varchar(255) DEFAULT NULL,
  `siswa_fields_validation` varchar(255) DEFAULT NULL,
  `siswa_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `siswa_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`siswa_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

#
# Data for table "siswa_fields"
#

INSERT INTO `siswa_fields` VALUES (1,'siswa_name','Nama','text',NULL,'required',1,1),(4,'siswa_nisn','NISN','text',NULL,'required',3,1),(5,'siswa_photo','Photo','s_file',NULL,'required',4,1),(6,'siswa_dob','Tanggal Lahir','date',NULL,'required',5,1),(7,'siswa_gender','Jenis Kelamin','singleoption','gender','required',6,1),(8,'siswa_kelas_id','Kelas','singleoption',NULL,'required',7,1),(9,'siswa_jurusan_id','Jurusan','singleoption',NULL,'required',8,1),(11,'siswa_subjurusan_id','Sub Jurusan','singleoption',NULL,'required',9,1),(12,'siswa_nis','NIS','text',NULL,'required',2,1),(17,'siswa_agama','Agama','singleoption','religion','required',10,1),(18,'siswa_phone','Nomor Telepon','text',NULL,NULL,11,1);

#
# Structure for table "soal"
#

DROP TABLE IF EXISTS `soal`;
CREATE TABLE `soal` (
  `soal_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `soal_uuid` varchar(255) DEFAULT NULL,
  `soal_description` text DEFAULT NULL,
  `soal_choices` text DEFAULT NULL,
  `soal_answer_key` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`soal_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

#
# Data for table "soal"
#

INSERT INTO `soal` VALUES (1,'81f98214-e254-46bf-bf5e-c293327e1c88','<p style=\"font-size: 14.4px;\">apa nama binatang dibawah ini ?<p style=\"font-size: 14.4px;\"><img data-filename=\"download.jpg\" style=\"background-color: transparent; font-size: 0.9rem; width: 257.25px;\" src=\"http://localhost/adminkuv2/apps/public//uploads/605bab52508eb.jpeg\"></p><p style=\"font-size: 14.4px;\"><br></p><p style=\"font-size: 14.4px;\">Silahkan tebak.</p></p>\n','{\"a\":\"Macan\",\"b\":\"Singa\",\"c\":\"Bajing\",\"d\":\"Kucing\",\"e\":\"Srigala\"}','c',0,'2021-03-25 04:12:50','2021-03-25 04:12:50',0),(2,'abe6ee7d-93ee-4e21-8119-bb6384736521','<p style=\"font-size: 14.4px;\">apa nama binatang dibawah ini ?<p style=\"font-size: 14.4px;\"><img data-filename=\"download.jpg\" style=\"background-color: transparent; font-size: 0.9rem; width: 257.25px;\" src=\"http://localhost/adminkuv2/apps/public//uploads/605bab95b4879.jpeg\"></p><p style=\"font-size: 14.4px;\"><br></p><p style=\"font-size: 14.4px;\">Silahkan tebak.</p></p>\n','{\"a\":\"Macan\",\"b\":\"Singa\",\"c\":\"Bajing\",\"d\":\"Kucing\",\"e\":\"Srigala\"}','c',1,'2021-03-25 04:13:57','2021-03-25 04:13:57',0),(3,'ef2a8893-80c6-4bff-91df-0154a05481de','<p>Sekarang smk kartika dimana?</p>\n','{\"a\":\"metro\",\"b\":\"jakarta\",\"c\":\"bandung\",\"d\":\"Jogja\",\"e\":\"Bali\"}','a',1,'2021-03-25 04:23:54','2021-03-25 04:23:54',0),(4,'882c1e82-0bf8-4fec-a003-46b667a3b5c0','Siapakah saya?','{\"a\":\"Rendi\",\"b\":\"Paijo\",\"c\":\"Agus\",\"d\":\"Sumarno\",\"e\":\"Joko\"}','a',1,'2021-03-25 05:31:22','2021-03-25 05:31:22',0),(5,'27b555f4-40b3-4604-964a-5eff971fd3b6','Siapakah saya?','{\"a\":\"Rendi\",\"b\":\"Paijo\",\"c\":\"Agus\",\"d\":\"Sumarno\",\"e\":\"Joko\"}','a',1,'2021-03-25 05:35:08','2021-03-25 05:35:08',0),(6,'c7cca7b9-8233-4912-bda2-05acf7c603ed','<p><span style=\'color: rgb(0, 0, 0); font-family: -apple-system, system-ui, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; font-size: 16px; text-align: justify;\'>Jika persamaan kuadrat&nbsp;</span><em style=\'color: rgb(0, 0, 0); font-family: -apple-system, system-ui, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; font-size: 16px; text-align: justify;\'>px<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">2</span>&nbsp;+ 30x + 25 = 0</em><span style=\'color: rgb(0, 0, 0); font-family: -apple-system, system-ui, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; font-size: 16px; text-align: justify;\'>&nbsp;mempunyai akar&acirc;&#128;&#147;akar sama, maka nilai p adalah....</span><br><p></p><p></p></p>\n','{\"a\":\"6\",\"b\":\"7\",\"c\":\"8\",\"d\":\"9\",\"e\":\"10\"}','d',1,'2021-03-28 03:03:56','2021-03-27 21:04:23',1),(7,'54f37f40-3f7e-4004-9934-5d37374e6a81','Siapakah saya?','{\"a\":\"Rendi\",\"b\":\"Paijo\",\"c\":\"Agus\",\"d\":\"Sumarno\",\"e\":\"Joko\"}','a',1,'2021-03-28 04:31:57','2021-03-27 21:32:04',1),(8,'0f737841-b433-46f0-82b4-cc41634050a0','Siapakah saya 1','{\"a\":\"Rendi 1\",\"b\":\"Paijo 1\",\"c\":\"Agus 1\",\"d\":\"Sumarno 1\",\"e\":\"Joko 1\"}','a',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(9,'41ebf15a-20b5-40cd-b9f8-5de7e775d4b2','Siapakah saya 2','{\"a\":\"Rendi 2\",\"b\":\"Paijo 2\",\"c\":\"Agus 2\",\"d\":\"Sumarno 2\",\"e\":\"Joko 2\"}','b',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(10,'3caf0314-b689-409e-bba7-016d7126210c','Siapakah saya 3','{\"a\":\"Rendi 3\",\"b\":\"Paijo 3\",\"c\":\"Agus 3\",\"d\":\"Sumarno 3\",\"e\":\"Joko 3\"}','c',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(11,'2cb2e0f2-cc24-4390-aa18-99e6fb7f8d6e','Siapakah saya 4','{\"a\":\"Rendi 4\",\"b\":\"Paijo 4\",\"c\":\"Agus 4\",\"d\":\"Sumarno 4\",\"e\":\"Joko 4\"}','b',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(12,'3f5e9cce-10ff-4216-b8bd-749239b27d2c','Siapakah saya 5','{\"a\":\"Rendi 5\",\"b\":\"Paijo 5\",\"c\":\"Agus 5\",\"d\":\"Sumarno 5\",\"e\":\"Joko 5\"}','d',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(13,'f3803f5c-43a5-4e6f-8817-7bad5c6a78ee','Siapakah saya 6','{\"a\":\"Rendi 6\",\"b\":\"Paijo 6\",\"c\":\"Agus 6\",\"d\":\"Sumarno 6\",\"e\":\"Joko 6\"}','d',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(14,'b8394c26-d5d9-416b-9c67-8be08a69c4ff','Siapakah saya 7','{\"a\":\"Rendi 7\",\"b\":\"Paijo 7\",\"c\":\"Agus 7\",\"d\":\"Sumarno 7\",\"e\":\"Joko 7\"}','e',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(15,'43a9b2a2-2443-46ba-8761-4043b622db3d','Siapakah saya 8','{\"a\":\"Rendi 8\",\"b\":\"Paijo 8\",\"c\":\"Agus 8\",\"d\":\"Sumarno 8\",\"e\":\"Joko 8\"}','d',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(16,'1b95fdc3-4c64-49c2-a4ed-8d78560b7fab','Siapakah saya 9','{\"a\":\"Rendi 9\",\"b\":\"Paijo 9\",\"c\":\"Agus 9\",\"d\":\"Sumarno 9\",\"e\":\"Joko 9\"}','b',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(17,'a23bc533-b238-4d42-9f55-e671ceb2a81e','Siapakah saya 10','{\"a\":\"Rendi 10\",\"b\":\"Paijo 10\",\"c\":\"Agus 10\",\"d\":\"Sumarno 10\",\"e\":\"Joko 10\"}','c',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(18,'94dc4622-acc3-45e2-8c28-c41f83d2c62c','Siapakah saya 11','{\"a\":\"Rendi 11\",\"b\":\"Paijo 11\",\"c\":\"Agus 11\",\"d\":\"Sumarno 11\",\"e\":\"Joko 11\"}','c',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(19,'1cdfe989-25a9-46dd-b75a-4d8bca28d6e9','Siapakah saya 12','{\"a\":\"Rendi 12\",\"b\":\"Paijo 12\",\"c\":\"Agus 12\",\"d\":\"Sumarno 12\",\"e\":\"Joko 12\"}','b',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(20,'94efda2a-7d4c-4c61-a83d-083603737a32','Siapakah saya 13','{\"a\":\"Rendi 13\",\"b\":\"Paijo 13\",\"c\":\"Agus 13\",\"d\":\"Sumarno 13\",\"e\":\"Joko 13\"}','d',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(21,'35114dc0-6718-4ebb-a879-7e77a1c4992e','Siapakah saya 14','{\"a\":\"Rendi 14\",\"b\":\"Paijo 14\",\"c\":\"Agus 14\",\"d\":\"Sumarno 14\",\"e\":\"Joko 14\"}','e',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(22,'7bbf96fc-4914-4d45-90bb-50f268d37390','Siapakah saya 15','{\"a\":\"Rendi 15\",\"b\":\"Paijo 15\",\"c\":\"Agus 15\",\"d\":\"Sumarno 15\",\"e\":\"Joko 15\"}','b',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(23,'f1ba8c36-bf0d-498f-9273-a8a2bb5109e7','Siapakah saya 16','{\"a\":\"Rendi 16\",\"b\":\"Paijo 16\",\"c\":\"Agus 16\",\"d\":\"Sumarno 16\",\"e\":\"Joko 16\"}','a',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(24,'821ddc37-34b5-4f96-8996-defa4f7c2eec','Siapakah saya 17','{\"a\":\"Rendi 17\",\"b\":\"Paijo 17\",\"c\":\"Agus 17\",\"d\":\"Sumarno 17\",\"e\":\"Joko 17\"}','c',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(25,'b09fff73-4de4-401e-b95d-9a6b0569060d','Siapakah saya 18','{\"a\":\"Rendi 18\",\"b\":\"Paijo 18\",\"c\":\"Agus 18\",\"d\":\"Sumarno 18\",\"e\":\"Joko 18\"}','a',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(26,'ccbb6eb4-960a-472b-9c2a-819d23e34d1c','Siapakah saya 19','{\"a\":\"Rendi 19\",\"b\":\"Paijo 19\",\"c\":\"Agus 19\",\"d\":\"Sumarno 19\",\"e\":\"Joko 19\"}','a',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(27,'739c1feb-9fd3-42d5-b158-75c71a7ea431','Siapakah saya 20','{\"a\":\"Rendi 20\",\"b\":\"Paijo 20\",\"c\":\"Agus 20\",\"d\":\"Sumarno 20\",\"e\":\"Joko 20\"}','a',1,'2021-03-28 07:25:23','2021-03-28 07:25:23',0),(28,'34096510-191f-418f-94bc-9a370c474143','<p>Hardisk adalah?</p>\n','{\"a\":\"Storage\",\"b\":\"Memory\",\"c\":\"Monitor\",\"d\":\"Mouse\",\"e\":\"Keyboard\"}','a',1,'2021-04-03 02:02:03','2021-04-03 02:02:03',0),(29,'89d20347-b104-4795-8645-dd67b17ea062','1 + 1 = ?','{\"a\":\"1\",\"b\":\"2\",\"c\":\"3\",\"d\":\"4\",\"e\":\"5\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(30,'f1130cb7-7445-4e51-892a-4aa43e271851','2 + 1 = ?','{\"a\":\"2\",\"b\":\"3\",\"c\":\"4\",\"d\":\"5\",\"e\":\"6\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(31,'23129e4c-f5b7-48f1-a817-f20fed70e422','3 + 1 = ?','{\"a\":\"3\",\"b\":\"4\",\"c\":\"5\",\"d\":\"6\",\"e\":\"7\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(32,'4d0a83ae-c4c9-4f5a-b86e-626982bc2c99','4 + 1 = ?','{\"a\":\"4\",\"b\":\"5\",\"c\":\"6\",\"d\":\"7\",\"e\":\"8\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(33,'76899f60-a3e5-4bf4-bac9-017e42d0e21e','5 + 1 = ?','{\"a\":\"5\",\"b\":\"6\",\"c\":\"7\",\"d\":\"8\",\"e\":\"9\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(34,'44da2446-2564-4c3e-bd5d-21abdd64b8c9','6 + 1 = ?','{\"a\":\"6\",\"b\":\"7\",\"c\":\"8\",\"d\":\"9\",\"e\":\"10\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(35,'29fcd00b-d297-449c-b114-3e80d1ec71d7','7 + 1 = ?','{\"a\":\"7\",\"b\":\"8\",\"c\":\"9\",\"d\":\"10\",\"e\":\"11\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(36,'3e4a966e-ba5d-49de-8da4-069d1a7aa76e','8 + 1 = ?','{\"a\":\"8\",\"b\":\"9\",\"c\":\"10\",\"d\":\"11\",\"e\":\"12\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(37,'4d48d5a5-0b3d-4972-90e6-ff75914237e6','9 + 1 = ?','{\"a\":\"9\",\"b\":\"10\",\"c\":\"11\",\"d\":\"12\",\"e\":\"13\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(38,'e82098c2-727e-4a4f-bc6d-a631403e8859','10 + 1 = ?','{\"a\":\"10\",\"b\":\"11\",\"c\":\"12\",\"d\":\"13\",\"e\":\"14\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(39,'204c8f25-63e2-4285-9e24-582174b51c75','11 + 1 = ?','{\"a\":\"11\",\"b\":\"12\",\"c\":\"13\",\"d\":\"14\",\"e\":\"15\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(40,'f1946d10-56b1-4506-a105-03e767a62a5b','12 + 1 = ?','{\"a\":\"12\",\"b\":\"13\",\"c\":\"14\",\"d\":\"15\",\"e\":\"16\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(41,'190053d4-5697-489d-b5a2-946c3d5afdef','13 + 1 = ?','{\"a\":\"13\",\"b\":\"14\",\"c\":\"15\",\"d\":\"16\",\"e\":\"17\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(42,'f688a7ee-12c4-4cfb-8f4e-abcffdfcbf8a','14 + 1 = ?','{\"a\":\"14\",\"b\":\"15\",\"c\":\"16\",\"d\":\"17\",\"e\":\"18\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(43,'be546ff6-08ea-450f-b56b-4d3cdb8143ab','15 + 1 = ?','{\"a\":\"15\",\"b\":\"16\",\"c\":\"17\",\"d\":\"18\",\"e\":\"19\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(44,'621d087c-617d-438b-8aaf-5db41a78b92a','16 + 1 = ?','{\"a\":\"16\",\"b\":\"17\",\"c\":\"18\",\"d\":\"19\",\"e\":\"20\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(45,'8679ca0e-b4c1-45fb-bb3b-739dd687e157','17 + 1 = ?','{\"a\":\"17\",\"b\":\"18\",\"c\":\"19\",\"d\":\"20\",\"e\":\"21\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(46,'18293a65-fd12-471f-b252-d7b248e7d40a','18 + 1 = ?','{\"a\":\"18\",\"b\":\"19\",\"c\":\"20\",\"d\":\"21\",\"e\":\"22\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(47,'ad7f005f-4133-4147-9415-5f6d58dd7e7d','19 + 1 = ?','{\"a\":\"19\",\"b\":\"20\",\"c\":\"21\",\"d\":\"22\",\"e\":\"23\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(48,'dda2ab89-5f1e-44ca-9409-2d3656051670','20 + 1 = ?','{\"a\":\"20\",\"b\":\"21\",\"c\":\"22\",\"d\":\"23\",\"e\":\"24\"}','b',1,'2021-04-03 02:04:51','2021-04-03 02:04:51',0),(49,'7e38a429-7619-4ea4-a5a5-6e8834de9bc7','<p>Pilih yg termasuk I/O.</p>\n','{\"a\":\"Mouse\",\"b\":\"Lan\",\"c\":\"Monitor\",\"d\":\"Mouse pad\",\"e\":\"Lcd\"}','a',1,'2021-04-03 07:38:04','2021-04-03 07:38:04',0);

#
# Structure for table "soal_fields"
#

DROP TABLE IF EXISTS `soal_fields`;
CREATE TABLE `soal_fields` (
  `soal_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `soal_fields_name` varchar(255) DEFAULT NULL,
  `soal_fields_label` varchar(255) DEFAULT NULL,
  `soal_fields_input_type` varchar(255) DEFAULT NULL,
  `soal_fields_options` varchar(255) DEFAULT NULL,
  `soal_fields_validation` varchar(255) DEFAULT NULL,
  `soal_fields_sorting` bigint(6) unsigned DEFAULT NULL,
  `soal_fields_status` tinyint(6) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`soal_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "soal_fields"
#

INSERT INTO `soal_fields` VALUES (1,'soal_description','Pertanyaan','textarea',NULL,'required',1,1),(2,'soal_choices','Pilihan','text',NULL,'required',2,1),(3,'soal_answer_key','Kunci Jawaban','singleoption',NULL,'required',3,1);

#
# Structure for table "subjurusan"
#

DROP TABLE IF EXISTS `subjurusan`;
CREATE TABLE `subjurusan` (
  `subjurusan_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjurusan_uuid` varchar(255) DEFAULT NULL,
  `subjurusan_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`subjurusan_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

#
# Data for table "subjurusan"
#

INSERT INTO `subjurusan` VALUES (1,'asd','A','2020-11-20 23:39:51','2020-11-20 23:39:51',0),(2,'sdf','B','2020-11-20 23:43:25','2020-11-20 23:43:25',0),(3,'wef','C','2020-11-20 23:44:42','2021-03-16 02:10:49',1),(4,'sdbv','DA','2020-11-21 00:23:03','2021-03-16 02:10:49',1),(5,'csdc','E','2021-02-28 23:50:45','2021-02-28 23:50:45',1),(6,'sdc','FCXAA','2021-02-28 23:51:24','2021-03-01 00:27:31',1),(7,'ca7a0d38-09cd-4d56-8fb0-041b4b0c3860','asd','2021-03-01 00:16:05','2021-03-01 00:16:05',1),(8,'4e2207a8-0edc-48d3-9e07-a060b6487862','asdcc','2021-03-01 00:16:34','2021-03-01 00:16:34',1),(9,'cc0ac6cc-5e5d-410e-aa98-5f7ea0c759c3','EA','2021-03-14 04:51:58','2021-03-16 02:10:49',1);

#
# Structure for table "subjurusan_fields"
#

DROP TABLE IF EXISTS `subjurusan_fields`;
CREATE TABLE `subjurusan_fields` (
  `subjurusan_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `subjurusan_fields_name` varchar(255) DEFAULT NULL,
  `subjurusan_fields_label` varchar(255) DEFAULT NULL,
  `subjurusan_fields_input_type` varchar(255) DEFAULT NULL,
  `subjurusan_fields_options` varchar(255) DEFAULT NULL,
  `subjurusan_fields_validation` varchar(255) DEFAULT NULL,
  `subjurusan_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `subjurusan_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`subjurusan_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "subjurusan_fields"
#

INSERT INTO `subjurusan_fields` VALUES (1,'subjurusan_name','Nama Sub Jurusan','text',NULL,'required',1,1);

#
# Structure for table "sys_rel"
#

DROP TABLE IF EXISTS `sys_rel`;
CREATE TABLE `sys_rel` (
  `rel_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_from_id` bigint(10) DEFAULT 0,
  `rel_from_module` varchar(255) DEFAULT NULL,
  `rel_to_id` bigint(10) DEFAULT 0,
  `rel_to_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rel_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4;

#
# Data for table "sys_rel"
#

INSERT INTO `sys_rel` VALUES (23,4,'siswa',25,'users'),(24,5,'siswa',26,'users'),(26,7,'siswa',28,'users'),(28,1,'admin',30,'users'),(37,8,'siswa',39,'users'),(64,40,'users',13,'siswa'),(65,28,'soal',8,'ujian'),(66,29,'soal',9,'ujian'),(67,30,'soal',9,'ujian'),(68,31,'soal',9,'ujian'),(69,32,'soal',9,'ujian'),(70,33,'soal',9,'ujian'),(71,34,'soal',9,'ujian'),(72,35,'soal',9,'ujian'),(73,36,'soal',9,'ujian'),(74,37,'soal',9,'ujian'),(75,38,'soal',9,'ujian'),(76,39,'soal',9,'ujian'),(77,40,'soal',9,'ujian'),(78,41,'soal',9,'ujian'),(79,42,'soal',9,'ujian'),(80,43,'soal',9,'ujian'),(81,44,'soal',9,'ujian'),(82,45,'soal',9,'ujian'),(83,46,'soal',9,'ujian'),(84,47,'soal',9,'ujian'),(85,48,'soal',9,'ujian'),(86,49,'soal',8,'ujian'),(93,7,'hasil_ujian',13,'siswa'),(94,8,'hasil_ujian',13,'siswa'),(95,9,'hasil_ujian',13,'siswa'),(96,10,'hasil_ujian',13,'siswa'),(97,11,'hasil_ujian',13,'siswa'),(98,12,'hasil_ujian',13,'siswa'),(99,13,'hasil_ujian',13,'siswa'),(100,14,'hasil_ujian',13,'siswa'),(101,15,'hasil_ujian',13,'siswa'),(102,16,'hasil_ujian',13,'siswa'),(103,17,'hasil_ujian',13,'siswa');

#
# Structure for table "ujian"
#

DROP TABLE IF EXISTS `ujian`;
CREATE TABLE `ujian` (
  `ujian_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_uuid` varchar(255) DEFAULT NULL,
  `ujian_name` varchar(255) DEFAULT NULL,
  `ujian_mata_pelajaran_id` bigint(10) DEFAULT NULL,
  `ujian_kelas_id` bigint(10) DEFAULT NULL,
  `ujian_jurusan_id` bigint(10) DEFAULT NULL,
  `ujian_duration` bigint(10) DEFAULT NULL,
  `ujian_start_date` datetime DEFAULT '1111-11-11 00:00:00',
  `ujian_end_date` datetime DEFAULT '1111-11-11 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`ujian_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

#
# Data for table "ujian"
#

INSERT INTO `ujian` VALUES (8,'94d51884-c810-415a-8080-8e8397903160','Ujian Nasional',5,3,3,60,'2021-04-02 00:00:00','2021-04-05 00:00:00',1,'2021-04-03 01:51:31','2021-04-03 01:51:31',0),(9,'28d09c32-1831-429e-be47-90968bdf1e15','Ujian Nasional 2',4,3,4,1,'2021-04-02 00:00:00','2021-04-12 00:00:00',1,'2021-04-03 01:53:05','2021-04-03 21:22:44',0),(10,'4f3e0495-2e77-4cf7-94a3-677d5a8b2931','Ujian MTK',4,3,4,2,'2021-04-06 00:00:00','2021-04-06 00:00:00',1,'2021-04-06 04:30:11','2021-04-06 04:30:11',0);

#
# Structure for table "ujian_fields"
#

DROP TABLE IF EXISTS `ujian_fields`;
CREATE TABLE `ujian_fields` (
  `ujian_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_fields_name` varchar(255) DEFAULT NULL,
  `ujian_fields_label` varchar(255) DEFAULT NULL,
  `ujian_fields_input_type` varchar(255) DEFAULT NULL,
  `ujian_fields_options` varchar(255) DEFAULT NULL,
  `ujian_fields_validation` varchar(255) DEFAULT NULL,
  `ujian_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `ujian_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`ujian_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

#
# Data for table "ujian_fields"
#

INSERT INTO `ujian_fields` VALUES (1,'ujian_name','Nama Ujian/Soal','text',NULL,'required',1,1),(2,'ujian_mata_pelajaran_id','Mata Pelajaran','s_autocomplete',NULL,'required',2,1),(3,'ujian_kelas_id','Kelas','singleoption',NULL,'required',3,1),(4,'ujian_jurusan_id','Jurusan','singleoption',NULL,'required',4,1),(5,'ujian_duration','Durasi Pengerjaan','number',NULL,'required',5,1),(6,'ujian_start_date','Tanggal Mulai','date',NULL,'required',6,1),(7,'ujian_end_date','Tanggal Selesai','date',NULL,'required',7,1);

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0 COMMENT '1 => admin, 2 => guru, 3 => siswa, => 4 => wali',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'Admin Rendi','admin@admin.com','$2y$12$d9QaMBWci2uHv/xfIiagDu8y1iV0AVgKbmcbXwf5PX1ByFOt6p/X2','',1,'uploads/SMK_UPLOAD-logo-kartikatama.png','bm8dbnbqbORDNmlsECFlBIeOfNdSHaspSK4sLeBAkTfBmP6kDDyQ31aEWxwdUUHhRCXgrz5TkCr2mcKU','2021-02-21 22:24:37','2021-02-21 13:27:10',0),(25,'Rendi Febrian','rendi21344234@smkkartikatama.sch.id','$2y$10$3hi.gQ70HxJ5lQ8Y2ZpTvO5TEs8hUDPFyiAy2PBMDZniLBds6kgsm','',3,'uploads/SMK_UPLOAD-Screen Shot 2021-03-18 at 1.48.12 PM.png',NULL,'2021-03-14 03:53:58','2021-03-27 20:42:27',0),(26,'Luciana Fitri Diawan','luciana14165465@smkkartikatama.sch.id','$2y$10$Gn/Sv2iYV9K0iIcImSKSKupGxNSz.PjaE9B9Ey4/RAWuVOVGmJViu','',3,NULL,NULL,'2021-03-14 03:53:59','2021-03-14 03:53:59',0),(28,'paijo','paijo123@smkkartikatama.sch.id','$2y$10$dj0O91xf/wPbyxcn5Sqp.eI9nvdEUz5sJ8gUUt7nWHbB/USu2fK0.','',3,'uploads/SMK_UPLOAD-209--90bc7c3f8e2e90b11b5ee9dc68dd378e.jpg',NULL,'2021-03-16 21:27:56','2021-03-16 21:27:56',0),(29,'Rendi Febrian','rendifebrian1@smkkartikatama.sch.id','$2y$10$Ik9azYiSOvA5WKmPSOJTcuWtGC5bwpCRDt76ViQxsA.0SokH5skQm','',1,'uploads/SMK_UPLOAD-rendi.png',NULL,'2021-03-21 23:26:06','2021-03-21 23:26:06',0),(30,'Paijo Superman','paijosuperman3@smkkartikatama.sch.id','$2y$10$erZ0EV1ABdV1Utm8x3LdKeOPpQ9oV3BOYHYgk9QgpB05.z6OWSU8G','',1,'uploads/SMK_UPLOAD-209--90bc7c3f8e2e90b11b5ee9dc68dd378e.jpg',NULL,'2021-03-21 23:32:42','2021-03-21 23:32:42',0),(39,'Renita','renita13565213@smkkartikatama.sch.id','$2y$10$7/PB08rQDjURyrGs6lYSKeMXy1.ddJ4BWnKK7RqlwwWxyE07A5BpS','',3,'uploads/SMK_UPLOAD-209--90bc7c3f8e2e90b11b5ee9dc68dd378e.jpg',NULL,'2021-03-22 07:51:06','2021-03-22 07:51:06',0),(40,'Demo','demo123@smkkartikatama.sch.id','$2y$12$xcfYxKCfjbzyot7Uoblh1evYBdoM.O5G11tdMqk.jzsSACDOkYnpC','VOwlGzl26A1N4MdN2zZE2a8GD2LVYZeMYkpoqQe3wUVpULHYw6qKk9z5nhBC',3,'uploads/SMK_UPLOAD-0001.jpg',NULL,'2021-03-31 14:28:52','2021-04-16 01:48:06',0);

#
# Structure for table "wali"
#

DROP TABLE IF EXISTS `wali`;
CREATE TABLE `wali` (
  `wali_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `wali_uuid` varchar(255) DEFAULT NULL,
  `wali_name` varchar(255) DEFAULT NULL,
  `wali_siswa_id` bigint(10) unsigned DEFAULT NULL,
  `wali_username` varchar(255) DEFAULT NULL,
  `wali_password` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`wali_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "wali"
#

INSERT INTO `wali` VALUES (1,'f3776e62-e60d-4790-8b8e-9973398e1cf3','Yahmin & Darojatun S.Pd',4,NULL,NULL,'2021-03-16 03:47:26','2021-03-16 04:23:04',0),(2,'9b784dfb-a1c4-4e25-b28b-3b42019cab7c','Eka Susilawati',5,'DI4SVWX','dtsxipf','2021-03-16 20:41:17','2021-03-16 20:41:17',0),(3,'c97c4770-3aa2-4362-8b7f-8a1987666a27','Papa Demo',13,'J1YGV0I','uoq1hf0','2021-04-13 01:43:41','2021-04-13 01:43:41',0);

#
# Structure for table "wali_fields"
#

DROP TABLE IF EXISTS `wali_fields`;
CREATE TABLE `wali_fields` (
  `wali_fields_serial_id` int(11) NOT NULL AUTO_INCREMENT,
  `wali_fields_name` varchar(255) DEFAULT NULL,
  `wali_fields_label` varchar(255) DEFAULT NULL,
  `wali_fields_input_type` varchar(255) DEFAULT NULL,
  `wali_fields_options` varchar(255) DEFAULT NULL,
  `wali_fields_validation` varchar(255) DEFAULT NULL,
  `wali_fields_sorting` bigint(1) unsigned DEFAULT NULL,
  `wali_fields_status` tinyint(3) unsigned DEFAULT 0 COMMENT '0 => Not Active, 1=> Active',
  PRIMARY KEY (`wali_fields_serial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "wali_fields"
#

INSERT INTO `wali_fields` VALUES (1,'wali_name','Nama Wali/Orang Tua','text',NULL,'required',1,1),(2,'wali_siswa_id','Nama Siswa','s_autocomplete','siswa_id','required',2,1);
